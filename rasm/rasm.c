/*  rasm.c - RoboAssembler with support 8080/8085, 8086, Tunguska, Lavr4 and Voja4.

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 1997-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if 0
#define DEBUG
#endif

#define RASM_VERSION "2.6.2"

/* HISTORY:

   19 Jan 1997 - First C++ version of 16-bit "RadioAssembler" with virtual memory
   21 Jan 2002 - Porting to 32-bit WATCOM-C++ with large arrays instead of virtual memory
   01 Dec 2005 - Now RASM is "RoboAssembler" instead of "RadioAssembler"
   04 Dec 2005 - v2.0 modified for ANSI-C with 8086 support
   06 Dec 2005 - v2.1 additional type of 8086 opcodes
   08 Dec 2005 - v2.2 near jump and near call for 8086 and fixes
   25 Apr 2007 - v2.3 released under GPL as part of NedoPC SDK
   18 Feb 2008 - v2.4 support for Tunguska the ternary emulator
   12 Dec 2015 - v2.5 support for Lavr4 (lower byte of label) and some cleanup
   30 Oct 2022 - v2.6 support for Voja4 (with 2-byte instructions and relative jumps)
   03 Nov 2022 - v2.6.1 added extra possible characters to names ($ and `)
   06 Jul 2024 - v2.6.2 fixed zeroing label bug + upgrade GPL to v3 to match nedoPC SDK license

   NOTE: This source code should be compatible with any 16-bit, 32-bit or 64-bit ANSI-C compilers
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "my_text.h"

#define EXT_ASM ".A"
#define EXT_LST ".LST"
#define EXT_BIN ".BIN"

#define SH_BOOL    int
#define SH_BYTE    unsigned char
#define SH_WORD    unsigned short
#define SH_TRUE    1
#define SH_FALSE   0

#define NAM_LEN 32   /* Maxinum length of name */
#define STR_LEN 1024 /* Maximum length of line */
#define PTH_LEN 256  /* Maximum length of path */

#define TAB_INC 0 /* include */
#define TAB_LBL 1 /* label */
#define TAB_EQU 2 /* equation */

int f_debug=0;
int f_tunguska=0; /* v2.4 */
int f_lavr4=0;    /* v2.5 */
int f_voja4=0;    /* v2.6 */
int var_num=1;
int lbl_num=1;
int inc_num=0;
int equ_num=0;
SH_WORD org=0;
FILE *txt = NULL;
Text *code;
Text *text;
Text *table;
Text *labels;
Line *nol; /* "Not Label" */
SH_BYTE *mem1,*mem2;

/* Delimiter of program lines */
#define kon(c) (((c)=='\n'||feof(txt))?SH_TRUE:SH_FALSE)

/* Delimiter of words in line */
#define spa(c) (((c)=='\r'||(c)==' '||(c)=='\t')?SH_TRUE:SH_FALSE)

/* Test for russian character in alternative coding page */
#define rus(c) (((((SH_BYTE)(c))>=128&&((SH_BYTE)(c))<=175)||(((SH_BYTE)(c))>=224&&((SH_BYTE)(c))<=241))?SH_TRUE:SH_FALSE)

/* Character is suitable for name */
#define naa(c) ((isalnum(c)||rus(c)||(c)=='_'||(c)=='.'||(c)=='@'||(c)=='$'||(c)==0x60)?SH_TRUE:SH_FALSE)

char* stri(char *aa) /* Read string from text */
{   static char s[STR_LEN];
    char ch=0,*sp;
    int no=0;
    do{ ch=fgetc(txt);
        s[no++]=ch;
    }while(!kon(ch));
    if(s[no-2]=='\r')
         s[no-2]=0;
    else s[no-1]=0;
    *aa=ch;
    sp=s;
    while(spa(*sp))sp++;
#ifdef DEBUG
    printf(">%s\n",sp);
#endif
    return sp;
}

char* word(char *s,int *u) /* Divide string to words */
{   int u1=0;
    static char sr[STR_LEN];
    if(s[*u])
    {  while(spa(s[*u])) *u=*u+1;
       while(!spa(s[*u])&&s[*u]!=0)
       { sr[u1++]=s[*u];
         *u=*u+1;
       }
    }
    sr[u1]=0;
    return sr;
}

int isnum(char *s) /* String is a number */
{   int i,o=SH_TRUE;
    int len=strlen(s);
    if(len>NAM_LEN) o=SH_FALSE;
    if(s[0]=='#') /* Hexadecimal number? */
    {
       for(i=1;i<len;i++)
          if(!isxdigit(s[i])) o=SH_FALSE;
    }
    else /* Decimal number? */
    {
       for(i=0;i<len;i++)
       {
          if(i>0&&!isdigit(s[i])) o=SH_FALSE;
          if(i==0&&!isdigit(*s)&&(*s!='-')) o=SH_FALSE;
       }
    }
    return o;
}

int isnam(char *s) /* String is a name */
{   int i,o=SH_TRUE;
    int len=strlen(s);
    for(i=0;i<len;i++)
    {  if(i==0)
       {  if(isdigit(s[i])||s[i]=='@') o=SH_FALSE;
       }
       else
       {  if(!naa(s[i])) o=SH_FALSE;
       }
    }
    return o;
}

int getnum(char *s) /* Get number from string */
{   int i=-1,j,k,h;
    int len=strlen(s);
    switch(s[0])
    {  case '#': /* Hexadecimal */
                 len--;
                 j=1;h=0;
                 for(k=len;k>=1;k--)
                 {   h+=hexx(s[k])*j;
                     j*=16;
                 }
                 i=h;
                 break;
       case '\'': /* Character */
                 if(s[2]!='\'') break;
                 i=s[1];
                 break;
       default:  /* Decimal */
                 for(j=0;j<len;j++)
                 {
                   if(j>0&&!isdigit(s[j])) return -1;
                   if(j==0&&!isdigit(*s)&&(*s!='-')) return -1;
                 }
                 i=atoi(s);
    }
    return i;
}

void Error(Line *l)
{   Line *lf;
    int i;
    i = l->id2;
    lf = TextFindFirstTypId(table,TAB_INC,i);
    printf("\n\nERROR in \"%s\": %u) %s\n\n",lf->str,l->id,l->str);
    exit(1);
}

SH_WORD getval(char *s)
{   int j;
    SH_WORD i;
    Line *l;
    if(s[0]=='@')
    {  l=TextFindFirstStrTyp(table,s,TAB_EQU);
       if(l==NULL) Error(nol);
       i=l->adr;
       return i;
    }
    j=getnum(s);
    if(j!=-1) i=j;
/*
    else
    {   l=TextFindFirstStrTyp(table,s,TAB_LBL);
        if(l==NULL) Error(nol);
        i=l->adr;
    }
*/
    return i;
}

int get_tryte(int num) /* v2.4 */
{  int i,n,m,k,s;
#ifdef DEBUG
   int a = num;
#endif
   s = 0;
   k = 19683;
   for(i=0;i<=9;i++)
   {
     n = num/k;
     num -= n*k;
     m = k>>1;
     if(num>+m){n++;num-=k;}
     if(num<-m){n--;num+=k;}
     if(k<729) s+=n*k;
     k /= 3;
   }
#ifdef DEBUG
   printf("get_tryte(%i) -> %i\n",a,s);
#endif
   return s;
}

int main(int argc,char **argv)
{int i,j,k,len,tabnum,uk,bb,i1,i2,ii,manyl,ftun,fnib,ouk,t1,t0,fempty;
 int lbl_id;
 int lbl_ty;
 long lbl_ad;
 long lbl_zz;
 long tek;
 signed short sis;
 char nam[NAM_LEN];
 char nam2[NAM_LEN];
 char *buff;
 char *str;
 char *st;
 char *wo;
 char *tabfile;
 char *orig;
 char *fna;
 char *po;
 char *po2;
 char *sp;
 char c00;
 SH_BOOL f_f;
 SH_WORD zz;
 SH_WORD ui;
 SH_WORD aa;
 Line *libu;
 Line *libu0;
 Line *libu1;
 Line *libu2;
 FILE *in,*lst,*fcod;
 printf("\n\nRoboAssembler v" RASM_VERSION " (c) 1997-2024 Alexander A. Shabarshin <me@shaos.net>\n");
#if 1
 if(argc<2 || argc>4)
 { printf("\nUsage:    RASM file.A [-tTABLE] [-d]\n\n");
   return 0;
 }
#else
#define TEST "TEST80.A"
#endif
 tabfile = (char*)malloc(PTH_LEN);
 if(tabfile==NULL) exit(11);
 strcpy(tabfile,"default.tab");
 if(argc>2 && !strcmp(argv[2],"-d")) f_debug=1;
 if(argc>3 && !strcmp(argv[3],"-d")) f_debug=1;
 if(argc>2 && !strncmp(argv[2],"-t",2)) strcpy(tabfile,&argv[2][2]);
 str = (char*)malloc(STR_LEN);
 if(str==NULL) exit(20);
#ifdef DEBUG
 f_debug=1;
#endif
#ifndef TEST
 strcpy(str,argv[1]);
#else
 strcpy(str,TEST);
#endif
 po=strchr(str,'.');
 if(po!=NULL) *po=0;
 orig = (char*)malloc(PTH_LEN);
 if(orig==NULL) exit(12);
 fna = (char*)malloc(PTH_LEN);
 if(fna==NULL) exit(13);
 strcpy(orig,str);
 strcpy(fna,str);
 strcat(fna,EXT_ASM);
 buff = (char*)malloc(STR_LEN);
 if(buff==NULL) exit(21);
 wo = (char*)malloc(STR_LEN);
 if(wo==NULL) exit(22);
 st = (char*)malloc(STR_LEN);
 if(st==NULL) exit(23);
 mem1 = (SH_BYTE*)malloc(32768);
 if(mem1==NULL) exit(31);
 mem2 = (SH_BYTE*)malloc(32768);
 if(mem2==NULL) exit(32);
 code = TextNew();
 text = TextNew();
 table = TextNew();
 labels = TextNew();
 nol = LineNew("Not Label");
 in=fopen(tabfile,"rt");
 if(in==NULL){printf("ERROR: Can't open %s\n",tabfile);exit(1);}
 tabnum=-1;
 while(1)
 { fgets(str,80,in);
   if(*str=='*')
   { if(tabnum>0) break;
     if(tabnum<0&&!strncmp(str,"*ASM",4))
     {
        tabnum=0;
        if(strstr(str,"Tunguska")!=NULL) f_tunguska=1;
        if(strstr(str,"LAVR4")!=NULL) f_lavr4=1; /* v2.5 */
        if(strstr(str,"VOJA4")!=NULL) f_voja4=1; /* v2.6 */
     }
   }
   if(feof(in)) break;
   if(tabnum>=0&&*str!='*')
   { po=strchr(str,'\n');
     if(po!=NULL) *po=0;
     po=strchr(str,'\r');
     if(po!=NULL) *po=0;
     po=str;
     while(*po!=' ') po++;
     *po=0;po++;
     while(*po==' ') po++;
     if(*po!='#')
     { printf("ERROR: unknown code definition %s %s\n",str,po);
       continue;
     }
     ii=0;
     i=(hexx(po[1])<<4)|hexx(po[2]);
     po+=3;
     while(*po==' '||*po=='\t') po++;
     if(*po==0||*po=='/'){j=0;k=1;}
     else if(*po=='B')
     { j=1;po++;k=2;
       /* v2.6 */
       while(*po==' '||*po=='\t') po++;
       if(*po=='#')
       { ii=(hexx(po[1])<<4)|hexx(po[2]);
         po+=3;
         while(*po==' '||*po=='\t') po++;
         if(!(ii&0xF0)&&*po=='#')
         { ii=((ii&0x0F)<<4)|hexx(po[1]);
           if(po[2]=='0'&&po[3]=='+'){j=21;po+=4;k=4;}
         }
       }
     }
     else if(po[0]=='L'&&po[1]==' '&&po[2]=='H')
     { j=2;po+=3;k=3;
       while(*po==' '||*po=='\t') po++;
       if(po[0]=='L'&&po[1]==' '&&po[2]=='H'){j=4;po+=3;k=4;}
     }
     else if(*po=='#')
     { ii=(hexx(po[1])<<4)|hexx(po[2]);
       po+=3;
       while(*po==' '||*po=='\t') po++;
       if(*po==0||*po=='/'){j=10;k=2;}
       else if(*po=='B'){j=11;po++;k=3;}
       else if(po[0]=='L'&&po[1]==' '&&po[2]=='H')
       { po+=3;
         while(*po==' '||*po=='\t') po++;
         if(*po==0||*po=='/'){j=12;k=4;}
         else if(*po=='B'){j=13;po++;k=5;}
         else if(po[0]=='L'&&po[1]==' '&&po[2]=='H'){j=14;po+=3;k=6;}
       }
       else if(*po=='+'){j=20;po++;k=2;} /* v2.6 */
     }
     while(*po==' '||*po=='\t') po++;
     if(po[0]=='/'&&po[1]=='/') *po=0;
     if(*po)
     { printf("ERROR: wrong code syntax %s...%s\n",str,po);
       continue;
     }
     libu0=TextAdd(code,str);
     libu0->type=j;
     libu0->id=i|(ii<<8);
     libu0->adr=i|(ii<<8);
     libu0->len=k;
#ifdef DEBUG
     printf("%s[%i] %s code=0x%2.2X type=%i\n",tabfile,tabnum,str,i,j);
#endif
     tabnum++;
   }
 }
 fclose(in);
 printf("\n### Initialization (%i codes from '%s')\n",tabnum,tabfile);
 free(tabfile);tabfile=NULL;
 libu = TextAdd(table,fna);
 libu -> type = TAB_INC;   /* Type of element */
 libu -> id   = inc_num++; /* Identifier of element */
 libu -> id2  = 0;         /* Additional identifier */
 txt=fopen(fna,"rt");
 if(txt==NULL){printf("ERROR: Can't open %s\n",fna);exit(1);}
 k=0;
 while(!feof(txt))
 { k++;
   strcpy(buff,stri(&c00));
   libu = TextAdd(text,buff);
   libu -> type = 0;
   libu -> id   = k;  /* Index if line (starts from 1) in source code */
   libu -> id2  = 0;  /* Main source file identifier */
 }
 fclose(txt);
 len=TextGetNumber(text);
 printf("\n### Includes\n");
 k=0;
 for(libu=text->first;libu!=NULL;libu=libu->next)
 {  k++;
    if(libu->str[0]=='+')
    {  po = libu -> str;
       *po='\\';
       po++;
       strcpy(str,po);
       strcat(str,".A");
//       for(j=0;j<strlen(str);j++) str[j]=toupper(str[j]);
       libu2 = TextAdd(table,str);
       libu2 -> type = TAB_INC;   /* It is included file */
       libu2 -> id   = inc_num++; /* Identifier of include */
       libu2 -> id2  = 0;         /* Additional identifier */
       if(f_debug) printf("Try include <%s>\n",str);
       txt=fopen(str,"rt");
       if(txt == NULL)
       {  (libu->str)[0]='+';
          Error(libu);
       }
       j = 0;
       while(!feof(txt))
       {  j++;
          strcpy(buff,stri(&c00));
          libu1 = libu -> next;
          libu -> next = LineNew(buff);
          libu = libu -> next;
          libu -> next = libu1;
          libu -> type = 0;
          libu -> id   = j; /* Index of line */
          libu -> id2  = inc_num - 1;  /* Identifier of include */
       }
       fclose(txt);
    }
 }
 len=TextGetNumber(text);
 if(f_debug)
 {
   printf("\n<CODE>\n");
   TextList(code);
   printf("\n\nNumber of Lines = %u\n\n<TABLE>\n",len);
   TextList(table);
   printf("\n<TEXT>\n");
   TextList(text);
   printf("\n");
 }
 printf("\n### Labels\n");
 for(libu=text->first;libu!=NULL;libu=libu->next)
 { strcpy(st,libu->str);
   /* Analize line of text */
   len=strlen(st);
   uk=0;
   strcpy(wo,word(st,&uk));
   switch(wo[0])
   {  case '\\': /* Comment */
                 break;
      case '/':  /* C-comment */
                 if(wo[1]!='/') Error(libu);
                 break;
      case '\0': /* Empty string */
                 break;
      case '@':  /* It's EQU */
                 sp=wo; /* sp=wo+1; */
                 strcpy(nam,sp);
                 strcpy(str,word(st,&uk));
                 if(strcmp(str,"EQU")&&strcmp(str,"=")) Error(libu);
                 strcpy(str,word(st,&uk));
                 j=getnum(str);
                 if(j==-1) Error(libu);
                 libu2=TextFindFirstStrTyp(table,nam,TAB_EQU);
                 if(libu2!=NULL) Error(libu);
                 libu0=TextAdd(table,nam);
                 libu0->type=TAB_EQU;
                 libu0->id=equ_num++;
                 libu0->adr=j;
                 (libu->str)[0]=0;
                 if(f_debug) printf("equ: %s=%u\n",nam,j);
                 break;
      case '+':  /* Unprocessed includes */
                 Error(libu);
                 break;
      default:   /* Probably it's a label */
                 sp=strchr(st,':');
                 strcpy(wo,word(st,&uk));
                 f_f=SH_FALSE;
                 if(!strcmp(wo,"DB")||!strcmp(wo,"DW"))
                 {  f_f=SH_TRUE;
                    sp=st;
                    while(!spa(*sp))sp++;
                 }
                 if(sp!=NULL||f_f)
                 {  *sp=0;
                    if(strlen(st)>NAM_LEN) Error(libu);
                    po=&sp[1]; /* po - tail of line */
                    i=0;
                    strcpy(wo,word(po,&i));
                    if(*wo=='\\'||*wo==0) *po=0;
                    else while(spa(*po))po++;
                    strcpy(str,po);
                    strcpy(libu->str,str);
                    libu->type=lbl_num;
                    strcpy(nam,st);
                    if(strlen(nam)>NAM_LEN) Error(libu);
                    libu2=TextFindFirstStrTyp(table,nam,TAB_LBL);
                    if(libu2!=NULL) Error(libu);
                    libu0=TextAdd(table,nam);
                    libu0->type=TAB_LBL;
                    libu0->id=lbl_num++;
                    libu0->id2=0;
                    if(f_debug) printf("label:%s\n",nam);
                 }
                 else
                 {  uk=0;
                    strcpy(wo,word(st,&uk));
                    if(!strcmp(wo,"ORG"))
                    {  if(org!=0) Error(libu);
                       strcpy(str,word(st,&uk));
                       j=getnum(str);
                       if(j==-1) Error(libu);
                       if(f_tunguska||f_voja4) j<<=1; /* v2.4 v2.6 */
                       org=j;
                       (libu->str)[0]='\\';
                    }
                 }
                 break;
   }
 }
 len=TextGetNumber(text);
 if(f_debug)
 {
   printf("\n\nNumber of Lines = %u\n\n<TABLE>\n",len);
   TextList(table);
   printf("\n<TEXT>\n");
   TextList(text);
   printf("\n");
 }
 printf("\n### Deleting comments and empty lines\n");
 libu = libu0 = text->first;
 while(libu!=NULL)
 {  fempty = 0;
    c00 = *libu->str;
    if(c00==0||c00=='\\'||c00=='/') fempty++;
    if(libu->type==0&&fempty)
    {  libu1 = libu->next;
       if(libu==text->first)
          text->first = libu1;
       else
          libu0->next = libu1;
       LineDel(libu);
       libu = libu1;
    }
    else
    {  if(fempty) *libu->str = 0;
       libu0 = libu;
       libu = libu->next;
    }
 }
 if(f_debug)
 {
   len=TextGetNumber(text);
   printf("\nNumber of Lines = %u\n",len);
   printf("\n<TEXT>\n");
   TextList(text);
   printf("\n\n");
 }
 printf("\n### First pass\n");
 tek=org;
 for(libu=text->first;libu!=NULL;libu=libu->next)
 { strcpy(str,libu->str);
   libu->adr=tek;
   if(libu->type!=0)
   {  libu0=TextFindFirstTypId(table,TAB_LBL,libu->type);
      if(libu0==NULL) Error(libu);
      libu0->adr=tek;
   }
   uk=0;
   f_f=SH_TRUE;
   strcpy(wo,word(str,&uk));
   if(!strcmp(wo,"DB"))
   {  f_f=SH_FALSE;
      sp=str+2;
      while(spa(*sp))sp++;
      strcpy(st,sp);
      i=0;
      strcpy(wo,word(st,&i));
      strcpy(buff,word(st,&i));
      po=strchr(buff,'(');
      if(po!=NULL) /* DUP */
      {  *po=0;
         if(strcmp(buff,"DUP")) Error(libu);
         po++;
         j=getval(wo);
         sp=strchr(po,')');
         if(sp==NULL) Error(libu);
         sp[0]=0;
         k=getval(po);
         if(k>255) Error(libu);
         for(i=0;i<j;i++)
         {  if(tek<32768) mem1[tek]=k;
            else mem2[tek-32768]=k;
            tek++;
         }
      }
      else
      {
       do{
#ifdef DEBUG
          printf("\ndb=|%s|",sp);
#endif
          if(*sp=='\"')
          {  while(1)
             {  sp++;
                if(*sp=='\"') break;
                if(*sp==0) Error(libu);
                if(tek<32768) mem1[tek]=*sp;
                else mem2[tek-32768]=*sp;
                tek++;
             }
             sp++;
             if(*sp==0) break;
             if(*sp!=',') Error(libu);
             sp++;
             po=sp;
          }
          else
          {  po=strchr(sp,',');
             if(po!=NULL)
             {  *po=0;
                po++;
             }
             j=getval(sp);
             if(j>255) Error(libu);
             if(tek<32768) mem1[tek]=j;
             else mem2[tek-32768]=j;
             tek++;
             sp=po;
          }
       }while(po!=NULL);
      }
   }
   if(f_f&&!strcmp(wo,"DW"))
   {  f_f=SH_FALSE;
      sp=str+2;
      while(spa(*sp))sp++;
      strcpy(st,sp);
      i=0;
      strcpy(wo,word(st,&i));
      strcpy(buff,word(st,&i));
      po=strchr(buff,'(');
      if(po!=NULL) /* DUP */
      {  *po=0;
         if(strcmp(buff,"DUP")) Error(libu);
         po++;
         j=getval(wo);
         sp=strchr(po,')');
         if(sp==NULL) Error(libu);
         sp[0]=0;
         ui=getval(po);
         i1=ui%256;
         i2=ui/256;
         for(i=0;i<j;i++)
         {   if(tek<32768) mem1[tek]=i1;
             else mem2[tek-32768]=i1;
             tek++;
             if(tek<32768) mem1[tek]=i2;
             else mem2[tek-32768]=i2;
             tek++;
         }
      }
      else
      {
       do{
#ifdef DEBUG
          printf("\ndw=|%s|",sp);
#endif
          if(sp[0]=='\"') Error(libu);
          po=strchr(sp,',');
          if(po!=NULL)
          {  *po=0;
             po++;
          }
          ui=getval(sp);
          i1=ui%256;
          i2=ui/256;
          if(tek<32768) mem1[tek]=i1;
          else mem2[tek-32768]=i1;
          tek++;
          if(tek<32768) mem1[tek]=i2;
          else mem2[tek-32768]=i2;
          tek++;
          sp=po;
       }while(po!=NULL);
      }
   }
   if(f_f && *wo)
   {  libu0=TextFindFirstStr(code,wo);
      if(libu0==NULL) Error(libu);
      strncpy(nam2,libu0->str,NAM_LEN);
      bb=libu0->type;
      zz=libu0->adr;
      libu->adr=tek;
      if(tek<32768) mem1[tek]=zz&0xFF;
      else mem2[tek-32768]=zz&0xFF;
      tek++;
      if(bb>=10) /* v2.4 instead of (zz&0xFF00) */
      {  if(tek<32768) mem1[tek]=zz>>8;
         else mem2[tek-32768]=zz>>8;
         tek++;
      }
      ftun=0; /* v2.4 */
      fnib=0; /* v2.6 */
      if(bb==14&&f_tunguska) ftun=1; /* v2.4 */
      if(bb>=20&&f_voja4){fnib=bb-19;bb=1;} /* v2.6 */
      if(bb>=10) bb-=10;
      while(bb)
      { ouk = uk;
        strcpy(nam,word(str,&uk));
        if(bb==1)
        {if(isnum(nam))
         {  ui=(SH_WORD)getnum(nam);
            if(ui==0xFFFF) Error(libu);
            if(f_voja4){if(ui>4095) Error(libu);}
            else{if(ui>255) Error(libu);}
            if(fnib==0)
            {  if(tek<32768) mem1[tek]=ui;
               else mem2[tek-32768]=ui;
               tek++;
            }
            else if(fnib==1) /* v2.6 */
            {  tek--;
               if(ui>15) Error(libu);
               if(tek<32768) mem1[tek]|=ui;
               else mem2[tek-32768]|=ui;
               tek++;
            }
            else if(fnib==2) /* v2.6 */
            {
               tek--;
               if(tek<32768) mem1[tek]=(ui&0xFF0)>>4;
               else mem2[tek-32768]=(ui&0xFF0)>>4;
               tek++;
               if(tek<32768) mem1[tek]=(zz&0xF000)>>12;
               else mem2[tek-32768]=(zz&0xF000)>>12;
               tek++;
               if(tek<32768) mem1[tek]=((zz&0xF00)>>4)|(ui&0x0F);
               else mem2[tek-32768]=((zz&0xF00)>>4)|(ui&0x0F);
               tek++;
            }
            else Error(libu);
         }
         else
         {  if(nam[0]=='@')
            {  libu0=TextFindFirstStrTyp(table,nam,TAB_EQU);
               if(libu0==NULL) Error(libu);
               ui=libu0->adr;
               if(f_voja4){if(ui>4095) Error(libu);}
               else{if(ui>255) Error(libu);}
               if(fnib==0)
               {  if(tek<32768) mem1[tek]=ui;
                  else mem2[tek-32768]=ui;
                  tek++;
               }
               else if(fnib==1) /* v2.6 */
               {  tek--;
                  if(ui>15) Error(libu);
                  if(tek<32768) mem1[tek]|=ui;
                  else mem2[tek-32768]|=ui;
                  tek++;
               }
               else if(fnib==2) /* v2.6 */
               {
                  tek--;
                  if(tek<32768) mem1[tek]=(ui&0xFF0)>>4;
                  else mem2[tek-32768]=(ui&0xFF0)>>4;
                  tek++;
                  if(tek<32768) mem1[tek]=(zz&0xF000)>>12;
                  else mem2[tek-32768]=(zz&0xF000)>>12;
                  tek++;
                  if(tek<32768) mem1[tek]=((zz&0xF00)>>4)|(ui&0x0F);
                  else mem2[tek-32768]=((zz&0xF00)>>4)|(ui&0x0F);
                  tek++;
               }
               else Error(libu);
            }
            else
            {  if(nam[0]=='\''&&nam[2]=='\'')
               {  if(fnib>0) Error(libu);
                  if(tek<32768) mem1[tek]=(SH_BYTE)nam[1];
                  else mem2[tek-32768]=(SH_BYTE)nam[1];
                  tek++;
               }
               else
               {  libu0=TextFindFirstStrTyp(table,nam,TAB_LBL);
                  if(libu0==NULL) Error(libu);
                  libu1=TextAdd(labels,nam);
                  libu1->id=libu0->id;
                  libu1->adr=tek;
                  libu1->type=1;
                  tek++;
                  if(fnib>0) /* v2.6 */
                  { libu1->adr--;
                    if(fnib==1)
                    { libu1->type=4;
                      tek--;
                    }
                    if(fnib==2)
                    { libu1->type=5;
                      tek-=2;
                      if(tek<32768) ui=mem1[tek];
                      else ui=mem2[tek-32768];
                      if(tek<32768) mem1[tek]=0;
                      else mem2[tek-32768]=0;
                      tek++;
                      if(tek<32768) mem1[tek]=(ui&0xF0)>>4;
                      else mem2[tek-32768]=(ui&0xF0)>>4;
                      tek++;
                      if(tek<32768) mem1[tek]=(ui&0x0F)<<4;
                      else mem2[tek-32768]=(ui&0x0F)<<4;
                      tek++;
                    }
                  }
               }
            }
         }
        }
        if(bb>=2)
        {if(isnum(nam))
         {  ui=(SH_WORD)getnum(nam);
            i1=ui&0xFF;
            i2=ui>>8;
            if(tek<32768) mem1[tek]=i1;
            else mem2[tek-32768]=i1;
            tek++;
            if(tek<32768) mem1[tek]=i2;
            else mem2[tek-32768]=i2;
            tek++;
         }
         else
         {  if(nam[0]=='@')
            {  libu0=TextFindFirstStrTyp(table,nam,TAB_EQU);
               if(libu0==NULL) Error(libu);
               ui=libu0->adr;
               if(ftun>0) /* v2.4 */
               { sis=(signed short)ui;
                 if(ftun==1)
                 { ui=(SH_WORD)((sis-get_tryte(sis))/729);
                   uk=ouk;
                   ftun=2;
                 }
                 else
                 { sis=get_tryte(sis);
                   ui=(SH_WORD)sis;
                 }
               }
               i1=ui&0xFF;
               i2=ui>>8;
               if(tek<32768) mem1[tek]=i1;
               else mem2[tek-32768]=i1;
               tek++;
               if(tek<32768) mem1[tek]=i2;
               else mem2[tek-32768]=i2;
               tek++;
            }
            else
            {  libu0=TextFindFirstStrTyp(table,nam,TAB_LBL);
               if(libu0==NULL) Error(libu);
               if(!f_tunguska||ftun==1)
               { libu1=TextAdd(labels,nam);
                 libu1->id=libu0->id;
                 libu1->adr=tek;
                 libu1->type=0;
                 po2=strchr(nam2,'_');
                 if(po2!=NULL&&!strcmp(po2,"_NEAR"))
                   libu1->type=2;
                 if(ftun>0) libu1->type=3; /* v2.4 */
               }
               if(ftun==1) /* v2.4 */
               { uk=ouk;
                 ftun=2;
               }
               tek+=2;
            }
         }
        }
        if(bb==1||bb==2) break;
        bb-=2;
      }
   }
 }
 if(f_debug)
 {
   printf("\n<TABLE>\n");
   TextList(table);
   len=TextGetNumber(text);
   printf("\n\nNumber of Lines = %u\n",len);
   printf("\n<TEXT>\n");
   TextList(text);
   printf("\n<LABELS>\n");
   TextList(labels);
   printf("\n\n");
 }
 printf("\n### Second pass\n");
 for(libu1=labels->first;libu1!=NULL;libu1=libu1->next)
 {  lbl_id=libu1->id;
    lbl_ad=libu1->adr;
    lbl_ty=libu1->type;
    libu0=TextFindFirstTypId(table,TAB_LBL,lbl_id);
    if(libu0==NULL)
    {  printf("\nLabel not Found!\n");
       exit(1);
    }
    lbl_zz=libu0->adr;
    if(lbl_ty==0)
    {  i1=lbl_zz&0xFF;
       i2=(lbl_zz>>8)&0xFF;
       if(lbl_ad<32768) mem1[lbl_ad]=i1;
       else mem2[lbl_ad-32768]=i1;
       lbl_ad++;
       if(lbl_ad<32768) mem1[lbl_ad]=i2;
       else mem2[lbl_ad-32768]=i2;
    }
    else if(lbl_ty==1)
    {  if(f_lavr4) /* v2.5 */
       {ii=lbl_zz&0xFF;
        if(lbl_ad<32768) mem1[lbl_ad]=ii;
        else mem2[lbl_ad-32768]=ii;
#ifdef DEBUG
        printf("Absoulte short jump detected: adr=0x%4.4X val=0x%2.2X\n",(SH_WORD)lbl_ad,ii);
#endif
       }
       else
       {ii=lbl_zz-lbl_ad-1;
        if(f_voja4) ii>>=1; /* v2.6 */
        if(ii<-128||ii>127)
        {  printf("\nToo far for relative jump to %s (%i)!\n",libu1->str,ii);
           exit(1);
        }
        if(lbl_ad<32768) mem1[lbl_ad]=ii&0xFF;
        else mem2[lbl_ad-32768]=ii&0xFF;
#ifdef DEBUG
        printf("Relative short jump detected: adr=0x%4.4X rel=0x%2.2X\n",(SH_WORD)lbl_ad,ii&0xFF);
#endif
       }
    }
    else if(lbl_ty==2)
    {  lbl_zz-=lbl_ad+2;
       if(lbl_ad<32768) mem1[lbl_ad]=lbl_zz&0xFF;
       else mem2[lbl_ad-32768]=lbl_zz&0xFF;
       lbl_ad++;
       if(lbl_ad<32768) mem1[lbl_ad]=(lbl_zz>>8)&0xFF;
       else mem2[lbl_ad-32768]=(lbl_zz>>8)&0xFF;
#ifdef DEBUG
       printf("Relative near jump detected: adr=0x%4.4X rel=0x%4.4X\n",(SH_WORD)lbl_ad,(SH_WORD)(lbl_zz&0xFFFF));
#endif
    }
    else if(lbl_ty==3) /* v2.4 */
    {  sis = (signed short)(lbl_zz>>1);
       t0 = get_tryte(sis);
       t1 = (sis-t0)/729;
       if(lbl_ad<32768) mem1[lbl_ad]=t1&0xFF;
       else mem2[lbl_ad-32768]=t1&0xFF;
       lbl_ad++;
       if(lbl_ad<32768) mem1[lbl_ad]=(t1>>8)&0xFF;
       else mem2[lbl_ad-32768]=(t1>>8)&0xFF;
       lbl_ad++;
       if(lbl_ad<32768) mem1[lbl_ad]=t0&0xFF;
       else mem2[lbl_ad-32768]=t0&0xFF;
       lbl_ad++;
       if(lbl_ad<32768) mem1[lbl_ad]=(t0>>8)&0xFF;
       else mem2[lbl_ad-32768]=(t0>>8)&0xFF;
#ifdef DEBUG
       printf("Ternary jump detected: adr=0x%4.4X jmp=0x%4.4X t1=%i t0=%i\n",(SH_WORD)lbl_ad,(SH_WORD)(lbl_zz&0xFFFF),t1,t0);
#endif
    }
    else if(lbl_ty==4) /* v2.6 */
    {  ii=lbl_zz;
       if(f_voja4) ii>>=1;
       ii&=0x0F;
       if(lbl_ad<32768) mem1[lbl_ad]|=ii;
       else mem2[lbl_ad-32768]|=ii;
#ifdef DEBUG
       printf("Absoulte 4-bit jump detected: adr=0x%4.4X val=0x%2.2X\n",(SH_WORD)lbl_ad,ii);
#endif
    }
    else if(lbl_ty==5) /* v2.6 */
    {  ii=lbl_zz;
       if(f_voja4) ii>>=1;
       if(lbl_ad<32768) mem1[lbl_ad]=(ii>>4)&0xFF;
       else mem2[lbl_ad-32768]=(ii>>4)&0xFF;
       lbl_ad+=2;
       if(lbl_ad<32768) mem1[lbl_ad]|=ii&0x0F;
       else mem2[lbl_ad-32768]|=ii&0x0F;
#ifdef DEBUG
       printf("Absoulte 12-bit jump detected: adr=0x%4.4X val=0x%2.2X\n",(SH_WORD)(lbl_ad-2),ii);
#endif
    }
    else
    {  printf("\nLabel of unknown type %i!\n",lbl_ty);
       exit(1);
    }
 }
 strcpy(fna,orig);
 strcat(fna,EXT_LST);
 lst=fopen(fna,"wt");
 if(lst==NULL) exit(1);
 fprintf(lst,"This listing was generated by RoboAssembler v" RASM_VERSION " (see http://nedopc.org)\n\n");
 for(aa=org;aa<tek;aa++)
 {  manyl=0;
    if(aa<32768) bb=mem1[aa];
    else bb=mem2[aa-32768];
    if(!f_voja4) fprintf(lst,"\n%4.4X %2.2X",aa,bb);
    else /* v2.6 */
    {
      if(aa&1) fprintf(lst,"\n     %2.2X",bb);
      else fprintf(lst,"\n %3.3X %2.2X",aa>>1,bb);
    }
    libu0=TextFindFirstAdr(text,aa);
    if(libu0!=NULL)
    { do
      { manyl++;
        if(libu0->type!=0)
        { libu1=TextFindFirstTypId(table,TAB_LBL,libu0->type);
          if(libu1==NULL) Error(libu0);
          fprintf(lst," %s:\t",libu1->str);
        }
        else if(manyl==1) fprintf(lst,"\t\t");
        if(*libu0->str) fprintf(lst,"%s",libu0->str);
        libu0 = TextFindNext(text);
      } while(libu0!=NULL);
    }
 }
 TextSort(table,TextFldStr);
 fprintf(lst,"\n\n\n");
 for(libu=table->first;libu!=NULL;libu=libu->next)
 { if(libu->type!=TAB_INC)
      fprintf(lst,"\n%s%c\t%4.4X",libu->str,(strlen(libu->str)>=8)?' ':'\t',
          (f_voja4&&libu->type==TAB_LBL)?(libu->adr>>1):libu->adr); /* v2.6 */
 }
 fprintf(lst,"\n\n");
 fclose(lst);
 strcpy(fna,orig);
 strcat(fna,EXT_BIN);
 fcod=fopen(fna,"wb");
 if(fcod==NULL) exit(1);
 for(aa=org;aa<tek;aa++)
 { if(aa<32768) bb=mem1[aa];
   else bb=mem2[aa-32768];
   fputc(bb,fcod);
 }
 fclose(fcod);
 if(f_debug)
 { printf("\n<TABLE>\n");
   TextList(table);
   len=TextGetNumber(text);
   printf("\n\nNumber of Lines = %u\n",len);
   printf("\n<TEXT>\n");
   TextList(text);
   printf("\n\n");
 }
 printf("\n### Compilation complete\n\n");
 TextDel(code);
 TextDel(text);
 TextDel(table);
 TextDel(labels);
 LineDel(nol);
 free(mem1);
 free(mem2);
 free(st);
 free(wo);
 free(str);
 free(buff);
 free(orig);
 free(fna);
 return 0;
}
