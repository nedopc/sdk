/*
 *  Добавлено:
 *   ■  Автоматическое сканирование на метки, содержащие первый символ
 *      имени "@" (@label) и запись их в файл *.tbl.
 *   ■  Добавлена директива INCBIN (вставить бинарный файл).
 *
 *  Фитчи:
 *    Понимает hex-цифры '#цифра'.
 *    Для 'include', 'incbin' имена файлов можно не заключать в кавычки.
 *    Не понимает индексные половинки.
 *
 *    Везде изменен вывод сообщений с "stderr" на "stdout".
 *    Теперь вывод можно переопределять в файл.
 *
 *
 *  Баг-фиксы:
 *    В версии 1.33 исправлен баг в процедуре "doincbin()", вызывавший
 *    ошибку "phase error".
 *
 */


#include <unistd.h>



/*  A Bison parser, made from zmac.y
    by GNU Bison version 1.28  */

#define ZMAC_VERSION	"1.33"
/* #define ZMAC_BETA	"b4" */


#define STRING		257
#define NOOPERAND	258
#define ARITHC		259
#define ADD		260
#define LOGICAL		261
#define AND		262
#define OR		263
#define XOR		264
#define BIT		265
#define CALL		266
#define INCDEC		267
#define DJNZ		268
#define EX		269
#define IM		270
#define PHASE		271
#define DEPHASE		272
#define IN		273
#define JP		274
#define JR		275
#define LD		276
#define OUT		277
#define PUSHPOP		278
#define RET		279
#define SHIFT		280
#define RST		281
#define REGNAME		282
#define ACC		283
#define C		284
#define RP		285
#define HL		286
#define INDEX		287
#define AF		288
#define SP		289
#define MISCREG		290
#define F		291
#define COND		292
#define SPCOND		293
#define NUMBER		294
#define UNDECLARED	295
#define END		296
#define ORG		297
#define DEFB		298
#define DEFS		299
#define DEFW		300
#define EQU		301
#define DEFL		302
#define LABEL		303
#define EQUATED		304
#define WASEQUATED	305
#define DEFLED		306
#define MULTDEF		307
#define MOD		308
#define SHL		309
#define SHR		310
#define NOT		311
#define LT		312
#define GT		313
#define EQ		314
#define LE		315
#define GE		316
#define NE		317
#define IF		318
#define ELSE		319
#define ENDIF		320
#define ARGPSEUDO	321
#define LIST		322
#define MINMAX		323
#define MACRO		324
#define MNAME		325
#define OLDMNAME	326
#define ARG		327
#define ENDM		328
#define MPARM		329
#define ONECHAR		330
#define TWOCHAR		331
#define UNARY		332


#line 1 "zmac.y"

/*
 *  zmac -- macro cross-assembler for the Zilog Z80 microprocessor
 *
 *  Bruce Norskog	4/78
 *
 *  Last modification 2000-07-01 by mgr
 *
 *  This assembler is modeled after the Intel 8080 macro cross-assembler
 *  for the Intel 8080 by Ken Borgendale.  The major features are:
 *	1.  Full macro capabilities
 *	2.  Conditional assembly
 *	3.  A very flexible set of listing options and pseudo-ops
 *	4.  Symbol table output
 *	5.  Error report
 *	6.  Elimination of sequential searching
 *	7.  Commenting of source
 *	8.  Facilities for system definiton files
 *
 * (Revision history is now in ChangeLog. -rjm)
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include "mio.h"
#include "getoptn.h"

#define UNUSED(var)   ((void) var)

#if defined (__riscos__) && !defined (__riscos)
#define __riscos
#endif

#ifdef __riscos
#include "swis.h"
#define DDEUtils_Prefix		0x42580
#define DDEUtils_ThrowbackStart	0x42587
#define DDEUtils_ThrowbackSend	0x42588
#define DDEUtils_ThrowbackEnd	0x42589
#endif

#ifndef OS_DIR_SEP
#if defined (MSDOS)
#define OS_DIR_SEP '\\'
#elif defined (__riscos)
#define OS_DIR_SEP '.'
#else
#define OS_DIR_SEP '/'
#endif
#endif

#ifndef OS_EXT_SEP
#if defined (__riscos)
#define OS_EXT_SEP '/'
#else
#define OS_EXT_SEP '.'
#endif
#endif

/*
 * DEBUG turns on pass reporting.
 * Macro debug and Token debug enables.
#define	DEBUG
#define	M_DEBUG
#define	T_DEBUG
 */

#define ITEMTABLESIZE	2000
#define TEMPBINBUFSIZE  512		/* buffer size incbin-files */
#define TEMPBUFSIZE	200
#define LINEBUFFERSIZE	200
#define EMITBUFFERSIZE	200
#define MAXSYMBOLSIZE	40
#define IFSTACKSIZE	20
#define MAXIFS		1024
#define TITLELEN	50
#define BINPERLINE	16
#define	PARMMAX		25
#define MAXEXP		25
#define SYMMAJIC	3715
#define	NEST_IN		8		/* число вложений include-файлов */


#define loop	for(;;)

void yyerror(char *err)
{
	UNUSED (err);		/* we will do our own error printing */
	/* printf ("Oops! %s\n", err); */
}

struct	item	{
	char	*i_string;
	int	i_value;
	int	i_token;
	int	i_uses;
	int	i_equbad;
};

FILE	*fout,
	*fbuf,
	*fin[NEST_IN],
	*now_file,
        *fspeclab,		/* TBL-file */
	*now_binfile;		/* incbin-file */

int	pass2,			/* set when pass one completed */
	dollarsign,		/* location counter */
	olddollar;		/* kept to put out binary */


/* program counter save for PHASE/DEPHASE */
int	phdollar, phbegin, phaseflag;

char	*src_name[NEST_IN];
int	linein[NEST_IN],
	now_in;


#define bflag	0	/* balance error */
#define eflag	1	/* expression error */
#define fflag	2	/* syntax error */
#define iflag	3	/* bad digits */
#define mflag	4	/* multiply defined */
#define pflag	5	/* phase error */
#define uflag	6	/* undeclared used */
#define vflag	7	/* value out of range */
#define oflag	8	/* phase/dephase error */
#define frflag	9	/* double forward ref. via equ error */
#define zflag	10	/* Z80-only instruction (when '-z' option in use) */
#define orgflag 11	/* retrograde org error (when '-h' option not in use) */

#define FLAGS	12	/* number of flags */

char	err[FLAGS];
int	keeperr[FLAGS];
char	errlet[FLAGS]="BEFIMPUVORZG";
char	*errname[FLAGS]={
	"Balance",
	"Expression",
	"Syntax",
	"Digit",
	"Mult. def.",
	"Phase",
	"Undeclared",
	"Value",
	"Phase/Dephase",
	"Forward ref. to EQU with forward ref.",
	"Z80-specific instruction",
	"Retrograde ORG"
};
char	*warnname[]={
	"Symbol length exceeded",
	"Non-standard syntax",
	"Could replace JP with JR",
	"Could replace LD A, 0 with XOR A if flags unimportant",
	"Could replace RLC A with RLCA if S, Z and P/V flags unimportant",
	"Could replace RRC A with RRCA if S, Z and P/V flags unimportant",
	"Could replace RL A with RLA if S, Z and P/V flags unimportant",
	"Could replace RR A with RRA if S, Z and P/V flags unimportant",
	"Could replace SLA A with ADD A, A if H and P/V flags unimportant"
};

/* for "0 symbols", "1 symbol", "2 symbols", etc. */
#define DO_PLURAL(x)	(x),((x)==1)?"":"s"

char	linebuf[LINEBUFFERSIZE];
char	*lineptr;
char	*linemax = linebuf+LINEBUFFERSIZE;

char	outbin[BINPERLINE];
char	*outbinp = outbin;
char	*outbinm = outbin+BINPERLINE;

char	emitbuf[EMITBUFFERSIZE];
char	*emitptr;

char	ifstack[IFSTACKSIZE];
char	*ifptr;
char	*ifstmax = ifstack+IFSTACKSIZE-1;


char	expif[MAXIFS];
char	*expifp;
char	*expifmax = expif+MAXIFS;

char	hexadec[] = "0123456789ABCDEF";
char	*expstack[MAXEXP];
int	expptr;


int	nitems;
int	linecnt;
int	nbytes;
int	invented;


char	tempbuf[TEMPBUFSIZE];
char    temp_binbuf[TEMPBINBUFSIZE];		/* buffer incbin-files */
char	*tempmax = tempbuf+TEMPBUFSIZE-1;

char	inmlex;
char	arg_flag;
char	quoteflag;
int	parm_number;
int	exp_number;
char	symlong[] = "Symbol too long";

int	disp;
#define FLOC	PARMMAX
#define TEMPNUM	PARMMAX+1
char	**est;
char	**est2;

char	*floc;
int	mfptr;
FILE	*mfile;


char	*title;
char	titlespace[TITLELEN];
char	*timp;
char	*sourcef;
/* changed to cope with filenames longer than 14 chars -rjm 1998-12-15 */
char	src[1024];
char	bin[1024];
char	mtmp[1024];
char	listf[1024];
char    specf[1024];		/* for tbl-file */
char	writesyms[1024];
#ifdef __riscos
char	riscos_thbkf[1024];
#endif

char	bopt = 1,		/* flag "no binary" */
	edef = 1,
	eopt = 1,
	fdef = 0,
	fopt = 0,
	gdef = 1,
	gopt = 1,
	iopt = 0,		/* list include files */
	lstoff = 0,
	lston = 0,		/* flag to force listing on */
	lopt = 0,		/* flag "create lst-file" */
	mdef = 0,
	mopt = 0,
	nopt = 1,		/* line numbers on as default */
	oldoopt = 0,
	popt = 1,		/* form feed as default page eject */
	sopt = 0,		/* turn on symbol table listing */
	output_hex = 0,		/* '-h', output .hex rather than .bin -rjm */
	output_8080_only = 0,	/* '-z', output 8080-compat. ops only -rjm */
	show_error_line = 0,	/* '-S', show line which caused error -rjm */
	terse_lst_errors = 0,	/* '-t', terse errors in listing -rjm */
	continuous_listing = 1,	/* '-d', discontinuous - with page breaks */
	suggest_optimise = 0,	/* '-O', suggest optimisations -mgr */
#ifdef __riscos
	riscos_thbk = 0,	/* '-T', RISC OS throwback -mep */
#endif
	output_amsdos = 0,	/* '-A', AMSDOS binary file output -mep */
	saveopt,
        put_spec_labels = 0;	/* flag presents "@label" labels */

char	xeq_flag = 0;
int	xeq;

time_t	now;
int	line;
int	page = 1;

int	had_errors = 0;		/* if program had errors, do exit(1) */
#ifdef __riscos
int	riscos_throwback_started = 0;
#endif
int	not_seen_org = 1;
int	first_org_store = 0;

struct stab {
	char	t_name[MAXSYMBOLSIZE+1];
	int	t_value;
	int	t_token;
};

/*
 *  push back character
 */
int	peekc;


/* function prototypes */
int addtoline(int ac);
int iflist(void);
int yylex(void);
int tokenofitem(int deftoken);
int nextchar(void);
int skipline(int ac);
void usage(void);
int main(int argc, char *argv[]);
int getarg(void);
int getm(void);
void yyerror(char *err);
void emit(int num, ...);
void emit1(int opcode,int regvalh,int data16,int type);
void emitdad(int rp1,int rp2);
void emitjr(int opcode,int expr);
void emitjp(int opcode,int expr);
void putbin(int v);
void flushbin(void);
void puthex(char byte, FILE *buf);
void list(int optarg);
void lineout(void);
void eject(void);
void space(int n);
void lsterr1(void);
void lsterr2(int lst);
void errorprt(int errnum);
void warnprt(int warnnum, int warnoff);
void list1(void);
void interchange(int i, int j);
void custom_qsort(int m, int n);
void setvars(void);
void error(char *as);
void fileerror(char *as,char *filename);
void justerror(char *as);
void putsymtab(void);
void erreport(void);
void mlex(void);
void suffix_if_none(char *str,char *suff);
void suffix(char *str,char *suff);
void decanonicalise(char *str);
void putm(char c);
void popsi(void);
char *getlocal(int c, int n);
void insymtab(char *name);
void outsymtab(char *name);
void copyname(char *st1, char *st2);
void next_source(char *sp);
void doatexit (void);
#ifdef __riscos
void riscos_set_csd(char *sp);
void riscos_throwback(int severity, char *file, int line, char *error);
#endif
void spec_labels(void);
void doincbin(char *sp);




/*
 *  add a character to the output line buffer
 */
int addtoline(int ac)
{
	/* check for EOF from stdio */
	if (ac == -1)
		ac = 0 ;
	if (inmlex)
		return(ac);
	if (lineptr >= linemax)
		error("line buffer overflow");
	*lineptr++ = ac;
	return(ac);
}


/*
 *  put values in buffer for outputing
 */

void emit(int bytes, ...)
{
	va_list ap;
	unsigned char *oldemitptr=(unsigned char *)emitptr;
	int c;

	va_start(ap,bytes);

	while	(--bytes >= 0)
		if (emitptr >= &emitbuf[EMITBUFFERSIZE])
			error("emit buffer overflow");
		else {
			*emitptr++ = va_arg(ap,int);
		}

	if (output_8080_only) {
		/* test for Z80-specific ops. These start with one of
		 * sixteen byte values, listed below. The values were
		 * taken from "A Z80 Workshop Manual" by E. A. Parr. -rjm
		 */
		/* As far as I can tell from my own literature
		 * review, 0x02, 0x0a, 0x12 and 0x1a are valid
		 * 8080 opcodes (LDAX/STAX B/D) -mgr
		 */
		c=*oldemitptr;
		if (/* c==0x02 || */ c==0x08 || /* c==0x0a || */ c==0x10 ||
		    /* c==0x12 || */ c==0x18 || /* c==0x1a || */ c==0x20 ||
		    c==0x28 || c==0x30 || c==0x38 || c==0xcb ||
		    c==0xd9 || c==0xdd || c==0xed || c==0xfd)
			err[zflag]++;
	}

	va_end(ap);
}

/* for emitted data - as above, without 8080 test.
 * Duplicating the code was easier than putting an extra arg in all
 * those emit()s. :-} Hopefully this isn't too unbearably nasty. -rjm
 */
void dataemit(int bytes, ...)
{
	va_list ap;

	va_start(ap,bytes);

	while	(--bytes >= 0)
		if (emitptr >= &emitbuf[EMITBUFFERSIZE])
			error("emit buffer overflow");
		else {
			*emitptr++ = va_arg(ap,int);
		}
	va_end(ap);
}


void emit1(int opcode,int regvalh,int data16,int type)
{
	if ((regvalh & 0x8000)) {	/* extra brackets to silence -Wall */
		if ((type & 1) == 0 && (disp > 127 || disp < -128))
			err[vflag]++;
		switch(type) {
		case 0:
			if (opcode & 0x8000)
				emit(4, regvalh >> 8, opcode >> 8, disp, opcode);
			else
				emit(3, regvalh >> 8, opcode, disp);
			break;
		case 1:
			emit(2, regvalh >> 8, opcode);
			break;
		case 2:
			if (data16 > 255 || data16 < -128)
				err[vflag]++;
			emit(4, regvalh >> 8, opcode, disp, data16);
			break;
		case 5:
			emit(4, regvalh >> 8, opcode, data16, data16 >> 8);
		}
	} else
		switch(type) {
		case 0:
			if (opcode & 0100000)
				emit(2, opcode >> 8, opcode);
			else
				emit(1, opcode);
			break;
		case 1:
			if (opcode & 0100000)
				emit(2, opcode >> 8, opcode);
			else
				emit(1, opcode);
			break;
		case 2:
			if (data16 > 255 || data16 < -128)
				err[vflag]++;
			emit(2, opcode, data16);
			break;
		case 3:
			if (data16 >255 || data16 < -128)
				err[vflag]++;
			emit(2, opcode, data16);
			break;
		case 5:
			if (opcode & 0100000)
				emit(4, opcode >> 8, opcode, data16, data16 >> 8);
			else
				emit(3, opcode, data16, data16 >> 8);
		}
}




void emitdad(int rp1,int rp2)
{
	if (rp1 & 0x8000)
		emit(2,rp1 >> 8, rp2 + 9);
	else
		emit(1,rp2 + 9);
}


void emitjr(int opcode,int expr)
{
	disp = expr - dollarsign - 2;
	if (disp > 127 || disp < -128)
		err[vflag]++;
	emit(2, opcode, disp);
}



void emitjp(int opcode,int expr)
{
	if (suggest_optimise && pass2 && opcode <= 0xda && !output_8080_only) {
		disp = expr - dollarsign - 2;
		if (disp <= 127 && disp >= -128)
			warnprt (2, 0);
	}
	emit(3, opcode, expr, expr >> 8);
}




/*
 *  put out a byte of binary
 */
void putbin(int v)
{
	if(!pass2 || !bopt) return;
	*outbinp++ = v;
	if (outbinp >= outbinm) flushbin();
}



/*
 *  output one line of binary in INTEL standard form
 */
void flushbin()
{
	char *p;
	int check=outbinp-outbin;

	if (!pass2 || !bopt)
		return;
	nbytes += check;
	if (check) {
		if (output_hex) {
			putc(':', fbuf);
			puthex(check, fbuf);
			puthex(olddollar>>8, fbuf);
			puthex(olddollar, fbuf);
			puthex(0, fbuf);
		}
		check += (olddollar >> 8) + olddollar;
		olddollar += (outbinp-outbin);
		for (p=outbin; p<outbinp; p++) {
			if (output_hex)
				puthex(*p, fbuf);
			else
				fputc(*p, fbuf);
			check += *p;
		}
		if (output_hex) {
			puthex(256-check, fbuf);
			putc('\n', fbuf);
		}
		outbinp = outbin;
	}
}



/*
 *  put out one byte of hex
 */
void puthex(char byte, FILE *buf)
{
	putc(hexadec[(byte >> 4) & 017], buf);
	putc(hexadec[byte & 017], buf);
}

/*
 *  put out a line of output -- also put out binary
 */
void list(int optarg)
{
	char *	p;
	int	i;
	int  lst;

	if (!expptr)
		linecnt++;
	addtoline('\0');
	if (pass2) {
		lst = iflist();
		if (lst) {
			lineout();
			if (nopt)
				fprintf(fout, "%4d:\t", linein[now_in]);
			puthex(optarg >> 8, fout);
			puthex(optarg, fout);
			fputs("  ", fout);
			for (p = emitbuf; (p < emitptr) && (p - emitbuf < 4); p++) {
				puthex(*p, fout);
			}
			for (i = 4 - (p-emitbuf); i > 0; i--)
				fputs("  ", fout);
			putc('\t', fout);
			fputs(linebuf, fout);
		}

		if (bopt) {
			for (p = emitbuf; p < emitptr; p++)
				putbin(*p);
		}


		p = emitbuf+4;
		while (lst && gopt && p < emitptr) {
			lineout();
			if (nopt) putc('\t', fout);
			fputs("      ", fout);
			for (i = 0; (i < 4) && (p < emitptr);i++) {
				puthex(*p, fout);
				p++;
			}
			putc('\n', fout);
		}


		lsterr2(lst);
	} else
		lsterr1();
	dollarsign += emitptr - emitbuf;
	emitptr = emitbuf;
	lineptr = linebuf;
}



/*
 *  keep track of line numbers and put out headers as necessary
 */
void lineout()
{
	if (continuous_listing) {
		line = 1;
		return;
	}
	if (line == 60) {
		if (popt)
			putc(0x0C, fout);	/* send the form feed */
		else
			fputs("\n\n\n\n\n", fout);
		line = 0;
	}
	if (line == 0) {
		fprintf(fout, "\n\n%s %s\t%s\t Page %d\n\n\n",
			&timp[4], &timp[20], title, page++);
		line = 4;
	}
	line++;
}


/*
 *  cause a page eject
 */
void eject()
{
	if (pass2 && !continuous_listing && iflist()) {
		if (popt) {
			putc(0x0C, fout);	/* send the form feed */
		} else {
			while (line < 65) {
				line++;
				putc('\n', fout);
			}
		}
	}
	line = 0;
}


/*
 *  space n lines on the list file
 */
void space(int n)
{
	int	i ;
	if (pass2 && iflist())
		for (i = 0; i<n; i++) {
			lineout();
			putc('\n', fout);
		}
}


/*
 *  Error handling - pass 1
 */
void lsterr1()
{
	int i;
	for (i = 0; i <= 4; i++)
		if (err[i]) {
			errorprt(i);
			err[i] = 0;
		}
}


/*
 *  Error handling - pass 2.
 */
void lsterr2(int lst)
{
	int i;
	for (i=0; i<FLAGS; i++)
		if (err[i]) {
			if (lst) {
				lineout();
				/* verbose inline error messages now,
				 * must override with '-t' to get old
				 * behaviour. -rjm
				 */
				if (terse_lst_errors)
					putc(errlet[i], fout);
				else
					fprintf(fout,"*** %s error ***",
							errname[i]);
				putc('\n', fout);
			}
			err[i] = 0;
			keeperr[i]++;
			if (i > 4)
				errorprt(i);
		}

	fflush(fout);	/* to avoid putc(har) mix bug */
}

/*
 *  print diagnostic to error terminal
 */
void errorprt(int errnum)
{
	had_errors=1;
	fprintf(stdout,"%s:%d: %s error\n",
		src_name[now_in], linein[now_in], errname[errnum]);
	if(show_error_line)
		fprintf(stdout, "%s\n", linebuf);
#ifdef __riscos
	if (riscos_thbk)
		riscos_throwback (1, src_name[now_in], linein[now_in], errname[errnum]);
#endif
}


/*
 *  print warning to error terminal
 */
void warnprt(int warnnum, int warnoff)
{
	fprintf(stdout,"%s:%d: warning: %s\n",
		src_name[now_in], linein[now_in] + warnoff, warnname[warnnum]);
		/* Offset needed if warning issued while line is being parsed */
#ifdef __riscos
	if (riscos_thbk)
		riscos_throwback (0, src_name[now_in], linein[now_in] + warnoff, warnname[warnnum]);
#endif
	/* if(show_error_line)
		Can't show line because it isn't necessarily complete
		fprintf(stdout, "%s\n", linebuf); */
}


/*
 *  list without address -- for comments and if skipped lines
 */
void list1()
{
	int lst;

	addtoline('\0');
	lineptr = linebuf;
	if (!expptr) linecnt++;
	if (pass2)
	{
		if ((lst = iflist())) {
			lineout();
			if (nopt)
				fprintf(fout, "%4d:\t", linein[now_in]);
			fprintf(fout, "\t\t%s", linebuf);
			lsterr2(lst);
		}
	} else
		lsterr1();
}


/*
 *  see if listing is desired
 */
int iflist()
{
	int i, j;

	if (lston)
		return(1) ;
	if (lopt)
		return(0);
	if (*ifptr && !fopt)
		return(0);
	if (!lstoff && !expptr)
		return(1);
	j = 0;
	for (i=0; i<FLAGS; i++)
		if (err[i])
			j++;
	if (expptr)
		return(mopt || j);
	if (eopt && j)
		return(1);
	return(0);
}


/* moved out of %{..%} bit in parse routine because 'bison -y'
 * didn't like it... -rjm
 */
char  *cp;

int list_tmp1,list_tmp2;
int equ_bad_label=0;


#line 829 "zmac.y"
typedef union	{
	struct item *itemptr;
	int ival;
	char *cval;
	} YYSTYPE;
#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		351
#define	YYFLAG		-32768
#define	YYNTBASE	102

#define YYTRANSLATE(x) ((unsigned)(x) <= 332 ? yytranslate[x] : 136)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    92,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,    89,     2,     2,    99,    88,    80,    98,    96,
    97,    86,    84,    93,    85,    95,    87,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    94,     2,    82,
    81,    83,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
   100,     2,   101,    79,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,    78,     2,    90,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     3,     4,     5,     6,
     7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
    37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
    57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
    67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
    77,    91
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     1,     4,     7,    11,    16,    21,    28,    32,    35,
    38,    42,    47,    52,    58,    63,    66,    70,    75,    78,
    84,    86,    88,    90,    91,    93,    94,    97,   100,   102,
   105,   108,   111,   114,   119,   122,   127,   130,   133,   136,
   139,   144,   149,   154,   159,   162,   167,   170,   175,   178,
   181,   184,   187,   192,   197,   202,   207,   210,   213,   218,
   223,   228,   231,   234,   239,   244,   249,   252,   257,   260,
   265,   268,   270,   273,   278,   283,   290,   295,   302,   307,
   312,   317,   322,   327,   332,   337,   342,   350,   357,   362,
   369,   376,   381,   388,   391,   394,   396,   399,   402,   405,
   407,   408,   410,   414,   416,   417,   419,   423,   425,   427,
   429,   431,   433,   435,   439,   444,   448,   450,   452,   454,
   456,   458,   460,   462,   464,   466,   468,   470,   472,   474,
   476,   478,   480,   484,   486,   488,   490,   492,   496,   498,
   500,   502,   504,   506,   510,   512,   514,   516,   518,   520,
   522,   524,   526,   528,   532,   536,   540,   544,   548,   552,
   556,   560,   564,   568,   572,   576,   580,   584,   588,   592,
   596,   600,   604,   608,   612,   616,   620,   624,   627,   630,
   633,   636,   639,   641,   643,   645,   647,   649,   651,   652,
   653,   654,   655
};

static const short yyrhs[] = {    -1,
   102,   103,     0,   106,    92,     0,   106,   107,    92,     0,
   130,    47,   127,    92,     0,   130,    48,   127,    92,     0,
   130,    69,   127,    93,   127,    92,     0,    64,   127,    92,
     0,    65,    92,     0,    66,    92,     0,   106,    42,    92,
     0,   106,    42,   127,    92,     0,   106,    45,   127,    92,
     0,    67,   132,    73,   133,    92,     0,    67,   132,   133,
    92,     0,    68,    92,     0,    68,   127,    92,     0,    41,
    70,   108,    92,     0,    72,    70,     0,   106,    71,   131,
   110,    92,     0,     1,     0,    94,     0,    95,     0,     0,
    94,     0,     0,   104,   130,     0,   130,   105,     0,     4,
     0,    20,   127,     0,    12,   127,     0,    27,   127,     0,
     6,   127,     0,     6,    29,    93,   127,     0,     5,   127,
     0,     5,    29,    93,   127,     0,     7,   127,     0,     8,
   127,     0,     9,   127,     0,    10,   127,     0,     7,    29,
    93,   127,     0,     8,    29,    93,   127,     0,     9,    29,
    93,   127,     0,    10,    29,    93,   127,     0,     6,   112,
     0,     6,    29,    93,   112,     0,     5,   112,     0,     5,
    29,    93,   112,     0,     7,   112,     0,     8,   112,     0,
     9,   112,     0,    10,   112,     0,     7,    29,    93,   112,
     0,     8,    29,    93,   112,     0,     9,    29,    93,   112,
     0,    10,    29,    93,   112,     0,    26,   112,     0,    13,
   112,     0,     5,    32,    93,   118,     0,     6,   119,    93,
   117,     0,     6,   119,    93,   119,     0,    13,   115,     0,
    24,   116,     0,    11,   127,    93,   112,     0,    20,   120,
    93,   127,     0,    20,    96,   119,    97,     0,    20,   119,
     0,    12,   120,    93,   127,     0,    21,   127,     0,    21,
   121,    93,   127,     0,    14,   127,     0,    25,     0,    25,
   120,     0,    22,   112,    93,   112,     0,    22,   112,    93,
   129,     0,    22,   112,    93,    96,    31,    97,     0,    22,
   112,    93,   128,     0,    22,    96,    31,    97,    93,    29,
     0,    22,   128,    93,    29,     0,    22,   112,    93,    36,
     0,    22,    36,    93,    29,     0,    22,   115,    93,   126,
     0,    22,   115,    93,   128,     0,    22,   128,    93,   115,
     0,    22,   115,    93,   119,     0,    15,    31,    93,    32,
     0,    15,    34,    93,    34,   134,    98,   135,     0,    15,
    96,    35,    97,    93,   119,     0,    19,   113,    93,   128,
     0,    19,   113,    93,    96,    30,    97,     0,    19,    37,
    93,    96,    30,    97,     0,    23,   128,    93,    29,     0,
    23,    96,    30,    97,    93,   113,     0,    16,   127,     0,
    17,   127,     0,    18,     0,    43,   127,     0,    44,   122,
     0,    46,   124,     0,    74,     0,     0,   109,     0,   108,
    93,   109,     0,    41,     0,     0,   111,     0,   110,    93,
   111,     0,    73,     0,   113,     0,   114,     0,    28,     0,
    29,     0,    30,     0,    96,    32,    97,     0,    96,    33,
   127,    97,     0,    96,    33,    97,     0,   117,     0,   119,
     0,    31,     0,    34,     0,   119,     0,    31,     0,    35,
     0,   117,     0,    32,     0,    32,     0,    33,     0,   121,
     0,    38,     0,    39,     0,    30,     0,   123,     0,   122,
    93,   123,     0,    77,     0,     3,     0,   127,     0,   125,
     0,   124,    93,   125,     0,   127,     0,   129,     0,    77,
     0,   128,     0,   129,     0,    96,   127,    97,     0,    49,
     0,    40,     0,    76,     0,    50,     0,    51,     0,    52,
     0,    99,     0,    41,     0,    53,     0,   127,    84,   127,
     0,   127,    85,   127,     0,   127,    87,   127,     0,   127,
    86,   127,     0,   127,    88,   127,     0,   127,    54,   127,
     0,   127,    80,   127,     0,   127,     8,   127,     0,   127,
    78,   127,     0,   127,     9,   127,     0,   127,    79,   127,
     0,   127,    10,   127,     0,   127,    55,   127,     0,   127,
    56,   127,     0,   127,    82,   127,     0,   127,    81,   127,
     0,   127,    83,   127,     0,   127,    58,   127,     0,   127,
    60,   127,     0,   127,    59,   127,     0,   127,    61,   127,
     0,   127,    62,   127,     0,   127,    63,   127,     0,   100,
   127,   101,     0,    57,   127,     0,    90,   127,     0,    89,
   127,     0,    84,   127,     0,    85,   127,     0,    41,     0,
    49,     0,    53,     0,    50,     0,    51,     0,    52,     0,
     0,     0,     0,     0,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   928,   930,   935,   940,   944,   975,   988,  1003,  1029,  1038,
  1044,  1049,  1056,  1075,  1112,  1118,  1124,  1172,  1184,  1200,
  1217,  1244,  1246,  1250,  1252,  1256,  1259,  1282,  1308,  1311,
  1314,  1317,  1329,  1332,  1335,  1338,  1341,  1344,  1347,  1350,
  1353,  1356,  1359,  1362,  1365,  1368,  1371,  1374,  1377,  1380,
  1383,  1386,  1389,  1392,  1395,  1398,  1401,  1410,  1413,  1420,
  1423,  1432,  1435,  1438,  1445,  1448,  1451,  1454,  1457,  1460,
  1463,  1466,  1469,  1472,  1481,  1488,  1496,  1505,  1508,  1511,
  1520,  1523,  1526,  1534,  1542,  1552,  1562,  1565,  1568,  1581,
  1584,  1587,  1594,  1597,  1605,  1617,  1627,  1653,  1655,  1657,
  1662,  1663,  1665,  1670,  1681,  1683,  1685,  1690,  1701,  1703,
  1706,  1711,  1716,  1722,  1727,  1733,  1740,  1742,  1745,  1750,
  1755,  1758,  1763,  1769,  1771,  1777,  1782,  1788,  1790,  1796,
  1801,  1805,  1807,  1810,  1815,  1822,  1832,  1834,  1839,  1848,
  1850,  1854,  1856,  1860,  1865,  1868,  1870,  1872,  1875,  1887,
  1890,  1893,  1900,  1903,  1906,  1909,  1912,  1915,  1918,  1921,
  1924,  1927,  1930,  1933,  1936,  1939,  1942,  1945,  1948,  1951,
  1954,  1957,  1960,  1963,  1966,  1969,  1972,  1975,  1978,  1981,
  1984,  1987,  1992,  1994,  1996,  1998,  2000,  2002,  2007,  2020,
  2024,  2028,  2032
};
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)

static const char * const yytname[] = {   "$","error","$undefined.","STRING",
"NOOPERAND","ARITHC","ADD","LOGICAL","AND","OR","XOR","BIT","CALL","INCDEC",
"DJNZ","EX","IM","PHASE","DEPHASE","IN","JP","JR","LD","OUT","PUSHPOP","RET",
"SHIFT","RST","REGNAME","ACC","C","RP","HL","INDEX","AF","SP","MISCREG","F",
"COND","SPCOND","NUMBER","UNDECLARED","END","ORG","DEFB","DEFS","DEFW","EQU",
"DEFL","LABEL","EQUATED","WASEQUATED","DEFLED","MULTDEF","MOD","SHL","SHR","NOT",
"LT","GT","EQ","LE","GE","NE","IF","ELSE","ENDIF","ARGPSEUDO","LIST","MINMAX",
"MACRO","MNAME","OLDMNAME","ARG","ENDM","MPARM","ONECHAR","TWOCHAR","'|'","'^'",
"'&'","'='","'<'","'>'","'+'","'-'","'*'","'/'","'%'","'!'","'~'","UNARY","'\\n'",
"','","':'","'.'","'('","')'","'\\''","'$'","'['","']'","statements","statement",
"colonordot","maybecolon","label.part","operation","parm.list","parm.element",
"arg.list","arg.element","reg","realreg","mem","evenreg","pushable","bcdesp",
"bcdehlsp","mar","condition","spcondition","db.list","db.list.element","dw.list",
"dw.list.element","lxexpression","expression","parenexpr","noparenexpr","symbol",
"al","arg_on","arg_off","setqf","clrqf", NULL
};
#endif

static const short yyr1[] = {     0,
   102,   102,   103,   103,   103,   103,   103,   103,   103,   103,
   103,   103,   103,   103,   103,   103,   103,   103,   103,   103,
   103,   104,   104,   105,   105,   106,   106,   106,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
   108,   108,   108,   109,   110,   110,   110,   111,   112,   112,
   113,   113,   113,   114,   114,   114,   115,   115,   116,   116,
   116,   117,   117,   118,   118,   119,   119,   120,   120,   121,
   121,   122,   122,   123,   123,   123,   124,   124,   125,   126,
   126,   127,   127,   128,   129,   129,   129,   129,   129,   129,
   129,   129,   129,   129,   129,   129,   129,   129,   129,   129,
   129,   129,   129,   129,   129,   129,   129,   129,   129,   129,
   129,   129,   129,   129,   129,   129,   129,   129,   129,   129,
   129,   129,   130,   130,   130,   130,   130,   130,   131,   132,
   133,   134,   135
};

static const short yyr2[] = {     0,
     0,     2,     2,     3,     4,     4,     6,     3,     2,     2,
     3,     4,     4,     5,     4,     2,     3,     4,     2,     5,
     1,     1,     1,     0,     1,     0,     2,     2,     1,     2,
     2,     2,     2,     4,     2,     4,     2,     2,     2,     2,
     4,     4,     4,     4,     2,     4,     2,     4,     2,     2,
     2,     2,     4,     4,     4,     4,     2,     2,     4,     4,
     4,     2,     2,     4,     4,     4,     2,     4,     2,     4,
     2,     1,     2,     4,     4,     6,     4,     6,     4,     4,
     4,     4,     4,     4,     4,     4,     7,     6,     4,     6,
     6,     4,     6,     2,     2,     1,     2,     2,     2,     1,
     0,     1,     3,     1,     0,     1,     3,     1,     1,     1,
     1,     1,     1,     3,     4,     3,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     3,     1,     1,     1,     1,     3,     1,     1,
     1,     1,     1,     3,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     3,     3,     3,     3,     3,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     2,     2,     2,
     2,     2,     1,     1,     1,     1,     1,     1,     0,     0,
     0,     0,     0
};

static const short yydefact[] = {     1,
     0,    21,   183,   184,   186,   187,   188,   185,     0,     0,
     0,   190,     0,     0,    22,    23,     2,     0,     0,    24,
   101,   146,   152,   145,   148,   149,   150,   153,     0,   147,
     0,     0,     0,     0,     0,   151,     0,     0,   142,   143,
     9,    10,   191,    16,     0,    19,   183,    27,    29,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    96,     0,     0,     0,     0,     0,     0,    72,
     0,     0,     0,     0,     0,     0,     0,   189,   100,     3,
     0,     0,     0,     0,    25,    28,   104,     0,   102,   178,
   181,   182,   180,   179,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     8,
   191,     0,    17,   111,   112,   113,     0,     0,    47,   109,
   110,    35,   112,   126,   127,    45,     0,    33,   112,    49,
    37,   112,    50,    38,   112,    51,    39,   112,    52,    40,
     0,   131,   129,   130,     0,   128,    31,   112,   122,   123,
     0,    58,    62,   117,   118,    71,     0,     0,     0,    94,
    95,     0,     0,     0,    67,     0,    30,     0,    69,     0,
     0,     0,     0,     0,     0,     0,   119,   120,    63,   121,
    73,    57,    32,    11,     0,    97,   135,   134,    98,   132,
   136,     0,    99,   137,   139,   105,     4,     0,     0,     0,
    18,     0,   144,   177,   161,   163,   165,   159,   166,   167,
   171,   173,   172,   174,   175,   176,   162,   164,   160,   169,
   168,   170,   154,   155,   157,   156,   158,     0,    15,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    12,     0,    13,     0,
   108,     0,   106,     5,     6,     0,   103,    14,    48,    36,
   125,   124,    59,   114,   116,     0,    46,    34,    60,    61,
    53,    41,    54,    42,    55,    43,    56,    44,    64,    68,
    86,   192,     0,     0,     0,    89,    66,    65,    70,    81,
     0,    80,     0,    74,     0,   142,   143,   141,    85,    82,
   142,   143,    79,    84,     0,    92,   133,   138,    20,     0,
     0,   115,     0,     0,     0,     0,     0,     0,     0,   107,
     7,   193,    88,    91,    90,    78,    76,    93,    87,     0,
     0
};

static const short yydefgoto[] = {     1,
    17,    18,    86,    19,    81,    88,    89,   272,   273,   129,
   130,   131,   163,   189,   164,   283,   165,   155,   156,   199,
   200,   203,   204,   320,    95,    39,    40,    20,   206,    43,
   122,   333,   349
};

static const short yypact[] = {-32768,
   321,-32768,   -65,-32768,-32768,-32768,-32768,-32768,  1851,   -69,
   -59,-32768,  1758,   -41,-32768,-32768,-32768,   114,   492,   -37,
    12,-32768,-32768,-32768,-32768,-32768,-32768,-32768,  1851,-32768,
  1851,  1851,  1851,  1851,  1851,-32768,  1851,   605,-32768,-32768,
-32768,-32768,    11,-32768,   640,-32768,-32768,-32768,-32768,  1060,
  1030,  1142,  1194,  1224,  1276,  1851,  1400,   119,  1851,   -22,
  1851,  1851,-32768,   143,  1371,  1452,    50,    25,   145,     1,
    -9,  1851,  1780,  1851,    85,  1851,  1851,-32768,-32768,-32768,
    -3,  1851,  1851,  1851,-32768,-32768,-32768,   -77,-32768,-32768,
-32768,-32768,-32768,-32768,   342,   212,  1851,  1851,  1851,  1851,
  1851,  1851,  1851,  1851,  1851,  1851,  1851,  1851,  1851,  1851,
  1851,  1851,  1851,  1851,  1851,  1851,  1851,  1851,  1851,-32768,
-32768,    32,-32768,-32768,    39,-32768,    51,  1706,-32768,-32768,
-32768,   913,    52,-32768,-32768,-32768,    60,   913,    66,-32768,
   913,    67,-32768,   913,    75,-32768,   913,    89,-32768,   913,
   513,-32768,-32768,-32768,    90,-32768,   913,-32768,-32768,-32768,
     9,-32768,-32768,-32768,-32768,   913,    98,    99,   158,   913,
   913,   111,   112,  1728,-32768,   113,   913,   115,   913,   116,
  1562,   117,   118,   120,  1481,   121,-32768,-32768,-32768,-32768,
-32768,-32768,   913,-32768,   696,   913,-32768,-32768,   123,-32768,
   913,   731,   124,-32768,   913,   134,-32768,   787,   822,   549,
-32768,    12,-32768,-32768,   926,   969,   382,-32768,   -50,   -50,
   102,   102,  1898,   102,   102,  1898,   969,   382,   926,  1898,
   102,   102,   -26,   -26,-32768,-32768,-32768,   126,-32768,  1306,
    87,   122,  1833,  1306,   108,  1306,  1306,  1306,  1306,    -9,
  1851,   191,   190,   128,   130,   131,   132,  1851,  1851,   199,
   133,  1112,  1652,   166,   135,   202,-32768,    85,-32768,  1851,
-32768,   -29,-32768,-32768,-32768,  1851,-32768,-32768,-32768,   913,
-32768,-32768,-32768,-32768,-32768,   398,-32768,   913,-32768,-32768,
-32768,   913,-32768,   913,-32768,   913,-32768,   913,-32768,   913,
-32768,-32768,   141,   205,  1533,-32768,-32768,   913,   913,-32768,
   150,-32768,  1623,-32768,   913,   146,   153,-32768,-32768,-32768,
   154,   155,-32768,-32768,   156,-32768,-32768,-32768,-32768,   134,
   878,-32768,   152,    95,   151,   157,   224,   160,    43,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   258,
-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,-32768,-32768,-32768,-32768,    47,-32768,   -70,    62,
   -63,-32768,   -64,-32768,  -228,-32768,   -51,   -14,   196,-32768,
    -4,-32768,    -5,-32768,    -7,   -60,  -132,   259,-32768,-32768,
   159,-32768,-32768
};


#define	YYLAST		1986


static const short yytable[] = {   137,
   173,    38,   183,   100,    21,    45,   184,   186,   167,    82,
    83,   168,   282,   175,   211,   212,   289,   190,   124,   158,
   126,    90,    41,    91,    92,    93,    94,   100,    46,    96,
   152,    84,    42,   115,   116,   117,   118,   119,   153,   154,
   242,   243,   132,   138,   141,   144,   147,   150,   151,   157,
   176,   166,    87,   170,   171,   191,    85,   177,   179,   117,
   118,   119,   329,   330,   193,   195,   196,   201,   202,   205,
   124,   158,   126,   169,   208,   209,   210,   124,   158,   126,
   159,   134,   135,   121,   160,   180,   161,   197,   207,   215,
   216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
   226,   227,   228,   229,   230,   231,   232,   233,   234,   235,
   236,   237,   136,   140,   143,   146,   149,   159,   281,   162,
   185,   160,   257,   239,    22,    23,   134,   135,   182,   317,
   322,   240,   192,    24,    25,    26,    27,    28,   159,   134,
   135,    29,   160,   241,   244,   181,   124,   158,   126,   159,
   134,   135,   245,   160,    47,   100,   101,   102,   246,   247,
    30,   198,     4,     5,     6,     7,     8,   248,    31,    32,
   124,   158,   126,    33,    34,   187,   134,   135,   188,   172,
    35,   249,   251,    36,    37,   115,   116,   117,   118,   119,
   252,   253,   254,   290,   323,   306,   159,   134,   135,   324,
   160,   316,   321,   255,   256,   258,   271,   259,   260,   262,
   263,   319,   264,   266,   161,   268,   270,   278,   284,    97,
    98,    99,   301,   302,   303,   304,   305,   310,   307,   311,
   326,   325,   280,   334,   335,   286,   288,   -77,   292,   294,
   296,   298,   337,   300,   -75,   -83,  -140,   344,   339,   342,
   308,   309,   346,   345,   315,   315,   347,   351,   277,   340,
   201,   178,   205,   327,   328,   100,   101,   102,   331,   103,
   104,   105,   106,   107,   108,   348,    48,     0,     0,   238,
     0,     0,   343,     0,     0,     0,     0,     0,     0,   109,
   110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     0,   279,     0,     0,     0,   287,     0,   291,   293,   295,
   297,   299,   214,     0,     0,     0,     0,     0,     0,     0,
   350,     2,     0,   314,   -26,   -26,   -26,   -26,   -26,   -26,
   -26,   -26,   -26,   -26,   -26,   -26,   -26,   -26,   -26,   -26,
   -26,   -26,   -26,   -26,   -26,   -26,   -26,   -26,     0,    97,
    98,    99,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     3,   -26,   -26,   -26,   -26,   -26,     0,     0,     4,
     5,     6,     7,     8,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     9,    10,    11,    12,    13,    97,
     0,   -26,    14,     0,   -26,   100,   101,   102,     0,   103,
   104,   105,   106,   107,   108,    97,    98,    99,     0,     0,
     0,     0,   -26,     0,    15,    16,     0,     0,     0,   109,
   110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     0,     0,     0,     0,     0,   100,   101,   102,   213,   103,
   104,   105,   106,   107,   108,     0,     0,     0,     0,     0,
     0,   100,   101,   102,     0,   103,   104,   105,   106,   107,
   108,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     0,     0,     0,     0,     0,   109,   110,   111,   112,   113,
   114,   115,   116,   117,   118,   119,     0,     0,     0,     0,
     0,     0,     0,     0,   332,    49,    50,    51,    52,    53,
    54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
    64,    65,    66,    67,    68,    69,    70,    71,    72,     0,
    97,    98,    99,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    73,    74,    75,    76,    77,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    97,    98,    99,     0,
     0,     0,    78,     0,     0,    79,   100,   101,   102,     0,
   103,   104,   105,   106,   107,   108,     0,     0,     0,     0,
     0,     0,     0,    80,     0,     0,     0,     0,     0,     0,
   109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
   119,     0,   100,   101,   102,   250,   103,   104,   105,   106,
   107,   108,    97,    98,    99,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,   109,   110,   111,   112,
   113,   114,   115,   116,   117,   118,   119,     0,     0,     0,
     0,   276,     0,     0,     0,     0,     0,    97,    98,    99,
     0,     0,     0,     0,     0,     0,     0,     0,   100,   101,
   102,     0,   103,   104,   105,   106,   107,   108,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,   109,   110,   111,   112,   113,   114,   115,   116,
   117,   118,   119,   100,   101,   102,   120,   103,   104,   105,
   106,   107,   108,    97,    98,    99,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   109,   110,   111,
   112,   113,   114,   115,   116,   117,   118,   119,     0,     0,
     0,   123,     0,     0,     0,     0,     0,     0,    97,    98,
    99,     0,     0,     0,     0,     0,     0,     0,     0,   100,
   101,   102,     0,   103,   104,   105,   106,   107,   108,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,   109,   110,   111,   112,   113,   114,   115,
   116,   117,   118,   119,   100,   101,   102,   267,   103,   104,
   105,   106,   107,   108,    97,    98,    99,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,   109,   110,
   111,   112,   113,   114,   115,   116,   117,   118,   119,     0,
     0,     0,   269,     0,     0,     0,     0,     0,     0,    97,
    98,    99,     0,     0,     0,     0,     0,     0,     0,     0,
   100,   101,   102,     0,   103,   104,   105,   106,   107,   108,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,   109,   110,   111,   112,   113,   114,
   115,   116,   117,   118,   119,   100,   101,   102,   274,   103,
   104,   105,   106,   107,   108,    97,    98,    99,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,   109,
   110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     0,     0,     0,   275,     0,     0,     0,     0,     0,     0,
    97,    98,    99,     0,     0,     0,     0,     0,     0,     0,
     0,   100,   101,   102,     0,   103,   104,   105,   106,   107,
   108,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,   109,   110,   111,   112,   113,
   114,   115,   116,   117,   118,   119,   100,   101,   102,   341,
   103,   104,   105,   106,   107,   108,    97,     0,    99,   100,
   101,   102,     0,   103,   104,   105,   106,   107,   108,     0,
   109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
   119,     0,     0,     0,     0,     0,   112,   113,   114,   115,
   116,   117,   118,   119,     0,     0,     0,     0,     0,     0,
     0,     0,   100,   101,   102,     0,   103,   104,   105,   106,
   107,   108,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   110,   111,   112,
   113,   114,   115,   116,   117,   118,   119,   124,   133,   126,
     0,   134,   135,     0,     0,     0,     0,     0,     0,    22,
    23,     0,     0,     0,     0,     0,     0,     0,    24,    25,
    26,    27,    28,     0,     0,     0,    29,   124,   125,   126,
     0,   127,     0,     0,     0,     0,     0,     0,     0,    22,
    23,     0,     0,     0,     0,    30,     0,     0,    24,    25,
    26,    27,    28,    31,    32,     0,    29,     0,    33,    34,
     0,     0,     0,     0,     0,   128,     0,     0,    36,    37,
     0,     0,     0,     0,     0,    30,     0,     0,     0,   124,
   158,   126,     0,    31,    32,     0,     0,   312,    33,    34,
     0,    22,    23,     0,     0,   128,     0,     0,    36,    37,
    24,    25,    26,    27,    28,     0,     0,     0,    29,   124,
   139,   126,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    22,    23,     0,     0,     0,     0,    30,     0,     0,
    24,    25,    26,    27,    28,    31,    32,     0,    29,     0,
    33,    34,     0,     0,     0,     0,     0,   313,     0,     0,
    36,    37,     0,     0,     0,     0,     0,    30,     0,     0,
     0,   124,   142,   126,     0,    31,    32,     0,     0,     0,
    33,    34,     0,    22,    23,     0,     0,   128,     0,     0,
    36,    37,    24,    25,    26,    27,    28,     0,     0,     0,
    29,   124,   145,   126,     0,     0,     0,     0,     0,     0,
     0,     0,     0,    22,    23,     0,     0,     0,     0,    30,
     0,     0,    24,    25,    26,    27,    28,    31,    32,     0,
    29,     0,    33,    34,     0,     0,     0,     0,     0,   128,
     0,     0,    36,    37,     0,     0,     0,     0,     0,    30,
     0,     0,     0,   124,   148,   126,     0,    31,    32,     0,
     0,     0,    33,    34,     0,    22,    23,     0,     0,   128,
     0,     0,    36,    37,    24,    25,    26,    27,    28,     0,
     0,     0,    29,   124,   158,   126,     0,     0,     0,     0,
     0,     0,     0,     0,     0,    22,    23,     0,     0,     0,
     0,    30,     0,     0,    24,    25,    26,    27,    28,    31,
    32,     0,    29,     0,    33,    34,     0,     0,     0,     0,
     0,   128,     0,     0,    36,    37,     0,     0,     0,     0,
     0,    30,     0,     0,     0,     0,     0,     0,     0,    31,
    32,     0,     0,     0,    33,    34,     0,     0,     0,     0,
   152,   128,   134,   135,    36,    37,     0,     0,   153,   154,
    22,    23,     0,     0,     0,     0,     0,     0,     0,    24,
    25,    26,    27,    28,     0,     0,     0,    29,     0,   152,
     0,     0,     0,     0,     0,     0,     0,   153,   154,    22,
    23,     0,     0,     0,     0,     0,    30,     0,    24,    25,
    26,    27,    28,     0,    31,    32,    29,     0,     0,    33,
    34,     0,     0,     0,     0,     0,   174,     0,     0,    36,
    37,     0,     0,     0,     0,    30,     0,     0,     0,     0,
     0,   152,     0,    31,    32,     0,     0,     0,    33,    34,
   154,    22,    23,     0,     0,    35,     0,     0,    36,    37,
    24,    25,    26,    27,    28,     0,     0,     0,    29,     0,
   265,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    22,    23,     0,     0,     0,     0,     0,    30,     0,    24,
    25,    26,    27,    28,     0,    31,    32,    29,     0,     0,
    33,    34,     0,     0,     0,     0,     0,    35,     0,     0,
    36,    37,     0,     0,     0,     0,    30,     0,     0,     0,
     0,     0,   336,     0,    31,    32,     0,     0,     0,    33,
    34,     0,    22,    23,     0,     0,    35,     0,     0,    36,
    37,    24,    25,    26,    27,    28,     0,     0,     0,    29,
     0,     0,   261,   242,   243,     0,     0,     0,     0,     0,
     0,    22,    23,     0,     0,     0,     0,     0,    30,     0,
    24,    25,    26,    27,    28,     0,    31,    32,    29,     0,
     0,    33,    34,     0,     0,     0,     0,     0,    35,     0,
     0,    36,    37,     0,     0,     0,     0,    30,     0,     0,
     0,     0,     0,     0,     0,    31,    32,     0,     0,     0,
    33,    34,     0,   338,   242,   243,     0,    35,     0,     0,
    36,    37,    22,    23,     0,     0,     0,     0,     0,     0,
     0,    24,    25,    26,    27,    28,     0,     0,     0,    29,
     0,     0,     0,   134,   135,     0,     0,     0,     0,     0,
     0,    22,    23,     0,     0,     0,     0,     0,    30,     0,
    24,    25,    26,    27,    28,     0,    31,    32,    29,     0,
     0,    33,    34,     0,     0,     0,     0,     0,    35,     0,
     0,    36,    37,     0,     0,     0,     0,    30,   318,     0,
     0,     0,     0,     0,     0,    31,    32,   242,   243,     0,
    33,    34,     0,     0,     0,    22,    23,    35,     0,     0,
    36,    37,     0,     0,    24,    25,    26,    27,    28,   134,
   135,     0,    29,     0,     0,     0,     0,    22,    23,     0,
     0,     0,     0,     0,     0,     0,    24,    25,    26,    27,
    28,    30,     0,     0,    29,     0,     0,     0,     0,    31,
    32,     0,     0,     0,    33,    34,     0,    22,    23,     0,
     0,    35,     0,    30,    36,    37,    24,    25,    26,    27,
    28,    31,    32,     0,    29,     0,    33,    34,     0,    22,
    23,     0,     0,    35,     0,     0,    36,    37,    24,    25,
    26,    27,    28,    30,     0,     0,    29,     0,     0,     0,
     0,    31,    32,     0,     0,     0,    33,    34,     0,    44,
     0,     0,     0,    35,     0,    30,    36,    37,     0,     0,
     0,     0,     0,    31,    32,     0,     0,     0,    33,    34,
     0,   194,    22,    23,     0,    35,     0,     0,    36,    37,
     0,    24,    25,    26,    27,    28,     0,     0,     0,    29,
    22,    23,     0,     0,     0,     0,     0,     0,     0,    24,
    25,    26,    27,    28,     0,     0,     0,    29,    30,     0,
     0,     0,     0,     0,     0,     0,    31,    32,     0,     0,
     0,    33,    34,     0,     0,     0,    30,     0,    35,   285,
     0,    36,    37,     0,    31,    32,     0,     0,     0,    33,
    34,     0,     0,     0,     0,     0,    35,     0,     0,    36,
    37,   100,   101,   102,     0,   103,   104,     0,   106,   107,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,   113,
   114,   115,   116,   117,   118,   119
};

static const short yycheck[] = {    51,
    64,     9,    67,    54,    70,    13,    67,    68,    31,    47,
    48,    34,   241,    65,    92,    93,   245,    69,    28,    29,
    30,    29,    92,    31,    32,    33,    34,    54,    70,    37,
    30,    69,    92,    84,    85,    86,    87,    88,    38,    39,
    32,    33,    50,    51,    52,    53,    54,    55,    56,    57,
    65,    59,    41,    61,    62,    70,    94,    65,    66,    86,
    87,    88,    92,    93,    72,    73,    74,    75,    76,    77,
    28,    29,    30,    96,    82,    83,    84,    28,    29,    30,
    31,    32,    33,    73,    35,    36,    96,     3,    92,    97,
    98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
   108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
   118,   119,    51,    52,    53,    54,    55,    31,    32,    58,
    96,    35,   174,    92,    40,    41,    32,    33,    67,   262,
   263,    93,    71,    49,    50,    51,    52,    53,    31,    32,
    33,    57,    35,    93,    93,    96,    28,    29,    30,    31,
    32,    33,    93,    35,    41,    54,    55,    56,    93,    93,
    76,    77,    49,    50,    51,    52,    53,    93,    84,    85,
    28,    29,    30,    89,    90,    31,    32,    33,    34,    37,
    96,    93,    93,    99,   100,    84,    85,    86,    87,    88,
    93,    93,    35,   245,    29,   256,    31,    32,    33,   264,
    35,   262,   263,    93,    93,    93,    73,    93,    93,    93,
    93,   263,    93,    93,    96,    93,    93,    92,    97,     8,
     9,    10,    32,    34,    97,    96,    96,    29,    97,    97,
    29,    97,   240,    93,    30,   243,   244,    92,   246,   247,
   248,   249,    93,   251,    92,    92,    92,    97,    93,    98,
   258,   259,    29,    97,   262,   263,    97,     0,   212,   330,
   268,    66,   270,   268,   270,    54,    55,    56,   276,    58,
    59,    60,    61,    62,    63,   339,    18,    -1,    -1,   121,
    -1,    -1,   334,    -1,    -1,    -1,    -1,    -1,    -1,    78,
    79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
    -1,   240,    -1,    -1,    -1,   244,    -1,   246,   247,   248,
   249,   250,   101,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     0,     1,    -1,   262,     4,     5,     6,     7,     8,     9,
    10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
    20,    21,    22,    23,    24,    25,    26,    27,    -1,     8,
     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    41,    42,    43,    44,    45,    46,    -1,    -1,    49,
    50,    51,    52,    53,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    64,    65,    66,    67,    68,     8,
    -1,    71,    72,    -1,    74,    54,    55,    56,    -1,    58,
    59,    60,    61,    62,    63,     8,     9,    10,    -1,    -1,
    -1,    -1,    92,    -1,    94,    95,    -1,    -1,    -1,    78,
    79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
    -1,    -1,    -1,    -1,    -1,    54,    55,    56,    97,    58,
    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
    -1,    54,    55,    56,    -1,    58,    59,    60,    61,    62,
    63,    80,    81,    82,    83,    84,    85,    86,    87,    88,
    -1,    -1,    -1,    -1,    -1,    78,    79,    80,    81,    82,
    83,    84,    85,    86,    87,    88,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    97,     4,     5,     6,     7,     8,
     9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
    19,    20,    21,    22,    23,    24,    25,    26,    27,    -1,
     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    42,    43,    44,    45,    46,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,     8,     9,    10,    -1,
    -1,    -1,    71,    -1,    -1,    74,    54,    55,    56,    -1,
    58,    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    92,    -1,    -1,    -1,    -1,    -1,    -1,
    78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
    88,    -1,    54,    55,    56,    93,    58,    59,    60,    61,
    62,    63,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    78,    79,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    -1,    -1,    -1,
    -1,    93,    -1,    -1,    -1,    -1,    -1,     8,     9,    10,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    54,    55,
    56,    -1,    58,    59,    60,    61,    62,    63,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    78,    79,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    54,    55,    56,    92,    58,    59,    60,
    61,    62,    63,     8,     9,    10,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    78,    79,    80,
    81,    82,    83,    84,    85,    86,    87,    88,    -1,    -1,
    -1,    92,    -1,    -1,    -1,    -1,    -1,    -1,     8,     9,
    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    54,
    55,    56,    -1,    58,    59,    60,    61,    62,    63,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    78,    79,    80,    81,    82,    83,    84,
    85,    86,    87,    88,    54,    55,    56,    92,    58,    59,
    60,    61,    62,    63,     8,     9,    10,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    78,    79,
    80,    81,    82,    83,    84,    85,    86,    87,    88,    -1,
    -1,    -1,    92,    -1,    -1,    -1,    -1,    -1,    -1,     8,
     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    54,    55,    56,    -1,    58,    59,    60,    61,    62,    63,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    78,    79,    80,    81,    82,    83,
    84,    85,    86,    87,    88,    54,    55,    56,    92,    58,
    59,    60,    61,    62,    63,     8,     9,    10,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    78,
    79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
    -1,    -1,    -1,    92,    -1,    -1,    -1,    -1,    -1,    -1,
     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    54,    55,    56,    -1,    58,    59,    60,    61,    62,
    63,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    78,    79,    80,    81,    82,
    83,    84,    85,    86,    87,    88,    54,    55,    56,    92,
    58,    59,    60,    61,    62,    63,     8,    -1,    10,    54,
    55,    56,    -1,    58,    59,    60,    61,    62,    63,    -1,
    78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
    88,    -1,    -1,    -1,    -1,    -1,    81,    82,    83,    84,
    85,    86,    87,    88,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    54,    55,    56,    -1,    58,    59,    60,    61,
    62,    63,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    79,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    28,    29,    30,
    -1,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    40,
    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    49,    50,
    51,    52,    53,    -1,    -1,    -1,    57,    28,    29,    30,
    -1,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,
    41,    -1,    -1,    -1,    -1,    76,    -1,    -1,    49,    50,
    51,    52,    53,    84,    85,    -1,    57,    -1,    89,    90,
    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    99,   100,
    -1,    -1,    -1,    -1,    -1,    76,    -1,    -1,    -1,    28,
    29,    30,    -1,    84,    85,    -1,    -1,    36,    89,    90,
    -1,    40,    41,    -1,    -1,    96,    -1,    -1,    99,   100,
    49,    50,    51,    52,    53,    -1,    -1,    -1,    57,    28,
    29,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    40,    41,    -1,    -1,    -1,    -1,    76,    -1,    -1,
    49,    50,    51,    52,    53,    84,    85,    -1,    57,    -1,
    89,    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
    99,   100,    -1,    -1,    -1,    -1,    -1,    76,    -1,    -1,
    -1,    28,    29,    30,    -1,    84,    85,    -1,    -1,    -1,
    89,    90,    -1,    40,    41,    -1,    -1,    96,    -1,    -1,
    99,   100,    49,    50,    51,    52,    53,    -1,    -1,    -1,
    57,    28,    29,    30,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    76,
    -1,    -1,    49,    50,    51,    52,    53,    84,    85,    -1,
    57,    -1,    89,    90,    -1,    -1,    -1,    -1,    -1,    96,
    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,    -1,    76,
    -1,    -1,    -1,    28,    29,    30,    -1,    84,    85,    -1,
    -1,    -1,    89,    90,    -1,    40,    41,    -1,    -1,    96,
    -1,    -1,    99,   100,    49,    50,    51,    52,    53,    -1,
    -1,    -1,    57,    28,    29,    30,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,
    -1,    76,    -1,    -1,    49,    50,    51,    52,    53,    84,
    85,    -1,    57,    -1,    89,    90,    -1,    -1,    -1,    -1,
    -1,    96,    -1,    -1,    99,   100,    -1,    -1,    -1,    -1,
    -1,    76,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    84,
    85,    -1,    -1,    -1,    89,    90,    -1,    -1,    -1,    -1,
    30,    96,    32,    33,    99,   100,    -1,    -1,    38,    39,
    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    49,
    50,    51,    52,    53,    -1,    -1,    -1,    57,    -1,    30,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    39,    40,
    41,    -1,    -1,    -1,    -1,    -1,    76,    -1,    49,    50,
    51,    52,    53,    -1,    84,    85,    57,    -1,    -1,    89,
    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    99,
   100,    -1,    -1,    -1,    -1,    76,    -1,    -1,    -1,    -1,
    -1,    30,    -1,    84,    85,    -1,    -1,    -1,    89,    90,
    39,    40,    41,    -1,    -1,    96,    -1,    -1,    99,   100,
    49,    50,    51,    52,    53,    -1,    -1,    -1,    57,    -1,
    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    40,    41,    -1,    -1,    -1,    -1,    -1,    76,    -1,    49,
    50,    51,    52,    53,    -1,    84,    85,    57,    -1,    -1,
    89,    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
    99,   100,    -1,    -1,    -1,    -1,    76,    -1,    -1,    -1,
    -1,    -1,    30,    -1,    84,    85,    -1,    -1,    -1,    89,
    90,    -1,    40,    41,    -1,    -1,    96,    -1,    -1,    99,
   100,    49,    50,    51,    52,    53,    -1,    -1,    -1,    57,
    -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    76,    -1,
    49,    50,    51,    52,    53,    -1,    84,    85,    57,    -1,
    -1,    89,    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,
    -1,    99,   100,    -1,    -1,    -1,    -1,    76,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,    -1,    -1,
    89,    90,    -1,    31,    32,    33,    -1,    96,    -1,    -1,
    99,   100,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    49,    50,    51,    52,    53,    -1,    -1,    -1,    57,
    -1,    -1,    -1,    32,    33,    -1,    -1,    -1,    -1,    -1,
    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    76,    -1,
    49,    50,    51,    52,    53,    -1,    84,    85,    57,    -1,
    -1,    89,    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,
    -1,    99,   100,    -1,    -1,    -1,    -1,    76,    77,    -1,
    -1,    -1,    -1,    -1,    -1,    84,    85,    32,    33,    -1,
    89,    90,    -1,    -1,    -1,    40,    41,    96,    -1,    -1,
    99,   100,    -1,    -1,    49,    50,    51,    52,    53,    32,
    33,    -1,    57,    -1,    -1,    -1,    -1,    40,    41,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    49,    50,    51,    52,
    53,    76,    -1,    -1,    57,    -1,    -1,    -1,    -1,    84,
    85,    -1,    -1,    -1,    89,    90,    -1,    40,    41,    -1,
    -1,    96,    -1,    76,    99,   100,    49,    50,    51,    52,
    53,    84,    85,    -1,    57,    -1,    89,    90,    -1,    40,
    41,    -1,    -1,    96,    -1,    -1,    99,   100,    49,    50,
    51,    52,    53,    76,    -1,    -1,    57,    -1,    -1,    -1,
    -1,    84,    85,    -1,    -1,    -1,    89,    90,    -1,    92,
    -1,    -1,    -1,    96,    -1,    76,    99,   100,    -1,    -1,
    -1,    -1,    -1,    84,    85,    -1,    -1,    -1,    89,    90,
    -1,    92,    40,    41,    -1,    96,    -1,    -1,    99,   100,
    -1,    49,    50,    51,    52,    53,    -1,    -1,    -1,    57,
    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    49,
    50,    51,    52,    53,    -1,    -1,    -1,    57,    76,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,    -1,
    -1,    89,    90,    -1,    -1,    -1,    76,    -1,    96,    97,
    -1,    99,   100,    -1,    84,    85,    -1,    -1,    -1,    89,
    90,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    99,
   100,    54,    55,    56,    -1,    58,    59,    -1,    61,    62,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    82,
    83,    84,    85,    86,    87,    88
};
/* -*-C-*-  Note some compilers choke on comments on '#line' lines.  */
#line 3 "/usr/local/share/bison.simple"
/* This file comes from bison-1.28.  */

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

#ifndef YYSTACK_USE_ALLOCA
#ifdef alloca
#define YYSTACK_USE_ALLOCA
#else /* alloca not defined */
#ifdef __GNUC__
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi) || (defined (__sun) && defined (__i386))
#define YYSTACK_USE_ALLOCA
#include <alloca.h>
#else /* not sparc */
/* We think this test detects Watcom and Microsoft C.  */
/* This used to test MSDOS, but that is a bad idea
   since that symbol is in the user namespace.  */
#if (defined (_MSDOS) || defined (_MSDOS_)) && !defined (__TURBOC__)
#if 0 /* No need for malloc.h, which pollutes the namespace;
	 instead, just don't use alloca.  */
#include <malloc.h>
#endif
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
/* I don't know what this was needed for, but it pollutes the namespace.
   So I turned it off.   rms, 2 May 1997.  */
/* #include <malloc.h>  */
 #pragma alloca
#define YYSTACK_USE_ALLOCA
#else /* not MSDOS, or __TURBOC__, or _AIX */
#if 0
#ifdef __hpux /* haible@ilog.fr says this works for HPUX 9.05 and up,
		 and on HPUX 10.  Eventually we can turn this on.  */
#define YYSTACK_USE_ALLOCA
#define alloca __builtin_alloca
#endif /* __hpux */
#endif
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc */
#endif /* not GNU C */
#endif /* alloca not defined */
#endif /* YYSTACK_USE_ALLOCA not defined */

#ifdef YYSTACK_USE_ALLOCA
#define YYSTACK_ALLOC alloca
#else
#define YYSTACK_ALLOC malloc
#endif

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Define __yy_memcpy.  Note that the size argument
   should be passed with type unsigned int, because that is what the non-GCC
   definitions require.  With GCC, __builtin_memcpy takes an arg
   of type size_t, but it can handle unsigned int.  */

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (to, from, count)
     char *to;
     char *from;
     unsigned int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *to, char *from, unsigned int count)
{
  register char *t = to;
  register char *f = from;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 217 "/usr/local/share/bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#ifdef __cplusplus
#define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#else /* not __cplusplus */
#define YYPARSE_PARAM_ARG YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YYPARSE_PARAM */
#define YYPARSE_PARAM_ARG
#define YYPARSE_PARAM_DECL
#endif /* not YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
#ifdef YYPARSE_PARAM
int yyparse (void *);
#else
int yyparse (void);
#endif
#endif

int
yyparse(YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;
  int yyfree_stacks = 0;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stdout, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  if (yyfree_stacks)
	    {
	      free (yyss);
	      free (yyvs);
#ifdef YYLSP_NEEDED
	      free (yyls);
#endif
	    }
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
#ifndef YYSTACK_USE_ALLOCA
      yyfree_stacks = 1;
#endif
      yyss = (short *) YYSTACK_ALLOC (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss, (char *)yyss1,
		   size * (unsigned int) sizeof (*yyssp));
      yyvs = (YYSTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs, (char *)yyvs1,
		   size * (unsigned int) sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) YYSTACK_ALLOC (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls, (char *)yyls1,
		   size * (unsigned int) sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stdout, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stdout, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stdout, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stdout, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stdout, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stdout, yychar, yylval);
#endif
	  fprintf (stdout, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stdout, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stdout, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stdout, "%s ", yytname[yyrhs[i]]);
      fprintf (stdout, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 3:
#line 936 "zmac.y"
{
		if (yyvsp[-1].itemptr) list(dollarsign);
		else  list1();
	;
    break;}
case 4:
#line 941 "zmac.y"
{
		list(dollarsign);
	;
    break;}
case 5:
#line 945 "zmac.y"
{
		/* a forward reference to a label in the expression cannot
		 * be fixed in a forward reference to the EQU;
		 * it would need three passes. -rjm
		 */
		if(!pass2 && equ_bad_label) {
			/* this indicates that the equ has an incorrect
			 * (i.e. pass 1) value.
			 */
			yyvsp[-3].itemptr->i_equbad = 1;
		} else {
			/* but if 2nd pass or no forward reference, it's ok. */
			yyvsp[-3].itemptr->i_equbad = 0;
		}
		equ_bad_label=0;
		switch(yyvsp[-3].itemptr->i_token) {
		case UNDECLARED: case WASEQUATED:
			yyvsp[-3].itemptr->i_token = EQUATED;
			yyvsp[-3].itemptr->i_value = yyvsp[-1].ival;
			break;
		case EQUATED:
			if (yyvsp[-3].itemptr->i_value == yyvsp[-1].ival)
				break; /* Allow benign redefinition -mgr */
			/* Drop-through intentional */
		default:
			err[mflag]++;
			yyvsp[-3].itemptr->i_token = MULTDEF;
		}
		list(yyvsp[-1].ival);
	;
    break;}
case 6:
#line 976 "zmac.y"
{
		switch(yyvsp[-3].itemptr->i_token) {
		case UNDECLARED: case DEFLED:
			yyvsp[-3].itemptr->i_token = DEFLED;
			yyvsp[-3].itemptr->i_value = yyvsp[-1].ival;
			break;
		default:
			err[mflag]++;
			yyvsp[-3].itemptr->i_token = MULTDEF;
		}
		list(yyvsp[-1].ival);
	;
    break;}
case 7:
#line 989 "zmac.y"
{
		switch (yyvsp[-5].itemptr->i_token) {
		case UNDECLARED: case DEFLED:
			yyvsp[-5].itemptr->i_token = DEFLED;
			if (yyvsp[-4].itemptr->i_value)	/* max */
				list(yyvsp[-5].itemptr->i_value = (yyvsp[-3].ival > yyvsp[-1].ival? yyvsp[-3].ival:yyvsp[-1].ival));
			else list(yyvsp[-5].itemptr->i_value = (yyvsp[-3].ival < yyvsp[-1].ival? yyvsp[-3].ival:yyvsp[-1].ival));
			break;
		default:
			err[mflag]++;
			yyvsp[-5].itemptr->i_token = MULTDEF;
			list(yyvsp[-5].itemptr->i_value);
		}
	;
    break;}
case 8:
#line 1004 "zmac.y"
{
		/* all $2's here were yypc[2].ival before.
		 * I think the idea was perhaps to allow constants
		 * only...? Anyway, it now allows any expression -
		 * which would seem to make sense given the definition
		 * above, right? :-)  -rjm
		 */
		if (ifptr >= ifstmax)
			error("Too many ifs");
		else {
			if (pass2) {
				*++ifptr = *expifp++;
				if (*ifptr != !(yyvsp[-1].ival)) err[pflag]++;
			} else {
				if (expifp >= expifmax)
					error("Too many ifs!");
				*expifp++ = !(yyvsp[-1].ival);
				*++ifptr = !(yyvsp[-1].ival);
			}
		}
		saveopt = fopt;
		fopt = 1;
		list(yyvsp[-1].ival);
		fopt = saveopt;
	;
    break;}
case 9:
#line 1030 "zmac.y"
{
		/* FIXME: it would be nice to spot repeated ELSEs, but how? */
		*ifptr = !*ifptr;
		saveopt = fopt;
		fopt = 1;
		list1();
		fopt = saveopt;
	;
    break;}
case 10:
#line 1039 "zmac.y"
{
		if (ifptr == ifstack) err[bflag]++;
		else --ifptr;
		list1();
	;
    break;}
case 11:
#line 1045 "zmac.y"
{
		list(dollarsign);
		peekc = 0;
	;
    break;}
case 12:
#line 1050 "zmac.y"
{
		xeq_flag++;
		xeq = yyvsp[-1].ival;
		list(yyvsp[-1].ival);
		peekc = 0;
	;
    break;}
case 13:
#line 1057 "zmac.y"
{
		if (yyvsp[-1].ival < 0) err[vflag]++;
		list(dollarsign);
		if (yyvsp[-1].ival) {
			flushbin();
			dollarsign += yyvsp[-1].ival;
			olddollar = dollarsign;

			/* if it's not hex output though, we also need
			 * to output zeroes as appropriate. -rjm
			 */
			if(!output_hex && pass2) {
				int f;
				for (f=0;f<(yyvsp[-1].ival);f++)
					fputc(0, fbuf);
			}
		}
	;
    break;}
case 14:
#line 1076 "zmac.y"
{
		list1();
		switch (yyvsp[-4].itemptr->i_value) {

		case 0:		/* title */
			lineptr = linebuf;
			cp = tempbuf;
			title = titlespace;
			while ((*title++ = *cp++) && (title < &titlespace[TITLELEN]));
			*title = 0;
			title = titlespace;
			break;

		case 1:		/* rsym */
			if (pass2) break;
			insymtab(tempbuf);
			break;

		case 2:		/* wsym */
			strcpy(writesyms, tempbuf);
			break;

		case 3:		/* include file */
			if (*tempbuf == '"' || *tempbuf == '\'')
			{
				if (tempbuf[strlen (tempbuf) - 1] == '"' || tempbuf[strlen (tempbuf) - 1] == '\'')
					tempbuf[strlen (tempbuf) - 1] = 0;
				next_source(tempbuf + 1);
			}
			else
			{
				next_source(tempbuf);
			}
			break;

                case 4:         /* incbin file */
                        if (*tempbuf == '"' || *tempbuf == '\'')
                        {
                          if(tempbuf[strlen (tempbuf) - 1] == '"' || tempbuf[strlen (tempbuf) - 1] == '\'')
                               tempbuf[strlen (tempbuf) - 1] = 0;
                          doincbin(tempbuf + 1);
                        }
                        else
                           doincbin(tempbuf);
                        break;
		}
	;
    break;}
case 15:
#line 1113 "zmac.y"
{
		fprintf(stdout,"ARGPSEUDO error\n");
		err[fflag]++;
		list(dollarsign);
	;
    break;}
case 16:
#line 1119 "zmac.y"
{
		list_tmp1=yyvsp[-1].itemptr->i_value;
		list_tmp2=1;
		goto dolopt;
	;
    break;}
case 17:
#line 1125 "zmac.y"
{
		list_tmp1=yyvsp[-2].itemptr->i_value;
		list_tmp2=yyvsp[-1].ival;
	dolopt:
		linecnt++;
		if (pass2) {
			lineptr = linebuf;
			switch (list_tmp1) {
			case 0:	/* list */
				if (list_tmp2 < 0) lstoff = 1;
				if (list_tmp2 > 0) lstoff = 0;
				break;

			case 1:	/* eject */
				if (list_tmp2) eject();
				break;

			case 2:	/* space */
				if ((line + list_tmp2) > 60) eject();
				else space(list_tmp2);
				break;

			case 3:	/* elist */
				eopt = edef;
				if (list_tmp2 < 0) eopt = 0;
				if (list_tmp2 > 0) eopt = 1;
				break;

			case 4:	/* fopt */
				fopt = fdef;
				if (list_tmp2 < 0) fopt = 0;
				if (list_tmp2 > 0) fopt = 1;
				break;

			case 5:	/* gopt */
				gopt = gdef;
				if (list_tmp2 < 0) gopt = 1;
				if (list_tmp2 > 0) gopt = 0;
				break;

			case 6: /* mopt */
				mopt = mdef;
				if (list_tmp2 < 0) mopt = 0;
				if (list_tmp2 > 0) mopt = 1;
			}
		}
	;
    break;}
case 18:
#line 1173 "zmac.y"
{
		yyvsp[-3].itemptr->i_token = MNAME;
		yyvsp[-3].itemptr->i_value = mfptr;
#ifdef M_DEBUG
		fprintf (stdout, "[UNDECLARED MACRO %s]\n", yyvsp[-3].itemptr->i_string);
#endif
		mfseek(mfile, (long)mfptr, 0);
		list1();
		mlex() ;
		parm_number = 0;
	;
    break;}
case 19:
#line 1185 "zmac.y"
{
		yyvsp[-1].itemptr->i_token = MNAME;
#ifdef M_DEBUG
		fprintf (stdout, "[OLDNAME MACRO %s]\n", yyvsp[-1].itemptr->i_string);
#endif
		while (yychar != ENDM && yychar) {
			while (yychar != '\n' && yychar)
				yychar = yylex();
			list1();
			yychar = yylex();
		}
		while (yychar != '\n' && yychar) yychar = yylex();
		list1();
		yychar = yylex();
	;
    break;}
case 20:
#line 1201 "zmac.y"
{
#ifdef M_DEBUG
		fprintf (stdout, "[MNAME %s]\n", yyvsp[-3].itemptr->i_string);
#endif
		yyvsp[-3].itemptr->i_uses++ ;
		arg_flag = 0;
		parm_number = 0;
		list(dollarsign);
		expptr++;
		est = est2;
		est2 = NULL;
		est[FLOC] = floc;
		est[TEMPNUM] = (char *)(long)exp_number++;
		floc = (char *)(long)(yyvsp[-3].itemptr->i_value);
		mfseek(mfile, (long)floc, 0);
	;
    break;}
case 21:
#line 1218 "zmac.y"
{
		err[fflag]++;
		quoteflag = 0;
		arg_flag = 0;
		parm_number = 0;

		if (est2)
		{
			int i;
			for (i=0; i<PARMMAX; i++) {
				if (est2[i])
#ifdef M_DEBUG
	fprintf (stdout, "[Freeing2 arg%u(%p)]\n", i, est2[i]),
#endif
						free(est2[i]);
			}
			free(est2);
			est2 = NULL;
		}

		while(yychar != '\n' && yychar != '\0') yychar = yylex();
		list(dollarsign);
		yyclearin;yyerrok;
	;
    break;}
case 26:
#line 1258 "zmac.y"
{	yyval.itemptr = NULL;	;
    break;}
case 27:
#line 1260 "zmac.y"
{
		switch(yyvsp[0].itemptr->i_token) {
		case UNDECLARED:
			if (pass2)
				err[pflag]++;
			else {
				yyvsp[0].itemptr->i_token = LABEL;
				yyvsp[0].itemptr->i_value = dollarsign;
			}
			break;
		case LABEL:
			if (!pass2) {
				yyvsp[0].itemptr->i_token = MULTDEF;
				err[mflag]++;
			} else if (yyvsp[0].itemptr->i_value != dollarsign)
				err[pflag]++;
			break;
		default:
			err[mflag]++;
			yyvsp[0].itemptr->i_token = MULTDEF;
		}
	;
    break;}
case 28:
#line 1283 "zmac.y"
{
		switch(yyvsp[-1].itemptr->i_token) {
		case UNDECLARED:
			if (pass2)
				err[pflag]++;
			else {
				yyvsp[-1].itemptr->i_token = LABEL;
				yyvsp[-1].itemptr->i_value = dollarsign;
			}
			break;
		case LABEL:
			if (!pass2) {
				yyvsp[-1].itemptr->i_token = MULTDEF;
				err[mflag]++;
			} else if (yyvsp[-1].itemptr->i_value != dollarsign)
				err[pflag]++;
			break;
		default:
			err[mflag]++;
			yyvsp[-1].itemptr->i_token = MULTDEF;
		}
	;
    break;}
case 29:
#line 1310 "zmac.y"
{ emit1(yyvsp[0].itemptr->i_value, 0, 0, 1); ;
    break;}
case 30:
#line 1313 "zmac.y"
{ emitjp(0303, yyvsp[0].ival); ;
    break;}
case 31:
#line 1316 "zmac.y"
{ emit(3, 0315, yyvsp[0].ival, yyvsp[0].ival >> 8); ;
    break;}
case 32:
#line 1319 "zmac.y"
{ int a = yyvsp[0].ival, doneerr=0;
		/* added support for normal RST form -rjm */
		if (a >= 8) {
			if ((a&7)!=0) doneerr=1,err[vflag]++;
			a >>= 3;
		}
		if ((a > 7 || a < 0) && !doneerr) /* don't give two errs... */
			err[vflag]++;
		emit(1, yyvsp[-1].itemptr->i_value + (a << 3));
	;
    break;}
case 33:
#line 1331 "zmac.y"
{ emit1(0306, 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 34:
#line 1334 "zmac.y"
{ emit1(0306, 0, yyvsp[0].ival, 3); ;
    break;}
case 35:
#line 1337 "zmac.y"
{ emit1(0306 + (yyvsp[-1].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 36:
#line 1340 "zmac.y"
{ emit1(0306 + (yyvsp[-3].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); ;
    break;}
case 37:
#line 1343 "zmac.y"
{ emit1(0306 | (yyvsp[-1].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); ;
    break;}
case 38:
#line 1346 "zmac.y"
{ emit1(0306 | (yyvsp[-1].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); ;
    break;}
case 39:
#line 1349 "zmac.y"
{ emit1(0306 | (yyvsp[-1].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); ;
    break;}
case 40:
#line 1352 "zmac.y"
{ emit1(0306 | (yyvsp[-1].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); ;
    break;}
case 41:
#line 1355 "zmac.y"
{ emit1(0306 | (yyvsp[-3].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 42:
#line 1358 "zmac.y"
{ emit1(0306 | (yyvsp[-3].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 43:
#line 1361 "zmac.y"
{ emit1(0306 | (yyvsp[-3].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 44:
#line 1364 "zmac.y"
{ emit1(0306 | (yyvsp[-3].itemptr->i_value << 3), 0, yyvsp[0].ival, 3); if (pass2) warnprt (1, 0); ;
    break;}
case 45:
#line 1367 "zmac.y"
{ emit1(0200 + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 46:
#line 1370 "zmac.y"
{ emit1(0200 + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 47:
#line 1373 "zmac.y"
{ emit1(0200 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 48:
#line 1376 "zmac.y"
{ emit1(0200 + (yyvsp[-3].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 49:
#line 1379 "zmac.y"
{ emit1(0200 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 50:
#line 1382 "zmac.y"
{ emit1(0200 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 51:
#line 1385 "zmac.y"
{ emit1(0200 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 52:
#line 1388 "zmac.y"
{ emit1(0200 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); ;
    break;}
case 53:
#line 1391 "zmac.y"
{ emit1(0200 + (yyvsp[-3].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 54:
#line 1394 "zmac.y"
{ emit1(0200 + (yyvsp[-3].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 55:
#line 1397 "zmac.y"
{ emit1(0200 + (yyvsp[-3].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 56:
#line 1400 "zmac.y"
{ emit1(0200 + (yyvsp[-3].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0); if (pass2) warnprt (1, 0); ;
    break;}
case 57:
#line 1403 "zmac.y"
{
			if (suggest_optimise && pass2 && (yyvsp[0].ival & 0377) == 7 && yyvsp[-1].itemptr->i_value <= 4)
				warnprt (yyvsp[-1].itemptr->i_value + 4, 0);
			if (pass2 && yyvsp[-1].itemptr->i_value == 6)
				warnprt (1, 0);
			emit1(0145400 + (yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0);
		;
    break;}
case 58:
#line 1412 "zmac.y"
{ emit1(yyvsp[-1].itemptr->i_value + ((yyvsp[0].ival & 0377) << 3) + 4, yyvsp[0].ival, 0, 0); ;
    break;}
case 59:
#line 1415 "zmac.y"
{ if (yyvsp[-3].itemptr->i_value == 1)
				emit(2,0355,0112+yyvsp[0].ival);
			else
				emit(2,0355,0102+yyvsp[0].ival);
		;
    break;}
case 60:
#line 1422 "zmac.y"
{ emitdad(yyvsp[-2].ival,yyvsp[0].ival); ;
    break;}
case 61:
#line 1425 "zmac.y"
{
			if (yyvsp[-2].ival != yyvsp[0].ival) {
				fprintf(stdout,"ADD mar, mar error\n");
				err[fflag]++;
			}
			emitdad(yyvsp[-2].ival,yyvsp[0].ival);
		;
    break;}
case 62:
#line 1434 "zmac.y"
{ emit1((yyvsp[-1].itemptr->i_value << 3) + (yyvsp[0].ival & 0377) + 3, yyvsp[0].ival, 0, 1); ;
    break;}
case 63:
#line 1437 "zmac.y"
{ emit1(yyvsp[-1].itemptr->i_value + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 1); ;
    break;}
case 64:
#line 1440 "zmac.y"
{
			if (yyvsp[-2].ival < 0 || yyvsp[-2].ival > 7)
				err[vflag]++;
			emit1(yyvsp[-3].itemptr->i_value + ((yyvsp[-2].ival & 7) << 3) + (yyvsp[0].ival & 0377), yyvsp[0].ival, 0, 0);
		;
    break;}
case 65:
#line 1447 "zmac.y"
{ emitjp(0302 + yyvsp[-2].ival, yyvsp[0].ival); ;
    break;}
case 66:
#line 1450 "zmac.y"
{ emit1(0351, yyvsp[-1].ival, 0, 1); ;
    break;}
case 67:
#line 1453 "zmac.y"
{ emit1(0351, yyvsp[0].ival, 0, 1); if (pass2) warnprt (1, 0); ;
    break;}
case 68:
#line 1456 "zmac.y"
{ emit(3, 0304 + yyvsp[-2].ival, yyvsp[0].ival, yyvsp[0].ival >> 8); ;
    break;}
case 69:
#line 1459 "zmac.y"
{ emitjr(030,yyvsp[0].ival); ;
    break;}
case 70:
#line 1462 "zmac.y"
{ emitjr(yyvsp[-3].itemptr->i_value + yyvsp[-2].ival, yyvsp[0].ival); ;
    break;}
case 71:
#line 1465 "zmac.y"
{ emitjr(yyvsp[-1].itemptr->i_value, yyvsp[0].ival); ;
    break;}
case 72:
#line 1468 "zmac.y"
{ emit(1, yyvsp[0].itemptr->i_value); ;
    break;}
case 73:
#line 1471 "zmac.y"
{ emit(1, 0300 + yyvsp[0].ival); ;
    break;}
case 74:
#line 1474 "zmac.y"
{
			if ((yyvsp[-2].ival & 0377) == 6 && (yyvsp[0].ival & 0377) == 6) {
				fprintf(stdout,"LD reg, reg error\n");
				err[fflag]++;
			}
			emit1(0100 + ((yyvsp[-2].ival & 7) << 3) + (yyvsp[0].ival & 7),yyvsp[-2].ival | yyvsp[0].ival, 0, 0);
		;
    break;}
case 75:
#line 1483 "zmac.y"
{
			if (suggest_optimise && pass2 && yyvsp[0].ival == 0 && (yyvsp[-2].ival & 0377) == 7)
				warnprt (3, 0);
			emit1(6 + ((yyvsp[-2].ival & 0377) << 3), yyvsp[-2].ival, yyvsp[0].ival, 2);
		;
    break;}
case 76:
#line 1490 "zmac.y"
{	if (yyvsp[-4].ival != 7) {
				fprintf(stdout,"LD reg, (RP) error\n");
				err[fflag]++;
			}
			else emit(1, 012 + yyvsp[-1].itemptr->i_value);
		;
    break;}
case 77:
#line 1498 "zmac.y"
{
			if (yyvsp[-2].ival != 7) {
				fprintf(stdout,"LD reg, (expr) error\n");
				err[fflag]++;
			}
			else emit(3, 072, yyvsp[0].ival, yyvsp[0].ival >> 8);
		;
    break;}
case 78:
#line 1507 "zmac.y"
{ emit(1, 2 + yyvsp[-3].itemptr->i_value); ;
    break;}
case 79:
#line 1510 "zmac.y"
{ emit(3, 062, yyvsp[-2].ival, yyvsp[-2].ival >> 8); ;
    break;}
case 80:
#line 1513 "zmac.y"
{
			if (yyvsp[-2].ival != 7) {
				fprintf(stdout,"LD reg, MISCREG error\n");
				err[fflag]++;
			}
			else emit(2, 0355, 0127 + yyvsp[0].itemptr->i_value);
		;
    break;}
case 81:
#line 1522 "zmac.y"
{ emit(2, 0355, 0107 + yyvsp[-2].itemptr->i_value); ;
    break;}
case 82:
#line 1525 "zmac.y"
{ emit1(1 + (yyvsp[-2].ival & 060), yyvsp[-2].ival, yyvsp[0].ival, 5); ;
    break;}
case 83:
#line 1528 "zmac.y"
{
			if ((yyvsp[-2].ival & 060) == 040)
				emit1(052, yyvsp[-2].ival, yyvsp[0].ival, 5);
			else
				emit(4, 0355, 0113 + yyvsp[-2].ival, yyvsp[0].ival, yyvsp[0].ival >> 8);
		;
    break;}
case 84:
#line 1536 "zmac.y"
{
			if ((yyvsp[0].ival & 060) == 040)
				emit1(042, yyvsp[0].ival, yyvsp[-2].ival, 5);
			else
				emit(4, 0355, 0103 + yyvsp[0].ival, yyvsp[-2].ival, yyvsp[-2].ival >> 8);
		;
    break;}
case 85:
#line 1544 "zmac.y"
{
			if (yyvsp[-2].ival != 060) {
				fprintf(stdout,"LD evenreg error\n");
				err[fflag]++;
			}
			else
				emit1(0371, yyvsp[0].ival, 0, 1);
		;
    break;}
case 86:
#line 1554 "zmac.y"
{
			if (yyvsp[-2].itemptr->i_value != 020) {
				fprintf(stdout,"EX RP, HL error\n");
				err[fflag]++;
			}
			else
				emit(1, 0353);
		;
    break;}
case 87:
#line 1564 "zmac.y"
{ emit(1, 010); ;
    break;}
case 88:
#line 1567 "zmac.y"
{ emit1(0343, yyvsp[0].ival, 0, 1); ;
    break;}
case 89:
#line 1570 "zmac.y"
{
			if (yyvsp[-2].ival != 7) {
				fprintf(stdout,"IN reg, (expr) error\n");
				err[fflag]++;
			}
			else	{
				if (yyvsp[0].ival < 0 || yyvsp[0].ival > 255)
					err[vflag]++;
				emit(2, yyvsp[-3].itemptr->i_value, yyvsp[0].ival);
			}
		;
    break;}
case 90:
#line 1583 "zmac.y"
{ emit(2, 0355, 0100 + (yyvsp[-4].ival << 3)); ;
    break;}
case 91:
#line 1586 "zmac.y"
{ emit(2, 0355, 0160); ;
    break;}
case 92:
#line 1589 "zmac.y"
{
			if (yyvsp[-2].ival < 0 || yyvsp[-2].ival > 255)
				err[vflag]++;
			emit(2, yyvsp[-3].itemptr->i_value, yyvsp[-2].ival);
		;
    break;}
case 93:
#line 1596 "zmac.y"
{ emit(2, 0355, 0101 + (yyvsp[0].ival << 3)); ;
    break;}
case 94:
#line 1599 "zmac.y"
{
			if (yyvsp[0].ival > 2 || yyvsp[0].ival < 0)
				err[vflag]++;
			else
				emit(2, yyvsp[-1].itemptr->i_value >> 8, yyvsp[-1].itemptr->i_value + ((yyvsp[0].ival + (yyvsp[0].ival > 0)) << 3));
		;
    break;}
case 95:		/* PHASE expression */
#line 1607 "zmac.y"
{
			if (phaseflag) {
				err[oflag]++;
			} else {
				phaseflag = 1;
				phdollar = dollarsign;
				dollarsign = yyvsp[0].ival;
				phbegin = dollarsign;
			}
		;
    break;}
case 96:		/* DEPHASE */
#line 1619 "zmac.y"
{
			if (!phaseflag) {
				err[oflag]++;
			} else {
				phaseflag = 0;
				dollarsign = phdollar + dollarsign - phbegin;
			}
		;
    break;}
case 97:		/* ORG expression */
#line 1629 "zmac.y"
{
			if (not_seen_org)
				first_org_store=yyvsp[0].ival;
			not_seen_org=0;
			if (phaseflag) {
				err[oflag]++;
				dollarsign = phdollar + dollarsign - phbegin;
				phaseflag = 0;
			}
			if (yyvsp[0].ival-dollarsign) {
				flushbin();
				if (pass2 && !output_hex && dollarsign != 0) {
					if (yyvsp[0].ival < dollarsign) {
						err[orgflag]++;
					} else {
						int f;
						for (f=0;f<(yyvsp[0].ival - dollarsign);f++)
							fputc(0, fbuf);
					}
				}
				olddollar = yyvsp[0].ival;
				dollarsign = yyvsp[0].ival;
			}
		;
    break;}
case 104:
#line 1672 "zmac.y"
{
			yyvsp[0].itemptr->i_token = MPARM;
			if (parm_number >= PARMMAX)
				error("Too many parameters");
			yyvsp[0].itemptr->i_value = parm_number++;
		;
    break;}
case 108:
#line 1692 "zmac.y"
{
			cp = malloc(strlen(tempbuf)+1);
#ifdef M_DEBUG
			fprintf(stdout, "[Arg%u(%p): %s]\n", parm_number, cp, tempbuf);
#endif
			est2[parm_number++] = cp;
			strcpy(cp, tempbuf);
		;
    break;}
case 111:
#line 1708 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 112:
#line 1713 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 113:
#line 1718 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 114:
#line 1724 "zmac.y"
{
			yyval.ival = 6;
		;
    break;}
case 115:
#line 1729 "zmac.y"
{
			disp = yyvsp[-1].ival;
			yyval.ival = (yyvsp[-2].itemptr->i_value & 0177400) | 6;
		;
    break;}
case 116:
#line 1735 "zmac.y"
{
			disp = 0;
			yyval.ival = (yyvsp[-1].itemptr->i_value & 0177400) | 6;
		;
    break;}
case 119:
#line 1747 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 120:
#line 1752 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 122:
#line 1760 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 123:
#line 1765 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 125:
#line 1773 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 126:
#line 1779 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 127:
#line 1784 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 129:
#line 1792 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 130:
#line 1798 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value;
		;
    break;}
case 131:
#line 1803 "zmac.y"
{	yyval.ival = 030;	;
    break;}
case 134:
#line 1812 "zmac.y"
{
			dataemit(2, yyvsp[0].ival, yyvsp[0].ival>>8);
		;
    break;}
case 135:
#line 1817 "zmac.y"
{
			cp = yyvsp[0].cval;
			while (*cp != '\0')
				dataemit(1,*cp++);
		;
    break;}
case 136:
#line 1824 "zmac.y"
{
			if (yyvsp[0].ival < -128 || yyvsp[0].ival > 255)
					err[vflag]++;
			dataemit(1, yyvsp[0].ival & 0377);
		;
    break;}
case 139:
#line 1841 "zmac.y"
{
			dataemit(2, yyvsp[0].ival, yyvsp[0].ival>>8);
		;
    break;}
case 144:
#line 1862 "zmac.y"
{	yyval.ival = yyvsp[-1].ival;	;
    break;}
case 145:
#line 1867 "zmac.y"
{	yyval.ival = yyvsp[0].itemptr->i_value; yyvsp[0].itemptr->i_uses++ ;	;
    break;}
case 148:
#line 1874 "zmac.y"
{	yyval.ival = yyvsp[0].itemptr->i_value; yyvsp[0].itemptr->i_uses++ ;	;
    break;}
case 149:
#line 1877 "zmac.y"
{
			yyval.ival = yyvsp[0].itemptr->i_value; yyvsp[0].itemptr->i_uses++ ;
			if (yyvsp[0].itemptr->i_equbad) {
				/* forward reference to equ with a forward
				 * reference of its own cannot be resolved
				 * in two passes. -rjm
				 */
				err[frflag]++;
			}
		;
    break;}
case 150:
#line 1889 "zmac.y"
{	yyval.ival = yyvsp[0].itemptr->i_value; yyvsp[0].itemptr->i_uses++ ;	;
    break;}
case 151:
#line 1892 "zmac.y"
{	yyval.ival = dollarsign;	;
    break;}
case 152:
#line 1895 "zmac.y"
{
			err[uflag]++;
			equ_bad_label=1;
			yyval.ival = 0;
		;
    break;}
case 153:
#line 1902 "zmac.y"
{	yyval.ival = yyvsp[0].itemptr->i_value;	;
    break;}
case 154:
#line 1905 "zmac.y"
{	yyval.ival = yyvsp[-2].ival + yyvsp[0].ival;	;
    break;}
case 155:
#line 1908 "zmac.y"
{	yyval.ival = yyvsp[-2].ival - yyvsp[0].ival;	;
    break;}
case 156:
#line 1911 "zmac.y"
{	if (yyvsp[0].ival == 0) err[eflag]++; else yyval.ival = yyvsp[-2].ival / yyvsp[0].ival;	;
    break;}
case 157:
#line 1914 "zmac.y"
{	yyval.ival = yyvsp[-2].ival * yyvsp[0].ival;	;
    break;}
case 158:
#line 1917 "zmac.y"
{	if (yyvsp[0].ival == 0) err[eflag]++; else yyval.ival = yyvsp[-2].ival % yyvsp[0].ival;	;
    break;}
case 159:
#line 1920 "zmac.y"
{	if (yyvsp[0].ival == 0) err[eflag]++; else yyval.ival = yyvsp[-2].ival % yyvsp[0].ival;	;
    break;}
case 160:
#line 1923 "zmac.y"
{	yyval.ival = yyvsp[-2].ival & yyvsp[0].ival;	;
    break;}
case 161:
#line 1926 "zmac.y"
{	yyval.ival = yyvsp[-2].ival & yyvsp[0].ival;	;
    break;}
case 162:
#line 1929 "zmac.y"
{	yyval.ival = yyvsp[-2].ival | yyvsp[0].ival;	;
    break;}
case 163:
#line 1932 "zmac.y"
{	yyval.ival = yyvsp[-2].ival | yyvsp[0].ival;	;
    break;}
case 164:
#line 1935 "zmac.y"
{	yyval.ival = yyvsp[-2].ival ^ yyvsp[0].ival;	;
    break;}
case 165:
#line 1938 "zmac.y"
{	yyval.ival = yyvsp[-2].ival ^ yyvsp[0].ival;	;
    break;}
case 166:
#line 1941 "zmac.y"
{	yyval.ival = yyvsp[-2].ival << yyvsp[0].ival;	;
    break;}
case 167:
#line 1944 "zmac.y"
{	yyval.ival = ((yyvsp[-2].ival >> 1) & 077777) >> (yyvsp[0].ival - 1);	;
    break;}
case 168:
#line 1947 "zmac.y"
{	yyval.ival = yyvsp[-2].ival < yyvsp[0].ival;	;
    break;}
case 169:
#line 1950 "zmac.y"
{	yyval.ival = yyvsp[-2].ival == yyvsp[0].ival;	;
    break;}
case 170:
#line 1953 "zmac.y"
{	yyval.ival = yyvsp[-2].ival > yyvsp[0].ival;	;
    break;}
case 171:
#line 1956 "zmac.y"
{	yyval.ival = yyvsp[-2].ival < yyvsp[0].ival;	;
    break;}
case 172:
#line 1959 "zmac.y"
{	yyval.ival = yyvsp[-2].ival == yyvsp[0].ival;	;
    break;}
case 173:
#line 1962 "zmac.y"
{	yyval.ival = yyvsp[-2].ival > yyvsp[0].ival;	;
    break;}
case 174:
#line 1965 "zmac.y"
{	yyval.ival = yyvsp[-2].ival <= yyvsp[0].ival;	;
    break;}
case 175:
#line 1968 "zmac.y"
{	yyval.ival = yyvsp[-2].ival >= yyvsp[0].ival;	;
    break;}
case 176:
#line 1971 "zmac.y"
{	yyval.ival = yyvsp[-2].ival != yyvsp[0].ival;	;
    break;}
case 177:
#line 1974 "zmac.y"
{	yyval.ival = yyvsp[-1].ival;	;
    break;}
case 178:
#line 1977 "zmac.y"
{	yyval.ival = ~yyvsp[0].ival;	;
    break;}
case 179:
#line 1980 "zmac.y"
{	yyval.ival = ~yyvsp[0].ival;	;
    break;}
case 180:
#line 1983 "zmac.y"
{	yyval.ival = !yyvsp[0].ival;	;
    break;}
case 181:
#line 1986 "zmac.y"
{	yyval.ival = yyvsp[0].ival;	;
    break;}
case 182:
#line 1989 "zmac.y"
{	yyval.ival = -yyvsp[0].ival;	;
    break;}
case 189:
#line 2008 "zmac.y"
{ int i;
		if (expptr >= MAXEXP)
			error("Macro expansion level");
		est2 = (char **) malloc((PARMMAX +4) * sizeof(char *));
		expstack[expptr] = (char *)est2 ;
		for (i=0; i<PARMMAX; i++)
			est2[i] = 0;
		arg_flag++;
	;
    break;}
case 190:
#line 2021 "zmac.y"
{	arg_flag++;	;
    break;}
case 191:
#line 2025 "zmac.y"
{	arg_flag = 0;	;
    break;}
case 192:
#line 2029 "zmac.y"
{	quoteflag++;	;
    break;}
case 193:
#line 2033 "zmac.y"
{	quoteflag = 0;	;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 543 "/usr/local/share/bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stdout, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stdout, " %d", *++ssp1);
      fprintf (stdout, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting '" : " or '");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stdout, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stdout, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stdout, " %d", *++ssp1);
      fprintf (stdout, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stdout, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;

 yyacceptlab:
  /* YYACCEPT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 0;

 yyabortlab:
  /* YYABORT comes here.  */
  if (yyfree_stacks)
    {
      free (yyss);
      free (yyvs);
#ifdef YYLSP_NEEDED
      free (yyls);
#endif
    }
  return 1;
}
#line 2037 "zmac.y"


#define F_END	0
#define OTHER	1
#define SPACE	2
#define DIGIT	3
#define LETTER	4
#define STARTER 5
#define HEXIN	6


/*
 *  This is the table of character classes.  It is used by the lexical
 *  analyser. (yylex())
 */
char	charclass[] = {
	F_END,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,
	OTHER,	SPACE,	OTHER,	OTHER,	OTHER,	SPACE,	OTHER,	OTHER,
	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,
	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,
	SPACE,	OTHER,	OTHER,	HEXIN,	HEXIN,	OTHER,	HEXIN,	OTHER,
	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,
	DIGIT,	DIGIT,	DIGIT,	DIGIT,	DIGIT,	DIGIT,	DIGIT,	DIGIT,
	DIGIT,	DIGIT,	OTHER,	OTHER,	OTHER,	OTHER,	OTHER,	STARTER,
	STARTER,LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, OTHER,	OTHER,	OTHER,	OTHER,	LETTER,
	OTHER,	LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER, LETTER,
	LETTER, LETTER, LETTER, OTHER,	OTHER,	OTHER,	OTHER,	OTHER,
};


/*
 *  the following table tells which characters are parts of numbers.
 *  The entry is non-zero for characters which can be parts of numbers.
 */
char	numpart[] = {
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	'0',	'1',	'2',	'3',	'4',	'5',	'6',	'7',
	'8',	'9',	0,	0,	0,	0,	0,	0,
	0,	'A',	'B',	'C',	'D',	'E',	'F',	0,
	'H',	0,	0,	0,	0,	0,	0,	'O',
	0,	'Q',	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0,	'a',	'b',	'c',	'd',	'e',	'f',	0,
	'h',	0,	0,	0,	0,	0,	0,	'o',
	0,	'q',	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,
	0};




/*
 *  Команды должны следовать (отсортированы) строго по алфавиту!
 *
 *  the following table is a list of assembler mnemonics;
 *  for each mnemonic the associated machine-code bit pattern
 *  and symbol type are given.
 */
struct	item	keytab[] = {
	{"a",	7,	ACC,		0},
	{"adc",	1,	ARITHC,		0},
	{"add",	0,	ADD,		0},
	{"af",	060,	AF,		0},
	{"and",	4,	AND,		0},
	{"ascii",0,	DEFB,		0},
	{"b",	0,	REGNAME,	0},
	{"bc",	0,	RP,		0},
	{"bit",	0145500,BIT,		0},
	{"block",0,	DEFS,		0},
	{"byte",0,	DEFB,		0},
	{"c",	1,	C,		0},
	{"call",0315,	CALL,		0},
	{"ccf",	077,	NOOPERAND,	0},
	{"cmp",	7,	LOGICAL,	0},		/* -cdk */
	{"cond",0,	IF,		0},
	{"cp",	7,	LOGICAL,	0},
	{"cpd",	0166651,NOOPERAND,	0},
	{"cpdr",0166671,NOOPERAND,	0},
	{"cpi",	0166641,NOOPERAND,	0},
	{"cpir",0166661,NOOPERAND,	0},
	{"cpl",	057,	NOOPERAND,	0},
	{"d",	2,	REGNAME,	0},
	{"daa",	0047,	NOOPERAND,	0},
	{"db",	0,	DEFB,		0},
	{"de",	020,	RP,		0},
	{"dec",	1,	INCDEC,		0},
	{"defb",0,	DEFB,		0},
	{"defl",0,	DEFL,		0},
	{"defm",0,	DEFB,		0},
	{"defs",0,	DEFS,		0},
	{"defw",0,	DEFW,		0},
	{"dephase",0,	DEPHASE,	0},
	{"di",	0363,	NOOPERAND,	0},
	{"djnz",020,	DJNZ,		0},
	{"ds",	0,	DEFS,		0},
	{"dw",	0,	DEFW,		0},
	{"e",	3,	REGNAME,	0},
	{"ei",	0373,	NOOPERAND,	0},
	{"eject",1,	LIST,		0},
	{"elist",3,	LIST,		0},
	{"else",0,	ELSE,		0},
	{"end",	0,	END,		0},
	{"endc",0,	ENDIF,		0},
	{"endif",0,	ENDIF,		0},
	{"endm", 0,	ENDM,		0},
	{"eq",	0,	EQ,		0},
	{"equ",	0,	EQU,		0},
	{"ex",	0,	EX,		0},
	{"exx",	0331,	NOOPERAND,	0},
	{"f",	0,	F,		0},
	{"flist",4,	LIST,		0},
	{"ge",	0,	GE,		0},
	{"glist",5,	LIST,		0},
	{"gt",	0,	GT,		0},
	{"h",	4,	REGNAME,	0},
	{"halt",0166,	NOOPERAND,	0},
	{"hl",	040,	HL,		0},
	{"i",	0,	MISCREG,	0},
	{"if",	0,	IF,		0},
	{"im",	0166506,IM,		0},
	{"in",	0333,	IN,		0},
	{"inc",	0,	INCDEC,		0},
        {"incbin", 4,   ARGPSEUDO,      0},
	{"include", 3,	ARGPSEUDO,	0},
	{"ind",	0166652,NOOPERAND,	0},
	{"indr",0166672,NOOPERAND,	0},
	{"ini",	0166642,NOOPERAND,	0},
	{"inir",0166662,NOOPERAND,	0},
	{"ix",	0156440,INDEX,		0},
	{"iy",	0176440,INDEX,		0},
	{"jmp",	0303,	JP,		0},		/* -cdk */
	{"jp",	0303,	JP,		0},
	{"jr",	040,	JR,		0},
	{"l",	5,	REGNAME,	0},
	{"ld",	0,	LD,		0},
	{"ldd",	0166650,NOOPERAND,	0},
	{"lddr",0166670,NOOPERAND,	0},
	{"ldi",	0166640,NOOPERAND,	0},
	{"ldir",0166660,NOOPERAND,	0},
	{"le",	0,	LE,		0},
	{"list",0,	LIST,		0},
	{"lt",	0,	LT,		0},
	{"m",	070,	COND,		0},
	{"macro",0,	MACRO,		0},
	{"max",	1,	MINMAX,		0},
	{"min",	0,	MINMAX,		0},
	{"mlist",6,	LIST,		0},
	{"mod",	0,	MOD,		0},
	{"nc",	020,	SPCOND,		0},
	{"ne",	0,	NE,		0},
	{"neg",	0166504,NOOPERAND,	0},
	{"nolist",-1,	LIST,		0},
	{"nop",	0,	NOOPERAND,	0},
	{"not",	0,	NOT,		0},
	{"nv",	040,	COND,		0},
	{"nz",	0,	SPCOND,		0},
	{"or",	6,	OR,		0},
	{"org",	0,	ORG,		0},
	{"otdr",0166673,NOOPERAND,	0},
	{"otir",0166663,NOOPERAND,	0},
	{"out",	0323,	OUT,		0},
	{"outd",0166653,NOOPERAND,	0},
	{"outi",0166643,NOOPERAND,	0},
	{"p",	060,	COND,		0},
	{"pe",	050,	COND,		0},
	{"phase",0,	PHASE,		0},
	{"po",	040,	COND,		0},
	{"pop",	0301,	PUSHPOP,	0},
	{"push", 0305,	PUSHPOP,	0},
	{"r",	010,	MISCREG,	0},
	{"read", 3,	ARGPSEUDO,	0},
	{"res",	0145600,BIT,		0},
	{"ret",	0311,	RET,		0},
	{"reti",0166515,NOOPERAND,	0},
	{"retn",0166505,NOOPERAND,	0},
	{"rl",	2,	SHIFT,		0},
	{"rla",	027,	NOOPERAND,	0},
	{"rlc",	0,	SHIFT,		0},
	{"rlca",07,	NOOPERAND,	0},
	{"rld",	0166557,NOOPERAND,	0},
	{"rmem",0,	DEFS,		0},
	{"rr",	3,	SHIFT,		0},
	{"rra",	037,	NOOPERAND,	0},
	{"rrc",	1,	SHIFT,		0},
	{"rrca",017,	NOOPERAND,	0},
	{"rrd",	0166547,NOOPERAND,	0},
	{"rst",	0307,	RST,		0},
	{"rsym",1,	ARGPSEUDO,	0},
	{"sbc",	3,	ARITHC,		0},
	{"scf",	067,	NOOPERAND,	0},
	{"set",	0145700,BIT,		0},
	{"shl",	0,	SHL,		0},
	{"shr",	0,	SHR,		0},
	{"sla",	4,	SHIFT,		0},
	{"sll",	6,	SHIFT,		0}, /* Undocumented */
	{"sp",	060,	SP,		0},
	{"space",2,	LIST,		0},
	{"sra",	5,	SHIFT,		0},
	{"srl",	7,	SHIFT,		0},
	{"sub",	2,	LOGICAL,	0},
	{"text",0,	DEFB,		0},
	{"title",0,	ARGPSEUDO,	0},
	{"v",	050,	COND,		0},
	{"word",0,	DEFW,		0},
	{"wsym",2,	ARGPSEUDO,	0},
	{"xor",	5,	XOR,		0},
	{"z",	010,	SPCOND,		0}
};

/*
 *  user-defined items are tabulated in the following table.
 */

struct item	itemtab[ITEMTABLESIZE];
struct item	*itemmax = itemtab+ITEMTABLESIZE;





/*
 *  lexical analyser, called by yyparse.
 */
int yylex()
{
	int	c;
	char *p;
	int	radix;
	int  limit;
	int leadinghex = 0;

	if (arg_flag)
		return(getarg());
loop switch(charclass[c = nextchar()]) {
	case F_END:
		if (expptr) {
			popsi();
			continue;
		} else return(0);

	case SPACE:
		break;
	case LETTER:
	case STARTER:
		p = tempbuf;
		do {
			if (p >= tempmax)
				error(symlong);
			*p++ = (c >= 'A' && c <= 'Z') ? c + 'a' - 'A' : c;
			while	((c = nextchar()) == '$')
				;
		} while	(charclass[c]==LETTER || charclass[c]==DIGIT);
		if (p - tempbuf > MAXSYMBOLSIZE)
		{
			if (pass2) warnprt (0, 1);
			p = tempbuf + MAXSYMBOLSIZE;
		}
		*p++ = '\0';
		peekc = c;
		return(tokenofitem(UNDECLARED));
	case HEXIN:
	{
		int corig = c;
		if (*ifptr) return (skipline(c));
		while ((c = nextchar ()) == '$');
		if (!numpart[c])
		{
			peekc = c;
			return (corig);
		}
		leadinghex = 1;
		/* fall through */
	}
	case DIGIT:
		if (*ifptr) return (skipline(c));
		p = tempbuf;
		do	{
			if (p >= tempmax)
				error(symlong);
			*p++ = (c >= 'A' && c <= 'Z') ? c + 'a' - 'A' : c;
			while	((c = nextchar()) == '$');
			}
			while(numpart[c]);
		peekc = c;
		if (leadinghex)
		{
			*p++ = 'h';
		}
		*p-- = '\0';
		switch(*p)	{
			case 'o':
			case 'q':
				radix = 8;
				limit = 020000;
				*p = '\0';
				break;
			case 'd':
				radix = 10;
				limit = 6553;
				*p = '\0';
				break;
			case 'h':
				radix = 16;
				limit = 010000;
				*p = '\0';
				break;
			case 'b':
				radix = 2;
				limit = 077777;
				*p = '\0';
				break;
			default:
				radix = 10;
				limit = 6553;
				p++;
				break;
			}

		/*
		 *  tempbuf now points to the number, null terminated
		 *  with radix 'radix'.
		 */
		yylval.ival = 0;
		p = tempbuf;
		do	{
			c = *p - (*p > '9' ? ('a' - 10) : '0');
			if (c >= radix)
				{
				err[iflag]++;
				yylval.ival = 0;
				break;
				}
			if (yylval.ival < limit ||
				(radix == 10 && yylval.ival == 6553 && c < 6) ||
				(radix == 2 && yylval.ival == limit))
				yylval.ival = yylval.ival * radix + c;
			else {
				err[vflag]++;
				yylval.ival = 0;
				break;
				}
			}
			while(*++p != '\0');
		return(NUMBER);
	default:
		if (*ifptr)
			return(skipline(c));
		switch(c) {
		int corig;
		case ';':
			return(skipline(c));
		case '\'':
			if (quoteflag) return('\'');
		case '"':
			corig = c;
			p = tempbuf;
			p[1] = 0;
			do	switch(c = nextchar())	{
			case '\0':
			case '\n':
				err[bflag]++;
				goto retstring;
			case '\'':
			case '"':
				if (c == corig && (c = nextchar()) != corig) {
				retstring:
					peekc = c;
					*p = '\0';
					if ((p-tempbuf) >2) {
						yylval.cval = tempbuf;
						return(STRING);
					} else if (p-tempbuf == 2)	{
						p = tempbuf;
						yylval.ival = *p++ ;
						yylval.ival |= *p<<8;
						return(TWOCHAR);
					} else	{
						p = tempbuf;
						yylval.ival = *p++;
						return(ONECHAR);
					}
				}
			default:
				*p++ = c;
			} while (p < tempmax);
			/*
			 *  if we break out here, our string is longer than
			 *  our input line
			 */
			error("string buffer overflow");
		case '<':
			corig = c;
			switch (c = nextchar ()) {
			case '=':
				return LE;
			case '<':
				return SHL;
			case '>':
				return NE;
			default:
				peekc = c;
				return corig;
			}
			/* break; suppress "unreachable" warning for tcc */
		case '>':
			corig = c;
			switch (c = nextchar ()) {
			case '=':
				return GE;
			case '>':
				return SHR;
			default:
				peekc = c;
				return corig;
			}
			/* break; suppress "unreachable" warning for tcc */
		case '!':
			corig = c;
			switch (c = nextchar ()) {
			case '=':
				return NE;
			default:
				peekc = c;
				return corig;
			}
			/* break; suppress "unreachable" warning for tcc */
		case '=':
			corig = c;
			switch (c = nextchar ()) {
			case '=':
				return EQ;
			default:
				peekc = c;
				return corig;
			}
			/* break; suppress "unreachable" warning for tcc */
		default:
			return(c);
		}
	}
}

/*
 *  return the token associated with the string pointed to by
 *  tempbuf.  if no token is associated with the string, associate
 *  deftoken with the string and return deftoken.
 *  in either case, cause yylval to point to the relevant
 *  symbol table entry.
 */

int tokenofitem(int deftoken)
{
	char *p;
	struct item *	ip;
	int  i;
	int  r, l, u, hash;


#ifdef T_DEBUG
	fputs("'tokenofitem entry'	", stdout);
	fputs(tempbuf, stdout);
#endif
	if (strcmp (tempbuf, "cmp") == 0 ||
	    strcmp (tempbuf, "jmp") == 0 ||
	    strcmp (tempbuf, "v")   == 0 ||
	    strcmp (tempbuf, "nv")  == 0)
		if (pass2) warnprt (1, 1);
	/*
	 *  binary search
	 */
	l = 0;
	u = (sizeof keytab/sizeof keytab[0])-1;
	while (l <= u) {
		i = (l+u)/2;
		ip = &keytab[i];
		if ((r = strcmp(tempbuf, ip->i_string)) == 0)
			goto found;
		if (r < 0)
			u = i-1;
		else
			l = i+1;
	}

	/*
	 *  hash into item table
	 */
	hash = 0;
	p = tempbuf;
	while (*p) hash += *p++;
	hash %= ITEMTABLESIZE;
	ip = &itemtab[hash];

	loop {
		if (ip->i_token == 0)
			break;
		if (strcmp(tempbuf, ip->i_string) == 0)
			goto found;
		if (++ip >= itemmax)
			ip = itemtab;
	}

	if (!deftoken) {
		i = 0 ;
		goto token_done ;
	}
	if (++nitems > ITEMTABLESIZE-20)
		error("item table overflow");
	ip->i_string = malloc(strlen(tempbuf)+1);
	ip->i_token = deftoken;
	ip->i_uses = 0;
	ip->i_equbad = 0;
	strcpy(ip->i_string, tempbuf);

found:
	if (*ifptr) {
		if (ip->i_token == ENDIF) {
			i = ENDIF ;
			goto token_done ;
		}
		if (ip->i_token == ELSE) {
			/* We must only honour the ELSE if it is not
			   in a nested failed IF/ELSE */
			char forbid = 0;
			char *ifstackptr;
			for (ifstackptr = ifstack; ifstackptr != ifptr; ++ifstackptr) {
				if (*ifstackptr) {
					forbid = 1;
					break;
				}
			}
			if (!forbid) {
				i = ELSE;
				goto token_done;
			}
		}
		if (ip->i_token == IF) {
			if (ifptr >= ifstmax)
				error("Too many ifs");
			else *++ifptr = 1;
		}
		i = skipline(' ');
		goto token_done ;
	}
	yylval.itemptr = ip;
	i = ip->i_token;
	if (i == EQU) equ_bad_label=0;
token_done:
#ifdef T_DEBUG
	fputs("\t'tokenofitem exit'\n", stdout);
#endif
	return(i) ;
}


/*
 *  interchange two entries in the item table -- used by custom_qsort
 */
void interchange(int i, int j)
{
	struct item *fp, *tp;
	struct item temp;

	fp = &itemtab[i];
	tp = &itemtab[j];
	temp.i_string = fp->i_string;
	temp.i_value = fp->i_value;
	temp.i_token = fp->i_token;
	temp.i_uses = fp->i_uses;
	temp.i_equbad = fp->i_equbad;

	fp->i_string = tp->i_string;
	fp->i_value = tp->i_value;
	fp->i_token = tp->i_token;
	fp->i_uses = tp->i_uses;
	fp->i_equbad = tp->i_equbad;

	tp->i_string = temp.i_string;
	tp->i_value = temp.i_value;
	tp->i_token = temp.i_token;
	tp->i_uses = temp.i_uses;
	tp->i_equbad = temp.i_equbad;
}



/*
 *  quick sort -- used by putsymtab to sort the symbol table
 */
void custom_qsort(int m, int n)
{
	int  i, j;

	if (m < n) {
		i = m;
		j = n+1;
		loop {
			do i++; while(strcmp(itemtab[i].i_string,
					itemtab[m].i_string) < 0);
			do j--; while(strcmp(itemtab[j].i_string,
					itemtab[m].i_string) > 0);
			if (i < j) interchange(i, j); else break;
		}
		interchange(m, j);
		custom_qsort(m, j-1);
		custom_qsort(j+1, n);
	}
}



/*
 *  get the next character
 */
int nextchar()
{
	int c, ch;
	static  char  *earg;

	if (peekc != -1) {
		c = peekc;
		peekc = -1;
		return(c);
	}

start:
	if (earg) {
		if (*earg)
			return(addtoline(*earg++));
		earg = 0;
	}

	if (expptr) {
		if ((ch = getm()) == '\1') {	/*  expand argument  */
			ch = getm() - 'A';
			if (ch >= 0 && ch < PARMMAX && est[ch])
				earg = est[ch];
			goto start;
		}
		if (ch == '\2') {	/*  local symbol  */
			ch = getm() - 'A';
			if (ch >= 0 && ch < PARMMAX && est[ch]) {
				earg = est[ch];
				goto start;
			}
			earg = getlocal(ch, (int)(long)est[TEMPNUM]);
			goto start;
		}

		return(addtoline(ch));
	}
	ch = getc(now_file) ;
	/* if EOF, check for include file */
	if (ch == EOF) {
		while (ch == EOF && now_in) {
			fclose(fin[now_in]) ;
			free(src_name[now_in]) ;
			now_file = fin[--now_in] ;
			ch = getc(now_file) ;
		}
		if (linein[now_in] < 0) {
			lstoff = 1 ;
			linein[now_in] = -linein[now_in] ;
		} else {
			lstoff = 0 ;
		}
		if (pass2 && iflist()) {
			lineout() ;
			fprintf(fout, "**** %s ****\n", src_name[now_in]) ;
		}
	}
	if (ch == '\n')
	{
		linein[now_in]++ ;
	}

	return(addtoline(ch)) ;
}


/*
 *  skip to rest of the line -- comments and if skipped lines
 */
int skipline(int ac)
{
	int  c;

	c = ac;
	while (c != '\n' && c != '\0')
		c = nextchar();
	return('\n');
}


/* Help screen */
void usage()
{
	printf("ZMAC v" ZMAC_VERSION
#ifdef ZMAC_BETA
	ZMAC_BETA
#endif
", a z80 macro cross-assembler.\n"
"Public domain by Bruce Norskog and others.\n\n"
#ifdef __riscos
"Usage: zmac [--help] [--version] [-AbcdefghilLmnOpsStTz]\n"
#else
"Usage: zmac [--help] [--version] [-AbcdefghilLmnOpsStz]\n"
#endif
"       [-o outfile] [-x listfile] [filename]\n\n"
"       --help     give this usage help.\n"
"       --version  report version number.\n"
"       -A      output AMSDOS binary file rather than default binary file.\n"
"       -b      don't generate the m/c output at all.\n"
"       -c      make the listing continuous, i.e. don't generate any\n"
"               page breaks or page headers. (This is the default.)\n"
"       -d      make the listing discontinuous.\n"
"       -e      omit the 'error report' section in the listing.\n"
"       -f      list instructions not assembled due to 'if' expressions being\n"
"               false. (Normally these are not shown in the listing.)\n"
"       -g      list only the first line of equivalent hex for a source line.\n"
"       -h      output CP/M-ish Intel hex format (using extension '.hex')\n"
"               rather than default binary file (extension '.bin').\n"
"       -i      don't list files included with 'include'.\n"
"       -l      don't generate a listing at all.\n"
"       -L      generate listing; overrides any conflicting options.\n"
"       -m      list macro expansions.\n");
//        puts("Press any key to continue...");
//        getch();
puts("       -n      omit line numbers from listing.\n"
"       -o      assemble output to 'outfile'.\n"
"       -O      suggest possible optimisations (as warnings).\n"
"       -p      use linefeeds for page break in listing rather than ^L.\n"
"       -s      omit the symbol table from the listing.\n"
"       -S      show relevant line when reporting errors.\n"
"       -t      give terse (single-letter) error codes in listing.\n"
#ifdef __riscos
"       -T      enable DDE throwback for reporting warnings and errors.\n"
#endif
"       -x      generate listing to 'listfile' ('-' for stdout).\n"
"       -z      accept 8080-compatible instructions only; flag any\n"
"               Z80-specific ones as errors.");
}



int main(int argc, char *argv[])
{
	struct item *ip;
	int  i, c;

#ifdef ZMAC_BETA
	printf ("*** THIS IS A BETA VERSION; NOT FOR GENERAL DISTRIBUTION ***\n");
#endif

	if(argc == 1)
		usage(), exit(0);

	if(argc >= 2) {
		if(strcmp(argv[1],"--help") == 0)
			usage(),exit(0);
		else if(strcmp(argv[1],"--version") == 0)
			puts("ZMAC v" ZMAC_VERSION
#ifdef ZMAC_BETA
					ZMAC_BETA
#endif
				), exit(0);
	}

	fout = stdout;
	fin[0] = stdin;
	now_file = stdin;

	*bin = *listf = 0;
	optnerr = 0;

	while((c = getoptn(argc,argv,
#ifdef __riscos
		"AbcdefghilLmno:OpsStTx:z"
#else
		"AbcdefghilLmno:OpsStx:z"
#endif
		)) != EOF) {
		switch(c) {

		case 'A':	/*  AMSDOS binary -mep */
			output_amsdos = 1;
			output_hex = 0;
			break;

		case 'b':	/*  no binary  */
			bopt = 0;
			break;

		case 'c':	/*  continuous listing  */
			continuous_listing = 1;
			break;

		case 'd':	/*  discontinuous listing  */
			continuous_listing = 0;
			break;

		case 'e':	/*  error list only  */
			eopt = 0;
			edef = 0;
			break;

		case 'f':	/*  print if skipped lines  */
			fopt++;
			fdef++;
			break;

		case 'g':	/*  do not list extra code  */
			gopt = 0;
			gdef = 0;
			break;

		case 'h':	/* output .hex not .bin -rjm */
			output_hex = 1;
			output_amsdos = 0;
			break;

		case 'i':	/* do not list include files */
			iopt = 1 ;
			break;

		case 'l':	/*  no list  */
			lopt++;
			break;

		case 'L':	/*  force listing of everything */
			lston++;
			break;

		case 'm':	/*  print macro expansions  */
			mdef++;
			mopt++;
			break;

		case 'n':	/*  put line numbers off */
			nopt-- ;
			break;

		case 'o':	/*  specify m/c output file */
			strcpy(bin, optnarg);
			break;

		case 'O':	/*  suggest optimisations  */
			suggest_optimise = 1;
			break;

		case 'p':	/*  put out four \n's for eject */
			popt-- ;
			break;

		case 's':	/*  don't produce a symbol list  */
			sopt++;
			break;

		case 'S':	/*  show line which caused error */
			show_error_line = 1;
			break;

		case 't':	/*  terse error messages in listing  */
			terse_lst_errors = 1;
			break;

#ifdef __riscos
		case 'T':	/*  RISC OS throwback  -mep */
			riscos_thbk = 1;
			break;
#endif

		case 'x':	/*  specify listing file */
			if(strcmp(optnarg, "-") == 0)
				oldoopt++;	/* list to stdout (old '-o') */
			else
				strcpy(listf, optnarg);
			break;

		case 'z':	/*  8080-compatible ops only  */
			output_8080_only = 1;
			break;

		case '?':
		default:	/*  error  */
			justerror("Unknown option or missing argument");
			break;

		}
	}

	if(optnind != argc-1) justerror("Missing, extra or mispositioned argument");

	atexit (doatexit);

	sourcef = argv[optnind];
	strcpy(src, sourcef);

	if ((now_file = fopen(src, "r")) == NULL)
	{
		/* If filename has no pre-existing suffix, then try .z */
		suffix_if_none (src, "z");
		if ((now_file = fopen(src, "r")) == NULL)
			fileerror("Cannot open source file", src);
	}
	now_in = 0 ;
	fin[now_in] = now_file ;
	src_name[now_in] = src ;
#ifdef __riscos
	riscos_set_csd(src); /* -mep */
#endif

	/* If we haven't got a bin file filename, then create one from the
	 * source filename (.hex extension if option -h is specified).
	 */
	if (*bin == 0) {
		strcpy(bin, sourcef);
		if (output_hex)
			suffix(bin,"hex");
		else
			suffix(bin,"bin");
	}
	if (bopt)
		if (( fbuf = fopen(bin, output_hex ? "w" : "wb")) == NULL)
			fileerror("Cannot create binary file", bin);

        if(*specf == 0) {
            strcpy(specf, sourcef);
            suffix(specf,"tbl");
        }
        if((fspeclab = fopen(specf, "w")) == NULL)
              fileerror("Cannot create tbl-file", specf);

	if (output_amsdos)
		for(i = 0; i < 128; i++)
			putc(0, fbuf);	/* -mep */

	if (!lopt && !oldoopt) {
		/* If we've not got a filename for the listing file
		 * (-x option) then create one from the source filename
		 * (.lst extension)
		 */
		if( *listf == 0 ) {
			strcpy(listf, sourcef);
			suffix(listf,"lst");
		}
		if ((fout = fopen(listf, "w")) == NULL)
			fileerror("Cannot create list file", listf);
	} else
		fout = stdout ;

	strcpy(mtmp, sourcef);
	suffix(mtmp,"tmp");
	mfile = mfopen(mtmp,"w+b") ;
	if (mfile == NULL) {
		fileerror("Cannot create temp file", mtmp);
	}

	/*
	 *  get the time
	 */
	time(&now);
	timp = ctime(&now);
	timp[16] = 0;
	timp[24] = 0;

	title = sourcef;
	/*
	 * Pass 1
	 */
#ifdef DEBUG
	fputs("DEBUG -pass 1\n", stdout);
#endif
	setvars();
	yyparse();
	pass2++;
	ip = &itemtab[-1];
	while (++ip < itemmax) {
		/* reset use count */
		ip->i_uses = 0;

		/* set macro names, equated and defined names */
		switch	(ip->i_token) {
		case MNAME:
			ip->i_token = OLDMNAME;
			break;

		case EQUATED:
			ip->i_token = WASEQUATED;
			break;

		case DEFLED:
			ip->i_token = UNDECLARED;
			break;
		}
	}
	setvars();
	fseek(now_file, (long)0, 0);

#ifdef DEBUG
	fputs("DEBUG -pass 2\n", stdout);
#endif
	yyparse();


	if (bopt) {		/* flag "no binary" */
		flushbin();
		if (output_hex) {
			putc(':', fbuf);
			if (xeq_flag) {
				puthex(0, fbuf);
				puthex(xeq >> 8, fbuf);
				puthex(xeq, fbuf);
				puthex(1, fbuf);
				puthex(255-(xeq >> 8)-xeq, fbuf);
			} else
				for	(i = 0; i < 10; i++)
					putc('0', fbuf);
			putc('\n', fbuf);
		}
		if (output_amsdos) {
			char leafname[] = "FILENAMEBIN";
			unsigned int chk;
			unsigned int filelen = dollarsign - first_org_store;
			if (filelen & 0x7f)
			{
				putc (0x1a, fbuf); /* CP/M EOF char */
			}
			rewind(fbuf);
			chk=0;
			putc(0,fbuf);
			for(i=0;i<11;i++) {
				putc(leafname[i],fbuf);
				chk+=leafname[i];
			}
			for(i=0;i<6;i++)
				putc(0,fbuf);
			putc(2,fbuf); /* Unprotected binary */
			chk+=2;
			putc(0,fbuf);
			putc(0,fbuf);
			putc(first_org_store & 0xFF,fbuf);
			chk+=first_org_store & 0xFF;
			putc(first_org_store >> 8,fbuf);
			chk+=first_org_store >> 8;
			putc(0,fbuf);
			putc(filelen & 0xFF,fbuf);
			chk+=filelen & 0xFF;
			putc(filelen >> 8,fbuf);
			chk+=filelen >> 8;
			/* Next bit should be entry address really */
			putc(first_org_store & 0xFF,fbuf);
			chk+=first_org_store & 0xFF;
			putc(first_org_store >> 8,fbuf);
			chk+=first_org_store >> 8;
			for(i=28;i<64;i++)
				putc(0,fbuf);
			putc(filelen & 0xFF,fbuf);
			chk+=filelen & 0xFF;
			putc(filelen >> 8,fbuf);
			chk+=filelen >> 8;
			putc(0,fbuf); /* this would be used if length>64K */
			putc(chk & 0xFF,fbuf);
			putc(chk >> 8,fbuf);
		}
		fflush(fbuf);
	}

	if (!lopt)
		fflush(fout);
	if (*writesyms)
		outsymtab(writesyms);
	if (eopt)
		erreport();
	if (!lopt && !sopt)
		putsymtab();
	if (!lopt) {
		eject();
		fflush(fout);
	}

	spec_labels();		/* test for "@label", and write to tbl-file */
	fflush(fspeclab);	/* сбросить буфер на диск */
	fclose(fspeclab);

	if(!put_spec_labels)	/* present "@label" labels ? */
            if(unlink(specf) == -1)
                 fprintf(stdout,"Cannot deleting file '%s'\n", specf);

	exit(had_errors);
	return(had_errors); /* main () does return int, after all... */
}


void doatexit (void)
{
#ifdef __riscos
	if (riscos_throwback_started)
	{
		_swix(DDEUtils_ThrowbackEnd,0);
	}
	_swix(DDEUtils_Prefix,1,0); /* Unset CSD */
#endif
}


/*
 *  set some data values before each pass
 */
void setvars()
{
	int  i;

	peekc = -1;
	linein[now_in] = linecnt = 0;
	exp_number = 0;
	emitptr = emitbuf;
	lineptr = linebuf;
	ifptr = ifstack;
	expifp = expif;
	*ifptr = 0;
	dollarsign = 0;
	olddollar = 0;
	phaseflag = 0;
	for (i=0; i<FLAGS; i++) err[i] = 0;
}



/*
 *  print out an error message and die
 */
void error(char *as)
{
	*linemax = 0;
	fprintf(fout, "%s\n", linebuf);
	fflush(fout);
	fprintf(stdout, "%s\n", as);
	exit(1);
}


/*
 *  alternate version
 */
void fileerror(char *as,char *filename)
{
	*linemax = 0;
	if (fout != NULL && fout != stdout)
		fprintf(fout, "%s\n", linebuf);
	fflush(fout);
	fprintf(stdout, "%s '%s'\n", as, filename);
	exit(1);
}



/*
 *  alternate alternate version
 */
void justerror(char *as)
{
	fprintf(stdout, "%s\n", as);
	exit(1);
}


/*
 *  output the symbol table
 */
void putsymtab()
{
	struct item *tp, *fp;
	int  i, j, k, t, rows;
	char c, c1 ;

	if (!nitems)
		return;

	/* compact the table so unused and UNDECLARED entries are removed */
	tp = &itemtab[-1];
	for (fp = itemtab; fp<itemmax; fp++) {
		if (fp->i_token == UNDECLARED) {
			nitems--;
			continue;
		}
		if (fp->i_token == 0)
			continue;
		tp++;
		if (tp != fp) {
			tp->i_string = fp->i_string;
			tp->i_value = fp->i_value;
			tp->i_token = fp->i_token;
			tp->i_uses = fp->i_uses ;
			tp->i_equbad = fp->i_equbad ;
		}
	}

	tp++;
	tp->i_string = "{";

	/*  sort the table */
	custom_qsort(0, nitems-1);

	title = "**  Symbol Table  **";

	rows = (nitems+3) / 3;
	if (rows+5+line > 60)
		eject();
	lineout();
	fprintf(fout,"\n\n\nSymbol Table:\n\n");
	line += 4;

	for (i=0; i<rows; i++) {
		for(j=0; j<3; j++) {
			k = rows*j+i;
			if (k < nitems) {
				tp = &itemtab[k];
				t = tp->i_token;
				c = ' ' ;
				if (t == EQUATED || t == DEFLED)
					c = '=' ;
				if (tp->i_uses == 0)
					c1 = '+' ;
				else
					c1 = ' ' ;
				fprintf(fout, "%-15s%c%04X%c    ",
					tp->i_string, c, tp->i_value & 0xFFFF, c1);
			}
		}
		lineout();
		putc('\n', fout);
	}
}


/*
 *  Test symbol table for "@label" labels, and write to file
 */
void spec_labels()
{
        struct item *tp, *fp;
        int  h, i, j, k, rows;
        char *op = "\tequ\t";

        if (!nitems)
                return;

        /* compact the table so unused and UNDECLARED entries are removed */
        tp = &itemtab[-1];
        for (fp = itemtab; fp<itemmax; fp++) {
                if (fp->i_token == UNDECLARED) {
                        nitems--;
                        continue;
                }
                if (fp->i_token == 0)
                        continue;
                tp++;
                if (tp != fp) {
                        tp->i_string = fp->i_string;
                        tp->i_value = fp->i_value;
                        tp->i_token = fp->i_token;
                        tp->i_uses = fp->i_uses;
                        tp->i_equbad = fp->i_equbad;
                }
        }

        /* sort the table */
        custom_qsort(0, nitems-1);

        rows = (nitems+3) / 3;

        for (i=0; i<rows; i++)
            {
              for (j=0; j<3; j++)
                  {
                    k = rows*j+i;
                    if(k < nitems)
                      {
                        tp = &itemtab[k];
                         /* сравнить с первой буквой имени */
                        if(tp->i_string[0] == '@')
                          { 
                            put_spec_labels = 1;  /* set flag */
                            h = tp->i_value & 0xFFFF;
                            h >>= 12;
                            if(h < 0x0A)
                               fprintf(fspeclab, "%s%s%04Xh\n", tp->i_string, op, tp->i_value & 0xFFFF);
                            else
                                fprintf(fspeclab, "%s%s0%04Xh\n", tp->i_string, op, tp->i_value & 0xFFFF);
                          }
                      }

                  }
            }
}


/*
 *  put out error report
 */
void erreport()
{
	int i, numerr;

	if (line > 50) eject();
	lineout();
	numerr = 0;
	for (i=0; i<FLAGS; i++) numerr += keeperr[i];
	if (numerr) {
		fputs("\n\n\nError report:\n\n", fout);
		fprintf(fout, "%6d error%s\n", DO_PLURAL(numerr));
		line += 5;
	} else {
		fputs("\n\n\nStatistics:\n", fout);
		line += 3;
	}

	for (i=0; i<FLAGS; i++)
		if (keeperr[i]) {
			lineout();
			if (terse_lst_errors)
				/* no plural on this because it would
				 * odd, I think. -rjm
				 */
				fprintf(fout, "%6d %c -- %s error\n",
					keeperr[i], errlet[i], errname[i]);
			else
				/* can't use DO_PLURAL for this due to
				 * the %s in the middle... -rjm
				 */
				fprintf(fout, "%6d %s error%s\n",
					keeperr[i], errname[i],
					(keeperr[i]==1)?"":"s");
		}

	if (line > 55) eject();
	lineout();
	fprintf(fout, "\n%6d\tsymbol%s\n", DO_PLURAL(nitems));
	fprintf(fout, "%6d\tbyte%s\n", DO_PLURAL(nbytes));
	line += 2;
	if (mfptr) {
		if (line > 53) eject();
		lineout();
		fprintf(fout, "\n%6d\tmacro call%s\n", DO_PLURAL(exp_number));
		fprintf(fout, "%6d\tmacro byte%s\n", DO_PLURAL(mfptr));
		fprintf(fout, "%6d\tinvented symbol%s\n",
						DO_PLURAL(invented/2));
		line += 3;
	}
}


/*
 *  lexical analyser for macro definition
 */
void mlex()
{
	char  *p;
	int  c;
	int  t;

	/* move text onto macro file, changing formal parameters */
#ifdef	M_DEBUG
	fprintf(stdout,"enter 'mlex'\n");
#endif

	inmlex++;

	c = nextchar();
loop {
	switch(charclass[c]) {

	case DIGIT:
		while (numpart[c]) {
			putm(c);
			c = nextchar();
		}
		continue;

	case STARTER:
	case LETTER:
		t = 0;
		p = tempbuf+MAXSYMBOLSIZE+2;
		do {
			if (p >= tempmax)
				error(symlong);
			*p++ = c;
			if (t < MAXSYMBOLSIZE)
				tempbuf[t++] = (c >= 'A' && c <= 'Z')  ?
					c+'a'-'A' : c;
			else
				if (pass2) warnprt (0, 1);
			c = nextchar();
		} while (charclass[c]==LETTER || charclass[c]==DIGIT);

		tempbuf[t] = 0;
		*p++ = '\0';
		p = tempbuf+MAXSYMBOLSIZE+2;
		t = tokenofitem(0);
		if (t != MPARM) while (*p) putm(*p++);
		else {
			if (*(yylval.itemptr->i_string) == '?') putm('\2');
			else putm('\1');
			putm(yylval.itemptr->i_value + 'A');
		}
		if (t == ENDM) goto done;
		continue;

	case F_END:
		if (expptr) {
			popsi();
			c = nextchar();
			continue;
		}

		goto done;

	default:
		if (c == '\n') {
			linecnt++;
		}
		if (c != '\1') putm(c);
		c = nextchar();
	}
}

	/*  finish off the file entry  */
done:
	while(c != EOF && c != '\n' && c != '\0') c = nextchar();
	linecnt++;
	putm('\n');
	putm('\n');
	putm(0);

	for (c = 0; c < ITEMTABLESIZE; c++)
		if (itemtab[c].i_token == MPARM) {
			itemtab[c].i_token = UNDECLARED;
		}
	inmlex = 0;
#ifdef	M_DEBUG
	fprintf(stdout,"exit 'mlex'\n");
#endif
}



/*
 *  lexical analyser for the arguments of a macro call
 */
int getarg()
{
	int c;
	char *p;
	static int comma;

	*tempbuf = 0;
	yylval.cval = tempbuf;
	while(charclass[c = nextchar()] == SPACE);

	switch(c) {

	case '\0':
		popsi();
	case '\n':
	case ';':
		comma = 0;
		return(skipline(c));

	case ',':
		if (comma) {
			comma = 0;
			return(',');
		}
		else {
			comma++;
			return(ARG);
		}

	case '\'':
		p = tempbuf;
		do switch (c = nextchar()) {
			case '\0':
			case '\n':
				peekc = c;
				*p = 0;
				err[bflag]++;
				return(ARG);
			case '\'':
				if ((c = nextchar()) != '\'') {
					peekc = c;
					*p = '\0';
					comma++;
					return(ARG);
				}
			default:
				*p++ = c;
		} while (p < tempmax);
		error(symlong);		/* doesn't return */

	default:  /* unquoted string */
		p = tempbuf;
		peekc = c;
		do switch(c = nextchar()) {
			case '\0':
			case '\n':
			case '\t':
			case ' ':
			case ',':
				peekc = c;
				*p = '\0';
				comma++;
				return(ARG);
			default:
				*p++ = c;
		} while (p < tempmax);
	}

	/* in practice it can't get here, but FWIW and to satisfy
	 * -Wall... -rjm */
	error("can't happen - in zmac.y:getarg(), infinite unquoted string!?");
	return(0);
}



/*
 * Add suffix to pathname if leafname doesn't already have a suffix.
 * The suffix passed should not include an extension separator.
 * The pathname passed should be in local format.
 */
void suffix_if_none (char *str, char *suff)
{
	char *leafname, *extension;

	leafname = strrchr (str, OS_DIR_SEP);
	if (leafname == NULL)
	{
		leafname = str;
	}

	extension = strchr (leafname, OS_EXT_SEP);
	if (extension == NULL)
	{
		size_t leafsize = strlen (leafname);

		leafname[leafsize] = OS_EXT_SEP;
		strcpy (leafname + leafsize + 1, suff);
	}
}



/*
 * Add or change pathname suffix.
 * The suffix passed should not include an extension separator.
 * The pathname passed should be in local format.
 * If the leafname passed has more than one extension, the last is changed.
 */
void suffix (char *str, char *suff)
{
	char *leafname, *extension;

	leafname = strrchr (str, OS_DIR_SEP);
	if (leafname == NULL)
	{
		leafname = str;
	}

	extension = strrchr (leafname, OS_EXT_SEP);
	if (extension == NULL)
	{
		extension = leafname + strlen (leafname);
	}

	*extension = OS_EXT_SEP;
	strcpy (extension + 1, suff);
}



/*
 * Decanonicalise a canonical pathname.
 * A canonical pathname uses '/' as the directory separator,
 * '.' as the extension separator, ".." as the parent directory,
 * "." as the current directory, and a leading '/' as the root
 * directory (it would be more user-friendly not to use this!).
 */
void decanonicalise (char *pathname)
{
#if defined (MSDOS)

	char *directory = pathname;

	/* Just need to change all '/'s to '\'s */

	while ((directory = strchr (directory, '/')) != NULL)
	{
		*directory = OS_DIR_SEP;
	}

#elif defined (__riscos)

	char *directory = pathname, *dirend;

	/* First deal with leading '/' */

	if (*directory == '/')
	{
		memmove (directory + 1, directory, strlen (directory) + 1);
		*directory = '$';
		++directory;
	}

	/* Then deal with non-leaf ".."s and "."s */

	while (1)
	{
		dirend = strchr (directory, '/');
		if (dirend == NULL)
		{
			break;
		}

		*dirend = '\0';

		if (strcmp (directory, "..") == 0)
		{
			*directory = '^';
			memmove (directory + 2, directory + 3, strlen (directory + 3) + 1);
			dirend = directory + 1;
		}
		else if (strcmp (directory, ".") == 0)
		{
			memmove (directory, directory + 2, strlen (directory + 2) + 1);
			continue;
		}

		*dirend = '/';
		directory = dirend + 1;
	}

	directory = pathname;

	/* Finally, swap '/' and '.' */

	while ((directory = strpbrk (directory, "/.")) != NULL)
	{
		if (*directory == '/')
		{
			*directory = OS_DIR_SEP;
		}
		else
		{
			*directory = OS_EXT_SEP;
		}
		++directory;
	}

#else

	/* Local form is canonical form */

	UNUSED (pathname);

#endif
}



/*
 *  put out a byte to the macro file, keeping the offset
 */
void putm(char c)
{
#ifdef M_DEBUG
	fputc (c, stdout);
#endif
	mfptr++;
	mfputc(c,mfile) ;
}



/*
 *  get a byte from the macro file
 */
int getm()
{
	int ch;

	floc++;
	ch = mfgetc(mfile) ;
	if (ch == EOF) {
		ch = 0;
		fprintf(stdout,"bad macro read\n");
	}
	return(ch);
}



/*
 *  pop standard input
 */
void popsi()
{
	int  i;

	if (est)
	{
		for (i=0; i<PARMMAX; i++) {
			if (est[i])
#ifdef M_DEBUG
	fprintf (stdout, "[Freeing arg%u(%p)]\n", i, est[i]),
#endif
					free(est[i]);
		}
		floc = est[FLOC];
		free(est);
		expptr--;
		est = expptr ? (char **) expstack[expptr-1] : (char **) 0;
		mfseek(mfile, (long)floc, 0);
		if (lineptr > linebuf) lineptr--;
	}
}



/*
 *  return a unique name for a local symbol
 *  c is the parameter number, n is the macro number.
 */

char *getlocal(int c, int n)
{
	static char local_label[10];

	invented++;
	if (c >= 26)
		c += 'a' - '0';
	sprintf(local_label, "?%c%04d", c+'a', n) ;
	return(local_label);
}



/*
 *  read in a symbol table
 */
void insymtab(char *name)
{
	struct stab *t;
	int  s, i;
	FILE *sfile;

	t = (struct stab *) tempbuf;
	decanonicalise (name);
	if ((sfile = fopen(name, "rb")) == NULL)
		return;
	fread((char *)t, 1, sizeof *t, sfile);
	if (t->t_value != SYMMAJIC)
	{
		fclose (sfile);
		return;
	}

	s = t->t_token;
	for (i=0; i<s; i++) {
		fread((char *)t, 1, sizeof *t, sfile);
		if (tokenofitem(UNDECLARED) != UNDECLARED)
			continue;
		yylval.itemptr->i_token = t->t_token;
		yylval.itemptr->i_value = t->t_value;
		if (t->t_token == MACRO)
			yylval.itemptr->i_value += mfptr;
	}

	while ((s = fread(tempbuf, 1, TEMPBUFSIZE, sfile)) > 0) {
		mfptr += s;
		mfwrite(tempbuf, 1, s, mfile) ;
	}

	fclose (sfile);
}



/*
 *  write out symbol table
 */
void outsymtab(char *name)
{
	struct stab *t;
	struct item *ip;
	int  i;
	FILE *sfile;

	t = (struct stab *) tempbuf;
	decanonicalise (name);
	if ((sfile = fopen(name, "wb")) == NULL)
		return;
	for (ip=itemtab; ip<itemmax; ip++) {
		if (ip->i_token == UNDECLARED) {
			ip->i_token = 0;
			nitems--;
		}
	}

	copyname(title, (char *)t);
	t->t_value = SYMMAJIC;
	t->t_token = nitems;
	fwrite((char *)t, 1, sizeof *t, sfile);

	for (ip=itemtab; ip<itemmax; ip++) {
		if (ip->i_token != 0) {
			t->t_token = ip->i_token;
			t->t_value = ip->i_value;
			copyname(ip->i_string, (char *)t);
			fwrite((char *)t, 1, sizeof *t, sfile);
		}
	}

	mfseek(mfile, (long)0, 0);
	while((i = mfread(tempbuf, 1, TEMPBUFSIZE, mfile) ) > 0)
		fwrite(tempbuf, 1, i, sfile);

	fclose (sfile);
}



/*
 *  copy a name into the symbol file
 */
void copyname(char *st1, char *st2)
{
	char  *s1, *s2;
	int  i;

	i = (MAXSYMBOLSIZE+2) & ~01;
	s1 = st1;
	s2 = st2;

	while((*s2++ = *s1++)) i--;		/* -Wall-ishness :-) -RJM */
	while(--i > 0) *s2++ = '\0';
}

/* get the next source file */
void next_source(char *sp)
{

	if(now_in == NEST_IN -1)
		error("Too many nested includes") ;
	decanonicalise (sp);
	if ((now_file = fopen(sp, "r")) == NULL) {
#ifdef __riscos
		if (riscos_thbk)
			riscos_throwback(2,src_name[now_in],linein[now_in],"Cannot open include file");
#endif
		fileerror("Cannot open include file", sp) ;
	}
	if (pass2 && iflist()) {
		lineout() ;
		fprintf(fout, "**** %s ****\n",sp) ;
	}

	/* save the list control flag with the current line number */
	if (lstoff)
		linein[now_in] = - linein[now_in] ;

	/* no list if include files are turned off */
	lstoff |= iopt ;

	/* save the new file descriptor. */
	fin[++now_in] = now_file ;
	/* start with line 0 */
	linein[now_in] = 0 ;
	/* save away the file name */
	src_name[now_in] = malloc(strlen(sp)+1) ;
	strcpy(src_name[now_in],sp) ;
}


/* INCBIN. Put binary file */
void doincbin(char *sp)
{
    int  i,	// число прочит. байтов
         p,	// local counter
         sz;	// size incbin-file


    if(!pass2) {	// pass 1
       decanonicalise(sp);		// обработка заданного пути к файлу
       if((now_binfile = fopen(sp, "rb")) == NULL)
           fileerror("Cannot open incbin-file", sp);
       fseek(now_binfile, 0L, SEEK_END);
       sz = ftell(now_binfile);		// file size
       fclose(now_binfile);
       dollarsign = dollarsign + sz;
    }
    else
       {

         decanonicalise(sp);
         if((now_binfile = fopen(sp, "rb")) == NULL)
             fileerror("Cannot open incbin-file", sp);

         while((i = fread(temp_binbuf, 1, TEMPBINBUFSIZE, now_binfile)) > 0)
              for(p = 0; p < i; p++) {
                    putbin(temp_binbuf[p]);	/* вставить по-блочно файл */
                    dollarsign++;
              }
         fclose(now_binfile);
       }

/*  if(pass2) {
       if(dollarsign > 65535) {
           puts("Warning: overlapping 64kB !\n");
           if(iflist()) {
               lineout();
               if(nopt)
                   fprintf(fout, "**** Warning: overlapping 64kB ! ****\n");
           }
       }
    } */
}


#ifdef __riscos
/*
 * On entry sp should point to the full pathname of a file in RISC OS form.
 * Searches for the last dot, and sets the local CSD to that path.
 * Does not corrupt the string.
 */
void riscos_set_csd(char *sp)
{
	char *s1 = strrchr (sp, '.');

	if (s1 != NULL)
	{
		*s1=0;
		_swix(DDEUtils_Prefix,1,sp);
		*s1='.';
	}
}

void riscos_throwback(int severity, char *file, int line, char *error)
{
	if (riscos_throwback_started==0)
	{
		riscos_throwback_started=1;
		*riscos_thbkf=0;
		_swix(DDEUtils_ThrowbackStart,0);
	}
	if (strcmp(file, riscos_thbkf)!=0)
	{
		_swix(DDEUtils_ThrowbackSend,4+1,0,file); /* Notify of a change of file */
		strcpy(riscos_thbkf,file);
	}
	_swix(DDEUtils_ThrowbackSend,32+16+8+4+1,1,file,line,severity,error);
}
#endif
