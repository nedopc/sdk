# Makefile for WATCOM 11.0
#
# usage: wmake /f makefile.wat
#


CC = wcl386
CFLAGS = -fp5 -fpi87 -ox


SRC	= zmac.c mio.c getoptn.c

all:	zmac.exe

zmac.exe: $(SRC)
	$(CC) $(CFLAGS) $(SRC)
