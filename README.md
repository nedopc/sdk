# nedoPC SDK

This repository was imported from SourceForge project nedopc/src and extended by other software from nedoPC.org

Moved to GitLab in June 2018: https://gitlab.com/nedopc/sdk/

## Status

**nedoPC SDK** is a software development kit for DIY and RETRO computers that consists of

- Robby bytecode compiler ```robbyc```
- Robby bytecode cross-compiler to arbitrary code```robbycc```
- JS-framework with Robby virtual machine ```rgrid```
- Graphical emulator of i8080 computer Orion ```orionix```
- Textual emulator of i8080 computer Radio-86RK ```pseudo-86rk```
- Simple command interpreter ```nedomake```
- Universal assembler ```rasm```
- Public domain Z80 assembler ```zmac``` (see below)
- Libraries and rules how to compile Robby bytecode to i8080 and z80 code (will be more later)
- Plus some additional tools and utilities

## Install on Linux

To get and build entire SDK on Linux system:

    git clone https://gitlab.com/nedopc/sdk.git nedoPC.org
    cd nedoPC.org/sdk
    make

Note: it will not build ```orionix``` and ```vultured``` because they require SDL - you may
build them separately if needed using make

## Credits

Alexander "Shaos" Shabarshin develops **nedoPC SDK** since about 1996 (from 2001 to 2004 it was called RW1P2),
but some parts were borrowed from other sources:

- ```sdk/LIB/I8080/_CLIB.A``` was based on Small-C Library CLIB.ASM (c) 1984 J.E.Hendrix, L.E.Payne (CLIB.ARC)

- ```sdk/LIB/Z80/_CLIB.A``` was based on code above that was automatically translated to Z80 mnemonics, so it's still (c) 1984 J.E.Hendrix, L.E.Payne

- ```sdk/bin2trd.c``` was based on BIN2TRD (c) 1999 Copper Feet (Vyacheslav Mednonogov)

- ```zmac``` is PUBLIC DOMAIN Z80 macro cross-assembler developed since 1978 by Bruce Norskog, John Providenza, Colin Kelley, Russell Marks, Mark Rison (up to v1.3) and some last changes were done by Vasil Ivanov (up to v1.33)
