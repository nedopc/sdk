<?php header('Content-Type: text/plain; charset=utf-8');

// THIS CODE IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISK!!!

include('../decode.php');

if(isset($_GET["s"])) robbyc($_GET["s"]);

function robbyc($source)
{
 $source2 = str_replace('/','0',$source);
 $source3 = str_replace('.','0',$source2);
 $source4 = str_replace('_','0',$source3);
 if(!ctype_alnum($source4)) return;
 $filename = dirname(__FILE__).'/robots/'.$source4;
 if(file_exists($filename.'.rjs'))
 {
  echo 'var robot = '.file_get_contents($filename.'.rjs');
  return;
 }
 ob_start();
 decode($source);
 $robot = ob_get_clean();
 if(empty($robot))
 {
   echo 'var robot = {"error":"Not exist"}';
   return;
 }
 if(stristr($robot,'robot')===false || stristr($robot,'author')===false)
 {
   echo 'var robot = {"error":"Not a robot"}';
   return;
 }
 file_put_contents($filename.'.Robot',$robot,LOCK_EX);
 $output = shell_exec('robbyc -p -l -r -n '.$filename.'.Robot');
 $error = '';
 $prev = '';
 $lines = explode("\n",$output);
 foreach($lines as $line)
 {
   if(strstr($line,'ERROR')!==false)
   {
      $error = $prev.'\n'.$line;
   }
   if(empty($line)) continue;
   $prev = $line;
 }
 echo 'var robot = ';
 if(empty($error))
 {
    echo file_get_contents($filename.'.rjs');
 }
 else
 {
    $errobj = '{"error":"'.$error.'"}';
    echo $errobj;
    file_put_contents($filename.'.rjs',$errobj);
 }
}
?>
