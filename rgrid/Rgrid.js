/*
    Rgrid.js = AJAX engine + ROBBY virtual machine (see http://rgrid.net)

    Copyright (C) 2012-2025 Alexander Shabarshin <me@shaos.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

var $R = {
  lib_name: "Rgrid",
  ver_major: 0,
  ver_minor: 9,
  copyright: "(c) 2012-2025, Alexander Shabarshin",
  license: "LGPL",
  sourcecode: "https://gitlab.com/nedopc/sdk/-/tree/master/rgrid",
  cache: {},
  threads: [],
  started: 0,
  table_prefix: "rtab",
  gif_prefix: "",
  opcache: {},
  robots: [null],
  queue: [],
  qindex: [null],
  files: {},
  commands: {},
  areaf: null,
  gtime: 0
};

$R.version = function() {
  return "v" + this.ver_major +
         "." + this.ver_minor;
};

$R.powered = function() {
  return "<a href=\"" + this.sourcecode+"\" target=\"_blank\">" + this.lib_name + "</a> " + this.version() + " " + this.copyright;
};

$R.clear = function() {
  this.cache = {};
};

function $(s) {
  return document.getElementById(s);
}

function $_(s) {
  var e = $(s);
  var v = arguments[1];
  if(e!=null)
  {
    if(v==null) {
      if(e.type=='checkbox' || e.type=='radio') {
        return e.checked?'1':'0';
      } else if(e.value!=null) {
        return e.value;
      } else {
        return e.innerHTML;
      }
    } else {
      if(e.type=='checkbox' || e.type=='radio') {
        if(v=='N' || v=='F' || v=='off') {
           e.checked = false;
        } else if(v=='Y' || v=='T' || v=='on' || eval("("+v+")")) {
           e.checked = true;
        } else {
           e.checked = false;
        }
        v = e.checked?'1':'0';
      } else if(e.value!=null) {
        e.value = v;
      } else {
        e.innerHTML = v;
      }
      return v;
    }
  }
  return null;
}

function $__(s,u) {
  var r = null;
  var p = arguments[2];
  try {
    r = new XMLHttpRequest();
  } catch(e) {
    r = null;
  }
  if(r!=null)
  {
    r.onreadystatechange = function() {
      if(r.readyState==4 && r.status==200) {
        if(typeof s == 'string') {
          $_(s,r.responseText);
        }
        if(typeof s == 'function') {
          s(r.responseText);
        }
      }
    }
    if(p==null) {
      r.open("GET",u,true);
      r.send(null);
    } else {
      r.open("POST",u,true);
      r.send(p);
    }
  }
}

function $$(s) {
  return $R.select(s);
}

function $$$(s) {
  return $map($$(s),function(e){return e.id;});
}

function $append(s,t) {
  var e = $(s);
  if(e==null) return 0;
  e.insertAdjacentHTML('beforeend',t);
  return 1;
}

function $time() {
  return Math.floor(+new Date/1000);
}

function $random(r) {
  return Math.floor(r*Math.random());
}

function $map(a,f) {
  var r = [];
  for(var i in a) {
    r.push(f(a[i]));
  }
  return r;
}

function $reduce(a,b,f) {
  for(var i in a) {
    b = f(b,a[i]);
  }
  return b;
}

function $filter(a,f) {
  var r = [];
  for(var i in a) {
    if(f(a[i])) {
      r.push(a[i]);
    }
  }
  return r;
}

function $hex(i,j) {
 var k,l,m=1,s="",r=i,g=1;
 var h=["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
 if(r<0) {
   g = -1;
   r = -i;
 }
 for(k=1;k<j;k++) {
   m = m*16;
 }
 if(r >= m*16) {
   g = 0;
   r = r&(m*16-1);
 }
 for(k=0;k<j;k++) {
   l = Math.floor(r/m);
   r = r-l*m;
   m = m/16;
   s += h[l];
 }
 if(g<0) {
  return "-" + s;
 }
 if(g==0) {
  return "~" + s;
 }
 return s;
}

$R.iterate = function() {
  var node = arguments[0];
  if(node==null) {
    this.cache["*"] = [];
    $R.iterate(document.body);
  } else {
    $map(node.childNodes,
       function(e) {
         if(e.id!=null && e.id!="") {
           $R.cache["*"].push(e);
         }
         $R.iterate(e);
         return null;
       }
      );
  }
}

$R.select = function(s) {
  if(this.cache["*"]==null) {
    $R.iterate();
  }
  if(this.cache[s]==null) {
    var regexp = s.replace(/\./g,"\\.").replace(/\*/g,".*");
    this.cache[s] = $filter(this.cache["*"],
                            function(e) {
                              return e.id.search(regexp)==0;
                            }
                           );
  }
  return this.cache[s];
};

$R.create = function(p,t,s) {
  if($(s)!=null) return 0;
  var e = $(p);
  if(e==null) return 0;
  var n = document.createElement(t);
  if(n==null) return 0;
  n.id = s;
  e.appendChild(n);
  this.clear();
  return 1;
}

$R.remove = function(s) {
  var e = $(s);
  if(e==null) return 0;
  var p = e.parentNode;
  p.removeChild(e);
  this.clear();
  return 1;
}

$R.table = function(p,h,w,s) {
  var d = arguments[4];
  var ts = s+"~";
  if($(p)==null) return 0;
  if($(s)!=null) return 0;
  if($(ts)!=null) return 0;
  if($$(s+'.*').lendth > 0) return 0;
  $(p).innerHTML = "";
  var r = this.create(p,"table",ts);
  var t = $(ts);
  t.setAttribute("cellPadding","0");
  t.setAttribute("cellSpacing","0");
  t.setAttribute("border","0");
  r += this.create(ts,"tbody",s);
  var i,j;
  for(j=0;j<h;j++) {
    var tr = s+"."+j;
    r += this.create(s,"tr",tr);
    for(i=0;i<w;i++) {
      var td = tr+"."+i;
      r += this.create(tr,"td",td);
      if(d!=null) $_(td,d);
      else $_(td,"&nbsp;");
    }
  }
  return r;
}

$R.thread = function(obj) {
 var m = this.threads.length;
 this.threads[m] = {o:obj,c:[],a:0,p:1};
 return m;
}

$R.stop = function() {
 if(this.started!=0) {
   clearInterval(this.started);
   this.started = 0;
 }
}

$R.start = function() {
 // optional percentage
 var p = arguments[0];
 if(p==null) p=16; // 0.8 duty
 else p=p/5; // for 20 ms
 this.stop();
 var sf = "";
 for(var t in this.threads) {
  sf += "var t"+t+"=$R.threads["+t+"];if(t"+t+".f) t"+t+".a=t"+t+".f(t"+t+".o);";
 }
 sf += "var t=+new Date;while(+ new Date-t<"+p+"){";
 var i,j=0;
 while(j<100) {
  for(var t in this.threads) {
   for(i=0;i<this.threads[t].p;i++) {
    sf += "t"+t+".a=t"+t+".c[t"+t+".a](t"+t+".o);";
    j++;
   }
  }
 }
 sf += "}";
 this.started = setInterval(new Function(sf),20);
}

// $R.grid - initialize RobbyVM and create screen for it
// s - id of the element where we will add a table
// w - width of the table (number of columns)
// h - height of the table (number of rows)
// f - handler for mouse clicks
// fs (opt) - handler for selected areas

$R.grid = function(s,w,h,f) {
  var fs = arguments[4];
  if(fs) {
    this.areaf = fs;
  }
  var r = $R.table(s,h,w,this.table_prefix,"<img src=" + this.gif_prefix + "0000.gif>");
  if(r > 0) {
    $map($$(this.table_prefix+'.*.*'),function(e){
      var a = e.id.split('.');
      e.onclick = function(){f(parseInt(a[2]),parseInt(a[1]))};
      e.setAttribute("param0","0");
      e.setAttribute("param1","0");
    });
  }
  return r;
};


// $R.add - add robot to the list of robots
// o - robot object or JSON-string of the robot

$R.add = function(o) {
  var ro = o;
  if(typeof o == 'string') {
   ro = eval("("+o+")");
  }
  if(this.opcache["x00"]==null) {
    this.opcache["x00"] = function(r) {
      return ++r.regs[13];
    };
    this.opcache["x33"] = function(r) {
      if(r.regs[15]<=0) {
        r.regs[13] = r.code.length-1;
      } else {
        r.regs[13] = r.regs[256-(r.regs[15]--)];
      }
      return r.regs[13];
    };
    this.opcache["x63"] = function(r) {
      var v = 0;
      if(r.regs[15]>0) {
        v = r.regs[256-(r.regs[15]--)];
      }
      regs[14] = v;
      return ++r.regs[13];
    };
    this.opcache["x73"] = function(r) {
      r.regs[256-(++r.regs[15])] = r.regs[14];
      return ++r.regs[13];
    };
    this.opcache["xFF"] = function(r) {
      r.regs[15] = -1;
      return r.regs[13];
    };
  }
  var m = this.robots.push(ro)-1;
  var i,j,j2,k,k2,l,n,v,h,t1,t2;
  this.qindex[m] = this.queue.length;
  ro.regs[9] = m;
  var t = this.thread(o);
  var f = function(r) {
    alert("Unexpected jump to 0x"+r.regs[13].toString(16));
    return r.code.length-1;
  }
  for(i=0;i<ro.code.length;i++) {
    this.threads[t].c[i] = f;
  }
  this.threads[t].f = function(r) {
    if(r.regs[9]==1) {
      $R.gtime++;
    }
    if(r.regs[13]==null) {
      for(i=0;i<256;i++) {
        if(i!=9) {
          r.regs[i] = 0;
        }
      }
    }
    if(r.vars[0]==null) {
      for(i=0;i<r.varm;i++) {
          r.vars[i] = 0;
      }
    }
    if(r.platform!=1) {
      $R.write(r,0xFF06,$R.gtime&0xFFFF);
      $R.write(r,0xFF16,$R.gtime>>16); 
    }
    return r.regs[13];
  }
  var hh = function(aa,ii,ss) {
    var iii,kkk=ii+ss,sss="";
    for(iii=ii;iii<kkk;iii++) {
      sss += $hex(aa[iii],2);
    }
    return "x"+sss;
  }
  var sss = "";
  var go = 1;
  i = 0;
  while(1) {
    switch(ro.code[i]) {
      case 0x01:
        h = hh(ro.code,i,5);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","r.regs[13]+=5;return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i += 4;
        break;
      case 0x02:
        j = ro.code[i+5]+(ro.code[i+6]<<8);
        k = j + j + 7;
        h = hh(ro.code,i,k);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]+="+k+";";
          n = ro.code[i+1]+(ro.code[i+2]<<8);
          for(j=7;j<k;j+=2) {
            v = ro.code[j]+(ro.code[j+1]<<8);
            if(v >= 0x8000) {
              v -= 0x10000;
            }
            sss += "r.vars["+n+"]="+v+";";
            n++;
          }
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i += k-1;
        break;
      case 0x20:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "$R.write(r";
          if(t1==0 || t1==1) {
            sss += ","+j;
          } else if(t1==2) {
            sss += ",$R.index(r,"+j+","+j2+")";
          }
          if(t2==0) {
            sss += ","+k+");";
          } else if(t2==1) {
            sss += ",$R.read(r,"+k+"));";
          } else if(t2==2) {
            sss += ",$R.read(r,$R.index(r,"+k+","+k2+")));";
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x37:
        n = i + 1;
        l = 2;
        sss = "";
        while(ro.code[n++]) {
          sss += String.fromCharCode(ro.code[n-1]);
          l++;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","alert(\""+sss+"\");r.regs[13]+="+l+";return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x38:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var t";
          if(t1==0) {
            sss += "="+j+";";
          } else if(t1==1) {
            sss += "=$R.read(r,"+j+");";
          } else if(t1==2) {
            sss += "=$R.read(r,$R.index(r,"+j+","+j2+"));";
          }
          sss += "var h=t;if(h<0){h+=0x10000;}";
          sss += "alert(\"[#\"+$hex(r.regs[13],4)+\"] #\"+$hex(h,4)+\" (\"+t+\")\");";
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x3F:
        t1 = ro.code[i+1];
        t2 = ro.code[i+2];
        h = hh(ro.code,i,3);
        f = this.opcache[h];
        if(f==null) {
          sss = "var i,s=\"\";for(i="+t1+";i<"+r.varm+";i++){s+=\" \"+$hex(r.vars[i],4);if(((i-"+t1+")%"+t2+")=="+t2+"-1){ss+=\"\\n\";}}";
          sss += "alert(s);r.regs[13]+=3;return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        i += 2;
        break;
      case 0x40:
        l = ro.code[i+1] + 2;
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var t,s=256-r.regs[15];";
          n = i + 2;
          go = 1;
          while(go) {
           switch(ro.code[n]) {
            case 0x80:
              sss += "r.regs[s+1]=(r.regs[s+1]&&r.regs[s])?1:0;s++;";
              break;
            case 0x81:
              sss += "r.regs[s+1]=(r.regs[s+1]||r.regs[s])?1:0;s++;";
              break;
            case 0x90:
              sss += "r.regs[s+1]=(r.regs[s+1]==r.regs[s])?1:0;s++;";
              break;
            case 0x91:
              sss += "r.regs[s+1]=(r.regs[s+1]!=r.regs[s])?1:0;s++;";
              break;
            case 0x92:
              sss += "r.regs[s+1]=(r.regs[s+1]>r.regs[s])?1:0;s++;";
              break;
            case 0x93:
              sss += "r.regs[s+1]=(r.regs[s+1]<r.regs[s])?1:0;s++;";
              break;
            case 0x94:
              sss += "r.regs[s+1]=(r.regs[s+1]>=r.regs[s])?1:0;s++;";
              break;
            case 0x95:
              sss += "r.regs[s+1]=(r.regs[s+1]<=r.regs[s])?1:0;s++;";
              break;
            case 0xA0:
              sss += "r.regs[s+1]=r.regs[s+1]+r.regs[s];s++;";
              break;
            case 0xA1:
              sss += "r.regs[s+1]=r.regs[s+1]-r.regs[s];s++;";
              break;
            case 0xB0:
              sss += "r.regs[s+1]=r.regs[s+1]*r.regs[s];s++;";
              break;
            case 0xB1:
              sss += "r.regs[s+1]=Math.floor(r.regs[s+1]/r.regs[s]);s++;";
              break;
            case 0xB2:
              sss += "r.regs[s+1]=r.regs[s+1]%r.regs[s];s++;";
              break;
            case 0xC0:
              sss += "r.regs[s+1]=r.regs[s+1]&r.regs[s];s++;";
              break
            case 0xC1:
              sss += "r.regs[s+1]=r.regs[s+1]|r.regs[s];s++;";
              break;
            case 0xC2:
              sss += "r.regs[s+1]=r.regs[s+1]^r.regs[s];s++;";
              break;
            case 0xD0:
              sss += "r.regs[s+1]=r.regs[s+1]>>r.regs[s];s++;";
              break;
            case 0xD1:
              sss += "r.regs[s+1]=r.regs[s+1]<<r.regs[s];s++;";
              break;
            case 0xE0:
              sss += "r.regs[s]=-r.regs[s];";
              break;
            case 0xE1:
              sss += "r.regs[s]=~r.regs[s];";
              break;
            case 0xE2:
              sss += "r.regs[s]=r.regs[s]?0:1;";
              break;
            case 0xF0:
              sss += "if(r.regs[s+2]){r.regs[s+2]=r.regs[s+1];}else{r.regs[s+2]=r.regs[s];}s+=2;";
              break;
            case 0xF1:
              sss += "s--;r.regs[s]=r.regs[s+1];";
              break;
            case 0xF2:
              sss += "t=r.regs[s];r.regs[s]=r.regs[s+1];r.regs[s+1]=t;";
              break;
            case 0xF3:
              sss += "r.regs[s]=$R.read(r,r.regs[s]);"
              break;
            case 0xF4:
              sss += "t=r.regs[s];$R.write(r,r.regs[s+1],t);s+=2;$R.write(r,0xFF0E,t);$R.write(r,0xFF1E,t>>16);";
              break;
            case 0xF5:
              k = ro.code[n+1]+(ro.code[n+2]<<8);
              if(k>=0x8000) {
                k -= 0x10000;
              }
              sss += "r.regs[--s]="+k+";";
              n+=2;
              break;
            case 0xF6:
              sss += "r.regs[s+1]=$R.index(r,r.regs[s+1],r.regs[s],1);s++;";
              break;
            default:
              alert("Invalid long expression at 0x"+i.toString(16));
              go = 0;
           }
           if(++n-i>=l) break;
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i += l - 1;
        break;
      case 0x41:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]+="+l+";if";
          if(t1==0) {
            sss += "("+j+")";
          } else if(t1==1) {
            sss += "($R.read(r,"+j+"))";
          } else if(t1==2) {
            sss += "($R.index(r,"+j+","+j2+"))";
          }
          sss += "{r.regs[13]";
          if(t2==0) {
            sss += "="+k+";}";
          } else if(t2==1) {
            sss += "=$R.read(r,"+k+");}";
          } else if(t2==2) {
            sss += "=$R.read(r,$R.index(r,"+k+","+k2+"));}";
          }
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x42:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]+="+l+";if";
          if(t1==0) {
            sss += "(!"+j+")";
          } else if(t1==1) {
            sss += "(!$R.read(r,"+j+"))";
          } else if(t1==2) {
            sss += "(!$R.index(r,"+j+","+j2+"))";
          }
          sss += "{r.regs[13]";
          if(t2==0) {
            sss += "="+k+";}";
          } else if(t2==1) {
            sss += "=$R.read(r,"+k+");}";
          } else if(t2==2) {
            sss += "=$R.read(r,$R.index(r,"+k+","+k2+"));}";
          }
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x43:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]";
          if(t1==0) {
            sss += "="+j+";";
          } else if(t1==1) {
            sss += "=$R.read(r,"+j+");";
          } else if(t1==2) {
            sss += "=$R.read(r,$R.index(r,"+j+","+j2+"));";
          }
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x44:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[256-(++r.regs[15])]=r.regs[13]+"+l+";r.regs[13]";
          if(t1==0) {
            sss += "="+j+";";
          } else if(t1==1) {
            sss += "=$R.read(r,"+j+");";
          } else if(t1==2) {
            sss += "=$R.read(r,$R.index(r,"+j+","+j2+"));";
          }
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x51:
        n = i + 1;
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        l = 4;
        sss = "";
        while(ro.code[n++]) {
          sss += String.fromCharCode(ro.code[n-1]);
          l++;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","alert(\"TEST is not supported!\");r.regs[13]+="+l+";return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x52:
        n = i + 1;
        l = 2;
        sss = "";
        while(ro.code[n++]) {
          sss += String.fromCharCode(ro.code[n-1]);
          l++;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","alert(\"TEST is not supported!\");r.regs[13]+="+l+";return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x60:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var a";
          if(t1==0 || t1==1) {
            sss += "="+j+";";
          } else if(t1==2) {
            sss += "=$R.index(r,"+j+","+j2+");";
          }
          sss += "var i;for(i=0;i<16;i++){$R.write(r,a+i,$R.read(r,0xFF00+i));}";
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x61:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var a";
          if(t1==0) {
            sss += "="+j+";";
          } else if(t1==1) {
            sss += "=$R.read(r,"+j+");";
          } else if(t1==2) {
            sss += "=$R.read(r,$R.index(r,"+j+","+j2+"));";
          }
          sss += "var b";
          if(t2==0) {
            sss += "="+k+";";
          } else if(t2==1) {
            sss += "=$R.read(r,"+k+");";
          } else if(t2==2) {
            sss += "=$R.read(r,$R.index(r,"+k+","+k2+"));";
          }
          sss += "$R.event(b,r.regs[9],a,r.regs[0],r.regs[1]);";
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x62:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]+="+l+";var c,i=r.regs[9];var e=$R.queue[$R.qindex[i]];";
          sss += "if(e&&(e[0]==i||(e[0]==0&&e[1]!=i))){if(e.length>6){c=e[6];}else{c=e[5];}";
          if(t1==0 || t1==1) {
            sss += "$R.write(r,"+j+",c);";
          } else if(t1==2) {
            sss += "$R.write(r,$R.index(r,"+j+","+j2+"),c);";
          }
          sss += "r.regs[3]=e[1];r.regs[0]=e[2];r.regs[1]=e[3];r.regs[4]=e[4];";
          sss += "}else{r.regs[3]=0;}";
          sss += "if(e){$R.qindex[i]++;}";
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x66:
        n = i + 1;
        l = 2;
        sss = "";
        while(ro.code[n++]) {
          sss += String.fromCharCode(ro.code[n-1]);
          l++;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","alert(\"TEXT is not supported!\");r.regs[13]+="+l+";return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x69:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[0]";
          if(t1==0) {
            sss += "="+j+";";
          } else if(t1==1) {
            sss += "=$R.read(r,"+j+");";
          } else if(t1==2) {
            sss += "=$R.read(r,$R.index(r,"+j+","+j2+"));";
          }
          sss += "r.regs[1]";
          if(t2==0) {
            sss += "="+k+";";
          } else if(t2==1) {
            sss += "=$R.read(r,"+k+");";
          } else if(t2==2) {
            sss += "=$R.read(r,$R.index(r,"+k+","+k2+"));";
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x6A:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "$R.set(r.regs[0],r.regs[1]";
          if(t1==0) {
            sss += ","+j;
          } else if(t1==1) {
            sss += ",$R.read(r,"+j+")";
          } else if(t1==2) {
            sss += ",$R.read(r,$R.index(r,"+j+","+j2+"))";
          }
          if(t2==0) {
            sss += ","+k+");";
          } else if(t2==1) {
            sss += ",$R.read(r,"+k+"));";
          } else if(t2==2) {
            sss += ",$R.read(r,$R.index(r,"+k+","+k2+")));";
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x6B:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[14]=$R.get(r.regs[0],r.regs[1]";
          if(t1==0) {
            sss += ","+j+");";
          } else if(t1==1) {
            sss += ",$R.read(r,"+j+"));";
          } else if(t1==2) {
            sss += ",$R.read(r,$R.index(r,"+j+","+j2+")));";
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x6C:
        n = i + 1;
        l = 2;
        sss = "";
        while(ro.code[n++]) {
          sss += String.fromCharCode(ro.code[n-1]);
          l++;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          f = new Function("r","alert(\"ASM is not supported!\");r.regs[13]+="+l+";return r.regs[13];");
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x70:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var a";
          if(t1==0 || t1==1) {
            sss += "="+j+";";
          } else if(t1==2) {
            sss += "=$R.index(r,"+j+","+j2+");";
          }
          sss += "var b";
          if(t2==0 || t2==1) {
            sss += "="+k+";";
          } else if(t2==2) {
            sss += "=$R.index(r,"+k+","+k2+");";
          }
          sss += "var i,z=$R.read(r,b);$R.write(r,a,z);";
          sss += "for(i=1;i<=z;i++){$R.write(r,a+i,$R.read(r,b+i));}";
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x71:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        t2 = ro.code[n++];
        k = ro.code[n++];
        k += ro.code[n++]<<8;
        if(t2==2) {
          k2 = ro.code[n++];
          k2 += ro.code[n++]<<8;
        }
        l = 7;
        if(t1==2) {
         l+=2;
        }
        if(t2==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "var a";
          if(t1==0 || t1==1) {
            sss += "="+j+";";
          } else if(t1==2) {
            sss += "=$R.index(r,"+j+","+j2+");";
          }
          sss += "var b";
          if(t2==0) {
            sss += "="+k+";";
          } else if(t2==1) {
            sss += "=$R.read(r,"+k+");";
          } else if(t2==2) {
            sss += "=$R.read(r,$R.index(r,"+k+","+k2+"));";
          }
          sss += "var i,z=$R.read(r,a);var aa=[z];";
          sss += "for(i=1;i<=z;i++){aa.push($R.read(r,a+i));}";
          sss += "$R.event(b,r.regs[9],aa,r.regs[0],r.regs[1]);";
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x72:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[13]+="+l+";var i=r.regs[9];var e=$R.queue[$R.qindex[i]];";
          sss += "if(e&&(e[0]==i||(e[0]==0&&e[1]!=i))){var a";
          if(t1==0 || t1==1) {
            sss += "="+j+";";
          } else if(t1==2) {
            sss += "=$R.index(r,"+j+","+j2+");";
          }
          sss += "if(e.length>6){$R.write(r,a,e[5]);var j;for(j=1;j<=e[5];j++){$R.write(r,a+j,e[5+j]);}}";
          sss += "else{$R.write(r,a,1);$R.write(r,a+1,e[5]);}";
          sss += "r.regs[3]=e[1];r.regs[0]=e[2];r.regs[1]=e[3];r.regs[4]=e[4];";
          sss += "}else{r.regs[3]=0;}";
          sss += "if(e){$R.qindex[i]++;}";
          sss += "return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      case 0x7F:
        n = i + 1;
        t1 = ro.code[n++];
        j = ro.code[n++];
        j += ro.code[n++]<<8;
        if(t1==2) {
          j2 = ro.code[n++];
          j2 += ro.code[n++]<<8;
        }
        l = 4;
        if(t1==2) {
         l+=2;
        }
        h = hh(ro.code,i,l);
        f = this.opcache[h];
        if(f==null) {
          sss = "r.regs[14]=$R.command(r";
          if(t1==0) {
            sss += ","+j+");";
          } else if(t1==1) {
            sss += ",$R.read(r,"+j+"));";
          } else if(t1==2) {
            sss += ",$R.read(r,$R.index(r,"+j+","+j2+")));";
          }
          sss += "r.regs[13]+="+l+";return r.regs[13];";
          f = new Function("r",sss);
          this.opcache[h] = f;
        }
        this.threads[t].c[i] = f;
        i = n - 1;
        break;
      default:
        h = hh(ro.code,i,1);
        f = this.opcache[h];
        if(f!=null) {
          this.threads[t].c[i] = f;
        } else {
          n = 0;
          switch(ro.code[i]) {
            case 0x39:
            case 0x3C:
            case 0x67:
            case 0x68:
               n = 1;break;
            case 0x21:
            case 0x2A:
            case 0x64:
            case 0x65:
            case 0x74:
               n = 2;break;
            case 0x22:
            case 0x23:
            case 0x24:
            case 0x25:
            case 0x26:
            case 0x27:
            case 0x28:
            case 0x29:
               n = 3;break;
          }
          l = i + 1;
          for(j=0;j<n;j++) {
            t1 = ro.code[l++];
            k = ro.code[l++];
            k += ro.code[l++]<<8;
            if(t1==2) {
              k2 = ro.code[l++];
              k2 += ro.code[l++]<<8;
            }
          }
          sss = "alert(\"Bytecode 0"+h+" is not supported!\");";
          if(n==0) {
            sss += "r.regs[13]=r.code.length-1;";
          } else {
            sss += "r.regs[13]+="+parseInt(l-i)+";";
          }
          sss += "return r.regs[13];";
          this.threads[t].c[i] = new Function("r",sss);
          i = l - 1;
        }
    }
    if(++i >= ro.code.length) break;
  }
  return m;
};

// $R.callback - get or set callback for external command
// 1st parameter - numeric identifier of the command
// 2nd parameter (optional) - function(robot) implementing command
// if 2nd parameter is not set then it returns stored function

$R.callback = function(i) {
 var f = arguments[1];
 if(f) {
   this.commands[i] = f;
 }
 return this.commands[i];
}

$R.read = function(r,a) {
 var m = r.vars;
 if(a<0) {
   a += 0x10000;
 }
 if(a>=0xFF00) {
   a -= 0xFF00;
   m = r.regs;
 } else if(a>=0x8000) {
   a -= 0x8000;
   m = r.negs;
 }
 var o = m[a];
 if(o) {
   return o;
 }
 return 0;
}

$R.write = function(r,a,v) {
 var m = r.vars;
 if(a<0) {
   a += 0x10000;
 }
 if(a>=0xFF00) {
   a -= 0xFF00;
   m = r.regs;
 } else if(a>=0x8000) {
   a -= 0x8000;
   m = r.negs;
 }
 v &= 0xFFFF;
 if(v>=0x8000) {
   v -= 0x10000;
 }
 var o = m[a];
 m[a] = v;
 if(o) {
   return o;
 }
 return 0;
}

$R.index = function(r,a,b) {
 f = arguments[3];
 if(f==null) {
   f = 0;
 }
 if(a<0) {
   a += 0x10000;
 }
 if(f==1) {
   b &= 0xFFFF;
   if(b>=0x8000) {
     b -= 0x10000;
   }
 } else {
   b = this.read(r,b);
 }
 var c = 0;
 if(a>=0xFF00) {
   c = this.read(r,a) + b;
 } else {
   c = a + b;
 }
 c &= 0xFFFF;
 if(c>=0x8000) {
   c -= 0x10000;
 }
 return c;
}

$R.event = function(to,from) {
 var code = arguments[2];
 var x = arguments[3];
 var y = arguments[4];
 if(code==null) {
   code = 0;
 }
 if(x==null) {
   x = 0;
 }
 if(y==null) {
   y = 0;
 }
 if(to>=0x8000) {
   to -= 0x10000;
 }
 if(to==-3) { // select hook
   if(typeof code == 'number' || code[0]!=4) {
     alert("Wrong format of select event!");
   } else {
     if(this.areaf) {
       this.areaf(code[1],code[2],code[3],code[4]);
     }
   }
 }
 if(to==-4) { // filesystem hook
   var i,filename = "DEFAULT";
   if(typeof code == 'number') {
     filename = code.toString();
   } else if(typeof code == 'string') {
     filename = code;
   } else {
     filename = "";
     for(i=1;i<=code[0];i++) {
       filename += String.fromCharCode(code[i]);
     }
   }
   var file = this.files[filename];
   if(file==null) {
     // TODO: try to read this file from the server
     this.event(from,-4,0)
   } else {
     var sz = file.length;
     var ng = this.robots[from].negs;
     if(ng) {
       for(i=0;i<sz;i++) {
         ng[i] = file[i];
       }
       this.event(from,-4,sz);
     }
     else
     {
       alert("Robot "+from+" is not there...");
     }
   }
 }
 var r = -1;
 if(typeof code == 'number') {
   r = this.queue.push([to,from,x,y,this.gtime,code]);
 } else if(typeof code == 'string') {
   var ascii = [];
   var slen = code.length;
   for(var i = 0; i<slen; i++) {
     ascii.push(code.charCodeAt(i));
   }
   r = this.queue.push([to,from,x,y,this.gtime,slen].concat(ascii));
 } else { // it must be array of numbers then
   r = this.queue.push([to,from,x,y,this.gtime].concat(code));
 }
 return r;
}

$R.command = function(r,n) {
 var f = this.commands[n];
 if(f) {
   return f(r);
 }
 alert("Command #"+$hex(n,4)+" is not supported!");
 return 0;
}

$R.get = function(x,y) {
 var s = arguments[2];
 var ee = $(this.table_prefix+"."+y+"."+x);
 if(ee==null) {
   return 0;
 }
 if(s==null) {
  s = 0;
 }
 var v = ee.getAttribute("param"+s);
 if(v==null) {
   return 0;
 }
 return parseInt(v);
}

$R.get2 = function(x,y) {
 return [ $R.get(x,y,0), $R.get(x,y,1) ];
}

$R.set = function(x,y,c) {
 var s = arguments[3];
 var e = this.table_prefix+"."+y+"."+x;
 var ee = $(e);
 if(ee) {
   if(s==null) {
     s = 0;
   }
   if(s==0) {
     $_(e,"<img src=" + this.gif_prefix + $hex(c,4).toLowerCase() + ".gif>");
   }
   ee.setAttribute("param"+s,c.toString());
 }
 return ee;
}

$R.set2 = function(x,y,n) {
 var i = arguments[3];
 $R.set(x,y,n);
 if(i!=null) {
  $R.set(x,y,i,1);
 }
}

$R.setc = function(x,y,c,o) {
 $R.set2(x,y,c.charCodeAt(0)+(o<<8));
}

$R.sets = function(x,y,s,o) {
 var i,l=s.length;
 for(i=0;i<l;i++) {
   $R.set2(x+i,y,s.charCodeAt(i)+(o<<8));
 }
 return x+i;
}

$R.seth = function(x,y,o) {
 var c = $R.get2(x,y)[0]&255;
 $R.set2(x,y,c+(o<<8));
 return c;
}
