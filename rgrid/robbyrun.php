<HTML>
<HEAD><TITLE>Robby Runner</TITLE>
<META HTTP-EQUIV="Content-Language" content="en">
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<SCRIPT type="text/javascript" src="/Rgrid.js"></SCRIPT>
<SCRIPT type="text/javascript">
/*
    robbyrun.php - universal way to run Robby webapps with Rgrid.js (see http://rgrid.net)

    Copyright (C) 2025 Alexander Shabarshin <me@shaos.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

var dx=80;
var dy=25;

$R.prefix = "/pcb/";

function command1(r) {
 alert("Robot "+r.regs[9]+" "+r.regs[0xA]+" "+r.regs[0xB]+" "+r.regs[0xC]);
}

$R.callback(1,command1);

function action(x,y) {
 $R.event(0,-1,1,x,y);
}

function dos(c) {
 const cp866 = [
 "&nbsp;","☺","☻","♥","♦","♣","♠","•","◘","○","◙","♂","♀","♪","♫","☼",
 "►","◄","↕","‼","¶","§","▬","↨","↑","↓","→","←","∟","↔","▲","▼",
 "&nbsp;","!","\"","#","$","%","&","'","(",")","*","+",",","-",".","/",
 "0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?",
 "@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O",
 "P","Q","R","S","T","U","V","W","X","Y","Z","[","\\","]","^","_",
 "`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
 "p","q","r","s","t","u","v","w","x","y","z","{","|","}","~","⌂",
 "А","Б","В","Г","Д","Е","Ж","З","И","Й","К","Л","М","Н","О","П",
 "Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я",
 "а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п",
 "░","▒","▓","│","┤","╡","╢","╖","╕","╣","║","╗","╝","╜","╛","┐",
 "└","┴","┬","├","─","┼","╞","╟","╚","╔","╩","╦","╠","═","╬","╧",
 "╨","╤","╥","╙","╘","╒","╓","╫","╪","┘","┌","█","▄","▌","▐","▀",
 "р","с","т","у","ф","х","ц","ч","ш","щ","ъ","ы","ь","э","ю","я",
 "Ё","ё","Є","є","Ї","ї","Ў","ў","°","∙","·","√","№","¤","■","□"
 ];
 return cp866[c&255];
}
</SCRIPT>
</HEAD>
<?php
$style = 0;
$duty = 80;
$robot = '';
if(isset($_GET["robot"])) $robot=$_GET["robot"];
if(isset($_GET["style"])) $style=intval($_GET["style"]);
if(isset($_GET["duty"])) $duty=intval($_GET["duty"]);
if($duty<=0) $duty=1;
if($style>=4) $style=0;
$dark = 0;
if($style==0) {
  $dark = 1;
  echo "<BODY BGCOLOR=#202020 TEXT=#AAAAAA LINK=#0000AA>\n";
} elseif($style==2) {
  $dark = 1;
  echo "<BODY BGCOLOR=#000000 TEXT=#00AA00 LINK=#00AAAA>\n";
} else {
  echo "<BODY BGCOLOR=#E0E0E0 TEXT=#000000 LINK=#0000AA>\n";
}
echo "<SCRIPT type=\"text/javascript\" src=\"/exec/robbyc.php?s=$robot\"></SCRIPT>\n";
echo "<SCRIPT type=\"text/javascript\">\n";
if($style==2) {
  echo "dy = 50;\n";
}
if($style==3) {
  echo "dx = 48;\n";
  echo "dy = 2;\n";
}
echo "window.addEventListener('load', function() {\n";
echo " \$_('pow','Powered by ' + \$R.powered());\n";
echo " if(\$R.grid('here',dx,dy,action)>0) {\n";
if($dark) {
  echo "  \$('rtab~').setAttribute('bgcolor','#000000');\n";
} else {
  echo "  \$('rtab~').setAttribute('bgcolor','#FFFFFF');\n";
}
if($style<2) {
  echo "  \$R.set = function(x,y,c) {\n";
  echo "   var e = this.table_prefix+'.'+y+'.'+x;\n";
  echo "   var ee = \$(e);\n";
  echo "   if(ee) {\n";
  echo "    \$_(e,'<code>' + dos(c) + '</code>');\n";
  echo "    ee.setAttribute(\"param0\",c.toString());\n";
  echo "   }\n";
  echo "   return ee;\n";
  echo "  }\n";
  echo "  for(var i=0;i<dx;i++){\n";
  echo "  for(var j=0;j<dy;j++){\n";
  echo "   \$_('rtab.'+j+'.'+i,\"<code>&nbsp;</code>\");\n";
  echo "  }}\n";
}
echo "  \$R.add(robot);\n";
echo "  \$R.start($duty);\n";
echo " }\n";
echo "});\n";
echo "</SCRIPT>\n";
echo "<p id=\"here\" align=center>Rgrid requires JS...</p>\n";
echo "<p align=center>\n";
$button = 1;
while(1) {
  $buttoname = "button$button";
  if(!isset($_GET[$buttoname])) {
    if($button!=1) break;
    $buttoname = "button";
    if(!isset($_GET[$buttoname])) break;
  }
  $buttonctrl = explode(',',$_GET[$buttoname]);
  if(count($buttonctrl)==1) array_push($buttonctrl,"ERROR"," ");
  else if(count($buttonctrl)==2) array_push($buttonctrl," ");
  echo "<input value=\"$buttonctrl[1]\" type=\"button\" onclick=\"\$R.event(0,".intval($buttonctrl[0]).",'$buttonctrl[2]')\" id=\"$buttoname\">\n";
  $button = $button + 1;
}
echo "</p><font size=-4><p id=\"pow\" align=center>Powered...</p></font>\n";
echo "</BODY>\n";
?>
</HTML>
