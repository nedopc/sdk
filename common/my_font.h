/* my_font.h - Alexander A. Shabarshin <me@shaos.net> */

#ifndef _FONT5X7_
#define _FONT5X7_

#define FONT5X7_CHARS 208

extern unsigned char font5x7[FONT5X7_CHARS][7];

/* actually it is font 6x8, but 6th column and 8th row must be always empty */

#endif
