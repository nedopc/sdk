/*  proc_sx.h - definition of class ProcessorSX and some macros (06-Jan-2007)

    Copyright (c) 2006-2007, Alexander Shabarshin (alexander@shabarshin.com)

    This file is part of NedoPC SDK (software development kit for simple devices).

    NedoPC SDK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    NedoPC SDK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NedoPC SDK; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __PROC_SX_H
#define __PROC_SX_H

#define PROC_SX_20 0 /* Simulation of SX-20 chip */
#define PROC_SX_28 1 /* Simulation of SX-28 chip */

#define PROC_SX_COMPT 0 /* Compatible mode - 4 clock per instruction */
#define PROC_SX_TURBO 1 /* Turbo mode - 1 clock per instruction */

#define PROC_SX_OUTPUT 0 /* Output direction for port */
#define PROC_SX_INPUT  1 /* Input direction for port */

#define PROC_SX_PC	0x82
#define PROC_SX_STATUS	0x83
#define PROC_SX_FSR	0x84
#define PROC_SX_RA	0x85
#define PROC_SX_RB	0x86
#define PROC_SX_RC	0x87

#define PROC_SX_STATUS_C  1
#define PROC_SX_STATUS_DC 2
#define PROC_SX_STATUS_Z  4

class ProcessorSX
{
 unsigned long timel;
 int proc,mode,ports,fskip;
protected:
 int work,stacki;
 unsigned char data[144],port[3],dir[3],stack[8];
 unsigned short code[2048];
 unsigned char M,W,OPTION;
 unsigned short PC,PC_;
 int getData0(int a);
 void setData0(int a, int b);
 int getData1(int a);
 void setData1(int a, int b);
 int getFlag(int f);
 void setFlag(int f, int b);
 int getFlagZ(void){return getFlag(PROC_SX_STATUS_Z);};
 void setFlagZ(void){setFlag(PROC_SX_STATUS_Z,1);};
 void clrFlagZ(void){setFlag(PROC_SX_STATUS_Z,0);};
 int getFlagC(void){return getFlag(PROC_SX_STATUS_C);};
 void setFlagC(void){setFlag(PROC_SX_STATUS_C,1);};
 void clrFlagC(void){setFlag(PROC_SX_STATUS_C,0);};
 int getFlagDC(void){return getFlag(PROC_SX_STATUS_DC);};
 void setFlagDC(void){setFlag(PROC_SX_STATUS_DC,1);};
 void clrFlagDC(void){setFlag(PROC_SX_STATUS_DC,0);};
public:
 ProcessorSX(int _proc=PROC_SX_28, int _mode=PROC_SX_TURBO);
 virtual ~ProcessorSX(){;}; 
 int step(unsigned long t);
 virtual void aux(int com){}; 
 int getCode(int a);
 int getData(int a);
 int getPort(int p);
 void setData(int a, int b);
 void setPort(int p, int b);
 int loadHex(char *fname);
};

#endif
