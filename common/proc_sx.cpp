/*  proc_sx.cpp - implementation of class ProcessorSX and some macros (06-Jan-2007)

    Copyright (c) 2006-2007, Alexander Shabarshin (alexander@shabarshin.com)

    This file is part of NedoPC SDK (software development kit for simple devices).

    NedoPC SDK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    NedoPC SDK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NedoPC SDK; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include "my_hex.h"
#include "proc_sx.h"

#define PROC_SX_TRACE
//#define PROC_SX_DEBUG
//#define PROC_SX_DEBUG1

#ifdef PROC_SX_TRACE
#define SX(s) printf(s" \n");
#define SX1(s,b) printf(s" 0x%2.2X\n",b);
#define SX2(s,b,i) printf(s" 0x%2.2X,%i\n",b,i);
#else
#define SX(s)
#define SX1(s,b)
#define SX2(s,b,i)
#endif

ProcessorSX::ProcessorSX(int _proc, int _mode)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::ProcessorSX(%i,%i)\n",proc,mode);
#endif 
 proc = _proc;
 mode = _mode;
 timel = 0; 
 fskip = 0;
 PC = 2047;
 M = 0x0F;
 dir[0] = 0xFF;
 dir[1] = 0xFF;
 dir[2] = 0xFF;
 stacki = 0;
 work = 1;
 switch(proc)
 {
  case PROC_SX_20: 
     ports = 2;
#ifdef PROC_SX_DEBUG
     printf("SX: proc=SX-20\n");
#endif     
     break;
  case PROC_SX_28: 
     ports = 3;
#ifdef PROC_SX_DEBUG
     printf("SX: proc=SX-28\n");
#endif     
     break;
  default:
     printf("SX-ERROR: unknown proc %i\n",proc);
     exit(-1);
 }
 for(int i=0;i<2048;i++) code[i]=0;
}

int ProcessorSX::getData0(int a)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getData0(0x%4.4X)\n",a);
#endif 
 int aa,o = 0;
 if(a<0||a>=16) return 0;
 switch(a)
 {
   case 0:
     aa = data[PROC_SX_FSR];
     if(aa!=0) o = getData(aa);
     break;
   case 2:
     o = PC&0xFF;
     break;     
   case 5:
     o = (port[0]&dir[0])|(data[PROC_SX_RA]&(~dir[0]));
     break; 
   case 6:
     o = (port[1]&dir[1])|(data[PROC_SX_RB]&(~dir[1]));
     break; 
   case 7:
     o = (port[2]&dir[2])|(data[PROC_SX_RC]&(~dir[2]));
     break; 
   default:     
     o = data[128+a];
 }  
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getData0 -> 0x%2.2X\n",o);
#endif 
 return o;
}

void ProcessorSX::setData0(int a, int b)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::setData0(0x%4.4X,0x%2.2X)\n",a,b);
#endif 
 int aa,o = 0;
 if(a<0||a>=16) return;
 switch(a)
 {
   case 0:
     aa = data[PROC_SX_FSR];
     if(aa!=0) setData(aa,b);
     break;
   case 2:
     PC = (PC&0x0F00)|b;
     timel+=2;
     break;     
   default:     
     data[128+a] = b;
 }  
}

int ProcessorSX::getData1(int a)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getData1(0x%4.4X)\n",a);
#endif 
 int o = data[((data[PROC_SX_FSR]>>1)&0x70)|(a&0xF)];
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getData1 -> 0x%2.2X\n",o);
#endif 
 return o;
} 

void ProcessorSX::setData1(int a, int b)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::setData1(0x%4.4X,0x%2.2X)\n",a,b);
#endif 
 data[((data[PROC_SX_FSR]>>1)&0x70)|(a&0xF)] = b;
}

int ProcessorSX::getFlag(int f)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getFlag(%i)\n",f);
#endif 
 int o = (data[PROC_SX_STATUS]&f)?1:0;
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getFlag -> %i\n",o);
#endif 
 return o;
}

void ProcessorSX::setFlag(int f, int b)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::setFlag(%i,%i)\n",f,b);
#endif 
 data[PROC_SX_STATUS] = data[PROC_SX_STATUS]&(~f);
 if(f) data[PROC_SX_STATUS] |= f;
}

int ProcessorSX::getCode(int a)
{
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getCode(0x%4.4X)\n",a);
#endif 
 int o = code[a];
#ifdef PROC_SX_DEBUG1
 printf("ProcessorSX::getCode -> 0x%3.3X\n",o);
#endif 
 return o;
}

int ProcessorSX::getData(int a)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::getData(0x%4.4X)\n",a);
#endif 
 int o = 0;
 if(a<0) exit(-11);
 if(a<0x10) o=getData0(a);
 else if(a<0x20) o=getData1(a-0x10);
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::getData -> 0x%2.2X\n",o);
#endif 
 return o;
}

int ProcessorSX::getPort(int p)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::getPort(%i)\n",p);
#endif 
 int o = 0;
 if(p==0||p==1||p==2) p+=PROC_SX_RA;
 if(p==PROC_SX_RA||p==PROC_SX_RB||p==PROC_SX_RC) o=data[p]|dir[p-PROC_SX_RA];
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::getPort -> 0x%2.2X\n",o);
#endif 
 return o;
}

void ProcessorSX::setData(int a, int b)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::setData(0x%4.4X,0x%2.2X)\n",a,b);
#endif 
 if(a<0) exit(-12);
 if(a<0x10) setData0(a,b);
 else if(a<0x20) setData1(a-0x10,b);
}

void ProcessorSX::setPort(int p, int b)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::setPort(%i,0x%2.2X)\n",p,b);
#endif 
 if(p==PROC_SX_RA||p==PROC_SX_RB||p==PROC_SX_RC) p-=PROC_SX_RA;
 if(p==0||p==1||p==2) port[p]=b;
}

int ProcessorSX::loadHex(char *fname)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::loadHex(%s)\n",fname);
#endif 
 int i,j,k,b1,b0,o = 0;
 char str[100];
 FILE *f = fopen(fname,"rt");
 if(f!=NULL)
 {
   while(1)
   {
      fgets(str,100,f);
      if(feof(f)) break;
      if(str[0]!=':') continue;
      j = ((hexx(str[1])<<4)|hexx(str[2]))>>1;
      k = ((hexx(str[3])<<12)|(hexx(str[4])<<8)|(hexx(str[5])<<4)|hexx(str[6]))>>1;
      if(str[7]=='0'&&str[8]=='0')
      {
//        printf("\n#%4.4X %i %s\n",k,j,str);
        for(i=9;i<100;i+=4)
        {
          b0 = (hexx(str[i])<<4)|hexx(str[i+1]);
          b1 = (hexx(str[i+2])<<4)|hexx(str[i+3]);
          if(k<0x0FFF)
          {
            code[k] = (b1<<8)|b0;
//            printf("[#%4.4X]=#%4.4X\n",k,code[k]);
            k++;
            o++;
          }   
          if(--j<=0) break;  
        }
      }	
   }
   fclose(f);
 }
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::loadHex -> %i\n",o);
#endif 
 return o; 
}

int ProcessorSX::step(unsigned long t)
{
#ifdef PROC_SX_DEBUG
 printf("ProcessorSX::step(%u)\n",t);
#endif 
 int tmp;
 int ok = 0;
 unsigned long tt = timel+t;
 while(timel < tt && work)
 {
   PC_ = PC;
   int command = code[PC++];
   if(fskip){fskip=0;continue;}
#ifdef PROC_SX_TRACE
   printf("---> [0x%3.3X] 0x%3.3X (t=%i)\n",PC-1,command,timel);
#endif
   if(PC>=2048) PC-=2048;
   short comm1 = command&0xF00;
   short comm2 = command&0x0F0;
   short comm3 = command&0x00F;
   switch(comm1)
   {
     case 0x000:
       switch(comm2)
       { 
         case 0x000:
	   switch(comm3)
	   { 
	     case 0x000:
	       SX("NOP");
	       break;
	     case 0x002:
               SX("OPTION");
               OPTION = W;
	       break;
	     case 0x003:
	       SX("-SLEEP");
	       break;  
	     case 0x004:
	       SX("-CLRWDT");
	       break;
	     case 0x005:  
	     case 0x006:  
	     case 0x007:  
               SX1("TRIS",comm3);
               if(M==0xF)
               {
                   if(comm3==5) dir[0]=W;
	           if(comm3==6) dir[1]=W;
	           if(comm3==7) dir[2]=W;
	       }	 
	       break;
	     case 0x00C:
	       SX("RET");
	       PC=(PC&0xFE00)|(stack[stacki--]&0x01FF);
	       timel+=2;
	       break;  
	     case 0x00D:
	       SX("RETP");
	       PC=stack[stacki--];
	       timel+=2;
	       break;  
	     case 0x00E:
	       SX("-RETI");  
	       timel+=2;
	       break;
	     case 0x00F:  
	       SX("-RETIW");
	       timel+=2;
	       break;
	   }
	   break;
	 case 0x010:
	   if(comm3<8)
	   {
	     SX1("PAGE",comm3);
	     data[PROC_SX_STATUS]=(data[PROC_SX_STATUS]&0x1F)|(comm3<<5);
	   }
	   else
	   {
	     SX1("BANK",comm3-8);
	     data[PROC_SX_FSR]=(data[PROC_SX_FSR]&0x1F)|((comm3-8)<<5);
	   }
	   break;  
	 case 0x020:
	   SX1("MOVWF",comm3);
	   setData0(comm3,W);
	   break;
	 case 0x030:
	   SX1("MOVWF",comm3+0x10);
	   setData1(comm3,W);
	   break;    
         case 0x040:
	   switch(comm3)
	   { 
	     case 0x000:
	       SX("CLRW");
	       W = 0;
	       setFlagZ();
	       break;
	     case 0x001:
	       SX("IREAD");
	       tmp = getCode((M<<8)|W);
	       W = tmp&0x00FF;
	       M = (tmp&0x0F00)>>8;
	       break;  
	     case 0x002:
	       SX("MOVMW");
	       W = M;
	       break;  
	     case 0x003:
	       SX("MOVWM");
	       M = W;
	       break;  
	   }       
	   break;
	 case 0x050:
	   SX1("MODE",comm3);
	   M = comm3;
	   break;  
         case 0x060:
	   SX1("CLRF",comm3);
	   setData0(comm3,0);
           setFlagZ();
	   break;
	 case 0x070:
	   SX1("CLRF",comm3+0x10);
	   setData1(comm3,0);
           setFlagZ();
	   break;  
	 case 0x080:
	   SX2("SUBWF",comm3,0);
	   tmp = (getData0(comm3)-W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData0(comm3)+(~W)+1 > 0xFF) clrFlagC();
	   else setFlagC();
	   if((getData0(comm3)&0x0F)+((~W)&0x0F)+1 > 0x0F) clrFlagDC();
	   else setFlagDC();
	   W = tmp;
	   break;
	 case 0x090:
	   SX2("SUBWF",comm3+0x10,0);
	   tmp = (getData1(comm3)-W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData1(comm3)+(~W)+1 > 0xFF) clrFlagC();
	   else setFlagC();
	   if((getData1(comm3)&0x0F)+((~W)&0x0F)+1 > 0x0F) clrFlagDC();
	   else setFlagDC();
	   W = tmp;
	   break;    
	 case 0x0A0:
	   SX2("SUBWF",comm3,1);
	   tmp = (getData0(comm3)-W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData0(comm3)+(~W)+1 > 0xFF) clrFlagC();
	   else setFlagC();
	   if((getData0(comm3)&0x0F)+((~W)&0x0F)+1 > 0x0F) clrFlagDC();
	   else setFlagDC();
	   setData0(comm3,tmp);
	   break;
	 case 0x0B0:
	   SX2("SUBWF",comm3+0x10,1);
	   tmp = (getData1(comm3)-W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData1(comm3)+(~W)+1 > 0xFF) clrFlagC();
	   else setFlagC();
	   if((getData1(comm3)&0x0F)+((~W)&0x0F)+1 > 0x0F) clrFlagDC();
	   else setFlagDC();
	   setData1(comm3,tmp);
	   break;    
	 case 0x0C0:
	   SX2("DECF",comm3,0);
	   tmp = (getData0(comm3)-1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
	 case 0x0D0:
	   SX2("DECF",comm3+0x10,0);
	   tmp = (getData1(comm3)-1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
	 case 0x0E0:
	   SX2("DECF",comm3,1);
	   tmp = (getData0(comm3)-1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData0(comm3,tmp);
	   break;
	 case 0x0F0:
	   SX2("DECF",comm3+0x10,1);
	   tmp = (getData1(comm3)-1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
       }
       break;
       
     case 0x100:
       switch(comm2)
       {
         case 0x000:
	   SX2("IORWF",comm3,0);
	   tmp = W|getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x010:
	   SX2("IORWF",comm3+0x10,0);
	   tmp = W|getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x020:
	   SX2("IORWF",comm3,1);
	   tmp = W|getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x030:
	   SX2("IORWF",comm3+0x10,1);
	   tmp = W|getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x040:
	   SX2("ANDWF",comm3,0);
	   tmp = W&getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x050:
	   SX2("ANDWF",comm3+0x10,0);
	   tmp = W&getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x060:
	   SX2("ANDWF",comm3,1);
	   tmp = W&getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x070:
	   SX2("ANDWF",comm3+0x10,1);
	   tmp = W&getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x080:
	   SX2("XORWF",comm3,0);
	   tmp = W^getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x090:
	   SX2("XORWF",comm3+0x10,0);
	   tmp = W^getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x0A0:
	   SX2("XORWF",comm3,1);
	   tmp = W^getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x0B0:
	   SX2("XORWF",comm3+0x10,1);
	   tmp = W^getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x0C0:
	   SX2("ADDWF",comm3,0);
	   tmp = (getData0(comm3)+W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData0(comm3)+W > 0xFF) setFlagC();
	   else clrFlagC();
	   if((getData0(comm3)&0x0F)+(W&0x0F) > 0x0F) setFlagDC();
	   else clrFlagDC();
	   W = tmp;
	   break;
         case 0x0D0:
	   SX2("ADDWF",comm3+0x10,0);
	   tmp = (getData1(comm3)+W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData1(comm3)+W > 0xFF) setFlagC();
	   else clrFlagC();
	   if((getData1(comm3)&0x0F)+(W&0x0F) > 0x0F) setFlagDC();
	   else clrFlagDC();
	   W = tmp;
	   break;
         case 0x0E0:
	   SX2("ADDWF",comm3,1);
	   tmp = (getData0(comm3)+W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData0(comm3)+W > 0xFF) setFlagC();
	   else clrFlagC();
	   if((getData0(comm3)&0x0F)+(W&0x0F) > 0x0F) setFlagDC();
	   else clrFlagDC();
	   setData0(comm3,tmp);
	   break;
         case 0x0F0:
	   SX2("ADDWF",comm3+0x10,1);
	   tmp = (getData1(comm3)+W)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   if(getData1(comm3)+W > 0xFF) setFlagC();
	   else clrFlagC();
	   if((getData1(comm3)&0x0F)+(W&0x0F) > 0x0F) setFlagDC();
	   else clrFlagDC();
	   setData1(comm3,tmp);
	   break;
       }
       break;
       
     case 0x200:
       switch(comm2)
       {
         case 0x000:
	   SX2("MOVF",comm3,0);
	   tmp = getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x010:
	   SX2("MOVF",comm3+0x10,0);
	   tmp = getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x020:
	   SX2("MOVF",comm3,1);
	   tmp = getData0(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   break;
         case 0x030:
	   SX2("MOVF",comm3+0x10,1);
	   tmp = getData1(comm3);
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   break;
         case 0x040:
	   SX2("COMF",comm3,0);
	   tmp = (~getData0(comm3))&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x050:
	   SX2("COMF",comm3+0x10,0);
	   tmp = (~getData1(comm3))&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x060:
	   SX2("COMF",comm3,1);
	   tmp = (~getData0(comm3))&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData0(comm3,tmp);
	   break;
         case 0x070:
	   SX2("COMF",comm3+0x10,1);
	   tmp = (~getData1(comm3))&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x080:
	   SX2("INCF",comm3,0);
	   tmp = (getData0(comm3)+1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x090:
	   SX2("INCF",comm3+0x10,0);
	   tmp = (getData1(comm3)+1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   W = tmp;
	   break;
         case 0x0A0:
	   SX2("INCF",comm3,1);
	   tmp = (getData0(comm3)+1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData0(comm3,tmp);
	   break;
         case 0x0B0:
	   SX2("INCF",comm3+0x10,1);
	   tmp = (getData1(comm3)+1)&0xFF;
	   if(tmp==0) setFlagZ();
	   else clrFlagZ();
	   setData1(comm3,tmp);
	   break;
         case 0x0C0:
	   SX2("DECFSZ",comm3,0);
	   tmp = (getData0(comm3)-1)&0xFF;
	   if(tmp==0) fskip = 1;
	   W = tmp;
	   break;
         case 0x0D0:
	   SX2("DECFSZ",comm3+0x10,0);
	   tmp = (getData1(comm3)-1)&0xFF;
	   if(tmp==0) fskip = 1;
	   W = tmp;
	   break;
         case 0x0E0:
	   SX2("DECFSZ",comm3,1);
	   tmp = (getData0(comm3)-1)&0xFF;
	   if(tmp==0) fskip = 1;
	   setData0(comm3,tmp);
	   break;
         case 0x0F0:
	   SX2("DECFSZ",comm3+0x10,1);
	   tmp = (getData1(comm3)-1)&0xFF;
	   if(tmp==0) fskip = 1;
	   setData1(comm3,tmp);
	   break;
       }
       break;
       
     case 0x300:
       switch(comm2)
       {
         case 0x000:
	   SX2("RRF",comm3,0);
	   tmp = getData0(comm3);
	   W = (tmp>>1)|(getFlagC()<<7);
	   if(tmp&1) setFlagC();
	   else clrFlagC();
	   break;
         case 0x010:
	   SX2("RRF",comm3+0x10,0);
	   tmp = getData1(comm3);
	   W = (tmp>>1)|(getFlagC()<<7);
	   if(tmp&1) setFlagC();
	   else clrFlagC();
	   break;
         case 0x020:
	   SX2("RRF",comm3,1);
	   tmp = getData0(comm3);
	   setData0(comm3,(tmp>>1)|(getFlagC()<<7));
	   if(tmp&1) setFlagC();
	   else clrFlagC();
	   break;
         case 0x030:
	   SX2("RRF",comm3+0x10,1);
	   tmp = getData1(comm3);
	   setData1(comm3,(tmp>>1)|(getFlagC()<<7));
	   if(tmp&1) setFlagC();
	   else clrFlagC();
	   break;
         case 0x040:
	   SX2("RLF",comm3,0);
	   tmp = getData0(comm3);
	   W = ((tmp<<1)&0xFE)|getFlagC();
	   if(tmp&128) setFlagC();
	   else clrFlagC();
	   break;
         case 0x050:
	   SX2("RLF",comm3+0x10,0);
	   tmp = getData1(comm3);
	   W = ((tmp<<1)&0xFE)|getFlagC();
	   if(tmp&128) setFlagC();
	   else clrFlagC();
	   break;
         case 0x060:
	   SX2("RLF",comm3,1);
	   tmp = getData0(comm3);
	   setData0(comm3,((tmp<<1)&0xFE)|getFlagC());
	   if(tmp&128) setFlagC();
	   else clrFlagC();
	   break;
         case 0x070:
	   SX2("RLF",comm3+0x10,1);
	   tmp = getData1(comm3);
	   setData1(comm3,((tmp<<1)&0xFE)|getFlagC());
	   if(tmp&128) setFlagC();
	   else clrFlagC();
	   break;
         case 0x080:
	   SX2("SWAPF",comm3,0);
	   tmp = getData0(comm3);
	   W = ((tmp&0x0F)<<4)|((tmp&0xF0)>>4);
	   break;
         case 0x090:
	   SX2("SWAPF",comm3+0x10,0);
	   tmp = getData1(comm3);
	   W = ((tmp&0x0F)<<4)|((tmp&0xF0)>>4);
	   break;
         case 0x0A0:
	   SX2("SWAPF",comm3,1);
	   tmp = getData0(comm3);
	   setData0(comm3,((tmp&0x0F)<<4)|((tmp&0xF0)>>4));
	   break;
         case 0x0B0:
	   SX2("SWAPF",comm3+0x10,1);
	   tmp = getData1(comm3);
	   setData1(comm3,((tmp&0x0F)<<4)|((tmp&0xF0)>>4));
	   break;
         case 0x0C0:
	   SX2("INCFSZ",comm3,0);
	   tmp = (getData0(comm3)+1)&0xFF;
	   if(tmp==0) fskip = 1;
	   W = tmp;
	   break;
         case 0x0D0:
	   SX2("INCFSZ",comm3+0x10,0);
	   tmp = (getData1(comm3)+1)&0xFF;
	   if(tmp==0) fskip = 1;
	   W = tmp;
	   break;
         case 0x0E0:
	   SX2("INCFSZ",comm3,1);
	   tmp = (getData0(comm3)+1)&0xFF;
	   if(tmp==0) fskip = 1;
	   setData0(comm3,tmp);
	   break;
         case 0x0F0:
	   SX2("INCFSZ",comm3+0x10,1);
	   tmp = (getData1(comm3)+1)&0xFF;
	   if(tmp==0) fskip = 1;
	   setData1(comm3,tmp);
	   break;
       }
       break;
       
     case 0x400:
       tmp = comm2>>5;
       if((comm2&0x010)==0)
       {
         SX2("BCF",comm3,tmp);
	 setData0(comm3,getData0(comm3)&(~(1<<tmp)));
       }
       else
       {
         SX2("BCF",comm3+0x10,tmp);
	 setData1(comm3,getData1(comm3)&(~(1<<tmp)));
       }
       break;
       
     case 0x500:
       tmp = comm2>>5;
       if((comm2&0x010)==0)
       {
         SX2("BSF",comm3,tmp);
	 setData0(comm3,(getData0(comm3)&(~(1<<tmp)))|(1<<tmp));
       }
       else
       {
         SX2("BSF",comm3+0x10,tmp);
	 setData1(comm3,(getData1(comm3)&(~(1<<tmp)))|(1<<tmp));
       }
       break;
       
     case 0x600:
       tmp = comm2>>5;
       if((comm2&0x010)==0)
       {
         SX2("BTFSC",comm3,tmp);
	 if((getData0(comm3)&(1<<tmp))==0) fskip=1;
       }
       else
       {
         SX2("BTFSC",comm3+0x10,tmp);
	 if((getData1(comm3)&(1<<tmp))==0) fskip=1;
       }
       break;
       
     case 0x700:
       tmp = comm2>>5;
       if((comm2&0x010)==0)
       {
         SX2("BTFSS",comm3,tmp);
	 if((getData0(comm3)&(1<<tmp))!=0) fskip=1;
       }
       else
       {
         SX2("BTFSS",comm3+0x10,tmp);
	 if((getData1(comm3)&(1<<tmp))!=0) fskip=1;
       }
       break;
       
     case 0x800:
       tmp = comm2|comm3;
       SX1("RETLW",tmp);
       W = tmp;
       PC=(PC&0xFE00)|(stack[stacki--]&0x01FF);
       timel+=2;
       break;
       
     case 0x900:
       tmp = comm2|comm3;
       SX1("CALL",tmp);
       stack[stacki++] = PC;
       PC = ((data[PROC_SX_STATUS]&0xE0)<<4)|tmp;
       timel+=2;
       break;
       
     case 0xA00:
       tmp = comm2|comm3;
       SX1("GOTO",tmp);
       PC = ((data[PROC_SX_STATUS]&0xE0)<<4)|tmp;
       timel+=2;
       break;
       
     case 0xB00:
       tmp = comm2|comm3|0x100;
       SX1("GOTO",tmp);
       PC = ((data[PROC_SX_STATUS]&0xE0)<<4)|tmp;
       timel+=2;
       break;
       
     case 0xC00:
       tmp = comm2|comm3;
       SX1("MOVLW",tmp);
       W = tmp;
       break;
       
     case 0xD00:
       tmp = comm2|comm3;
       SX1("IORLW",tmp);
       W |= tmp;
       break;
       
     case 0xE00:
       tmp = comm2|comm3;
       SX1("ANDLW",tmp);
       W &= tmp;
       break;
       
     case 0xF00:
       tmp = comm2|comm3;
       SX1("XORLW",tmp);
       W ^= tmp;
       break;
   }
   timel++;
   ok++;
   aux(command);
 }
 return ok;
}
