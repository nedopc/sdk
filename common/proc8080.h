/*  proc8080.h - class Processor8080 for emulation of 8080/8085.

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 1994-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __PROC8080_H
#define __PROC8080_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SH_FALSE 0
#define SH_TRUE 1
#define SH_BIT int
#define SH_BYTE unsigned char
#define SH_WORD unsigned short

#define SH_FLAG_8085 1

class Processor8080
{
 int flags;
protected: 
 unsigned long time;
 unsigned long timel;
 SH_BYTE lastcom;
 SH_BYTE A,B,C,D,E,H,L;
 SH_WORD SP,PC,PC_;
 SH_BIT z_,p_,c_,s_,a_,i_;
 SH_BYTE par[256];
 SH_WORD mul256(SH_BYTE b) { return ((SH_WORD)b)<<8; };
 SH_BYTE plus(SH_BYTE b1,SH_BYTE b2,SH_BYTE b3=0)
 {
   SH_BYTE b1l,b1r,b2l,b2r,b_l,b_r;
   b1l=(b1&0xF0)>>4;
   b1r=b1&0x0F;
   b2l=(b2&0xF0)>>4;
   b2r=b2&0x0F;
   b_r=b1r+b2r+b3;
   a_=(b_r&0x10)>>4;
   b_r=b_r&0x0F;
   b_l=b1l+b2l+a_;
   c_=(b_l&0x10)>>4;
   b_l=b_l&0x0F;
   return (b_l<<4)|b_r;
 }
 SH_BYTE minus(SH_BYTE b1,SH_BYTE b2,SH_BYTE b3=0)
 {
   SH_BYTE b1l,b1r,b2l,b2r,b_l,b_r;
   b1l=(b1&0xF0)>>4;
   b1r=b1&0x0F;
   b2l=(b2&0xF0)>>4;
   b2r=b2&0x0F;
   b_r=b1r-b2r-b3;
   if(b_r&0x10) a_=SH_TRUE;
   else a_=SH_FALSE;
   b_r=b_r&0x0F;
   b_l=b1l-b2l-a_;
   if(b_l&0x10) c_=SH_TRUE;
   else c_=SH_FALSE;
   b_l=b_l&0x0F;
   if(a_) a_=SH_FALSE;
   else a_=SH_TRUE;
   return (b_l<<4)|b_r;
 }
 void flag_p(SH_BYTE b) { p_=(SH_BIT)par[b]; };
 void flag_z(SH_BYTE b) { if(!b) z_=SH_TRUE; else z_=SH_FALSE; };
 void flag_s(SH_BYTE b) { s_=(b&0x80)>>7; };
 void flag_all(SH_BYTE b) { flag_s(b); flag_z(b); flag_p(b); };
 void ret(void) { PC=getw(SP); SP+=2; };
 void call(SH_WORD a) { SP-=2; putw(SP,PC); PC=a; };
public:
 SH_BIT INTR;
 SH_BIT HALT;
 long getTime(void){return timel;};
 void setPC(SH_WORD a) { PC=a; };
 void setSP(SH_WORD a) { SP=a; };
 void setBC(SH_WORD x) { B=x>>8; C=x&0xFF; };
 void setDE(SH_WORD x) { D=x>>8; E=x&0xFF; };
 void setHL(SH_WORD x) { H=x>>8; L=x&0xFF; };
 void setPSW(SH_WORD x)
 {
   A=(x>>8)&0xFF;
   s_=(x&0x0080)>>7;
   z_=(x&0x0040)>>6;
   a_=(x&0x0010)>>4;
   p_=(x&0x0004)>>2;
   c_=(x&0x0001);
 }
 SH_WORD getPC(void) { return PC; };
 SH_WORD getSP(void) { return SP; };
 SH_WORD getBC(void) { return mul256(B)|C; };
 SH_WORD getDE(void) { return mul256(D)|E; };
 SH_WORD getHL(void) { return mul256(H)|L; };
 SH_WORD getPSW(void) { return (mul256(A)|(s_<<7)|(z_<<6)|(a_<<4)|(p_<<2)|2|c_); };
 SH_BYTE getM(void) { return get(getHL()); };
 int is8085(void) { return flags&SH_FLAG_8085; };
 Processor8080(int f=0);
 virtual ~Processor8080() { ; };
 virtual SH_BYTE get(SH_WORD a) { return 0xFF; }; // read from memory
 virtual void put(SH_WORD a,SH_BYTE b) { ; }; // write to memory
 virtual SH_BYTE interruption(void) { return 0; }; // interrupt
 virtual int aux(SH_BYTE com,SH_BIT ok) { return 0; }; // parallel activity
 virtual SH_BYTE inp(SH_BYTE p) { return 0xFF; }; // read from port
 virtual void outp(SH_BYTE p,SH_BYTE b) { ; }; // write to port
 SH_WORD getw(SH_WORD a) { SH_WORD w=get(a); w|=mul256(get(a+1)); return w; };
 void putw(SH_WORD a, SH_WORD w) { put(a,w&0xFF); put(a+1,(w>>8)&0xFF); };
 unsigned long step(unsigned long t=1); // Step of emulation
 int load(const char *s,SH_WORD adr,SH_WORD len=0xFFFF,SH_WORD off=0);
 int save(const char *s,SH_WORD adr,SH_WORD len);
};

class Processor8085 : public Processor8080
{
public:
 Processor8085() : Processor8080(SH_FLAG_8085) {;};
 virtual ~Processor8085() {;};
};

#endif
