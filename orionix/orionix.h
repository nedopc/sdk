/*  orionix.h - emulator of soviet hobby computer Orion-128/256

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1994-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ORIONIX_H
#define __ORIONIX_H
#include "proc8080.h"

#define ORIONIX_VER "v2.0"

#define ORCOL 384
#define ORROW 256
#define PAGE_SIZE 0x4000
#define PAGE_MASK 0x3FFF
#define PAGE_BITS 14
#define LOADADR 0x1610
#define PERIF_R 0
#define PERIF_W 1

class Orion : public Processor8080
{
 SH_BYTE *memory;
 SH_WORD page_mem;
 SH_WORD ascr;
 int pscr;
 SH_BYTE fcolors;
 SH_BYTE fpages;
 SH_BYTE fscreens;
 unsigned long tlast;
 SH_BIT fmem;
 SH_BIT start;
public:
 SH_BYTE page[4];
 SH_BYTE kb_a,rus_lat;
 SH_BYTE rom_a,rom_b,rom_c;
 SH_BYTE rom2_a,rom2_b,rom2_c;
 unsigned short key;
 Orion(int pmem=4,char *code=NULL);
 ~Orion();
 void error(int n,SH_BYTE b=0);
 SH_BYTE get(SH_WORD a); // read from memory
 void put(SH_WORD a,SH_BYTE b); // write to memory
 SH_BYTE interruption(void); // interrupt
 int aux(SH_BYTE com,SH_BIT ok); // parallel activity
 SH_BYTE inp(SH_BYTE p); // read from port
 void outp(SH_BYTE p,SH_BYTE b); // write to port 
 int keyRK(void);
 int load(const char *s,SH_WORD adr);
 int save(const char *s,SH_WORD adr,SH_WORD len);
 SH_BIT perif(SH_WORD a,SH_BIT f,SH_BYTE *b);
};

#endif
