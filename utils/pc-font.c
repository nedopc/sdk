/* pc-font.c - Alexander Shabarshin (19 Apr 2007) */

#include <stdio.h>
#include "my_font.h"

int main()
{
 int i,j;
 FILE *f;
 f = fopen("pc-font.txt","wt");
 if(f==NULL) return -1;
 fprintf(f,"FONT1.SPR\n");
 for(i=0;i<FONT5X7_CHARS;i++)
 {
    printf("#%2.2X #%2.2X #%2.2X #%2.2X #%2.2X #%2.2X #%2.2X\n",
       font5x7[i][0],font5x7[i][1],font5x7[i][2],font5x7[i][3],
       font5x7[i][4],font5x7[i][5],font5x7[i][6]);
    fprintf(f,"06");       
    for(j=0;j<7;j++) fprintf(f,"%2.2X",font5x7[i][j]);
    fprintf(f,"00: 0x%2.2X '%c'\n",i+32,(char)(i+32));
 }
 fclose(f);
 return 0;
}
