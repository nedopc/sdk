/* cmp8080.c - Alexander Shabarshin  03.07.2011 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0
#define DEBUG
#endif
#if 1
#define WITHLXI
#endif
#define MINSEC 4
#define MINNAD 2

int main(int argc, char** argv)
{
 int i,j,k,g,a,m,n,p,b,f,l1,l2,ma,mb;
 FILE *f1,*f2;
 short *m1,*m2;
 if(argc<3) return -1;
 f1 = fopen(argv[1],"rb");
 if(f1==NULL) return -2;
 fseek(f1,0,SEEK_END);
 l1 = ftell(f1);
 fseek(f1,0,SEEK_SET);
 m1 = (short*)malloc(l1*2);
 if(m1==NULL){fclose(f1);return -3;}
 for(i=0;i<l1;i++) m1[i]=fgetc(f1);
 fclose(f1);
 printf("%s loaded : %i bytes\n",argv[1],l1);
 f2 = fopen(argv[2],"rb");
 if(f2==NULL) return -3;
 fseek(f2,0,SEEK_END);
 l2 = ftell(f2);
 fseek(f2,0,SEEK_SET);
 m2 = (short*)malloc(l2*2);
 if(m2==NULL){fclose(f2);return -3;}
 for(i=0;i<l2;i++) m2[i]=fgetc(f2);
 fclose(f2);
 printf("%s loaded : %i bytes\n",argv[2],l2);
 ma = -1;
 m = 0;
 p = 0;
 for(i=0;i<l1;i++)
 {
   if(m1[i]<0) continue;
#ifdef DEBUG
   printf("0x%2.2X\t%i max=%i\n",i,p,m);
#endif
   for(j=0;j<l2;j++)
   {
     if(m2[j]<0) continue;
     a=-1;
     n=0;
     k=0;
     g=1;
     while(g)
     {
       f=0;
       if(i+k<l1 && j+k<l2) 
       {
         b=m1[i+k];
#ifdef DEBUG
         printf("compare m1[0x%4.4X]=0x%2.2X and m2[0x%4.4X]=0x%2.2X (k=%i a=%i)\n",i+k,b,j+k,m2[j+k],k,a);
#endif
       }
       else b=-1000;
       if(b>=0&&b==m2[j+k])
       {
         f=1;
         if(b==0x22||b==0x2A||b==0x32||b==0x3A||
#ifdef WITHLXI
            b==0x01||b==0x11||b==0x21||b==0x31||
#endif
            b==0xC2||b==0xC3||b==0xC4||b==0xCA||b==0xCC||b==0xCD||
            b==0xD2||b==0xD4||b==0xDA||b==0xDC||
            b==0xE2||b==0xE4||b==0xEA||b==0xEC||
            b==0xF2||b==0xF4||b==0xFA||b==0xFC) a=0;
         else if(b!=0x00&&a<0) n++;
       }
       else
       {
         if(b>=0&&(a==1||a==2)) f=1;
       }
       if(f)
       {
#ifdef DEBUG
           printf("SAME!\n");
#endif
           m1[i+k]=-m1[i+k];
           if(m1[i+k]==0) m1[i+k]=-256;
       }
       else
       {
           g=0;
           if(n>=MINNAD&&k>=MINSEC)
           {
             if(k>m){m=k;ma=i;mb=j;}
             p+=k;
#ifdef DEBUG
             printf("DETECTED %i BYTES (%i)!\n",k,n);
#endif
             while(--k>=0)
             {
               m2[j+k]=-m2[j+k];
               if(m2[j+k]==0) m2[j+k]=-256;
             }
           }
           else
           {
             while(--k>=0)
             {
               m1[i+k]=-m1[i+k];
               if(m1[i+k]==256) m1[i+k]=0;
             }
           }
       }
       if(a==2) a=-1;
       if(a>=0) a++;
       k++;
     }
   }
 }
 printf("%2.2lf percent copy (max=%i maxa=0x%4.4X maxb=0x%4.4X)\n",100.0*p/l1,m,ma,mb);
#if 0
 for(i=0;i<m;i++) printf("0x%4.4X 0x%2.2X | 0x%4.4X 0x%2.2X\n",
    ma+i,m1[ma+i]<0?(-m1[ma+i])&0xFF:-1,mb+i,m2[mb+i]<0?(-m2[mb+i])&0xFF:-1);
#endif
 free(m1);
 free(m2);
 return 0;
}
