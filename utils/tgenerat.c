// tgenerat.c - Alexander Shabarshin 18.02.2008

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int ternary(int num, char* buf, int bufsz)
{
  int n,m,k,i,j;
  *buf = 0;
  j = 0;
  k = 387420489;
  for(i=0;i<=18;i++)
  {
    if(j==bufsz-1) break;
    n = num/k;
    num -= n*k;
    m = k/2;
    if(num>+m){n++;num-=k;}
    if(num<-m){n--;num+=k;}
    if(n==-1) buf[j++]='N';
    if(n==0 && k<729) buf[j++]='O';
    if(n==1) buf[j++]='P';
    k /= 3;
  }
  buf[j] = 0;
  return j;
}

char ternary9(char* s) // for Tunguska
{
  char c = '?';
  switch(s[0])
  {
   case 'N': switch(s[1]){ case 'N': c='D';break; case 'O': c='C';break; case 'P': c='B';break;};break;
   case 'O': switch(s[1]){ case 'N': c='A';break; case 'O': c='0';break; case 'P': c='1';break;};break;
   case 'P': switch(s[1]){ case 'N': c='2';break; case 'O': c='3';break; case 'P': c='4';break;};break;
  }
  return c;
}

int main()
{
 int i,j,k;
 unsigned short sis;
 char str[16],ss[8];
 FILE *frwi,*finc,*ftab;
 frwi = fopen("ternary.rwi","wt");
 finc = fopen("ternary.inc","wt");
 ftab = fopen("tungutab.txt","wt");
 if(finc!=NULL&&ftab!=NULL)
 {
   for(i=-364;i<=364;i++)
   {
     fprintf(finc,"\\ %i\n",i);
     ternary(i,str,16);
     fprintf(frwi,"@T_%s=%i\n",str,i);
     sis = (unsigned short)i;
     fprintf(finc,"@T_%s EQU #%4.4X\n",str,sis);
     str[0] = ternary9(&str[0]);
     str[1] = ternary9(&str[2]);
     str[2] = ternary9(&str[4]);
     str[3] = 0;
     fprintf(frwi,"@T9_%s=%i\n",str,i);
     sis = (unsigned short)i;
     fprintf(finc,"@T9_%s EQU #%4.4X\n",str,sis);
     *ss = 0;
     k = 0;
     switch(str[1])
     {
       case 'D': k-=4; break;
       case 'C': k-=3; break;
       case 'B': k-=2; break;
       case 'A': k-=1; break;
       case '1': k+=1; break;
       case '2': k+=2; break;
       case '3': k+=3; break;
       case '4': k+=4; break;
     }
     switch(str[0])
     {
       case 'D': k-=36; break;
       case 'C': k-=27; break;
       case 'B': k-=18; break;
       case 'A': k-=9; break;
       case '1': k+=9; break;
       case '2': k+=18; break;
       case '3': k+=27; break;
       case '4': k+=36; break;
     }
     j = 0;
     switch(str[2])
     {
       case 'D': strcpy(ss,"_AB"); j=2; k-=324; break;
       case 'C': strcpy(ss,"_IM"); j=1; k-=243; break;
       case 'B': strcpy(ss,"_AX"); j=2; k-=162; break;
       case 'A': strcpy(ss,"_AY"); j=2; k-=81; break;
       case '0': strcpy(ss,"_AC"); j=0; break;
       case '1': strcpy(ss,"_IX"); j=2; k+=81; break;
       case '2': strcpy(ss,"_IY"); j=2; k+=162; break;
       case '3': strcpy(ss,"_IN"); j=2; k+=243; break;
       case '4': strcpy(ss,"_XY"); j=0; k+=324; break;
     }
     fprintf(ftab," %c %c%c %s #%2.2X #%2.2X",str[2],str[0],str[1],ss,k&0xFF,(k>>8)&0xFF);
     if(j>0) fprintf(ftab," L H");
     if(j>1) fprintf(ftab," L H");
     fprintf(ftab,"\n");
     if(str[2]=='4') fprintf(ftab,"\n");
   }
 }
 if(frwi!=NULL) fclose(frwi);
 if(finc!=NULL) fclose(finc);
 if(ftab!=NULL) fclose(ftab);
 return 0;
}
