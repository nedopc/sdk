/* Generation of 1st part of VOJA4.TAB for RASM v2.6 */

/* It was written in October 2022 by Shaos and moved to PUBLIC DOMAIN */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int printreg(int i)
{
 int k=2;
 switch(i)
 {
  case 0:  printf("R0"); break;
  case 1:  printf("R1"); break;
  case 2:  printf("R2"); break;
  case 3:  printf("R3"); break;
  case 4:  printf("R4"); break;
  case 5:  printf("R5"); break;
  case 6:  printf("R6"); break;
  case 7:  printf("R7"); break;
  case 8:  printf("R8"); break;
  case 9:  printf("R9"); break;
  case 10: printf("R10"); k=3; break;
  case 11: printf("R11"); k=3; break;
  case 12: printf("JSR"); k=3; break;
  case 13: printf("PCL"); k=3; break;
  case 14: printf("PCM"); k=3; break;
  case 15: printf("PCH"); k=3; break;
  default: printf("ERROR");k=5;
 }
 return k;
}

int main()
{
 int i,j,k,l;
/*
ADD_R0,R0        #01 #00
ADD_R0,R1        #01 #01
ADD_R0,R2        #01 #02
ADD_R0,R3        #01 #03
ADD_R0,R4        #01 #04
ADD_R0,R5        #01 #05
ADD_R0,R6        #01 #06
ADD_R0,R7        #01 #07
ADD_R0,R8        #01 #08
ADD_R0,R9        #01 #09
ADD_R0,R10       #01 #0A
ADD_R0,R11       #01 #0B
ADD_R0,JSR       #01 #0C
ADD_R0,PCL       #01 #0D
ADD_R0,PCM       #01 #0E
ADD_R0,PCH       #01 #0F
...
MOV_R0,[PCH,PCH] #0B #FF
*/
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("ADD_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#01 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("ADC_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#02 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("SUB_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#03 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("SBB_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#04 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=3;printf("OR_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#05 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("AND_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#06 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("XOR_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#07 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=4;printf("MOV_");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       for(l=0;l<17-k;l++) printf(" ");
       printf("#08 #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    k=4;printf("MOV_");
    k+=printreg(i);
    k++;printf(",");
    for(l=0;l<17-k;l++) printf(" ");
    printf("#09 #%2.2X+\n",i<<4);
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=5;printf("MOV_[");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       k+=4;printf("],R0");
       for(l=0;l<17-k;l++) printf(" ");
       printf("#0A #%2.2X\n",(i<<4)|j);
    }
 }
 for(i=0;i<16;i++)
 {
    for(j=0;j<16;j++)
    {
       k=8;printf("MOV_R0,[");
       k+=printreg(i);
       k++;printf(",");
       k+=printreg(j);
       k+=1;printf("]");
       for(l=0;l<17-k;l++) printf(" ");
       printf("#0B #%2.2X\n",(i<<4)|j);
    }
 }
 return 0;
}
