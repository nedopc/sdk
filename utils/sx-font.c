/* sx-font.c - Alexander Shabarshin (27 Dec 2006) */

#include <stdio.h>
#include "my_font.h"

int main()
{
 int i,j;
 FILE *f;
 f = fopen("sx-font.inc","wt");
 if(f==NULL) return -1;
 for(i=0;i<FONT5X7_CHARS;i++)
 {
    printf("#%2.2X #%2.2X #%2.2X #%2.2X #%2.2X #%2.2X #%2.2X\n",
       font5x7[i][0],font5x7[i][1],font5x7[i][2],font5x7[i][3],
       font5x7[i][4],font5x7[i][5],font5x7[i][6]);
#if 0       
    if(i==0) fprintf(f,"\n\torg 0x200\n\tmacro_prefix1\n");
    if(i==32) fprintf(f,"\n\torg 0x400\n\tmacro_prefix2\n");
    if(i==64) fprintf(f,"\n\torg 0x600\n\tmacro_prefix3\n");
    if(i < 96)
    {
       for(j=0;j<7;j++) fprintf(f,"\tretlw 0x%2.2X\n",font5x7[i][j]);
    }   
#else
    fprintf(f,"\t; 0x%2.2X '%c'\n",i+32,(char)(i+32));
    for(j=0;j<7;j++) fprintf(f,"\tdata 0x%2.2X\n",font5x7[i][j]);
    if(i!=FONT5X7_CHARS-1) fprintf(f,"\tdata 0x00\n\n");
#endif    
 }
 fclose(f);
 return 0;
}
