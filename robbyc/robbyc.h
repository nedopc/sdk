/*  robbyc.h - Header file for ROBBY compiler (formerly known as RW1C)

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1998-2025, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ROBBYC_H
#define __ROBBYC_H

#include <ctype.h>
#include "my_text.h"

#define VERSION "2.3.4"

/*
 v2.3.4  (22.01.2025)
 v2.3.3  (30.07.2024)
 v2.3.2  (17.07.2024)
 v2.3.1  (08.07.2024)
 v2.3.0  (20.05.2018)
 v2.2.0  (13.12.2012)
 v2.1.6  (21.04.2007)
 v2.1.5  (26.12.2006)
 v2.1.4  (19.12.2005)
 v2.1.3  (29.11.2005)
 v2.1.2  (04.04.2003)
 v2.1.1  (30.09.2002)
 v2.1.0  (18.09.2002)
 v2.0.12 (15.05.2002)
 v2.0.11 (29.01.2002)
 v2.0.10 (22.01.2002)
 v2.0.9  (21.01.2002)
 v2.0.8  (15.01.2002)
 v2.0.7  (05.08.2001)
 v2.0.6  (10.05.2001)
 v2.0.5  (04.05.2001)
 v2.0.4  (18.04.2001)
 v2.0.3  (13.04.2001)
 v2.0.2  (12.04.2001)
 v2.0.1  (10.04.2001)
 v2.0.0  (02.11.2000)
*/

#define MAXLINESIZE  1024

#if DOS && i8086
/* for Pacific C */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

typedef struct _COMPTAB
{ char str[16];
  char kod[8];
} COMPTAB;

extern COMPTAB CompTab[];

extern int f_n; /* v2.3.4 */

#define IsName(c) (isdigit(c) || isalpha(c) || (c=='_') || (c=='$'))

#ifdef __cplusplus
extern "C" {
#endif

void RW1Error(int e, Line *l);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

