/*  robbyc1.c - Source code for ROBBY preprocessor

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1998-2025, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "robbyc.h"
#include "robbyc1.h"

int NumFile = 0;
int NumLabel = 1;
Text *Files = NULL;
Text *Macros = NULL;

void RobotFree(void)
{
  TextDel(Macros);
  Macros = NULL;
  TextDel(Files);
  Files = NULL;
}

int IsCommand(char *s)
{
  int i,k=0,o=1;
  if(s[0]=='(') return 0;
  if(s[strlen(s)-1]!=')') return 0;
  for(i=0;i<(int)strlen(s);i++)
  {
     if(s[i]=='(') k++;
     if(!k&&!IsName(s[i])) o=0;
     if(s[i]==')') k--;
     if(!o) break;
  }
  return o;
}

int DeleteComments(char *po)
{
  int i,k;
  char *poo,*pp;
  k = 0;
  if(*po=='%')
  {  *po = 0;
     return 1;
  }
  poo = strchr(po,'/');
  if(poo!=NULL)
  {  if(poo[1]=='/')
     {  k = 1;
        *poo = 0;
     }
  }
  pp = po;
  while(1)
  {  poo = strchr(pp,'%');
     if(poo!=NULL)
     {  poo--;
        if(*poo==' '||*poo=='\t')
        {  k = 1;
           *poo = 0;
           break;
        }
        else pp=&poo[2];
     }
     else break;
  }
  i = strlen(po)-1;
  while(po[i]==' '||po[i]=='\t') po[i--]=0;
  return k;
}

int IncludeFile(Text *t, long n, char *path1, char *path2)
{
  char *po;
  long i;
  FILE *f;
  Line *l;
  char str[MAXLINESIZE];
  l = TextGet(t,n);
  DeleteComments(l->str);
  po = l->str;
  while(*po==' '||*po=='\t') po++;
  po++;
  /* since v2.3.0 we work with .R files */
  sprintf(str,"%s%s.R",path1,po);
  f = fopen(str,"rt");
  if(f==NULL)
  {
     sprintf(str,"%s%s.R",path2,po);
     f = fopen(str,"rt");
  }
  if(f==NULL) return 0;
  *l->str = '%';
  NumFile++;
  TextAdd(Files,po);
  i = n;
  while(1)
  {
     i++;
     fgets(str,MAXLINESIZE,f);
     if(feof(f)) break;
     po = strrchr(str,'\n');
     if(po!=NULL) *po=0;
     po = strrchr(str,'\r');
     if(po!=NULL) *po=0;
     l = TextInsert(t,i,str);
     l->id = NumFile;
     l->id2 = i-n;
  }
  fclose(f);
  return 1;
}

int AddMacros(Line *l)
{
  int i,j,k;
  char *po,*po1,*po2;
  static char str1[MAXLINESIZE];
  static char str2[MAXLINESIZE];
  Line *ll;
  po = l->str;
  while(*po==' '||*po=='\t') po++; /* 2.1.3 */
  if(*po!='@') RW1Error(23,l);
  po++;
  strcpy(str1,po);
  /* 2.1.3 BEGIN */
  po = strchr(po,'=');
  if(po==NULL) RW1Error(23,l);
  po++;
  while(*po==' '||*po=='\t') po++;
  /* 2.1.3 END */
  po1 = strchr(str1,'=');
  if(po1==NULL) RW1Error(23,l);
  *po1 = 0;
  /* 2.1.3 BEGIN */
  j = 0;
  for(i=0;i<(int)strlen(str1);i++)
  {
     if(str1[i]!=' '&&str1[i]!='\t')
     {
        str1[j] = str1[i];
        j++;
     }
  }
  str1[j] = 0;
  /* 2.1.3 END */
  k = 0;
  po1 = strchr(str1,'(');
  if(po1!=NULL)
  {
     po2 = strchr(str1,')');
     if(po2==NULL) RW1Error(23,l);
     if(po1>po2) RW1Error(23,l);
     if(po1[0]=='(' && po1[2]==')') k=po1[1]-'0';
     if(!k) RW1Error(23,l);
     *po1 = 0;
  }
  for(ll=Macros->first;ll!=NULL;ll=ll->next)
  {
     strcpy(str2,ll->str);
     po2 = strchr(str2,'=');
     *po2 = 0;
     if(!strcmp(str1,str2)) RW1Error(24,l);
  }
  sprintf(str2,"%s=%s",str1,po); /* 2.1.3 */
  ll = TextAdd(Macros,str2); /* 2.1.3 */
  ll->id2 = k;
  return 1;
}

int DelMacros(Line *li,char *s)
{
  int i,j,k,sko;
  Line *l;
  char *po,*pv,*pm1,*pm2,*pa;
  static char str[MAXLINESIZE];
  char mak[50],args[4][256];
  po = s;
  if(po==NULL) return 0;
  i = k = 0;
  while(*po)
  {
     if(*po=='@')
     {
        pm1 = po;
        po++;
        k = 1;
        break;
     }
     str[i++] = *po;
     po++;
  }
  if(!k) return 0;
  j = 0;
  while(IsName(*po))
  {
     mak[j++] = *po;
     po++;
  }
  mak[j] = 0;
  pm2 = po;
  k = 0;
  if(*pm2=='(')
  {
     pm2++;
     sko = j = 0;
     while(1)
     {
        args[k][j++] = *pm2;
        if(*pm2==0) RW1Error(25,li);
        if((*pm2==')' || *pm2==',') && sko==0)
        {
           args[k++][j-1] = 0;
           j = 0;
#ifdef DEBUG
           printf("@%s @%i='%s'\n",mak,k,args[k-1]);
#endif
           if(*pm2==')')
           {
              pm2++;
              break;
           }
        }
        if(*pm2=='(') sko++;
        if(*pm2==')') sko--;
        pm2++;
     }
  }
  for(l=Macros->first;l!=NULL;l=l->next)
  {
     po = strchr(l->str,'=');
     if(po==NULL) continue;
     pv = ++po;
     strcpy(str,l->str);
     po = strchr(str,'=');
     *po = 0;
     po--;
     if(*po==')')
     {
        po--;
        po--;
        *po = 0;
     }
     if(!strcmp(mak,str))
     {
        if(k!=l->id2) RW1Error(25,li);
        break;
     }
  }
  if(l==NULL) RW1Error(25,li);
  po = s;
  i = 0;
  while(po!=pm1) str[i++]=*po++;
  po = pv;
  while(1)
  {
     if(po[0]=='@'&&isdigit(po[1]))
     {
        po++;
        j = *po - '1';
        if(j<0||j>=k) RW1Error(25,li);
        pa = args[j];
        while(*pa) str[i++]=*pa++;
     }
     else str[i++] = *po;
     po++;
     if(*po==0) break;
  }
  po = pm2;
  while(*po!=0) str[i++]=*po++;
  str[i] = 0;
#ifdef DEBUG
  printf("@DelMacros|%s|\n",str);
#endif
  strcpy(s,str);
  return 1;
}

int TestFor(char *po) /* 2.0.10 */
{
  char *poo,*pooo;
  poo = strstr(po,"FOR(");
  if(poo==NULL) return 0;
  pooo = strchr(poo,')');
  if(pooo==NULL) return 0;
  while(poo!=pooo)
  {  if(*poo==';') *poo=',';
     poo++;
  }
  return 0;
}

char* RobotGetFile(int id, char *df)
{
  Line *l;
  if(Files==NULL) return df;
  l = TextGet(Files,id);
  if(l==NULL) return df;
  return l->str;
}

void RobotWriteFiles(FILE *f)
{
  Line *l;
  if(Files==NULL) return;
  for(l=Files->first;l!=NULL;l=l->next)
  {
    fprintf(f,"%s",l->str);
    fputc(0,f);
  }
}

void RobotWriteMacros(FILE *f)
{
  Line *l;
  if(Macros==NULL) return;
  TextSort(Macros,0);
  for(l=Macros->first;l!=NULL;l=l->next)
    fprintf(f,"%s\n",l->str);
}

int RobotPreprocessor(Text *t, Text *tt, char *df, char *p1, char *p2)
{
  int i,j,k,kk,ii,num,fnum,sti,fend,fif,obr,fig_b,fig_e,fdef;
  static char str[MAXLINESIZE];
  static char str_[MAXLINESIZE];
  static char str0[MAXLINESIZE];
  static char str1[MAXLINESIZE];
  static char str2[MAXLINESIZE];
  static char str3[MAXLINESIZE];
  static char str4[MAXLINESIZE];
  static char stack1[MAXLOOPDEPTH];
  static short lab[MAXLOOPDEPTH];
  static char* dop[MAXLOOPDEPTH];
  char cur[100],*po,*poo1,*poo2;
  Line *l,*ll;
  Files = TextNew();
  TextAdd(Files,df);
  Macros = TextNew();
  num = 1;
  sti = 0; /* {} level */
  fend = 0;
  *cur = 0;
  dop[sti] = NULL;
  fnum = NumFile;
  fig_b = 0;
  fig_e = 0;
  fif = 0;
  for(l=t->first;l!=NULL;l=l->next)
  {
    if(!l->id)
    {
      l->id = fnum;
      l->id2 = num++;
    }
    /* Include file */
    po = l->str;
    while(*po==' '||*po=='\t') po++;
    if(*po=='+')
    {
       i = TextIndex(t,l);
       if(f_n||!IncludeFile(t,i,p1,p2)) RW1Error(22,l);
    }
    /* Upper case */
    k = 0;
    kk = 0;
    for(i=0;i<(int)strlen(l->str);i++)
    {
       j = l->str[i];
       if(j=='{') fig_b++;
       if(j=='}') fig_e++;
       if(j=='"')
       {  if(k) k=0;
          else  k=1;
       }
       if(!k&&j=='\'')
       {  if(kk) kk=0;
          else  kk=1;
       }
       if(!k&&!kk) l->str[i]=toupper(l->str[i]);
    }
  }
  l = t->first;
  if(fig_b!=fig_e) RW1Error(37,l);
  while(1)
  {
    if(l==NULL) RW1Error(4,l);
    po = l->str;
    while(*po==' '||*po=='\t') po++;
    DeleteComments(po);
    if(*po)
    {
      if(!strcmp(po,"START:")) break;
      if(!strcmp(po,"MAIN()")) break;
      strcpy(str,po);
      if(*str=='@') AddMacros(l);
      else
      {
         ll = TextAdd(tt,str);
         ll->id = l->id;
         ll->id2 = l->id2;
      }
    }
    l = l->next;
  }
  po = l->str;
  *str3 = *str4 = 0;
  while(1)
  {
    /* Skip empty lines */
    while(!*po)
    {  l = l->next;
       if(l==NULL) break;
       po = l->str;
       DeleteComments(po);
    }
    if(l==NULL) break;
    while(*po==' '||*po=='\t') po++;
    TestFor(po);
    fdef = 0; /* 2.1.1 */
    if(!strncmp(po,"DEF ",4)) fdef = 1;
    if(!strncmp(po,"}WHILE(",7)) fdef = 1;
    ii = 0;
    j = 1;
    while(*po!=0 && *po!=';')
    {
       k = *po;
       str[ii++] = k;
       if(!fdef&&(*po=='{'||*po=='}')) /* 2.1.1 */
       {
          if(ii>1){ii--;po--;}
          break;
       }
       if(!IsName(k)) j = 0;
       po++;
       if(j && *po==':')
       {
          str[ii++] = ':';
          break;
       }
    }
    str[ii] = 0;
    if(*po) po++;
    if(*str=='@'&&sti==0) AddMacros(l); /* 2.1.3 */
    else while(DelMacros(l,str));
#ifdef DEBUG
    printf("1|%s|\n",str);
#endif
    obr = 0;
    if(!strcmp(str,"ELSE"))
    {
       if(!fif) RW1Error(28,l);
       sprintf(str,"GOTO _l%u;_l%u:",lab[sti]+1,lab[sti]);
       stack1[sti] = 111;
       obr = 1;
    }
    if(fif==2)
    {
       if(*str4)
       {  sprintf(str2," _l%u:",lab[sti]+1);
          strcat(str4,str2);
       }
       else sprintf(str4,";_l%u:",lab[sti]+1);
       fif = 0;
    }
    if(fif==3)
    {
       if(*str3)
       {  sprintf(str2,"_l%u: ",lab[sti]+1);
          strcat(str3,str2);
       }
       else sprintf(str3,"_l%u: ",lab[sti]+1);
       fif = 0;
    }
    if(!strncmp(str,"ELSE ",5))
    {
       if(!fif) RW1Error(28,l);
       strcpy(str_,&str[5]); /* !!! */
       po = str_;
       sprintf(str,"GOTO _l%u;_l%u:",lab[sti]+1,lab[sti]);
       fif = 2;
       obr = 1;
    }
    if(fif==1)
    {
       if(!obr)
       {
          if(*str3) /* 2.0.9 */
          {  sprintf(str2,"_l%u: ",lab[sti]);
             strcat(str3,str2);
          }
          else sprintf(str3,"_l%u: ",lab[sti]);
       }
       fif = 0;
    }
    if(fif==5)
    {
       if(*str3)
       {  sprintf(str1,"%s;",str2);
          strcat(str3,str1);
       }
       else sprintf(str3,"%s;",str2);
       fif = 1;
    }
    if(str[0]=='I'&&str[1]=='F'&&(str[2]==' '||str[2]=='\t'||str[2]=='('))
    {
       poo1 = &str[2];
       stack1[sti] = 110;
       lab[sti] = NumLabel;
       NumLabel += 2;
       if(str[2]=='(')
       {
          if(str[strlen(str)-1]==')')
          {
             strcpy(str2,poo1);
             sprintf(str,"%s;IFN L _l%u",str2,lab[sti]);
          }
          else
          {  /* something exists after ')' */
             j = 2;
             k = 0;
             while(1)
             {
                if(*poo1=='(') k++;
                if(*poo1==')')
                {  k--;
                   if(!k) break;
                }
                if(*poo1==0) RW1Error(29,l);
                poo1++;
                j++;
             }
             poo1++;
             j++;
             if(*poo1!=' '&&*poo1!='\t') RW1Error(29,l);
             *poo1 = 0;
             j++;
             strcpy(str_,&str[j]); /* !!! */
             po = str_;
             sprintf(str2,"%s;IFN L _l%u",&str[2],lab[sti]);
             fif = 5;
             continue;
          }
       }
       else
       {  /* it's 'if expr :\n label' construction */
          poo2 = strrchr(str,':');
          if(poo2==NULL) RW1Error(29,l);
          *poo2 = 0;
          poo2++;
          while(*poo1==' '||*poo1=='\t') poo1++;
          while(*poo2==' '||*poo2=='\t') poo2++;
          strcpy(str1,poo1);
          strcpy(str2,poo2);
          while(str1[strlen(str1)-1]==' ') str1[strlen(str1)-1] = 0;
          while(str1[strlen(str2)-1]==' ') str1[strlen(str2)-1] = 0;
          sprintf(str,"(%s);IFY L %s",str1,str2);
       }
       obr = 1;
    }
    if(!strncmp(str,"WHILE(",6))
    {
       poo1 = &str[5];
       while(*poo1==' '||*poo1=='\t') poo1++;
       if(poo1[strlen(poo1)-1]!=')') RW1Error(26,l);
       strcpy(str2,poo1);
       stack1[sti] = 120;
       lab[sti] = NumLabel;
       NumLabel += 2;
       sprintf(str,"_l%u: %s;IFN L _l%u",lab[sti],str2,lab[sti]+1);
       obr = 1;
    }
    if(!strcmp(str,"DO"))
    {
       stack1[sti] = 121;
       lab[sti] = NumLabel;
       NumLabel += 2;
       sprintf(str,"_l%u:",lab[sti]);
       obr = 1;
    }
    if(!strncmp(str,"}WHILE(",7))
    {
       poo1 = &str[6];
       while(*poo1==' '||*poo1=='\t') poo1++;
       if(poo1[strlen(poo1)-1]!=')') RW1Error(26,l);
       strcpy(str2,poo1);
       sti--;
       if(stack1[sti]!=21) RW1Error(26,l);
       sprintf(str,"%s;IFY L _l%u;_l%u:",str2,lab[sti],lab[sti]+1);
       obr = 1;
    }
    if(!strncmp(str,"FOR(",4))
    {
       poo1 = &str[4];
       while(*poo1==' '||*poo1=='\t') poo1++;
       if(poo1[strlen(poo1)-1]!=')') RW1Error(26,l);
       poo1[strlen(poo1)-1] = 0;
       strcpy(str2,poo1);
       stack1[sti] = 122;
       poo1 = strtok(str2,",");
       if(poo1==NULL) RW1Error(26,l);
       lab[sti] = NumLabel;
       NumLabel += 2;
       sprintf(str,"%s;_l%u: ",poo1,lab[sti]);
       poo1 = strtok(NULL,",");
       if(poo1==NULL) RW1Error(26,l);
       strcat(str,"(");
       strcat(str,poo1);
       strcat(str,")");
       strcat(str,";");
       poo1 = strtok(NULL,",");
       if(poo1==NULL) RW1Error(26,l);
       strcpy(str2,poo1);
       if((poo1[strlen(poo1)-1]=='+'&&poo1[strlen(poo1)-2]=='+') ||
          (poo1[strlen(poo1)-1]=='-'&&poo1[strlen(poo1)-2]=='-')) {
          str2[strlen(str2)-2] = 0;
          for(i=0;i<(int)strlen(str2);i++) if(!IsName(str2[i])) break;
          if(i!=(int)strlen(str2)) RW1Error(26,l);
          sprintf(str1,"%s=%s%c1",str2,str2,poo1[strlen(poo1)-1]);
          strcpy(str2,str1);
       }
       dop[sti] = (char*)malloc(strlen(str2)+1);
       if(dop[sti]==NULL) RW1Error(0,l);
       strcpy(dop[sti],str2);
       poo1 = strtok(NULL,",");
       if(poo1!=NULL) RW1Error(26,l);
       sprintf(str2,"IFN L _l%u",lab[sti]+1);
       strcat(str,str2);
       obr = 1;
    }
    if(!strncmp(str,"RETURN ",7)) /* 2.1.1 */
    {
       poo1 = &str[7];
       while(*poo1==' '||*poo1=='\t') poo1++;
       sprintf(str,"(%s);RET",poo1);
       obr = 1;
    }
    k = strlen(str);
    if(!obr&&str[k-1]==')'&&str[k-2]=='(')
    {
       if(!strcmp(str,"MAIN()"))
       {
          stack1[sti] = 101;
          strcpy(cur,"MAIN");
          strcpy(str,"START:");
          obr = 1;
       }
       else if(sti==0) /* 2.1.3 */
       {
          for(j=0;j<k-2;j++) if(!IsName(str[j])) break;
          if(j==k-2)
          {
             stack1[sti] = 102;
             str[k-2] = 0;
             strcpy(cur,str);
             strcat(str,":");
          }
          obr = 1;
       }
    }
    if(!strcmp(str,"CONTINUE"))
    {
       j = sti-1;
       while(j>=0)
       {
          k = 1;
          switch(stack1[j])
          {
            case 1:
                 strcpy(str,"GOTO START");
                 break;
            case 2:
                 sprintf(str,"GOTO %s",cur);
                 break;
            case 20:
            case 21:
            case 22:
                 sprintf(str,"GOTO _l%u",lab[j]);
                 break;
            default:
                 k = 0;
          }
          if(k) break;
          j--;
       }
       obr = 1;
    }
    if(!strcmp(str,"BREAK"))
    {
       j = sti-1;
       while(j>=0)
       {
          k = 1;
          switch(stack1[j])
          {
            case 1:
                 strcpy(str,"GOTO __END");
                 break;
            case 2:
                 strcpy(str,"RET");
                 break;
            case 20:
            case 21:
            case 22:
                 sprintf(str,"GOTO _l%u",lab[j]+1);
                 break;
            default:
                 k = 0;
          }
          if(k) break;
          j--;
       }
       obr = 1;
    }
    if(!strcmp(str,"END"))
    {
       fend = 1;
       strcpy(str,"__END: END");
       obr = 1;
    }
    if(!strcmp(str,"{"))
    {
       if(stack1[sti]<100) RW1Error(4,l);
       stack1[sti] -= 100;
       sti++;
       dop[sti] = NULL;
       continue;
    }
    if(!strcmp(str,"}"))
    {
       sti--;
       if(stack1[sti]==1) strcpy(str,"RET"); /* Since v2.3.2 for MAIN it's also RET */
       else if(stack1[sti]==2) strcpy(str,"RET");
       else if(stack1[sti]==10) {fif=1;continue;}
       else if(stack1[sti]==11) {fif=3;continue;}
       else if(stack1[sti]==20) sprintf(str,"GOTO _l%u;_l%u:",lab[sti],lab[sti]+1);
       else if(stack1[sti]==21) RW1Error(26,l);
       else if(stack1[sti]==22)
       {  sprintf(str,"%s;GOTO _l%u;_l%u:",dop[sti],lab[sti],lab[sti]+1);
          free(dop[sti]);
       }
       obr = 1;
    }
    if(!obr&&IsCommand(str))
    {
#ifdef DEBUG
       printf(">ISCOMMAND:%s\n",str);
#endif
       poo1 = strchr(str,'(');
       if(poo1[strlen(poo1)-1]!=')') RW1Error(26,l);
       poo1[strlen(poo1)-1] = 0;
       *poo1 = 0;
       poo1++;
       strcpy(str2,poo1);
       strcpy(str0,str);
       strcpy(str,"");
       poo1 = strtok(str2,",");
       k = 0;
       while(1)
       {
          if(poo1==NULL) break;
          str1[0] = 'A' + k;
          str1[1] = '=';
          str1[2] = 0;
          strcat(str,str1);
          strcat(str,poo1);
          strcat(str,";");
          poo1 = strtok(NULL,",");
          k++;
       }
       kk = 0;
       j = 0;
       while(1)
       {
          strcpy(str1,CompTab[j++].str);
          if(!strcmp(str1,"END")) break;
          poo1 = strchr(str1,' ');
          if(poo1!=NULL) *poo1=0;
          if(!strcmp(str1,str0))
          {
             kk = 1;
             break;
          }
       }
       if(kk) /* keyword */
       {
          strcat(str,str0);
          for(j=0;j<k;j++)
          {
             str1[0] = ' ';
             str1[1] = 'A' + j;
             str1[2] = 0;
             strcat(str,str1);
          }
       }
       else /* subprogram */
       {
          strcat(str,"CALL ");
          strcat(str,str0);
       }
    }
    if(*str3)
    {
       strcat(str3,str);
       strcpy(str,str3);
       *str3 = 0;
    }
    if(*str4)
    {
       strcat(str,str4);
       *str4 = 0;
    }
#ifdef DEBUG
    printf("2|%s|\n",str);
#endif
    if(*str!='@')
    {
       ll = TextAdd(tt,str);
       ll->id = l->id;
       ll->id2 = l->id2;
    }
  }
  if(!fend)
  {
    strcpy(str,"__END: END");
    ll = TextAdd(tt,str);
    ll->id = 0;
    ll->id2 = 0;
  }
#ifdef DEBUG
  printf("\nMACROS:\n");
  TextList(Macros);
  printf("\nFILES:\n");
  TextList(Files);
#endif
  return 1;
}
