/*  robbyc1.h - Header file for ROBBY preprocessor

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1998-2018, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ROBBYC1_H
#define __ROBBYC1_H

#include "my_text.h"

#define MAXLOOPDEPTH  100

#ifdef __cplusplus
extern "C" {
#endif

int RobotPreprocessor(Text *t, Text *tt, char *df, char *p1, char *p2);
void RobotWriteMacros(FILE *f);
void RobotWriteFiles(FILE *f);
char* RobotGetFile(int id, char *df);
void RobotFree(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
