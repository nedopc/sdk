/*  robbyc.c - Source code for ROBBY compiler (formerly known as RW1C)

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 1998-2025, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "robbyc.h"
#include "robbyc1.h"
#include "robbyc2.h"
#include "my_text.h"

#define MAXCODESIZE 32767 /* v2.3.1 reduced to fit 16-bit signed int */
#define MAXEXPRSIZE   255
#define MAXSTRILEN    256
#define MAXNAMELEN     32
#define LASTOPERAND     2

COMPTAB CompTab[] = {
  { "NOP",          "\x00\xFF" },

  { "ACT %m",       "\x3A\x1\xFF" },
  { "ASM %s",       "\x6C\x1\xFF" },
  { "CALL %m",      "\x44\x1\xFF" },
  { "CLEAR %m %m",  "\x74\x1\x2\xFF" },
  { "COLOR %m",     "\x68\x1\xFF" },
  { "COMMAND %m",   "\x7F\x1\xFF" },
  { "COPYP %a %a",  "\x70\x1\x2\xFF" },
  { "DUMP %b",      "\x3F\x0\x1\xFF" },
  { "DUMP %b %b",   "\x3F\x1\x2\xFF" },
  { "FILL %m %m",   "\x65\x1\x2\xFF" },
  { "GET %m",       "\x6B\x1\xFF" },
  { "GET",          "\x6B\x0\x0\x0\xFF" },
  { "GOTO %m",      "\x43\x1\xFF" },
  { "IFY %m %m",    "\x41\x1\x2\xFF" },
  { "IFN %m %m",    "\x42\x1\x2\xFF" },
  { "LEFT",         "\x35\xFF" },
  { "PIXEL %m %m",  "\x64\x1\x2\xFF" },
  { "PLANE %m",     "\x67\x1\xFF" },
  { "POP",          "\x63\xFF" },
  { "PRINT %m",     "\x38\x1\xFF" },
  { "PUSH",         "\x73\xFF" },
  { "RADAR %m",     "\x3C\x1\xFF" },
  { "RECV %a",      "\x62\x1\xFF" },
  { "RECVP %a",     "\x72\x1\xFF" },
  { "RET",          "\x33\xFF" },
  { "RIGHT",        "\x36\xFF" },
  { "SAY %s",       "\x37\x1\xFF" },
  { "SELECT %m %m", "\x69\x1\x2\xFF" },
  { "SEND %m %m",   "\x61\x1\x2\xFF" },
  { "SEND %m",      "\x61\x1\x0\x0\x0\xFF" },
  { "SENDP %a %m",  "\x71\x1\x2\xFF" },
  { "SENDP %a",     "\x71\x1\x0\x0\x0\xFF" },
  { "SET %m %m",    "\x6A\x1\x2\xFF" },
  { "SET %m",       "\x6A\x1\x0\x0\x0\xFF" },
  { "SHOT %a",      "\x60\x1\xFF" },
  { "SPY",          "\x3B\xFF" },
  { "STEP",         "\x34\xFF" },
  { "TEST %s",      "\x52\x1\xFF" },
  { "TEXT %s",      "\x66\x1\xFF" },

  { "END",          "\xFF" }
};

char Path[256] = "";

int f_p = 0;
int f_d = 0;
int f_l = 0;
int f_r = 0;
int f_n = 0;

Text *Vars;
/*
 type=1  - array
 type=2  - variable
 type=3  - readonly
 type=10 - known label
 type=11 - unknown label
*/

char lst[100] = "";
char rjs[100] = "";
char fname[100] = "";
char rname[100] = "";
char path[100] = "";
char sname[100] = "";

FILE *flst; /* listing file */

int Platform = 0;
int Pass = 0;
int ExpError = 0;

void RW1Error(int e, Line *l)
{
  char ss[100];
  switch(e)
  {
     case 0 : strcpy(ss,"Out of memory"); break;
     case 1 : strcpy(ss,"Can't open file"); break;
     case 2 : strcpy(ss,"Illegal extention"); break;
     case 3 : strcpy(ss,"Too big name"); break;
     case 4 : strcpy(ss,"Syntax error"); break;
     case 5 : strcpy(ss,"Quotation is expected"); break;
     case 6 : strcpy(ss,"Duplicate definition"); break;
     case 7 : strcpy(ss,"Unknown command after START"); break;
     case 8 : strcpy(ss,"Unknown command before START"); break;
     case 9 : strcpy(ss,"Too many vars"); break;
     case 10: strcpy(ss,"Unknown name"); break;
     case 11: strcpy(ss,"Out of index"); break;
     case 12: strcpy(ss,"Variable is read only"); break;
     case 13: strcpy(ss,"Command after END"); break;
     case 14: strcpy(ss,"END is expected"); break;
     case 15: strcpy(ss,"Bad file format"); break;
     case 16: strcpy(ss,"Too many lines"); break;
     case 17: strcpy(ss,"Unknown label"); break;
     case 22: strcpy(ss,"Bad including file"); break;
     case 23: strcpy(ss,"Bad macro definition"); break;
     case 24: strcpy(ss,"Duplicate macro definition"); break;
     case 25: strcpy(ss,"Unknown macros used"); break;
     case 26: strcpy(ss,"Preprocessor error"); break;
     case 27: strcpy(ss,"Useless { or }"); break;
     case 28: strcpy(ss,"Bad ELSE using"); break;
     case 29: strcpy(ss,"Bad IF construction"); break;
     case 30: strcpy(ss,"ROBOT name is needed"); break;
     case 31: strcpy(ss,"AUTHOR name is needed"); break;
     case 32: strcpy(ss,"IMAGE is not full"); break;
     case 33: strcpy(ss,"Bad expression"); break;
     case 34: strcpy(ss,"Array is not defined"); break;
     case 35: strcpy(ss,"Invalid array definition"); break;
     case 36: strcpy(ss,"Too big code"); break;
     case 37: strcpy(ss,"Numbers of { and } are not equal"); break;
     case 38: strcpy(ss,"Invalid string definition"); break;
     case 39: strcpy(ss,"Duplicated label"); break;
     default: strcpy(ss,"Unknown error"); break;
  }
  if(f_l)
  {
     flst = fopen(lst,"wt");
  }
  printf("\n");
  if(l!=NULL)
  {
     printf("%s(%i): %s\n",RobotGetFile(l->id,sname),l->id2,l->str);
     if(f_l && flst!=NULL)
        fprintf(flst,"%s(%i): %s\n",RobotGetFile(l->id,sname),l->id2,l->str);
  }
  printf("\nERROR %u: %s\n",e,ss);
  if(f_l && flst!=NULL)
     fprintf(flst,"ERROR %u: %s [%i]\n",e,ss,ExpError);
  switch(ExpError)
  {
     case EXPR_ERR_OPER:
          printf("\tOPERATOR\n");
          break;
     case EXPR_ERR_UNKNOWN:
          printf("\tUNKNOWN VARIABLE\n");
          break;
     case EXPR_ERR_LOOP:
          printf("\tBIG LOOP\n");
          break;
     case EXPR_ERR_FULL:
          printf("\tFULL STACK\n");
          break;
     case EXPR_ERR_INVALID:
          printf("\tINVALID SYMBOL\n");
          break;
     case EXPR_ERR_EMPTY:
          printf("\tEMPTY STACK\n");
          break;
     case EXPR_ERR_CODE:
          printf("\tCODE ERROR\n");
          break;
  }
  printf("\nSorry !\n\n");
  exit(1);
}

char* SkipSpaces(char *po)
{
  while(*po==' '||*po=='\t') po++;
  return po;
}

int DelSpaces(char *s)
{
  int i,j=0;
  for(i=0;i<(int)strlen(s);i++)
  {
     if(s[i]!=' '&&s[i]!='\t')
     {
        s[j] = s[i];
        j++;
     }
  }
  s[j] = 0;
  return 1;
}

char _str[MAXSTRILEN];
unsigned char _bytes[4][6]; /* |len|01234| len=0 (string) */

char* IsVar(char *s,int n)
{
  int i,arr;
  char *po,*poo,*pooo,str[MAXSTRILEN];
  Line *l,*ll;
  if(ExpNumb(s,&po,&i))
  {
     _bytes[n][0] = 3;
     _bytes[n][1] = 0;
     _bytes[n][2] = i&0xFF;
     _bytes[n][3] = i>>8;
  }
  else
  {
     po = strchr(s,' ');
     if(po==NULL) po = &s[strlen(s)];
     i = po-s;
     strncpy(str,s,i);
     str[i] = 0;
     for(i=0;i<(int)strlen(str);i++)
     {
        if(!IsName(str[i]) && str[i]!='[' && str[i]!=']') return NULL;
     }
     arr = 0;
     poo = strchr(str,'[');
     if(poo!=NULL)
     {  arr = 1;
        *poo = 0;
        poo++;
        if(poo[strlen(poo)-1]!=']') return NULL;
        poo[strlen(poo)-1] = 0;
     }
     for(l=Vars->first;l!=NULL;l=l->next)
     {
        if(!strcmp(l->str,str)) break;
     }
     if(l==NULL)
     {
        if(Pass==2) return NULL;
        if(arr) return NULL;
        l = TextAdd(Vars,str);
        l->type = 11;
        l->adr = 0;
     }
     else
     {
        if(arr && l->type!=1) return NULL;
        if(!arr && l->type==1) return NULL;
     }
     switch(l->type)
     {
        case 1:
          if(ExpNumb(poo,&pooo,&i))
          {
             if(*pooo) return NULL;
             i += l->adr;
             _bytes[n][0] = 3;
             _bytes[n][1] = 1;
             _bytes[n][2] = i & 0xFF;
             _bytes[n][3] = i >> 8;
          }
          else
          {
             for(ll=Vars->first;ll!=NULL;ll=ll->next)
             {
                if(!strcmp(ll->str,poo)) break;
             }
             if(ll==NULL) return NULL;
             if(ll->type!=2 && ll->type!=3) return NULL;
             _bytes[n][0] = 5;
             _bytes[n][1] = 2;
             _bytes[n][2] = l->adr & 0xFF;
             _bytes[n][3] = l->adr >> 8;
             _bytes[n][4] = ll->adr & 0xFF;
             _bytes[n][5] = ll->adr >> 8;
          }
          break;
        case 2:
        case 3:
          _bytes[n][0] = 3;
          _bytes[n][1] = 1;
          _bytes[n][2] = l->adr & 0xFF;
          _bytes[n][3] = l->adr >> 8;
          break;
        case 10:
        case 11:
          _bytes[n][0] = 3;
          _bytes[n][1] = 0;
          _bytes[n][2] = l->adr & 0xFF;
          _bytes[n][3] = l->adr >> 8;
          break;
        default: return NULL;
     }
  }
  return po;
}

char* IsByte(char *s,int n)
{
  int i;
  char *po;
  if(!ExpNumb(s,&po,&i)) return NULL;
  if(i<0||i>255) return NULL;
  _bytes[n][0] = 1;
  _bytes[n][1] = i;
  return po;
}

char* IsString(char *s,int n)
{
  int i,j;
  char *po,ss[100];
  i = 0;
  po = s;
  if(*po!='"') return NULL;
  po++;
  while(*po!='"')
  {
    if(*po=='&')
    {
       po++;
       j = 0;
       while(1)
       {
         if(po[j]==0) break;
         if(po[j]==',') break;
         if(po[j]==' ') break;
         po[j] = toupper(po[j]);
         j++;
       }
       po = IsVar(po,3);
       if(po==NULL) return NULL;
       switch(_bytes[3][1])
       {
          case 0: sprintf(ss,"#%4.4X",
                         _bytes[3][2]+_bytes[3][3]*256);
                  break;
          case 1: sprintf(ss,"&%4.4X",
                         _bytes[3][2]+_bytes[3][3]*256);
                  break;
          case 2: sprintf(ss,"&%4.4X,%4.4X",
                         _bytes[3][2]+_bytes[3][3]*256,
                         _bytes[3][4]+_bytes[3][5]*256);
                  break;
       }
       for(j=0;j<(int)strlen(ss);j++) _str[i++]=ss[j];
       po--;
    }
    else _str[i++] = *po;
    if(i>=MAXSTRILEN-1) return NULL;
    po++;
  }
  _str[i] = 0;
  _bytes[n][0] = 0;
  po++;
  return po;
}

int StringEq(char *sh, char *st)
{
  int i,j,nu=0;
  char *po;
  po = strchr(sh,'%');
  if(po==NULL)
  {
     if(!strcmp(sh,st)) return 1;
  }
  else
  {
     i = j = 0;
     while(1)
     {
       if(sh[i]==' ')
       {
          if(st[j]!=' '&&st[j]!='\t') return 0;
          while(st[j]==' '||st[j]=='\t') j++;
          i++;
          continue;
       }
       if(sh[i]=='%')
       {
          nu++;
          i++;
          po = &st[j];
          switch(sh[i])
          {
             case 'b':
               po = IsByte(po,nu);
               if(po==NULL) return 0;
               break;
             case 'm':
               po = IsVar(po,nu);
               if(po==NULL) return 0;
               break;
             case 'a':
               po = IsVar(po,nu);
               if(po==NULL) return 0;
               if(_bytes[nu][1]==1 && _bytes[nu][3]==0xFF) return 0;
               break;
             case 's':
               po = IsString(po,nu);
               if(po==NULL) return 0;
               break;
          }
          j += po - &st[j];
          i++;
          continue;
       }
       if(sh[i]==0&&st[j]==0) return 1;
       if(sh[i]==st[j])
       {
          i++;j++;
          continue;
       }
       break;
     }
  }
  return 0;
}

char* IsKeyword(char *s, char *po)
{
  char* o = NULL;
  int i,k;
  static char str[MAXLINESIZE];
  if(s!=NULL)
  {
     strcpy(str,s);
     k = strlen(str);
     if(!strncmp(po,str,k)&&(po[k]==' '||po[k]=='\t'))
     {
        o = &po[k];
     }
  }
  else
  {
     i = k = 0;
     while(1)
     {
       if(StringEq(CompTab[i].str,po))
       {
          o = CompTab[i].kod;
          break;
       }
       if(!strcmp(CompTab[i].str,"END")) break;
       i++;
     }
  }
  return o;
}

int DeQuote(char *s1, char *s2)
{
  int i;
  char *po;
  po = SkipSpaces(s1);
  if(*po!='"') return 0;
  po++;
  i = 0;
  while(*po!='"')
  {
     s2[i++] = *po;
     po++;
     if(*po==0) return 0;
  }
  s2[i] = 0;
  po++;
  if(*po!=0) return 0;
  return 1;
}

unsigned char color[3];
unsigned char equip[4];
char name[MAXNAMELEN];
char author[MAXNAMELEN];
unsigned char image[32];
int fimage = 0;
unsigned short checksum;
unsigned short offset;
unsigned short magic = 0;
int version = 0x02;
int varlen;
unsigned char *code;
int codelen;

int SaveRObj(char *nname,Text *t)
{
  int i,k,size;
  FILE *fkod;
  Line *l,*l1;
  fkod = fopen(nname,"wb");
  if(fkod==NULL) RW1Error(1,NULL);
  fputc(version,fkod);
  for(i=0;i<4;i++) fputc(0x00,fkod);
  fputc(strlen(name),fkod);
  for(i=0;i<=(int)strlen(name);i++) fputc(name[i],fkod);
  fputc(varlen&0xFF,fkod);
  fputc(varlen>>8,fkod);
  fputc(codelen&0xFF,fkod);
  fputc(codelen>>8,fkod);
  for(i=0;i<3;i++) fputc(color[i],fkod);
  for(i=0;i<4;i++) fputc(equip[i],fkod);
  fputc(fimage,fkod);
  for(i=0;i<fimage;i++) fputc(image[i],fkod);
  for(i=0;i<=(int)strlen(author);i++) fputc(author[i],fkod);
  fputc(magic&0xFF,fkod);
  fputc(magic>>8,fkod);
  offset = ftell(fkod);
  for(i=0;i<codelen;i++) fputc(code[i],fkod);
  if(f_d)
  {
    fprintf(fkod,"DEBUG");
    fputc(0,fkod);
    RobotWriteFiles(fkod);
    fputc(0,fkod);
    for(l=Vars->first;l!=NULL;l=l->next)
    {
      if(l->type==2||l->type==3)
      {
        for(k=0;k<(int)strlen(l->str);k++) fputc(l->str[k],fkod);
        fputc(0,fkod);
        k = l->len;
        if(!k) k++;
        fputc(k&0xFF,fkod);
        fputc(k>>8,fkod);
        fputc(l->adr&0xFF,fkod);
        fputc(l->adr>>8,fkod);
      }
    }
    fputc(0,fkod);
    for(l=t->first;l!=NULL;l=l->next)
    {
      if(l->adr!=0) break;
      l1 = l;
    }
    for(l=l1;l!=NULL;l=l->next)
    {
      if(l->id2)
      {
        fputc(l->id,fkod);
        fputc(l->id2&0xFF,fkod);
        fputc(l->id2>>8,fkod);
        fputc(l->adr&0xFF,fkod);
        fputc(l->adr>>8,fkod);
      }
    }
    fputc(0xFF,fkod);
  }
  fclose(fkod);
  fkod = fopen(nname,"rb+");
  if(fkod==NULL) RW1Error(1,NULL);
  fseek(fkod,0,SEEK_END);
  size = ftell(fkod);
  fseek(fkod,5,SEEK_SET);
  checksum = 0;
  for(i=5;i<size;i++) checksum += fgetc(fkod);
  fseek(fkod,1,SEEK_SET);
  fputc(checksum&0xFF,fkod);
  fputc(checksum>>8,fkod);
  fputc(offset&0xFF,fkod);
  fputc(offset>>8,fkod);
  fclose(fkod);
  return 1;
}

int main(int argc,char **argv)
{
  int i,j,k,kk,kbo,kle,knu,ok,fdef;
  signed short ssh;
  Line *l,*ll,*l0;
  Text *t,*t0;
  static char str[MAXLINESIZE];
  static char str1[MAXLINESIZE];
  static char str2[MAXLINESIZE];
  char st1[100],st2[100];
  char *po,*poo,*pooo;
  FILE *ftmp,*fkod,*frjs;
  int fequip,fcolor;
  printf("\nROBBYC - Robby Preprocessor & Compiler v" VERSION " (" __DATE__ ")\n");
  printf("Copyright (c) 1998-2025, Alexander \"Shaos\" Shabarshin\n");
  printf("http://nedoPC.org (E-mail: me@shaos.net)\n\n");
  if(argc>1&&(argv[1][0]=='?'||(argv[1][0]=='-'&&argv[1][1]=='h'))) ok=1;
  else ok=0;
  if(argc<2 || ok)
  {
     printf("Usage:\n");
     printf("   ROBBYC [options] FILE.R\n");
     printf("   options :\n");
     printf("        -h : help\n");
     printf("        -p : create preprocessor file (.R!)\n");
     printf("        -l : create listing file (.RLS)\n");
     printf("        -r : create JSON file (.RJS)\n");
     printf("        -d : append debug info\n");
     printf("        -n : disable includes\n");
     printf("\n");
     if(ok)
     {
        printf(" SUPPORTED COMMANDS IN ROBBY LANGUAGE :\n");
        printf("  Var is number, register, label, variable or array element\n");
        printf("\tVar = Var\n");
        printf("\tVar = Expression\n");
        printf("\tDEF name[SIZE]\n");
        printf("\tDEF name[SIZE] = {NUMBER, NUMBER, ...}\n");
        k = 0;
        while(1)
        {
           po = CompTab[k].str;
           i = 0;
           while(*po)
           {
              if(*po=='%')
              {
                 po++;
                 str[i++]='\t';
                 switch(*po)
                 {
                   case 'a':
                      str[i++]='a';
                      str[i++]='V';
                      str[i++]='a';
                      str[i++]='r';
                      break;
                   case 'm':
                      str[i++]='V';
                      str[i++]='a';
                      str[i++]='r';
                      break;
                   case 'b':
                      str[i++]='B';
                      str[i++]='y';
                      str[i++]='t';
                      str[i++]='e';
                      break;
                   case 's':
                      str[i++]='"';
                      str[i++]='S';
                      str[i++]='t';
                      str[i++]='r';
                      str[i++]='"';
                      break;
                 }
                 po++;
              }
              else
              {
                 str[i++] = *po;
                 po++;
              }
           }
           str[i] = 0;
           printf("\t%s\n",str);
           if(!strcmp(str,"END")) break;
           k++;
        }
        printf("\n");
     }
     return 0;
  }
  strcpy(Path,argv[0]);
  po = strrchr(Path,'/');
  if(po==NULL)
     po = strrchr(Path,'\\');
  if(po!=NULL)
  {
     po++;
     *po = 0;
  }
  else
  {
     *Path = 0;
  }
  j = 1;
  for(i=1;i<argc;i++)
  {
     if(argv[i][0]=='-')
     {  k=0;
        if(argv[i][1]=='p'&&argv[i][2]==0){k=1;f_p=1;}
        if(argv[i][1]=='d'&&argv[i][2]==0){k=1;f_d=1;}
        if(argv[i][1]=='l'&&argv[i][2]==0){k=1;f_l=1;}
        if(argv[i][1]=='r'&&argv[i][2]==0){k=1;f_r=1;}
        if(argv[i][1]=='n'&&argv[i][2]==0){k=1;f_n=1;}
        if(k==0)
        {
           printf("ERROR: Bad option %s\n\n",argv[i]);
           return -1;
        }
     }
     else j=i;
  }
  strcpy(fname,argv[j]);
  strcpy(path,fname);
  po = strrchr(path,'/');
  if(po==NULL)
     po = strrchr(path,'\\');
  if(po!=NULL)
  {
     po++;
     strcpy(sname,po);
     *po = 0;
  }
  else
  {
     strcpy(sname,path);
     *path = 0;
  }
  ftmp = fopen(fname,"rb");
  if(ftmp==NULL)
  {
     printf("ERROR: Nonexistent file %s\n\n",fname);
     return -2;
  }
  fclose(ftmp);
  strcpy(lst,fname);
  po = strrchr(lst,'.');
  if(po!=NULL)
  {
     if(po[1]!='R')
     {
        printf("ERROR: Bad file %s\n\n",fname);
        return -3;
     }
     *po = 0;
  }
  strcat(lst,".RLS");
  strcpy(rjs,fname);
  po = strrchr(rjs,'.');
  if(po!=NULL) *po = 0;
  strcat(rjs,".rjs");
  Vars = TextNew();
  t0 = TextNew();
  t = TextNew();
  TextLoad(t0,fname);
  RobotPreprocessor(t0,t,sname,path,Path);
  TextDel(t0);
  printf("Preprocessor\n");
  if(f_p)
  {
     strcpy(str,fname);
     strcat(str,"!"); /* since v2.3.0 preprocessor file has .R! extension */
     printf("<%s>\n",str);
     TextSave(t,str);
  }
  varlen = codelen = 0;
  name[0] = 0;
  author[0] = 0;
  for(i=0;i<4;i++) equip[i] = 0x00;
  for(i=0;i<3;i++) color[i] = 0xFF;
  l=TextAdd(Vars,"X");l->type=3;l->adr=0xFF00;
  l=TextAdd(Vars,"Y");l->type=3;l->adr=0xFF01;
  l=TextAdd(Vars,"D");l->type=3;l->adr=0xFF02;
  l=TextAdd(Vars,"N");l->type=3;l->adr=0xFF03;
  l=TextAdd(Vars,"K");l->type=3;l->adr=0xFF04;
  l=TextAdd(Vars,"R");l->type=3;l->adr=0xFF05;
  l=TextAdd(Vars,"T");l->type=3;l->adr=0xFF06;
  l=TextAdd(Vars,"E");l->type=3;l->adr=0xFF07;
  l=TextAdd(Vars,"M");l->type=3;l->adr=0xFF08;
  l=TextAdd(Vars,"I");l->type=3;l->adr=0xFF09;
  l=TextAdd(Vars,"A");l->type=2;l->adr=0xFF0A;
  l=TextAdd(Vars,"B");l->type=2;l->adr=0xFF0B;
  l=TextAdd(Vars,"C");l->type=2;l->adr=0xFF0C;
  l=TextAdd(Vars,"P");l->type=3;l->adr=0xFF0D;
  l=TextAdd(Vars,"L");l->type=3;l->adr=0xFF0E;
  l=TextAdd(Vars,"S");l->type=3;l->adr=0xFF0F;
  l=TextAdd(Vars,"U");l->type=3;l->adr=0xFF16;
  l=TextAdd(Vars,"H");l->type=3;l->adr=0xFF1E;
  l=TextAdd(Vars,"FRONT");l->type=10;l->adr=0;l->len=1;
  l=TextAdd(Vars,"RIGHT");l->type=10;l->adr=1;l->len=1;
  l=TextAdd(Vars,"BACK");l->type=10;l->adr=2;l->len=1;
  l=TextAdd(Vars,"LEFT");l->type=10;l->adr=3;l->len=1;
  l=TextAdd(Vars,"TRUE");l->type=10;l->adr=1;l->len=1;
  l=TextAdd(Vars,"FALSE");l->type=10;l->adr=0;l->len=1;
  l = t->first;
  while(1)
  {
     if(l==NULL) RW1Error(4,l);
     po = SkipSpaces(l->str);
     if(!strcmp(po,"START:")) break;
     ok = 0;
     if(!ok)
     {  poo = IsKeyword("ROBOT",po);
        if(poo!=NULL)
        {  ok = 1;
           if(!DeQuote(poo,name)) RW1Error(5,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("AUTHOR",po);
        if(poo!=NULL)
        {  ok = 1;
           if(!DeQuote(poo,author)) RW1Error(5,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("COLOR",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           if(strlen(poo)!=6) RW1Error(4,l);
           color[0] = (hexx(poo[0])<<4)|hexx(poo[1]);
           color[1] = (hexx(poo[2])<<4)|hexx(poo[3]);
           color[2] = (hexx(poo[4])<<4)|hexx(poo[5]);
        }
     }
     if(!ok)
     {  poo = IsKeyword("IMAGE",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           j = 0;
           for(i=0;i<(int)strlen(poo);i++)
           {
              if(poo[i]==' '||poo[i]=='\t') continue;
              kk = hexx(poo[i]);
              if(kk<0) RW1Error(32,l);
              if(j==0)
              {
                 image[fimage] = kk<<4;
                 j = 1;
              }
              else /* j==1 */
              {
                 image[fimage] |= kk;
                 fimage++;
                 j = 0;
              }
           }
           if(j==1) RW1Error(32,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("FRONT",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           equip[0] = 0;
           if(!strcmp(poo,"EYE")) equip[0] = 1;
           if(!strcmp(poo,"GUN")) equip[0] = 2;
           if(equip[0]==0) RW1Error(4,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("RIGHT",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           equip[1] = 0;
           if(!strcmp(poo,"EYE")) equip[1] = 1;
           if(!strcmp(poo,"GUN")) equip[1] = 2;
           if(equip[1]==0) RW1Error(4,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("BACK",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           equip[2] = 0;
           if(!strcmp(poo,"EYE")) equip[2] = 1;
           if(!strcmp(poo,"GUN")) equip[2] = 2;
           if(equip[2]==0) RW1Error(4,l);
        }
     }
     if(!ok)
     {  poo = IsKeyword("LEFT",po);
        if(poo!=NULL)
        {  ok = 1;
           poo = SkipSpaces(poo);
           equip[3] = 0;
           if(!strcmp(poo,"EYE")) equip[3] = 1;
           if(!strcmp(poo,"GUN")) equip[3] = 2;
           if(equip[3]==0) RW1Error(4,l);
        }
     }
     if(!ok) RW1Error(8,l);
     l = l->next;
  }
  if(!name[0]) RW1Error(30,l);
  if(!author[0]) RW1Error(31,l);
  if(fimage!=0&&fimage!=32) RW1Error(32,l);
  code = (unsigned char*)malloc(MAXCODESIZE);
  if(code==NULL) RW1Error(0,l);
  varlen = 0;
  l0 = l;
  for(Pass=1;Pass<=2;Pass++)
  {
    printf("Pass %i\n",Pass);
    codelen = 0;
    l = l0;
    po = l->str;
    while(1)
    {
       while(!*po)
       {  l = l->next;
          if(l==NULL) break;
          po = l->str;
       }
       if(l==NULL) break;
       i = 0;
       po = SkipSpaces(po);
       j = 1;
       while(*po!=0 && *po!=';')
       {
          k = *po;
          str[i++] = k;
          if(!IsName(k)) j = 0;
          po++;
          if(j && *po==':')
          {
             str[i++] = ':';
             break;
          }
       }
       str[i] = 0;
       if(*po) po++;
       if(!strncmp(str,"DEF ",4))
          fdef = 1;
       else
          fdef = 0;
       if(!fdef)
       {
         poo = strchr(str,'{');
         if(poo!=NULL) RW1Error(27,l);
         poo = strchr(str,'}');
         if(poo!=NULL) RW1Error(27,l);
       }
       poo = &str[strlen(str)-1];
       while(*poo==' '||*poo=='\t'){*poo=0;poo--;}
#ifdef DEBUG
       printf("%i>>>|%s|\n",Pass,str);
#endif
       if(l->adr==0&&l!=l0) l->adr=codelen;
       ok = 0;
       if(!ok && *str=='(')
       {
          if(str[strlen(str)-1]!=')') RW1Error(4,l);
          str[strlen(str)-1] = 0;
          poo = str;
          poo++;
          strcpy(str1,poo);
          DelSpaces(str1);
#ifdef DEBUG
          printf("EXPRESSION1:%s\n",str1);
#endif
          code[codelen] = 0x40;
          k = Expression(str1,Vars,&code[codelen+2],1,Pass-1);
          if(k<=0)
          {
             ExpError = k;
             RW1Error(33,l);
          }
          code[codelen+1] = k;
          codelen += 2;
          codelen += k;
          ok = 1;
       }
       if(!ok && !fdef && strchr(str,'=')!=NULL && strchr(str,'"')==NULL)
       {
          strcpy(str1,str);
          DelSpaces(str1);
#ifdef DEBUG
          printf("EXPRESSION2:%s\n",str1);
#endif
          strcpy(str2,str1);
          poo = strchr(str2,'=');
          *poo = 0;
          poo++;
          if(strcmp(str2,poo))
          {
            if(strlen(str2)<100) strcpy(st1,str2);
            else *st1 = 0;
            if(strlen(poo)<100) strcpy(st2,poo);
            else *st2 = 0;
            k = 2;
            poo = strchr(str2,'[');
            if(poo!=NULL)
            {
               *poo = 0;
               k = 1;
            }
            for(ll=Vars->first;ll!=NULL;ll=ll->next)
            {
               if(!strcmp(ll->str,str2)) break;
            }
            if(ll==NULL)
            {
               if(k==1) RW1Error(34,l);
               ll = TextAdd(Vars,str2);
               ll->type = 2;
               ll->adr = varlen++;
            }
            else
            {
               if(ll->type>=3) RW1Error(12,l);
            }
            /* test Var=Var */
            pooo = IsVar(st1,1);
            if(pooo!=NULL && !*pooo)
            {
               pooo = IsVar(st2,2);
               if(pooo!=NULL && !*pooo)
               {
                  ok = 1;
               }
            }
            if(ok) /* Var=Var */
            {
               code[codelen++] = 0x20;
               for(j=0;j<_bytes[1][0];j++) code[codelen++]=_bytes[1][j+1];
               for(j=0;j<_bytes[2][0];j++) code[codelen++]=_bytes[2][j+1];
            }
            else /* Expression */
            {
               code[codelen] = 0x40;
               k = Expression(str1,Vars,&code[codelen+2],0,Pass-1);
               if(k<=0)
               {
                  ExpError = k;
                  RW1Error(33,l);
               }
               code[codelen+1] = k;
               codelen += 2;
               codelen += k;
            }
          }
          ok = 1;
       }
       if(!ok && !strncmp(str,"DEF ",4))
       {
          poo = &str[4];
          pooo = strchr(poo,'[');
          if(pooo==NULL) RW1Error(35,l);
          *pooo = 0;
          pooo++;
          for(ll=Vars->first;ll!=NULL;ll=ll->next)
          {
             if(!strcmp(ll->str,poo)) break;
          }
          if(Pass==1 && ll!=NULL) RW1Error(6,l);
          if(Pass==2 && ll==NULL) RW1Error(35,l);
          if(Pass==1)
          {
             ll = TextAdd(Vars,poo);
             ll->type = 1;
             ll->adr = varlen;
          }
          poo = pooo;
          pooo = strchr(poo,']');
          if(pooo==NULL) RW1Error(35,l);
          *pooo = 0;
          pooo++;
          k = atoi(poo);
          if(!k) RW1Error(35,l);
          ll->len = k;
          if(!*pooo)
          {
             code[codelen++] = 0x01;
             if(Pass==2) codelen += 2;
             else
             {
               code[codelen++] = varlen&0xFF;
               code[codelen++] = varlen>>8;
             }
             code[codelen++] = k&0xFF;
             code[codelen++] = k>>8;
             if(Pass==1) varlen += k;
          }
          else
          {
             pooo = SkipSpaces(pooo);
             if(*pooo!='=') RW1Error(35,l);
             pooo++;
             pooo = SkipSpaces(pooo);
             if(*pooo=='\'') /* 2.0.10 */
             {  poo = pooo;
                pooo++;
                if(strchr(pooo,'\'')==NULL) RW1Error(38,l);
                if(pooo[strlen(pooo)-1]!='\'') RW1Error(38,l);
                pooo[strlen(pooo)-1] = 0;
                strcpy(str2,pooo);
                *pooo = 0;
                *poo = '{';
                kbo = 0;
                knu = j = 0;
                *str1 = 0;
                while(str2[j])
                {
                   if(str2[j]=='\\')
                   {  j++;
                      if(str2[j++]!='x') RW1Error(38,l);
                      if(str2[j]==0||str2[j+1]==0) RW1Error(38,l);
                      kk = (hexx(str2[j])<<4)|hexx(str2[j+1]);
                      j += 2;
                   }
                   else kk = str2[j++];
                   if(!kbo)
                   {    kbo = 1;
                        kle = kk;
                        if(str2[j]==0)
                        {
                                sprintf(st1,",#%4.4X",kle);
                                strcat(str1,st1);
                                knu++;
                        }
                   }
                   else
                   {    kbo = 0;
                        kle |= kk<<8;
                        sprintf(st1,",#%4.4X",kle);
                        strcat(str1,st1);
                        knu++;
                   }
                }
                sprintf(st1,"%i",knu);
                strcat(poo,st1);
                strcat(poo,str1);
                strcat(poo,"}");
                pooo = poo;
             }
             if(*pooo=='\"') /* 2.0.10 */
             {  poo = pooo;
                pooo++;
                if(strchr(pooo,'\"')==NULL) RW1Error(38,l);
                if(pooo[strlen(pooo)-1]!='\"') RW1Error(38,l);
                pooo[strlen(pooo)-1] = 0;
                strcpy(str2,pooo);
                *pooo = 0;
                *poo = '{';
                knu = j = 0;
                *str1 = 0;
                while(str2[j])
                {
                   if(str2[j]=='\\')
                   {  j++;
                      if(str2[j++]!='x') RW1Error(38,l);
                      if(str2[j]==0||str2[j+1]==0) RW1Error(38,l);
                      kk = (hexx(str2[j])<<4)|hexx(str2[j+1]);
                      j += 2;
                   }
                   else kk = str2[j++];
                   sprintf(st1,",#%4.4X",kk);
                   strcat(str1,st1);
                   knu++;
                }
                sprintf(st1,"%i",knu);
                strcat(poo,st1);
                strcat(poo,str1);
                strcat(poo,"}");
                pooo = poo;
             }
#ifdef DEBUG
             printf("DEF|%s|\n",pooo);
#endif
             if(*pooo!='{') RW1Error(35,l);
             if(pooo[strlen(pooo)-1]!='}') RW1Error(35,l);
             pooo[strlen(pooo)-1] = 0;
             pooo++;
             kk = 1;
             for(j=0;j<(int)strlen(pooo);j++)
             {
                if(pooo[j]==',') kk++;
             }
             if(kk>k) RW1Error(35,l);
             code[codelen++] = 0x02;
             if(Pass==2) codelen += 2;
             else
             {
               code[codelen++] = varlen&0xFF;
               code[codelen++] = varlen>>8;
             }
             code[codelen++] = k&0xFF;
             code[codelen++] = k>>8;
             code[codelen++] = kk&0xFF;
             code[codelen++] = kk>>8;
             if(Pass==1) varlen += k;
             for(j=0;j<kk;j++) /* DEF a[]={...} */
             {
                pooo = SkipSpaces(pooo);
                k = 0;
                while(1)
                {
                   if(*pooo==',') break;
                   if(*pooo==' ') break;
                   if(*pooo==0) break;
                   str1[k++] = *pooo;
                   pooo++;
                }
                str1[k] = 0;
                poo = IsVar(str1,3);
                if(poo==NULL) RW1Error(35,l);
                if(*poo!=0 && *poo!=' ') RW1Error(35,l);
                if(_bytes[3][0]!=3) RW1Error(35,l);
                if(_bytes[3][1]!=0) RW1Error(35,l);
                code[codelen++] = _bytes[3][2];
                code[codelen++] = _bytes[3][3];
                pooo = SkipSpaces(pooo);
                if(*pooo) pooo++;
             }
             if(*pooo) RW1Error(35,l);
          }
          ok = 1;
       }
       if(!ok && str[strlen(str)-1]==':')
       {
          str[strlen(str)-1] = 0;
#ifdef DEBUG
          printf("LABEL <%s>\n",str);
#endif
          for(ll=Vars->first;ll!=NULL;ll=ll->next)
          {
             if(!strcmp(ll->str,str)) break;
          }
          if(ll!=NULL && Pass==1 && ll->type==10)
          {
            RW1Error(39,l); /* 2.1.3 */
          }
          if(ll==NULL)
          {
            ll = TextAdd(Vars,str);
          }
          ll->type = 10;
          ll->adr = codelen;
          ok = 1;
       }
       if(!ok && strchr(str,':')!=NULL) RW1Error(4,l);
       if(!ok)
       {
          poo = IsKeyword(NULL,str);
          if(poo!=NULL)
          {
             if((unsigned char)*poo==0xFF)
             {
                code[codelen++] = 0xFF;
             }
             else
             {
                k = 0;
                while(1)
                {
                   if(poo[k]==1 || poo[k]==2)
                   {
                      if(_bytes[poo[k]][0]==0)
                      {
                         for(j=0;j<(int)strlen(_str);j++)
                            code[codelen++] = _str[j];
                         code[codelen++] = 0;
                      }
                      else
                      {
                         for(j=0;j<_bytes[poo[k]][0];j++)
                            code[codelen++] = _bytes[poo[k]][j+1];
                      }
                   }
                   else
                   {
                      code[codelen++] = poo[k];
                   }
                   k++;
                   if((unsigned char)poo[k]==0xFF) break;
                }
             }
             ok = 1;
          }
       }
       if(!ok) RW1Error(7,l);
       if(codelen>=MAXCODESIZE) RW1Error(36,l);
    }
  } /* end of for(Pass) */
  /* test for unresolved label */
  for(l=Vars->first;l!=NULL;l=l->next)
  {
     if(l->type==11)
     {
        printf("\n\tLabel %s ?\n",l->str);
        RW1Error(17,NULL);
     }
  }
  printf("Ok!\n");
  strcpy(rname,fname);
  po = strrchr(rname,'.');
  if(po!=NULL) *po = 0;
  strcat(rname,".RO"); /* since v2.3.0 bytecode file has .RO extension */
  printf("<%s>\n",rname);
  SaveRObj(rname,t);
  if(f_l)
  {
     printf("<%s>\n",lst);
     flst = fopen(lst,"wt");
     if(flst==NULL) RW1Error(1,NULL);
     fprintf(flst,"// Generated by ROBBYC version %s\n", VERSION);
     fprintf(flst,"// Alexander \"Shaos\" Shabarshin <me@shaos.net>\n");
     ftmp = fopen(fname,"rt");
     if(ftmp==NULL) RW1Error(1,NULL);
     l = l0;
     fdef = 1;
     kk = 1;
     for(i=0;i<codelen;i++)
     {
        while(l!=NULL&&i>=l->adr)
        {
          k = 0;
          while(kk<=l->id2 && l->id==0 && !feof(ftmp))
          {
             if(fdef && kk==l->id2)
             {
                fdef = 0;
                fprintf(flst,"\n// HEADER OF THE ROBOT\n\n");
                fprintf(flst,"// Robot '%s'\n",name);
                fprintf(flst,"// %i variables\n",varlen);
                fprintf(flst,"// %i bytes of code\n",codelen);
                fprintf(flst,"// %i bytes of image\n\n",fimage);
                fkod = fopen(rname,"rb");
                if(fkod==NULL) RW1Error(1,NULL);
                for(j=0;j<offset;j++)
                {
                   image[j&0xF] = fgetc(fkod);
                   fprintf(flst,"%2.2X ",image[j&0xF]);
                   if((j&0xF)==0xF || j==offset-1)
                   {
                      for(k=15;k>(j&0xF);k--) fputs("   ",flst);
                      fputc('|',flst);
                      for(k=0;k<=(j&0xF);k++)
                      {
                         if(image[k]>=0x20 && image[k]<0x80)
                              fputc(image[k],flst);
                         else fputc('.',flst);
                      }
                      fputc('\n',flst);
                   }
                }
                fclose(fkod);
                fprintf(flst,"\n// CODE OF THE ROBOT\n\n");
             }
             if(!k++) fputc('\n',flst);
             fgets(str,MAXLINESIZE,ftmp);
             fprintf(flst,">>>>(%i) %s",kk++,str);
          }
#ifdef DEBUG
          printf("%s(%i) %s\n",RobotGetFile(l->id,sname),l->id2,l->str);
#endif
          fprintf(flst,"%s(%i) %s\n",RobotGetFile(l->id,sname),l->id2,l->str);
          l = l->next;
          ok = code[i];
          k = i;
        }
#ifdef DEBUG
        printf("%4.4X %2.2X\n",i,code[i]);
#endif
        fprintf(flst,"%4.4X %2.2X ",i,code[i]);
        if(ok==0x37||ok==0x52||ok==0x66||ok==0x6C)
        {
           if(code[i]>=0x20 && i!=k)
           {
              fprintf(flst," '%c'",code[i]);
           }
        }
        if(ok==0x40)
        {
           switch(code[i])
           {
             case 0xF3: fputs("LOAD",flst); break;
             case 0xF4: fputs("SAVE",flst); break;
             case 0xF5: ssh = code[i+1];
                        ssh |= code[i+2]<<8;
                        switch(((int)ssh)&0xFFFF)
                        {
                           case 0xFF00: fputs("REG X?",flst); break;
                           case 0xFF01: fputs("REG Y?",flst); break;
                           case 0xFF02: fputs("REG D?",flst); break;
                           case 0xFF03: fputs("REG N?",flst); break;
                           case 0xFF04: fputs("REG K?",flst); break;
                           case 0xFF05: fputs("REG R?",flst); break;
                           case 0xFF06: fputs("REG T?",flst); break;
                           case 0xFF07: fputs("REG E?",flst); break;
                           case 0xFF08: fputs("REG M?",flst); break;
                           case 0xFF09: fputs("REG I?",flst); break;
                           case 0xFF0A: fputs("REG A?",flst); break;
                           case 0xFF0B: fputs("REG B?",flst); break;
                           case 0xFF0C: fputs("REG C?",flst); break;
                           case 0xFF0D: fputs("REG P?",flst); break;
                           case 0xFF0E: fputs("REG L?",flst); break;
                           case 0xFF0F: fputs("REG S?",flst); break;
                           case 0xFF16: fputs("REG U?",flst); break;
                           case 0xFF1E: fputs("REG H?",flst); break;
                           default: fprintf(flst,"PUSH %i",ssh);
                        }
                        break;
             case 0x80: fputs("&&",flst); break;
             case 0x81: fputs("||",flst); break;
             case 0x90: fputs("==",flst); break;
             case 0x91: fputs("!=",flst); break;
             case 0x92: fputs(">",flst); break;
             case 0x93: fputs("<",flst); break;
             case 0x94: fputs(">=",flst); break;
             case 0x95: fputs("<=",flst); break;
             case 0xA0: fputs("+",flst); break;
             case 0xA1: fputs("-",flst); break;
             case 0xB0: fputs("*",flst); break;
             case 0xB1: fputs("/",flst); break;
             case 0xB2: fputs("%",flst); break;
             case 0xC0: fputs("&",flst); break;
             case 0xC2: fputs("^",flst); break;
             case 0xD0: fputs(">>",flst); break;
             case 0xD1: fputs("<<",flst); break;
             case 0xE0: fputs("(-)",flst); break;
             case 0xE1: fputs("(~)",flst); break;
             case 0xE2: fputs("(!)",flst); break;
             case 0xF0: fputs("?:",flst); break;
             case 0xF1: fputs("DUP",flst); break;
             case 0xF2: fputs("ROL",flst); break;
             case 0xF6: fputs("[]",flst); break;
           }
        }
        fputs("\n",flst);
     }
     fputs("\n\n>>> MACROS <<<\n\n",flst);
     RobotWriteMacros(flst);
     fputs("\n\n>>> NAMES <<<\n\n",flst);
     TextSort(Vars,0);
     for(l=Vars->first;l!=NULL;l=l->next)
     {
        fprintf(flst,"%s\t",l->str);
        switch(l->type)
        {
          case 1:  fprintf(flst,"array[%i]",l->len); break;
          case 2:  fprintf(flst,"variable"); break;
          case 3:  fprintf(flst,"readonly"); break;
          case 10: if(l->len==1) fprintf(flst,"(number)");
                   else fprintf(flst,"--label--"); break;
        }
        fprintf(flst,"\t%4.4X\n",l->adr);
     }
     fputs("\n\n>>> LABELS <<<\n\n",flst);
     TextSort(Vars,4);
     for(l=Vars->first;l!=NULL;l=l->next)
     {
        if(l->type==10 && l->len!=1)
        {
           fprintf(flst,"%4.4X %s\n",l->adr,l->str);
        }
     }
  } /* if(f_l) */
  if(f_r)
  {
     printf("<%s>\n",rjs);
     frjs = fopen(rjs,"wt");
     if(frjs==NULL) RW1Error(1,NULL);
     fprintf(frjs,"{\"robot\":\"%s\",\n",name);
     fprintf(frjs," \"author\":\"%s\",\n",author);
     fprintf(frjs," \"varm\":%i,\n",varlen);
     fprintf(frjs," \"vars\":[],\n");
     fprintf(frjs," \"negs\":[],\n");
     fprintf(frjs," \"regs\":[],\n");
     fequip = fcolor = 0;
     if(equip[0]||equip[1]||equip[2]||equip[3])
     {
       fequip = 4;
       fprintf(frjs," \"equipment\":[");
       for(i=0;i<fequip;i++)
       {
         fprintf(frjs,"%i",equip[i]);
         if(i<fequip-1) fprintf(frjs,",");
       }
       fprintf(frjs,"],\n");
     }
     fprintf(frjs," \"color\":\"#%2.2X%2.2X%2.2X\",\n",color[0],color[1],color[2]);
     if(color[0]!=255||color[1]!=255||color[2]!=255) fcolor=1;
     if(fimage)
     {
       fprintf(frjs," \"image\":[");
       for(i=0;i<fimage;i++)
       {
         fprintf(frjs,"%i,%i",image[i]>>4,image[i]&15);
         if(i<fimage-1) fprintf(frjs,",");
       }
       fprintf(frjs,"],\n");
     }
     fprintf(frjs," \"code\":[");
     for(i=0;i<codelen;i++)
     {
       fprintf(frjs,"%i",code[i]);
       if(i<codelen-1) fprintf(frjs,",");
       if((i&31)==31) fprintf(frjs,"\n        ");
     }
     fprintf(frjs,"],\n");
     switch(fimage|fequip|fcolor)
     {
       case 0:
         Platform = 2;
         break;
       case 1:
       case 32:
       case 33:
         Platform = 1;
         break;
     }
     fprintf(frjs," \"platform\":%i\n",Platform);
     fprintf(frjs,"}\n");
     fclose(frjs);
  } /* if(f_r) */
#ifdef DEBUG
  printf("\nOUTPUT:\n");
  TextList(t);
  printf("\nVARS:\n");
  TextList(Vars);
#endif
  if(f_l&&flst!=NULL)
  {
     fclose(flst);
     fclose(ftmp);
  }
  free(code);
  TextDel(t);
  TextDel(Vars);
  RobotFree();
  printf("Good Luck !\n\n");
  return 0;
}

