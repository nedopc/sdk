/*  robbyc2.c - Source code for ROBBY expressions

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1998-2018, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "robbyc2.h"

typedef struct _ExpElement
{ char v[16];
  long i;
  short c;
} ExpElement;

typedef struct _ExpStack
{ int u;
  int n;
  ExpElement *p;
} ExpStack;

ExpStack* ExpStackNew(int n)
{
   ExpStack *s;
   s = (ExpStack*)malloc(sizeof(ExpStack));
   if(!s) return 0;
   s->p = (ExpElement*)malloc(n*sizeof(ExpElement));
   if(!s->p)
   {
      free(s);
      return 0;
   }
   s->u = 0;
   return s;
}

void ExpStackDel(ExpStack *s)
{
   if(s)
   {
      if(s->p) free(s->p);
      free(s);
   }
}

int ExpStackPush(ExpStack *s, ExpElement* b)
{
   s->p[s->u++] = *b;
   if(s->u==s->n) return 0;
   return 1;
}

ExpElement ExpStackPop(ExpStack *s)
{
   ExpElement ee,o;
   ee.c = -1;
   o = s->p[--s->u];
   if(s->u<0) return ee;
   return o;
}

int ExpStackGet(ExpStack *s)
{
   return s->u;
}

ExpElement ExpStackGetN(ExpStack *s, int i) /* 1,2,3 */
{
   ExpElement ee,o;
   ee.c = -1;
   o = s->p[s->u-i];
   if(s->u-i<0) return ee;
   return o;
}

void ExpStackList(ExpStack *s)
{
   int i;
   printf("\nSTACK LIST\n");
   printf("==========\n");
   for(i=s->u-1;i>=0;i--)
       printf("%i) %i 0x%2.2X\n",s->u-i,s->p[i].c,(int)s->p[i].i);
   printf("==========\n\n");
}

/*
 0x60 // =
 0x80 // &&  A,B -> A&&B
 0x81 // ||  A,B -> A||B
 0x90 // ==  A,B -> A==B
 0x91 // !=  A,B -> A!=B
 0x92 // >   A,B -> A>B
 0x93 // <   A,B -> A<B
 0x94 // >=  A,B -> A>=B
 0x95 // <=  A,B -> A<=B
 0xA0 // +   A,B -> A+B
 0xA1 // -   A,B -> A-B
 0xB0 // *   A,B -> A*B
 0xB1 // /   A,B -> A/B
 0xB2 // %   A,B -> A%B
 0xC0 // &   A,B -> A&B
 0xC1 // |   A,B -> A|B
 0xC2 // ^   A,B -> A^B
 0xD0 // >>  A,B -> A>>B
 0xD1 // <<  A,B -> A<<B
 0xE0 // -   A -> -A
 0xE1 // ~   A -> ~A
 0xE2 // !   A -> !A
 0xE3 // &   A -> &A
 0xF0 // ?:         A,B,C -> A?B:C
 0xF1 // DUP        A -> A,A
 0xF2 // ROL        A,B -> B,A
 0xF3 // LOAD       A -> mem[A]
 0xF4 // SAVE       A,B -> .  (mem[A]=B)
 0xF5 W // PUSH W   . -> W
 0xF6 // []  A,B -> A[B]
*/

int ExpOper2(char *s, char **ss)
{
  int op = 0;
  int dd = 0;
  switch(s[0])
  {
     case '=': if(s[1]=='=')
               {op=0x90;dd=2;}
               else
               {op=0x60;dd=1;}
               break;
     case '&': if(s[1]=='&')
               {op=0x80;dd=2;}
               else
               {op=0xC0;dd=1;}
               break;
     case '|': if(s[1]=='|')
               {op=0x81;dd=2;}
               else
               {op=0xC1;dd=1;}
               break;
     case '!': if(s[1]=='=')
               {op=0x91;dd=2;}
               break;
     case '>': if(s[1]=='=')
               {op=0x94;dd=2;}
               else
               { if(s[1]=='>')
                 {op=0xD0;dd=2;}
                 else
                 {op=0x92;dd=1;}
               }
               break;
     case '<': if(s[1]=='=')
               {op=0x95;dd=2;}
               else
               { if(s[1]=='<')
                 {op=0xD1;dd=2;}
                 else
                 {  if(s[1]=='>')
                    {op=0x91;dd=2;}
                    else
                    {op=0x93;dd=1;}
                 }
               }
               break;
     case '+': op=0xA0;dd=1;break;
     case '-': op=0xA1;dd=1;break;
     case '*': op=0xB0;dd=1;break;
     case '/': op=0xB1;dd=1;break;
     case '%': op=0xB2;dd=1;break;
     case '^': op=0xC2;dd=1;break;
     case '?': op=0xFF;dd=1;break;
     case ':': op=0xF0;dd=1;break;
  }
  *ss = &s[dd];
  return op;
}

int ExpOper1(char *s, char **ss)
{
  int op = 0;
  int dd = 0;
  switch(s[0])
  {
     case '+': op=0xEF;dd=1;break;
     case '-': op=0xE0;dd=1;break;
     case '~': op=0xE1;dd=1;break;
     case '!': op=0xE2;dd=1;break;
     case '&': op=0xE3;dd=1;break;
  }
  *ss = &s[dd];
  return op;
}

int ExpName(char *str, char **tail, char *name)
{
  int i,j,f;
  *tail = str;
  f = 1;
  i = 0;
  if(!isalpha(str[i])&&str[i]!='_'&&str[i]!='$'&&str[i]!='@') return 0;
  while(1)
  {  j = 0;
     if( (!f&&isdigit(str[i])) ||
         isalpha(str[i]) ||
         str[i]=='_' ||
         str[i]=='$' ||
         str[i]=='@'
       ) j=1;
     if(!j) break;
     name[i] = str[i];
     if(f==1) f=0;
     i++;
  }
  name[i] = 0;
  *tail = &str[i];
#ifdef DEBUG
  printf(">>> NAME '%s' (%s)\n",name,*tail);
#endif
  return 1;
}

int ExpNumb(char *str, char **tail, int *number)
{
  int i,j,k,m,i_i,sgn=1;
  char s[10];
  *number = 0;
  *tail = str;
  if(str[0]=='\'') /* Character */
  {
     if(str[1]=='\\')
     {
        if(str[3]!='\'') return 0;
        if(str[2]=='n') *number=10;
        if(str[2]=='r') *number=13;
        if(str[2]=='t') *number=10;
        *tail = &str[4];
     }
     else
     {
        if(str[2]!='\'') return 0;
        *number = str[1];
        *tail = &str[3];
     }
     return 1;
  }
  if((str[0]=='0'&&(str[1]=='x'||str[1]=='X'))||str[0]=='#') /* Hexadecimal */
  {
     if(str[0]=='#')
          i = 1;
     else i = 2;
     i_i = i;
     while(1)
     {
        if( !(str[i]>='0'&&str[i]<='9') &&
            !(str[i]>='A'&&str[i]<='F') &&
            !(str[i]>='a'&&str[i]<='f') ) break;
        i++;
     }
     j = i;
     k = 1;
     *number = 0;
     for(i=j-1;i>=i_i;i--)
     {
       m = str[i];
       switch(m)
       {
         case 'a':
         case 'A':
              m = 10;
              break;
         case 'b':
         case 'B':
              m = 11;
              break;
         case 'c':
         case 'C':
              m = 12;
              break;
         case 'd':
         case 'D':
              m = 13;
              break;
         case 'e':
         case 'E':
              m = 14;
              break;
         case 'f':
         case 'F':
              m = 15;
              break;
      }
      if(m>=48&&m<58) m-=48;
      if(m<0||m>15) return 0;
      *number = *number + k*m;
      k *= 16;
     }
     *tail = &str[j];
#ifdef DEBUG
     printf(">>> HEX %4.4X | %s |\n",*number,*tail);
#endif
     return 1;
  }
  j = 0;
  if(str[0]=='-'){j++;sgn=-1;}
  if(str[0]=='+'){j++;sgn=1;}
  if(isdigit(str[j])) /* Integer */
  {  i = j;
     while(1)
     {
       if(!isdigit(str[i])) break;
       i++;
     }
     k = i-j;
     memcpy(s,&str[j],k);
     s[k] = 0;
     *number = sgn*atoi(s);
     *tail = &str[i];
#ifdef DEBUG
     printf(">>> INT %s=%i | %s |\n",s,*number,*tail);
#endif
     return 1;
  }
  return 0;
}

int Expression(char *str, Text *t, unsigned char *mem, int without, int pass)
{
  char *po;
  char *tail = str;
  char s[100];
  int j,k,num;
  int lastth = 0;
  int posle = 0;
  Line *lin;
  ExpElement e;
  ExpStack *stack;
  int count = 10000;
  if(without) posle=1;
  stack = ExpStackNew(1000);
  j = 0;
  if(without)
  {
    mem[j++] = 0xF5; /* PUSH */
    mem[j++] = 0x0E; /* reg L */
    mem[j++] = 0xFF;
    e.c = 1;
    e.i = 0x60;
    ExpStackPush(stack,&e);
  }
  while(1)
  {
#ifdef DEBUG
    printf("|%s|\n",tail);
#endif
    if(--count<0)
    {
       ExpStackDel(stack);
       return EXPR_ERR_LOOP;
    }
    po = tail;
    if(lastth)
    {
       if(*po=='['&&lastth==1) /* Array */
       {
#ifdef DEBUG
          printf("===> ARRAY\n");
#endif
          e.c = 1;
          e.i = 4;
          if(!ExpStackPush(stack,&e))
          {
             ExpStackDel(stack);
             return EXPR_ERR_FULL;
          }
          tail++;
          lastth=0;
          continue;
       }
       if(*po==']')
       {
#ifdef DEBUG
          printf("===> ]\n");
#endif
          e = ExpStackPop(stack);
          while(e.i!=4)
          {
             mem[j++] = e.i;
             if(ExpStackGet(stack)==0)
             {
                ExpStackDel(stack);
                return EXPR_ERR_EMPTY;
             }
             e = ExpStackPop(stack);
          }
          mem[j++] = 0xF6; /* [] ??? */
          if(ExpStackGet(stack)>0 || posle)
          {
             mem[j++] = 0xF3; /* LOAD */
          }
          lastth = 3;
          tail++;
          continue;
       }
       if(*po==')')
       {
#ifdef DEBUG
          printf("===> )\n");
#endif
          e = ExpStackPop(stack);
          while(e.i!=2)
          {
             mem[j++] = e.i;
             if(ExpStackGet(stack)==0)
             {
                ExpStackDel(stack);
                return EXPR_ERR_EMPTY;
             }
             e = ExpStackPop(stack);
          }
          lastth = 3;
          tail++;
          continue;
       }
       /* Binary Operator */
       if(*tail!=0)
       {
          k = ExpOper2(po,&tail);
#ifdef DEBUG
          printf("ExpOper2(%s,%s)=0x%2.2X\n",po,tail,k);
#endif
          if(k==0)
          {
             ExpStackDel(stack);
             return EXPR_ERR_OPER;
          }
       }
       e = ExpStackGetN(stack,1);
       if(e.c!=-1&&((e.i&0xF0)>=(k&0xF0)||*tail==0))
       {
          e = ExpStackPop(stack);
          if(e.i==0x60) mem[j++]=0xF4; /* SAVE */
          else mem[j++]=e.i;
          if(mem[j-1]==0xE3)
          {
             if(j>=2&&mem[j-2]==0xF3)
             {
                j -= 2;
             }
             else
             {
                ExpStackDel(stack);
                return EXPR_ERR_CODE;
             }
          }
       }
       if(*tail!=0)
       {
          e.c = 1;
          e.i = k;
          if(k==0x60) posle=1;
          if(k!=0xFF && !ExpStackPush(stack,&e))
          {
             ExpStackDel(stack);
             return EXPR_ERR_FULL;
          }
       }
       if(*tail==0&&!ExpStackGet(stack)) break;
       if(*tail!=0) lastth=0;
       continue;
    }
    /* Unary Operator */
    k = ExpOper1(po,&tail);
    if(k>0)
    {
#ifdef DEBUG
       printf("ExpOper1(%s,%s)=0x%2.2X\n",po,tail,k);
#endif
       e.c = 1;
       e.i = k;
       if(k<0xEF&&!ExpStackPush(stack,&e))
       {
          ExpStackDel(stack);
          return EXPR_ERR_FULL;
       }
       continue;
    }
    if(ExpName(po,&tail,s))
    {
       for(lin=t->first;lin;lin=lin->next)
       {
          if(lin->type > 0) /* !!! */
          {
             if(!strcmp(lin->str,s)) break;
          }
       }
       if(!lin)
       {
          if(pass)
          {
             ExpStackDel(stack);
             return EXPR_ERR_UNKNOWN;
          }
          lin = TextAdd(t,s);
          lin->adr = 0;
          lin->type = 11;
       }
       mem[j++] = 0xF5; /* PUSH */
       mem[j++] = (lin->adr)&255;
       mem[j++] = ((lin->adr)>>8)&255;
       if((ExpStackGet(stack)>0 || posle) && lin->type<10 && *tail!='[')
       {
          mem[j++] = 0xF3; /* LOAD */
       }
       lastth = 1;
       if(lin->type>=10) lastth = 2;
       continue;
    }
    if(ExpNumb(po,&tail,&num))
    {
       mem[j++] = 0xF5; /* PUSH */
       mem[j++] = num&255;
       mem[j++] = (num>>8)&255;
       lastth = 2;
       continue;
    }
    if(*po=='(')
    {
#ifdef DEBUG
       printf("===> (\n");
#endif
       e.c = 1;
       e.i = 2;
       if(!ExpStackPush(stack,&e))
       {
          ExpStackDel(stack);
          return EXPR_ERR_FULL;
       }
       lastth = 0;
       tail++;
       continue;
    }
    ExpStackDel(stack);
    return EXPR_ERR_INVALID;
  }
  ExpStackDel(stack);
  return j;
}
