/*  robbyc2.h - Header file for ROBBY expressions

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 1998-2018, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ROBBYC2_H
#define __ROBBYC2_H

#include "my_text.h"

#define EXPR_ERR_OPER    -1
#define EXPR_ERR_UNKNOWN -2
#define EXPR_ERR_LOOP    -3
#define EXPR_ERR_FULL    -4
#define EXPR_ERR_INVALID -5
#define EXPR_ERR_EMPTY   -6
#define EXPR_ERR_CODE    -7

#ifdef __cplusplus
extern "C" {
#endif

int ExpOper1(char *s, char **ss);
int ExpOper2(char *s, char **ss);
int ExpName(char *str, char **tail, char *name);
int ExpNumb(char *str, char **tail, int *number);
int Expression(char *str, Text *t, unsigned char *mem, int without, int pass);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
