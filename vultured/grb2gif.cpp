/*  grb2gif.cpp - gerber/excellon converter to GIF graphics format

    Part of NedoPC SDK (software development kit for simple devices)

    Copyright (C) 2010, Alexander A. Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

//#define DEBUG
//#define STEPS
//#define NOPRINT
//#define NOGIF
//#define ANIMATION

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "unigraf.h"
#ifdef __WATCOMC__
#define NOPRINT
#define NOGIF
#endif
#ifndef NOGIF
#include <gif_lib.h>
#endif

#define KO 10 // DPI=10000/KO

int DX = 4000;
int DY = 3000;

int last_x = 0;
int last_y = 0;

int mirror_x = 0;

int board_x,board_y,board_w,board_h;

UniGraf *ug = NULL;

#ifdef ANIMATION
int anim = 348;
int anik = 0;
#endif

#ifndef NOGIF

// Gif-handling - thanks to Gershon Elber

ColorMapObject *colormap = NULL;

static void QuitGifError(GifFileType *GifFile)
{
    PrintGifError();
    if (GifFile != NULL) EGifCloseFile(GifFile);
    exit(EXIT_FAILURE);
}

static void SaveGif(const char* FileName,
		    GifByteType *OutputBuffer,
		    ColorMapObject *OutputColorMap,
		    int ExpColorMapSize, int Width, int Height)
{
//    return;
    
    int i;
    GifFileType *GifFile;
    GifByteType *Ptr = OutputBuffer;

    /* Open stdout for the output file: */
    if ((GifFile = EGifOpenFileName(FileName,0)) == NULL)
	QuitGifError(GifFile);

    if (EGifPutScreenDesc(GifFile,
			  Width, Height, ExpColorMapSize, 0,
			  OutputColorMap) == GIF_ERROR ||
	EGifPutImageDesc(GifFile,
			 0, 0, Width, Height, FALSE, NULL) == GIF_ERROR)
	QuitGifError(GifFile);

    printf("Image '%s' at (%d, %d) [%dx%d]\n", FileName,
	       GifFile->Image.Left, GifFile->Image.Top,
	       GifFile->Image.Width, GifFile->Image.Height);

    Ptr = &Ptr[board_y*DX+board_x];

    for (i = 0; i < Height; i++) {
	if (EGifPutLine(GifFile, Ptr, Width) == GIF_ERROR)
	    QuitGifError(GifFile);
	Ptr += DX;
    }

    if (EGifCloseFile(GifFile) == GIF_ERROR)
	QuitGifError(GifFile);
}

#endif

void fill(int x, int y, int w, int h, int color)
{
#ifdef DEBUG
 printf("fill %i %i %i %i %i\n",x,y,w,h,color);
#endif
 for(int xx=x;xx<=x+w;xx++){
 for(int yy=y;yy<=y+h;yy++){
   if(mirror_x)
      ug->SetScreenPixel(DX-xx,DY-yy,color);
   else
      ug->SetScreenPixel(xx,DY-yy,color);
 }}
}

void circle(int x, int y, int width, int color)
{
#ifdef DEBUG
 printf("circle %i %i %i %i\n",x,y,width,color);
#endif
 y = DY-y;
 if(mirror_x)
    x = DX-x;
 if(width==0)
 {
   return;
 }
 if(width==1)
 {
   ug->SetScreenPixel(x,y,color);
   return;
 }
 int radius = width>>1;
 int xx2;
 for(int xx=-radius;xx<=radius;xx++)
 {
   xx2 = xx*xx;
   for(int yy=-radius;yy<=radius;yy++)
   {
     if((xx2+yy*yy) <= (radius*radius)) 
     {
       ug->SetScreenPixel(x+xx,y+yy,color);
     }
   }
 }
}

void moveto(int x, int y)
{
#ifdef DEBUG
 printf("moveto %i %i\n",x,y);
#endif
 last_x = x;
 last_y = y;
}

void lineto(int x, int y, int width, int color)
{
#ifdef DEBUG
 printf("lineto %i %i %i %i\n",x,y,width,color);
#endif
 int l_x = x;
 int l_y = y;
 int l_dx = 0;
 int l_dy = 0;
 int l_xlen = 0;
 int l_ylen = 0;
 int l_step = 0;
 int l_mode = 0;
 if(l_x > last_x)
 {
   l_dx = 1;
   l_xlen = l_x - last_x;
 }
 if(l_x < last_x)
 {
   l_dx = -1;
   l_xlen = last_x - l_x;
 }
 l_dy = 0;
 l_ylen = 0;
 if(l_y > last_y)
 {
   l_dy = 1;
   l_ylen = l_y - last_y;
 }
 if(l_y < last_y)
 {
   l_dy = -1;
   l_ylen = last_y - l_y;
 }
 if(l_dy==0)
 {
   l_step = 0;
   l_mode = 1;
 }
 if(l_dx==0)
 {
   l_step = 0;
   l_mode = 2;
 }
 if(l_dx!=0 && l_dy!=0)
 {
   if(l_xlen > l_ylen)
   {
     l_step = (l_xlen<<7)/l_ylen;
     l_mode = 3;
   }
   else
   {
     l_step = (l_ylen<<7)/l_xlen;
     l_mode = 4;
   }
 }
 int l_curs = l_step;
 while(1)
 {
   if(last_x < 0 || last_x >= DX || last_y <0 || last_y >= DY) break;
   circle(last_x,last_y,width,color);
   int C = 0;
   if(l_dx >= 0 && l_x-last_x < 2) C=C+1;
   if(l_dx < 0 && last_x-l_x < 2) C=C+1;
   if(l_dy >= 0 && l_y-last_y < 2) C=C+1;
   if(l_dy < 0 && last_y-l_y < 2) C=C+1;
   if(C==2)
   {
     if(l_x!=last_x || l_y!=last_y)
     {
       circle(l_x,l_y,width,color);
       last_x = l_x;
       last_y = l_y;
     }
     break;
   }
   if(l_mode==1)
   {
     last_x = last_x + l_dx;
     continue;
   }
   if(l_mode==2)
   {
     last_y = last_y + l_dy;
     continue;
   }
   if(l_mode==3)
   {
     last_x = last_x + l_dx;
     l_curs = l_curs - 0x80;
     if(l_curs < 0x80)
     {
       l_curs = l_curs + l_step;
       last_y = last_y + l_dy;
     }
     continue;
   }
   if(l_mode==4)
   {
     last_y = last_y + l_dy;
     l_curs = l_curs - 0x80;
     if(l_curs < 0x80)
     {
       l_curs = l_curs + l_step;
       last_x = last_x + l_dx;
     }
     continue;
   }
   printf("LINE ERROR!");
   break;
 }
}

int main(int argc, char **argv)
{
 FILE *f;
 float dd;
 int xmin=100000,xmax=0,ymin=100000,ymax=0;
 int i,x,y,d,r,l=1,w=1,color=0,done=0;
 int dsize[100],ysize[100];
 char *po,str[256],fname[100];
 if(argc<2) 
 {
    printf("Usage: grb2gif filename\n");
    return -1;
 } 
 ug = new UniGraf(UG256_640x480,1);
 if(ug==NULL) return -2;
 ug->SetPalette(0,0x00,0x00,0x00); // drills (transparent)
 ug->SetPalette(1,0x0B,0x50,0x28); // board 
 ug->SetPalette(2,0x1D,0x66,0x50); // light traces
 ug->SetPalette(3,0x09,0x3B,0x1E); // dark traces
 ug->SetPalette(4,0xAD,0xC9,0xD2); // solder mask
 ug->SetPalette(5,0xFC,0xF1,0xF7); // silkscreen
 ug->SetPalette(6,0x00,0x00,0x00); // reserved
 ug->SetPalette(7,0x00,0x00,0x00); // reserved
 ug->Update();
#ifndef NOGIF
 GifColorType ScratchMap[8];
 for(i=0;i<8;i++)
 {
	r = ug->GetPalette(i);
	ScratchMap[i].Red = r&0xFF;
	ScratchMap[i].Green = (r>>8)&0xFF;
	ScratchMap[i].Blue = (r>>16)&0xFF;
 }
 colormap = MakeMapObject(8, ScratchMap);
 if(colormap==NULL) return -3;
#endif
 for(i=1;i<=15;i++)
 {
   if(done) break;
   switch(i)
   {
      case 1: sprintf(fname,"%s.oln",argv[1]); color=7; break;
      case 2: sprintf(fname,"%s.L2",argv[1]); color=3; break;
      case 3: sprintf(fname,"%s.L1",argv[1]); color=2; break;
      case 4: sprintf(fname,"%s.top",argv[1]); color=2; break;
      case 5: sprintf(fname,"%s.smt",argv[1]); color=4; break;
      case 6: sprintf(fname,"%s.slk",argv[1]); color=5; break;
      case 7: sprintf(fname,"%s.smt",argv[1]); color=4; break;
      case 8: sprintf(fname,"%s.drd",argv[1]); color=0; break;
      case 9: sprintf(fname,"%s-top.gif",argv[1]); color=-1; break;
      case 10: sprintf(fname,"%s.L3",argv[1]); color=3; break;
      case 11: sprintf(fname,"%s.L4",argv[1]); color=2; break;
      case 12: sprintf(fname,"%s.bot",argv[1]); color=2; break;
      case 13: sprintf(fname,"%s.smb",argv[1]); color=4; break;
      case 14: sprintf(fname,"%s.drd",argv[1]); color=0; break;
      case 15: sprintf(fname,"%s-bot.gif",argv[1]); color=-2; break;
   }
   if(color < 0)
   {
#ifndef NOGIF
      SaveGif(fname,ug->GetImage(),colormap,8,board_w,board_h);
#endif
#ifdef ANIMATION
      for(int ia=0;ia<120;ia++)
      {
         anik = 0;
         char sanim[16];
         sprintf(sanim,"%06d.gif",++anim);
         printf("Frame '%s' - PAUSE\n",sanim);
         SaveGif(sanim,ug->GetImage(),colormap,8,DX,DY);
      }
#endif
      if(color==-1)
      {
         while(!done)
         {
            ug->Update();
            if(ug->KeyPressed(1)) done=1;
            if(ug->KeyPressed(28)) break;
         }
         if(!done)
         {
            fill(0,0,DX-1,DY-1,0);
            fill(board_x,board_y,board_w,board_h,1);
            ug->Update();
            mirror_x = 1;
         }    
      }
      continue;
   }
#ifndef NOPRINT   
   printf("Open '%s'\n",fname);
#endif
   f = fopen(fname,"rt");
   if(f==NULL) 
   {
#ifndef NOPRINT
     printf("--- no file '%s'\n",fname);
#endif
     continue;
   }
   for(r=0;r<100;r++) 
   {
     dsize[r]=0;
     ysize[r]=0;
   }
   while(!done)
   {
     fgets(str,256,f);
     if(feof(f)) break;
     po = strchr(str,'\n');
     if(po!=NULL) *po = 0;
     po = strchr(str,'\r');
     if(po!=NULL) *po = 0;
     if(*str=='X' && color==0) // This is coordinates for Excellon
     {
       po = strchr(str,'Y');
       if(po==NULL) 
       {
#ifndef NOPRINT   
          printf("EXCELLON ERROR!");
#endif
       }
       else
       {
         *po=0;
         x = atoi(&str[1]);
         y = atoi(&po[1]);
#ifdef DEBUG
         printf("x=%i y=%i (EXCELLON)",x,y);
#endif
         r = 0;
         if((x%KO)>=5) r=1;
         x = x/KO + r;
         r = 0;
         if((y%KO)>=5) r=1;
         y = y/KO + r;
#ifdef DEBUG
         printf(" --> w=%i x=%i y=%i\n",w,x,y);
#endif
         circle(x-xmin,y-ymin,w,color);
         ug->Update();
         if(ug->KeyPressed(1)) done=1;
       }
     }
     if(*str=='X' && color!=0) // This is coordinates for Gerber
     {
       sscanf(str,"X%06dY%06dD%02d",&x,&y,&d);
#ifdef DEBUG
       printf("x=%i y=%i d=%d",x,y,d);
#endif 
       r = 0;
       if((x%KO)>=5) r=1;
       x = x/KO + r;
       r = 0;
       if((y%KO)>=5) r=1;
       y = y/KO + r;
#ifdef DEBUG
       printf(" --> w=%i l=%i x=%i y=%i\n",w,l,x,y);
#endif
       if(i==1)
       {
         if(x>xmax) xmax=x;
         if(x<xmin) xmin=x;
         if(y>ymax) ymax=y;
         if(y<ymin) ymin=y;
       }
       else
       {
         if(d==1) lineto(x-xmin,y-ymin,w,color);
         if(d==2) moveto(x-xmin,y-ymin);
         if(d==3)
         { 
           if(l==0) 
           {
             circle(x-xmin,y-ymin,w,color);
             moveto(x-xmin,y-ymin);
           } 
           else
           {
             if(l>0)
             {
                fill(x-xmin-(w>>1),y-ymin-(l>>1),w,l,color);
             }
             else // l<0
             {
                if(w > -l)
                {
                   r = (w+l)>>1;
                   moveto(x-xmin-r,y-ymin);
                   lineto(x-xmin+r,y-ymin,-l,color);
                }
                else
                {
                   r = (-l-w)>>1;
                   moveto(x-xmin,y-ymin-r);
                   lineto(x-xmin,y-ymin+r,w,color);
                }
             }
             moveto(x-xmin,y-ymin);
           }
         }
         ug->Update();
         if(ug->KeyPressed(1)) done=1;
#ifdef STEPS
         while(!done)
         {
            ug->Update();
            if(ug->KeyPressed(28)) break;
            if(ug->KeyPressed(1)) done=1;
         }
#endif
       }
     }
#ifdef ANIMATION
     if(*str=='X' && i!=7)
     {
         if(color==0 || ++anik==7)
         {
           anik = 0;
           char sanim[16];
           sprintf(sanim,"%06d.gif",++anim);
           printf("Frame '%s'\n",sanim);
           SaveGif(sanim,ug->GetImage(),colormap,8,DX,DY);
         }  
     }
#endif
     if(*str=='T') // Drill (Excellon)
     {
       po = strchr(str,'C');
       if(po==NULL) // Drill using
       {
         sscanf(str,"T%02d",&d);
#ifdef DEBUG
         printf("t=%d\n",d);
#endif
         w = dsize[d];
       }
       else // Drill adding
       {
         *po = 0;
         sscanf(str,"T%02d",&d);
         sscanf(&po[1],"%f",&dd);
         r = dd*10000/KO;
         if(r==0) r=1;
#ifdef DEBUG
         printf("add %i %f -> %i\n",d,dd,r);
#endif
         dsize[d] = r;
       } 
     }
     if(*str=='D') // Brush using (Gerber)
     {
       sscanf(str,"D%02d",&d);
#ifdef DEBUG
       printf("d=%d\n",d);
#endif
       w = dsize[d];
       l = ysize[d];
     }
     if(*str=='%') // Brush adding (Gerber)
     {
       if(str[1]=='A' && str[2]=='D' && str[3]=='D')
       {
          x = str[4]-'0';
          y = str[5]-'0';
          d = x*10 + y;
          po = strchr(str,',');
          if(po!=NULL)
          {
            sscanf(&po[1],"%f",&dd);
            r = dd*10000/KO;
            if(r==0) r=1;
#ifdef DEBUG
            printf("add %i %f (%c) -> %i\n",d,dd,str[6],r);
#endif
            dsize[d] = r;
            po = strchr(po,'X');
            if(po!=NULL)
            {
              sscanf(&po[1],"%f",&dd);
              r = dd*10000/KO;
              if(r==0) r=1;
              
#ifdef DEBUG
              printf("    %i %f (X) -> %i\n",d,dd,r);
#endif
              if(str[6]!='O')
                 ysize[d] = r;
              else
                 ysize[d] = -r;
            }
          }
       }
     }
   }
   fclose(f);
   if(color==7) // first pass
   {
     DX = xmax-xmin;
     DY = ymax-ymin;
     board_x = xmin;
     board_y = ymin;
     board_w = DX;
     board_h = DY;
     int mx = DX/4;
     int my = DY/3;
     if(mx > my)
     {
       DY = mx*3;
       ymin += (board_h-DY)>>1;
     }
     if(mx < my)
     {
       DX = my*4;
       xmin += (board_w-DX)>>1;
     }
     board_x -= xmin;
     board_y -= ymin;
#ifndef NOPRINT
     printf("%i x %i (%i,%i) [%i,%i,%i,%i]\n",
        DX,DY,xmin,ymin,board_x,board_y,board_w,board_h);
#endif
     ug->Screen(DX,DY);
     fill(board_x,board_y,board_w,board_h,1);
     ug->Update();
   }
 }
 while(1)
 {
   ug->Update();
   if(ug->KeyPressed(1)) break;
   if(ug->KeyPressed(28)) break;
 }
 delete ug;
 return 0;
}
