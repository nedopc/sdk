/*  grb_mul.c - multiplier for gerber/excellon files

    Part of NedoPC SDK (software development kit for simple devices)

    Copyright (C) 2010-2011, Alexander A. Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_text.h"

//#define DEBUG

#define HSTEP 1000

int parse1(char *s, int *x, int *y, int *d)
{
 char *px,*py,*pd,str[256];
#ifdef DEBUG
 printf("parse %s\n",s);
#endif
 strncpy(str,s,256);
 str[255] = 0;
 if(*str!='X') return -11;
 px = &str[1];
 py = strchr(px,'Y');
 if(py==NULL) return -12;
 *py = 0;
 py++;
 pd = strchr(py,'D');
 if(pd==NULL)
   *d = 0;
 else
 {
   *pd = 0;
   pd++;
   *d = atoi(pd);
 }
 *x = atoi(px);
 *y = atoi(py);
#ifdef DEBUG
 printf("\tX=%i Y=%i D=%i\n",*x,*y,*d);
#endif
 return 0;
}

int main(int argc, char** argv)
{
 Line *l;
 Text *ls = NULL;
 FILE *fi,*fo;
 int nx,ny,i,j,k,x,y,d,b,f,xl,yl,dx=0,dy=0,x0=0,y0=0,t=0,n=0,err=0,ih=0;
 char *po,*pext,fname[100],fnew[100],str[256];
 if(argc<4)
 {
   printf("\nUsage:\n\tgrb_mul NAME NX NY [IH]\n\n");
   printf("\twhere\n");
   printf("\t\tNAME - name without extension\n");
   printf("\t\tNX - number of horizontal copies\n");
   printf("\t\tNY - number of vertical copies\n");
   printf("\t\tIH - index of hole descriprion to use on borders (optional)\n");
   printf("\n");
   return -1;
 }
 nx = atoi(argv[2]);
 ny = atoi(argv[3]);
 if(argc>4)
 {
   ih = atoi(argv[4]);
   printf("Border is created by holes #%i\n",ih);
 }
 if(nx==0 || ny==0)
 {
   printf("\nERROR: number must be greater than 0!\n\n");
   return -2;
 }
 if(nx==1 && ny==1)
 {
   printf("\nERROR: at least one number must be greater than 1!\n\n");
   return -3;
 }
 for(k=1;k<=12;k++)
 {
   switch(k)
   {
      case 1:  sprintf(fname,"%s.oln", argv[1]); t=0; break;
      case 2:  sprintf(fname,"%s.L2",  argv[1]); t=1; break;
      case 3:  sprintf(fname,"%s.L1",  argv[1]); t=1; break;
      case 4:  sprintf(fname,"%s.top", argv[1]); t=1; break;
      case 5:  sprintf(fname,"%s.smt", argv[1]); t=1; break;
      case 6:  sprintf(fname,"%s.slk", argv[1]); t=1; break;
      case 7:  sprintf(fname,"%s.smt", argv[1]); t=1; break;
      case 8:  sprintf(fname,"%s.L3",  argv[1]); t=1; break;
      case 9:  sprintf(fname,"%s.L4",  argv[1]); t=1; break;
      case 10: sprintf(fname,"%s.bot", argv[1]); t=1; break;
      case 11: sprintf(fname,"%s.smb", argv[1]); t=1; break;
      case 12: sprintf(fname,"%s.drd", argv[1]); t=2; break;
   }
   pext = strrchr(fname,'.');
   fi = fopen(fname,"rt");
   if(fi==NULL) continue;
   if(t<=1) printf("\nGerber file '%s':\n",fname);
   if(t==2) printf("\nExcellon file '%s':\n",fname);
   sprintf(fnew,"%s_%ix%i%s",argv[1],nx,ny,pext);
   printf("New file '%s'\n",fnew);
   fo = fopen(fnew,"wt");
   if(fo!=NULL)
   {
     b = 0;
     xl = 0;
     yl = 0;
     while(1)
     {
       fgets(str,256,fi);
       if(feof(fi)) break;
       po = strrchr(str,'\n');
       if(po!=NULL) *po=0;
       po = strrchr(str,'\r');
       if(po!=NULL) *po=0;
       if(t==0) /* outline */
       {
         if(*str!='X') fprintf(fo,"%s\n",str);
         else
         {
            if(strstr(str,"D02"))
            {
              err = parse1(str,&x,&y,&d);
              if(err) break;
              x0 = x;
              y0 = y;
            }
            else
            {
              err = parse1(str,&x,&y,&d);
              if(err) break;
              if(x!=x0)
              {
                dx = x-x0;
                x = dx*nx+x0;
              }
              if(y!=y0)
              {
                dy = y-y0;
                y = dy*ny+y0;
              }
            }
            fprintf(fo,"X%06dY%06dD%02d*\n",x,y,d);
         }
       }
       if(t==1) /* gerber */
       {
         if(*str=='X')
         {
           if(xl==0 && x!=0) xl=x;
           if(yl==0 && y!=0) yl=y;
           err = parse1(str,&x,&y,&d);
           if(ls==NULL){err=-4;break;}
           l = TextAdd(ls,str);
           if(l==NULL){err=-5;break;}
           l->id = x;
           l->id2 = y;
           l->type = d;
         }
         else if(*str=='D'||*str=='M')
         {
           if(b!=0)
           {
             f = 0;
             for(i=0;i<nx;i++){
             for(j=0;j<ny;j++){
               for(l=ls->first;l!=NULL;l=l->next)
               {
                 if(l==ls->first && l->type!=2)
                 {
                    if(f++==0) printf("Current pointer for D%02d was corrected\n",b);
                    fprintf(fo,"X%06dY%06dD%02d*\n",xl+dx*i,yl+dy*j,2);
                 }
                 fprintf(fo,"X%06dY%06dD%02d*\n",l->id+dx*i,l->id2+dy*j,l->type);
               }
             }}
           }
           fprintf(fo,"%s\n",str);
           if(ls!=NULL)
           {
             TextDel(ls);
             ls = NULL;
           }
           if(*str=='M') b=0;
           else
           {
             b = atoi(&str[1]);
             ls = TextNew();
             if(ls==NULL){err=-3;break;}
             xl = 0;
             yl = 0;
           }
         }
         else fprintf(fo,"%s\n",str);
       }
       if(t==2) /* excellon */
       {
         if(*str=='X')
         {
           err = parse1(str,&x,&y,&d);
           if(ls==NULL){err=-6;break;}
           l = TextAdd(ls,str);
           if(l==NULL){err=-7;break;}
           l->id = x;
           l->id2 = y;
         }
         else
         {
           if(b!=0)
           {
             for(i=0;i<nx;i++){
             for(j=0;j<ny;j++){
               for(l=ls->first;l!=NULL;l=l->next)
               {
                 fprintf(fo,"X%dY%d\n",l->id+dx*i,l->id2+dy*j);
               }
             }}
           }
           fprintf(fo,"%s\n",str);
           if(ls!=NULL)
           {
             TextDel(ls);
             ls = NULL;
           }
           if(*str=='T' && strchr(str,'.')==NULL)
           {
             b = atoi(&str[1]);
             if(ih==b)
             {
               for(i=1;i<nx;i++)
                 for(j=HSTEP/2;j<dy*ny;j+=HSTEP)
                   if(dy*ny-j>HSTEP && (j%dy)>HSTEP && (j%dy)<dy-HSTEP)
                     fprintf(fo,"X%dY%d\n",x0+dx*i,y0+j);
               for(j=1;j<ny;j++)
                 for(i=HSTEP/2;i<dx*nx;i+=HSTEP)
                   if(dx*nx-i>HSTEP && (i%dx)>HSTEP && (i%dx)<dx-HSTEP)
                     fprintf(fo,"X%dY%d\n",x0+i,y0+dy*j);
             }
             ls = TextNew();
             if(ls==NULL){err=-8;break;}
           }
         }
       }
     }
     fclose(fo);
     n++;
   }
   else err=-10;
   fclose(fi);
   if(err) break;
 }
 if(err) printf("\nERROR %i\n\n",err);
 else printf("\nOK %i\n\n",n);
 return err;
}
