/*  nedomake.c - Simple command interpreter (formerly known as SHJOB)

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 2002-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef linux
#define PATHTERM '/'
#include <unistd.h>
#define _chdir chdir
#define stricmp strcasecmp
#else
#define PATHTERM '\\'
#include <direct.h>
#ifdef __BORLANDC__
#define _chdir chdir
#endif
#endif
#include "my_text.h"

#define VERSION "v2.0.3"

/* NEDOMAKE HISTORY:

 19 Jul 2024 - NEDOMAKE v2.0.3

 - If provided path starts with "prj/" then it will be capitalized
 - New command \check to compare last line of text file with provided string

 15 Jul 2024 - NEDOMAKE v2.0.2

 - Supported DEFAULT.N in EXEPATH directory if not found in project directory
 - Fixed \if ${variable} : label

 14 Jul 2024 - NEDOMAKE v2.0.1

 - Supported default N-script with name DEFAULT.N if script name is not given
 - Added new predefined variables ${PROJECT} taken from name of file or subdir
 - Variables ${1}..${9} represent arguments if present
 - Added new embedded commands \exit and \say
 - Added conditional execution with \label, \goto and \if (forward jumps only)
 - External command ADDBIN became embedded command \glue
 - External connand LEADZERO became embedded command \ltrim
 - Added new embedded command \rtrim to remove trailing zeros from binaries
 - Renamed to NEDOMAKE just because why not? ;)

 08 Jul 2024 - NEDO v2.0

 - New GPL-ed reincarnation named NEDO for open source nedoPC SDK
 - Converted from C++ to C to be compatible with 16-bit ANSI-C compilers
 - Command \del became \remove that is used to remove files
 - Command \add became \append that is used to append text files

 17 Apr 2003 - SHJOB v1.3

 - Released for RW1P2 v1.5 aka "Sprinter SDK by NedoPC Team"
 - Command \ren became \rename that is used to rename files
 - Added new embedded command \copy to copy files
 - Added new predefined variable ${EXEPATH} to support project directories

 01 Jun 2002 - SHJOB v1.0

 - Initial release written in C++ for RW1P2 v1.4
 - Supported variables and embedded commands \need, \ren, \del and \add

*/

Text *job;
Text *var;
Text *lab;
char path[256];
char full[256];
char proj[64];
int spaces = 0;

#define ZAMSPACE 7

#define DEFAULT "DEFAULT.N"

void remspace(char *s)
{
 int i;
 for(i=0;i<strlen(s);i++)
 {
   if(s[i]==ZAMSPACE) s[i]=' ';
 }
}

void myerror(char* s, int n)
{
 if(!s)
    printf("\nSYNTAX ERROR%i\n\n",n);
 else
    printf("\nUNKNOWN ERROR%i %s\n\n",n,s);
 exit(n);
}

int mycopy(char *s1, char *s2)
{
 int i,k;
 FILE *f1;
 FILE *f2;
 char *p1 = s1;
 char *p2 = s2;
 if(*p1=='"') p1++;
 if(*p2=='"') p2++;
 if(p1[strlen(p1)-1]=='"') p1[strlen(p1)-1]=0;
 if(p2[strlen(p2)-1]=='"') p2[strlen(p2)-1]=0;
 f1 = fopen(p1,"rb");
 if(f1==NULL) return 0;
 f2 = fopen(p2,"wb");
 if(f2==NULL){fclose(f1);return 0;}
 fseek(f1,0,SEEK_END);
 k = ftell(f1);
 fseek(f1,0,SEEK_SET);
 for(i=0;i<k;i++) fputc(fgetc(f1),f2);
 fclose(f1);
 fclose(f2);
 return 1;
}

int glue(char *s1, char *s2, unsigned short off)
{
 unsigned int i,j,size;
 FILE *f1,*f2;
 f1 = fopen(s1,"r+b");
 if(f1==NULL)
 {
    f1 = fopen(s1,"wb");
    if(f1==NULL)
    {
      printf("Can't create file %s\n",s1);
      return 0;
    }
    for(i=0;i<off;i++) fputc(0,f1);
 }
 else
 {
    fseek(f1,0L,SEEK_END);
    j = ftell(f1);
    fseek(f1,0L,SEEK_SET);
    if(off==0xFFFF) off = j;
    if(j>off) fseek(f1,off,SEEK_SET);
    else
    {
       fseek(f1,j,SEEK_SET);
       for(i=j;i<off;i++) fputc(0,f1);
    }
 }
 if(f1==NULL) return 0;
 f2 = fopen(s2,"rb");
 if(f2==NULL)
 {
    fclose(f1);
    printf("Can't open file %s\n",s2);
    return 0;
 }
 fseek(f2,0L,SEEK_END);
 size = ftell(f2);
 fseek(f2,0L,SEEK_SET);
 for(i=0;i<size;i++) fputc(fgetc(f2),f1);
 fclose(f1);
 fclose(f2);
 return 1;
}

#define TMPFILE "__temp__.bin"

int trim(char *s, int left)
{
  char ss[100];
  unsigned long sz;
  unsigned char* buf;
  FILE *f,*f2;
  long l;
  int c,z=1;
  f = fopen(s,"rb");
  if(f==NULL)
  {
    printf("Can't open file %s\n",s);
    return 0;
  }
  fseek(f,0L,SEEK_END);
  sz = ftell(f);
  fseek(f,0L,SEEK_SET);
  f2 = fopen(TMPFILE,"wb");
  if(f2==NULL)
  {
    fclose(f);
    printf("Can't open temporary file\n");
    return 0;
  }
  if(left) /* ltrim */
  {
    for(l=0;l<sz;l++)
    {
      c = fgetc(f);
      if(z && c==0) continue;
      z = 0;
      fputc(c,f2);
    }
  }
  else /* rtrim */
  {
    buf = (unsigned char*)malloc(sz);
    if(buf==NULL)
    {
      printf("Can't allocate %lu bytes\n",sz);
      return 0;
    }
    fread(buf,1,sz,f);
    for(l=sz-1;l>=0;l--)
    {
      c = buf[l];
      if(z & c==0) continue;
      break;
    }
    fwrite(buf,l+1,1,f2);
    free(buf);
  }
  fclose(f);
  fclose(f2);
  unlink(s);
  if(!rename(TMPFILE,s)) return 1;
  return 0;
}

int evalexpr(char *s)
{
  int i,j,l,d=1,m=0;
  char *po,*p2;
  po = s;
  if(*po=='-'){m++;po++;}
  l = strlen(po);
  for(i=0;i<l;i++) if(!isdigit(po[i])) d=0;
  if(d)
  {
     i = atoi(po);
     if(m) i=-i;
     return i?1:0;
  }
  p2 = strstr(po,"==");
  if(p2)
  {
    *p2 = 0;
    p2++;p2++;
    if(!stricmp(po,p2)) return 1;
    return 0;
  }
  p2 = strstr(po,"!=");
  if(p2)
  {
    *p2 = 0;
    p2++;p2++;
    if(!stricmp(po,p2)) return 0;
    return 1;
  }
  p2 = strstr(po,">=");
  if(p2)
  {
    *p2 = 0;
    p2++;p2++;
    if(*po=='#'){po++;i=hex2i(po);}
    else i=atoi(po);
    if(*p2=='#'){po++;i=hex2i(p2);}
    else i=atoi(p2);
    return (i>=j)?1:0;
  }
  p2 = strstr(po,"<=");
  if(p2)
  {
    *p2 = 0;
    p2++;p2++;
    if(*po=='#'){po++;i=hex2i(po);}
    else i=atoi(po);
    if(*p2=='#'){po++;i=hex2i(p2);}
    else i=atoi(p2);
    return (i<=j)?1:0;
  }
  p2 = strstr(po,">");
  if(p2)
  {
    *p2 = 0;
    p2++;
    if(*po=='#'){po++;i=hex2i(po);}
    else i=atoi(po);
    if(*p2=='#'){po++;i=hex2i(p2);}
    else i=atoi(p2);
    return (i>j)?1:0;
  }
  p2 = strstr(po,"<");
  if(p2)
  {
    *p2 = 0;
    p2++;
    if(*po=='#'){po++;i=hex2i(po);}
    else i=atoi(po);
    if(*p2=='#'){po++;i=hex2i(p2);}
    else i=atoi(p2);
    return (i<j)?1:0;
  }
  /* assume true */
  return 1;
}

int main(int argc, char **argv)
{
 int i,j,k,l;
 unsigned short u;
 static char str[256],str1[256],str2[256],st[100],skipto[64];
 char *po,*p1,*p2,*p3;
 FILE *f,*ff;
 Line *line,*lin;
 int nofile = 0;
 skipto[0] = 0;
 printf("\n\nNEDOMAKE " VERSION " (c) 2002-2024, Alexander A. Shabarshin <me@shaos.net>\n");
 if(argc<2)
 {
   printf("Simple command interpreter, usage: NEDO FILE.N [args]\n");
   printf("\nFormat of N-file:\n");
   printf("#!./nedo\n");
   printf("# comments start with the hash sign character\n");
   printf("$name=value # set variable\n");
   printf("# predefined variables are ${EXEPATH}, ${PROJECT} and arguments ${1}..${9}\n");
   printf("command ... ${name} ... ${1} # local command with variables and args\n");
   printf("!command ... ${name} # system command with possible variables\n");
   printf("\\need file # check file existance and exit if it doesn't exist\n");
   printf("\\remove file # remove file if it exists\n");
   printf("\\append file string # append text file with a string as a new line\n");
   printf("\\rename oldfile newfile # rename file\n");
   printf("\\copy oldfile newfile # copy file\n");
   printf("\\exit # preliminary quit scripted sequence\n");
   printf("\\say ... ${name} ... # say something with possible variables\n");
   printf("\\glue targetfile binfile offset # combine binary files\n");
   printf("\\ltrim binfile # remove leading zeros from binary file\n");
   printf("\\rtrim binfile # remove trailing zeros from binary file\n");
   printf("\\check textfile string # compare last line of text file with string\n");
   printf("\\label name # set label name (must be unique)\n");
   printf("\\goto name # jump to label name\n");
   printf("\\if EXPR : name # evaluate expression and jump to label name if true\n");
   printf("# EXPR could be ${name}, ${name}==value, ${name}!=value, ${name}>num etc.\n");
   printf("\nAlternative way to use: NEDO path_to_project [args]\n");
   printf("In this case it will try to open file DEFAULT.N located in specified directory.\n");
   printf("\n");
   return 0;
 }
 job = TextNew();
 if(job==NULL) return -101;
 var = TextNew();
 if(var==NULL) return -102;
 lab = TextNew();
 if(lab==NULL) return -103;
 strcpy(path,argv[0]);
 k = 0;
 for(i=0;i<strlen(path);i++)
 {
   if(path[i]==PATHTERM) k = i+1;
   if(path[i]==' ') spaces = 1;
 }
 path[k] = 0;
 strcpy(str,argv[1]);
 if(!strncmp(str,"prj/",4))
      k = 1;
 else k = 0;
 l = strlen(str);
 for(i=0;i<l;i++)
 {
   if(str[i]=='/' || str[i]=='\\')
      str[i] = PATHTERM;
#if 1
   else if(k) str[i] = toupper(str[i]);
#endif
 }
 i = str[l-2];
 j = str[l-1];
 if(i!='.'||(j!='n')&&(j!='N'))
 {
   nofile++;
   if(j==PATHTERM) str[l-1]=0;
   po = strrchr(str,PATHTERM);
   if(po) po++;
   else po = str;
   strncpy(proj,po,63);
   proj[63] = 0;
   for(i=0;i<strlen(proj);i++)
     proj[i] = toupper(proj[i]);
   strcat(str," ");
   str[strlen(str)-1] = PATHTERM;
   strcat(str,DEFAULT);
   strcpy(full,str);
   f = fopen(str,"rt");
   if(f==NULL)
   {
      strcpy(str,path);
      strcat(str,DEFAULT);
      f = fopen(str,"rt");
   }
   if(f==NULL) return -200;
   fclose(f);
 }
 else
 {
   strcpy(full,str);
   po = strrchr(str,PATHTERM);
   if(po) po++;
   else po = str;
   strncpy(proj,po,63);
   proj[63] = 0;
   for(i=0;i<strlen(proj);i++)
     proj[i] = toupper(proj[i]);
   po = strstr(proj,".N");
   if(po!=NULL) *po = 0;
 }
 printf("\nJOBFILE=%s\n",str);
 TextLoad(job,str);
 if(argc>2)
 {
   for(i=2;i<argc;i++)
   {
      sprintf(str,"%i=%s",i-1,argv[i]);
      TextAdd(var,str);
   }
 }
 printf("EXEPATH=%s\n",path);
 sprintf(str,"EXEPATH=%s",path);
 TextAdd(var,str);
 printf("PROJECT=%s\n",proj);
 sprintf(str,"PROJECT=%s",proj);
 TextAdd(var,str);
 strcpy(str,argv[1]);
 k = 0;
 for(i=0;i<strlen(full);i++)
 {
   if(full[i]==PATHTERM) k = i;
 }
 full[k] = 0;
 printf("CURPATH=%s\n",full);
 if(*full) _chdir(full);
 l = 0;
 for(line=job->first;line!=NULL;line=line->next)
 {
   if(*line->str=='#') *line->str = 0;
   if(*line->str==0) continue;
   po = strstr(line->str," # ");
   if(po!=NULL) *po = 0;
   po = line->str;
   while(*po==' '||*po=='\t') po++;
   if(*po=='#') *po = 0;
   while(po[strlen(po)-1]==' '||po[strlen(po)-1]=='\t')
      po[strlen(po)-1] = 0;
   if(!*po) continue;
   if(!line->id) line->id=++l;
   strcpy(str2,po);
   if(*skipto)
   {
      k = 0;
      if(strncmp(str2,"\\label",6)) k++;
      else
      {
        po = &str2[6];
        while(*po==' '||*po=='\t') po++;
        if(stricmp(skipto,po)) k++;
        else *skipto = 0;
      }
      if(k)
      {
        printf("---%s\n",str2);
        continue;
      }
   }
   po = str2;
   if(*po=='$' && po[1]!='{')
   {
      strcpy(str,po);
      p2 = strchr(str,'=');
      if(p2==NULL) myerror(NULL,-10);
      po++;
      printf(">>>$%s<<<\n",po);
      TextAdd(var,po);
   }
   else
   {
      i=0;j=0;
      while(str2[j])
      {
         if(str2[j]=='$')
         {
            j++;
            if(str2[j]!='{')
            {
              str[i++] = str2[j-1];
              continue;
            }
            j++;
            p2 = strchr(&str2[j],'}');
            if(p2==NULL) myerror(NULL,-11);
            *p2 = 0;
            p3 = &str2[j];
            while(p2!=&str2[j]) j++;
            j++;
            *str1 = 0;
            for(lin=var->first;lin!=NULL;lin=lin->next)
            {
               strcpy(st,lin->str);
               p2 = strchr(st,'=');
               *p2 = 0;
               p2++;
               if(!stricmp(p3,st)) strcpy(str1,p2);
            }
            if(!*str1)
            {
               if(strncmp(str2,"\\if",3)) myerror(p3,-12);
               strcpy(str1,"0");
            }
            for(k=0;k<strlen(str1);k++) str[i++] = str1[k];
         }
         else
         {
            str[i] = str2[j];
            i++;j++;
         }
      }
      str[i] = 0;
      printf(">>>%s\n",str);
      if(*str=='\\')
      {
         k = 0;
         for(i=0;i<strlen(str);i++)
         {
            if(str[i]=='"'&&k==0){k=1;continue;}
            if(str[i]=='"'&&k==1){k=0;continue;}
            if(str[i]==' '&&k==1) str[i]=ZAMSPACE;
         }
         po = strtok(str," \t");
         if(po==NULL) myerror(NULL,-13);
         if(!strcmp(po,"\\need"))
         {
            p1 = strtok(NULL," ");
            if(p1==NULL) myerror(NULL,-14);
            remspace(p1);
            f = fopen(p1,"rb");
            if(f==NULL) return -1;
            fclose(f);
         }
         else if(!strcmp(po,"\\remove"))
         {
            p1 = strtok(NULL," ");
            if(p1==NULL) myerror(NULL,-15);
            remspace(p1);
            unlink(p1);
         }
         else if(!strcmp(po,"\\append"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-16);
            p2 = strtok(NULL,"\n");
            if(p2==NULL) myerror(NULL,-17);
            remspace(p1);
            remspace(p2);
            ff = fopen(p1,"a+t");
            if(ff==NULL) break;
            fprintf(ff,"%s\n",p2);
            fclose(ff);
         }
         else if(!strcmp(po,"\\rename"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-18);
            p2 = strtok(NULL," \t");
            if(p2==NULL) myerror(NULL,-19);
            remspace(p1);
            remspace(p2);
            k=1;
#ifdef linux
            if(!strcmp(p1,p2)) k=0;
#else
            if(!stricmp(p1,p2)) k=0;
#endif
            if(k)
            {
              unlink(p2);
              if(rename(p1,p2)) myerror(NULL,-20);
            }
         }
         else if(!strcmp(po,"\\copy"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-21);
            p2 = strtok(NULL," \t");
            if(p2==NULL) myerror(NULL,-22);
            remspace(p1);
            remspace(p2);
            if(!mycopy(p1,p2)) myerror(NULL,-23);
         }
         else if(!strcmp(po,"\\say"))
         {
            p1 = strtok(NULL,"\n");
            remspace(p1);
            if(p1!=NULL) printf("%s\n",p1);
         }
         else if(!strcmp(po,"\\glue"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-24);
            p2 = strtok(NULL," \t");
            if(p2==NULL) myerror(NULL,-25);
            p3 = strtok(NULL," \t");
            if(p3==NULL) u = 0xFFFF;
            else if(*p3=='#') u = hex2i(&p3[1]);
            else u = atoi(p3);
            remspace(p1);
            remspace(p2);
            if(!glue(p1,p2,u)) myerror(NULL,-26);
         }
         else if(!strcmp(po,"\\ltrim"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-27);
            remspace(p1);
            if(!trim(p1,1)) myerror(NULL,-28);
         }
         else if(!strcmp(po,"\\rtrim"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-29);
            remspace(p1);
            if(!trim(p1,0)) myerror(NULL,-30);
         }
         else if(!strcmp(po,"\\label"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-31);
            for(lin=lab->first;lin!=NULL;lin=lin->next)
            {
               if(!strcmp(p1,lin->str)) myerror(NULL,-32);
            }
            lin = TextAdd(lab,p1);
            lin->id = line->id;
         }
         else if(!strcmp(po,"\\goto"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-33);
            strcpy(skipto,p1);
         }
         else if(!strcmp(po,"\\if"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-34);
            p2 = strtok(NULL," \t");
            if(p2==NULL) myerror(NULL,-35);
            if(p2[0]!=':'||p2[1]!=0) myerror(NULL,-36);
            p3 = strtok(NULL," \t");
            if(p3==NULL) myerror(NULL,-37);
            for(lin=lab->first;lin!=NULL;lin=lin->next)
            {
               if(!strcmp(p3,lin->str)) myerror(NULL,-38);
            }
            if(evalexpr(p1)) strcpy(skipto,p3);
         }
         else if(!strcmp(po,"\\check"))
         {
            p1 = strtok(NULL," \t");
            if(p1==NULL) myerror(NULL,-39);
            p2 = strtok(NULL,"\n");
            if(p2==NULL) myerror(NULL,-40);
            remspace(p1);
            remspace(p2);
            ff = fopen(p1,"rt");
            if(ff==NULL) myerror(NULL,-41);
            *str1 = 0;
            while(1)
            {
              fgets(str1,256,ff);
              if(feof(ff)) break;
              p3 = strrchr(str1,'\n');
              if(p3!=NULL) *p3=0;
              p3 = strrchr(str1,'\r');
              if(p3!=NULL) *p3=0;
            }
            fclose(ff);
            if(strcmp(str1,p2)) return -1;
         }
         else if(!strcmp(po,"\\exit")) return -1;
         else myerror(po,-99);
      }
      else
      {
         fflush(stdout);
         if(*str=='!') system(&str[1]);
         else
         {
            sprintf(str1,"%s%s",path,str);
            if(spaces) /* for Windows ??? */
            {
               i = j = k = 0;
               str2[j++] = '"';
               while(str1[i])
               {
                  if(i>strlen(path))
                  {
                     if(!k && str1[i]==' ')
                     {
                        str2[j++] = '"';
                        k = 1;
                     }
                  }
                  str2[j++] = str1[i++];
               }
               str2[j] = 0;
               printf("!!!%s\n",str2);
               system(str2);
            }
            else
            {
#ifdef linux
               j = 0;
               k = 0;
               for(i=0;i<strlen(str1);i++)
               {
                  if(k==' '&&str1[i]=='#')
                  {
                     str2[j++] = '\\';
                  }
                  str2[j++] = str1[i];
                  k = str1[i];
               }
               str2[j] = 0;
#else
               strcpy(str2,str1);
#endif
               printf("!!!%s\n",str2);
               system(str2);
            }
         }
      }
   }
 }
 if(!*skipto) printf("\nOk!\n");
#if 0
 printf("JOB:\n");
 TextList(job);
 printf("VAR:\n");
 TextList(var);
 printf("LAB:\n");
 TextList(lab);
#endif
 TextDel(job);
 TextDel(var);
 TextDel(lab);
 return 0;
}
