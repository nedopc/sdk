// CREATED: 17-JUL-2024
// UPDATED: 20-JUL-2024
ROBOT "TEST-0001"
AUTHOR "SHAOS"
+INC/ROBBY
main()
{
 text "<><><><><><><><><><> MAIN-BEGIN"

 def ar[5] = "XYZ" // ar[0] holds size
 def arr[10] = {100, 101, 102}

 A = &arr
 B = A[0]
 C = A[1]

 say "&A &B &C #&A #&B #&C $&AR[1] SZ=&AR[0] "

 text "<><><><><><><><><><> MAIN-END"
}
