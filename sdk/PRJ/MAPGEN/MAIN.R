robot "MapGen"
author "Shaos"

+INC/ROBBY
+TILES

@DMBOX=10
@DX=20
@DY=10
@SIZE=200

main()
{
 DEF map[@SIZE]
 buff = 0
 SetRand(14)
 _M_reactor = 0x2
 _M_hole    = 2
 _M_box     = 5
 _M_rock    = 20
 DEF _X_box[@DMBOX]
 DEF _Y_box[@DMBOX]
 _N_box = 0;
 dx = 0
 dy = 0

 command @P2_TERMINFO
 dx=A;dy=B
 x0=(dx-20)/2
 y0=(dy-12)/2
 A=x0;B=y0;C=2
 command @P2_SETSAY
 say "  ROBOT WARFARE  1  "
 A=x0;B=y0+11;C=1
 command @P2_SETSAY
 SayRand(0)

again:

 SayRand(1)
 MakeMap(2,@DX,@DY)

 kk = 0
 for(jj=1,jj<=@DY,jj++)
 {
   for(ii=0,ii<@DX,ii++)
   {
       c = map[kk]>>4;
       kk = kk+1
       select(x0+ii,y0+jj)
       if(c==0) set @sp_e
       if(c==1) set @sp_h
       if(c==2) set @sp_s
       if(c==3) set @sp_b
       if(c==4) set @sp_r
       if(c==6) set @sp_r0
   }
 }

 while(1)
 {
    recv buff
    if(buff) break
 }

 goto again
}


+LIB/RANDOM

SayRand()
{
  say "MAPGEN #&RAND        "
}

// A-N, B-X, C-Y
MakeMap()
{
   num = A
   dx = B
   dy = C
   sz = dx*dy
   reactor = num * _M_reactor
   hole    = num * _M_hole
   box     = num * _M_box
   rock    = num * _M_rock
   for(A=0,A<sz,A++)
   {
      map[A] = 0
   }
   C = 0
   for(B=0,B<rock,B++)
   {
      while(map[C])
      {
         Random(sz)
      }
      map[C] = 0x25
   }
   for(B=0,B<hole,B++)
   {
      while(map[C])
      {
         Random(sz)
      }
      map[C] = 0x10
   }
   for(B=0,B<box,B++)
   {
      while(map[C])
      {
         Random(sz)
      }
      map[C] = 0x30
      if(_N_box<@DMBOX)
      {
         _X_box[_N_box]=B%dx
         _Y_box[_N_box]=B/dx
         _N_box = _N_box+1
      }
   }
   for(B=0,B<reactor,B++)
   {
      while(map[C])
      {
         Random(sz)
      }
      map[C] = 0x40
   }
   for(B=0,B<num,B++)
   {
      while(map[C])
      {
         Random(sz)
      }
      map[C] = 0x60 + B
   }
}


