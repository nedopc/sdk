/*  robbycc.c - Source code for Robby cross-compiler (formerly known as RW0COMP)

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 2001-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "my_text.h"

#define VERSION "v2.0.2"

#define COPYR "Copyright (c) 2001-2024, Alexander A. Shabarshin <me@shaos.net>"
#define PARTO "Part of nedoPC SDK, see http://www.nedopc.org/nedopc/SDK"

#define SEPARAT "/"
#define LIB "LIB/"
#define RULES "__RULES"
#define PLATF "_PLATF"

#define B_LEN 16
#define PSEUDOCHAR 240

#ifdef linux
#define stricmp strcasecmp
#define strnicmp strncasecmp
#endif

char Path[256] = "";

typedef struct _Mem
{
  char str[6];
  signed short w3;
  signed short w4;
} Mem;

Text* rules;
Text* libs;
Text* labels;
unsigned char *ro;
signed short w[5];
unsigned char b[5];
Mem m[5];
int size;
int start;
int cur;
int vars;
int base;
int ssize;
int regs;
int reg_x,reg_y,reg_d,reg_n,reg_k,reg_r,reg_t,reg_e;
int reg_m,reg_i,reg_a,reg_b,reg_c,reg_p,reg_l,reg_s;
int reg_h;
int labcur;
char str_org[16];
char str_base[16];
char str_regs[16];
char str_path[100];
char str_sys[100];
char str_plat[100];
char str_name[32];
char comm[4];
char hex1[4];
char hex2[4];
int b_i0[B_LEN];
int b_i1[B_LEN];
int b_i2[B_LEN];
FILE *fa;

char s_proc[16];
char s_file[16];
char s_system[16];
char s_path[100];
char s_spath[100];
char s_platf[100];
char s_rules[100];
char s_org[16];
char s_base[16];
char s_ignore[16];
int platform = 2;

void Option(char *s)
{
  int i;
  char *po = NULL;
  if(*s=='-')
  {
     if(!strnicmp(s,"-P",2)) po=s_proc;
     if(!strnicmp(s,"-S",2)) po=s_system;
     if(!strnicmp(s,"-O",2)) po=s_org;
     if(!strnicmp(s,"-B",2)) po=s_base;
     if(!strnicmp(s,"-I",2)) po=s_ignore; /* since v2.0.2 */
     if(po)
     {
       for(i=2;i<strlen(s);i++) po[i-2]=toupper(s[i]);
       po[i-2] = 0;
     }
  }
  else strcpy(s_file,s);
}

Line* Find(char *s1,char *s2);
Line* FindCode(void);
int Write(Line *l);
int ReadHex(char *s);
void Comment(char *s);
int Command(char *s);

Line* Find1(char *s1)
{
  return Find(s1,NULL);
}

void LoadRules(char *s)
{
  int i = 0;
  char str[256];
  Line *l,*l1;
  FILE *f;
  libs = TextNew();
  labels = TextNew();
  rules = TextNew();
  f = fopen(s,"rt");
  if(f!=NULL)
  {
    fclose(f);
    TextLoad(rules,s);
  }
  else
  {
    sprintf(str,"%s%s",Path,s);
    TextLoad(rules,str);
  }
  for(l=rules->first;l!=NULL;l=l->next)
  {
    loop:
     if((l->str[0]=='/'&&l->str[1]=='/') || l->str[0]==0)
     {  l1 = l->next;
        TextDelete(rules,i);
        l = l1;
        if(l==NULL) break;
        goto loop;
     }
     i++;
  }
  ro = NULL;
  *str_path = 0;
  labcur = 1;
}

void RulesDel(void)
{
  if(fa!=NULL) fclose(fa);
  if(ro!=NULL) free(ro);
  TextDel(libs);
  TextDel(rules);
}

int GetCur(void)
{
  return ro[cur];
}

int Step(void)
{
  cur++;
  if(cur==size) return 0;
  else return 1;
}

void Write1(char *s)
{
  fprintf(fa,"%s\n",s);
}

void Write0(char *s)
{
  fprintf(fa,"%s ",s);
}

void Space(void)
{
  Write1("");
}

void Comment(char *s)
{
  fprintf(fa,"%s %s\n",comm,s);
}

void SetPath(char *s1,char *s2,char *s3)
{
  strcpy(str_path,Path);
  strcpy(str_sys,Path);
  strcpy(str_plat,Path);
  strcat(str_path,s1);
  strcat(str_sys,s2);
  strcat(str_plat,s3);
}

int LoadRobj(char *s)
{
  int i;
  FILE *f;
  char str[100];
  f = fopen(s,"rb");
  if(f==NULL) return 0;
  fseek(f,0,SEEK_END);
  size = ftell(f);
  fseek(f,0,SEEK_SET);
  ro = (unsigned char*)malloc(size);
  if(ro==NULL){fclose(f);return 0;}
  for(i=0;i<size;i++) ro[i]=fgetc(f);
  fclose(f);
  if(ro[0]!=0x02) return 0;
  start = ro[3];
  start += ro[4]*256L;
  cur = start;
  i = 6;
  while(ro[i]){str_name[i-6]=ro[i];i++;}
  str_name[i-6] = 0;
  i++;
  vars = ro[i++];
  vars += ro[i]*256L;
  strcpy(str,s);
  str[strlen(str)-2] = 'A';
  str[strlen(str)-1] = 0;
  fa = fopen(str,"wt");
  if(fa==NULL) return 0;
  return 1;
}

int Compile(void)
{
  int i;
  Line *l;
  char str[100],*po;
  l = Find1("COMMENT");
  if(l==NULL) return -1;
  l = l->next;
  strcpy(comm,l->str);
  l = Find1("HEX");
  if(l==NULL) return -2;
  l = l->next;
  strcpy(str,l->str);
  po = strchr(str,'.');
  if(po==NULL) return -3;
  *po = 0;
  po++;
  strcpy(hex1,str);
  strcpy(hex2,po);
  Comment("Generated by ROBBYCC " VERSION " (" __DATE__ ")");
  Comment(PARTO);
/*  Comment(COPYR); */
  Space();
  l = Find1("BEGIN");
  if(l==NULL) return -10;
  if(Write(l->next)<0) return -11;
  while(1)
  {
    sprintf(str,"_j%4.4X:",cur-start); /* !!! */
    l = FindCode();
    if(l==NULL) return -20;
    Comment(l->str);
#if 0
    Write0(str);
#else
    Write1(str);
#endif
    i = Write(l->next);
/*    printf("<%s>%i\n",l->str,i); */
    if(i<0) return -21;
    if(cur>=size) break;
  }
  return 1;
}

Line* Find(char *s1,char *s2)
{
  Line *l;
  char str[100],*po;
  for(l=rules->first;l!=NULL;l=l->next)
  {
     if(*l->str=='*'&&l->str[1]!=0)
     {
        po = l->str;
        po++;
        strcpy(str,po);
        po = strtok(str," ");
        if(!strcmp(s1,po))
        {
           if(s2==NULL) break;
           po = strtok(NULL," ");
           if(!strcmp(s2,po)) break;
        }
     }
  }
  return l;
}

Line* FindCode(void)
{
  int i,j,k,t;
  Line *l;
  signed short *sh;
  char str[100],*po;
  printf("%4.4X FindCode 0x%2.2X (offset=0x%4.4X)\n",cur-start,ro[cur],cur);
  for(l=rules->first;l!=NULL;l=l->next)
  {
     if(!strncmp(l->str,"*0x",3))
     {
        k = cur;
        po = l->str;
        po++;
        strcpy(str,po);
        po = strtok(str," ");
        while(po!=NULL)
        {
           if(*po=='%')
           {
              if(po[1]=='w')
              {
                 j = po[2]-'0';
                 w[j] = ro[k++];
                 w[j] += ro[k++]*256;
              }
              else if(po[1]=='b')
              {
                 j = po[2]-'0';
                 b[j] = ro[k++];
              }
              else if(po[1]=='m')
              {
                 j = po[2]-'0';
                 t = ro[k++];
                 if(t==0)
                 {
                    sh = (signed short*)&ro[k];
                    k += 2;
                    m[j].w3 = *sh;
                    strcpy(m[j].str,"MEMN");
                 }
                 if(t==1)
                 {
                    if(ro[k+1]==0xFF)
                    {
                       m[j].w3 = ro[k];
                       k += 2;
                       strcpy(m[j].str,"MEMR");
                    }
                    else
                    {
                       sh = (signed short*)&ro[k];
                       k += 2;
                       m[j].w3 = *sh;
                       strcpy(m[j].str,"MEMV");
                    }
                 }
                 if(t==2)
                 {
                    if(ro[k+1]==0xFF) return NULL;
                    else
                    {
                       sh = (signed short*)&ro[k];
                       k += 2;
                       m[j].w3 = *sh;
                       if(ro[k+1]==0xFF)
                       {
                          m[j].w4 = ro[k];
                          strcpy(m[j].str,"MEMAR");
                          k += 2;
                       }
                       else
                       {
                          sh = (signed short*)&ro[k];
                          m[j].w4 = *sh;
                          strcpy(m[j].str,"MEMAV");
                          k += 2;
                       }
                    }
                 }
              }
              else if(po[1]=='a')
              {
                 j = po[2]-'0';
                 t = ro[k++];
                 if(t==0) return NULL;
                 if(t==1)
                 {
                    if(ro[k+1]==0xFF) return NULL;
                    else
                    {
                       sh = (signed short*)&ro[k];
                       k += 2;
                       m[j].w3 = *sh;
                       strcpy(m[j].str,"ADRV");
                    }
                 }
                 if(t==2)
                 {
                    if(ro[k+1]==0xFF) return NULL;
                    else
                    {
                       sh = (signed short*)&ro[k];
                       k += 2;
                       m[j].w3 = *sh;
                       if(ro[k+1]==0xFF)
                       {
                          m[j].w4 = ro[k];
                          strcpy(m[j].str,"ADRAR");
                          k += 2;
                       }
                       else
                       {
                          sh = (signed short*)&ro[k];
                          k += 2;
                          m[j].w4 = *sh;
                          strcpy(m[j].str,"ADRAV");
                       }
                    }
                 }
              }
              else return NULL;
           }
           else
           {
              i = ReadHex(po);
              if(ro[k]!=i) break;
              k++;
           }
           po = strtok(NULL," ");
        }
        if(po==NULL) break;
     }
  }
  cur = k;
  return l;
}

int ReadHex(char *s)
{
  int i,j=0;
  if(s[0]=='0'&&s[1]=='x') j=2;
  i = hex2i(&s[j]);
  return i;
}

int Write(Line *l)
{
  int sko,k=0;
  Line *ll;
  char str[256],*po;
  while(1)
  {
/*    printf("|%s|%i\n",l->str,k); */
    strcpy(str,l->str);
    if(*str=='#')
    {
      if(!Command(&str[1])) return -1;
    }
    else
    {
      if(*str=='*') break;
      po = strchr(str,'%');
      if(po!=NULL)
      {
         if(!strcmp(po,"%org")) strcpy(po,str_org);
         if(!strcmp(po,"%base")) strcpy(po,str_base);
         if(!strcmp(po,"%regs")) strcpy(po,str_regs);
         if(!strcmp(po,"%vars")) sprintf(po,"#%4.4X",vars);
         if(!strcmp(po,"%reg_x")) sprintf(po,"#%4.4X",reg_x);
         if(!strcmp(po,"%reg_y")) sprintf(po,"#%4.4X",reg_y);
         if(!strcmp(po,"%reg_d")) sprintf(po,"#%4.4X",reg_d);
         if(!strcmp(po,"%reg_n")) sprintf(po,"#%4.4X",reg_n);
         if(!strcmp(po,"%reg_k")) sprintf(po,"#%4.4X",reg_k);
         if(!strcmp(po,"%reg_r")) sprintf(po,"#%4.4X",reg_r);
         if(!strcmp(po,"%reg_t")) sprintf(po,"#%4.4X",reg_t);
         if(!strcmp(po,"%reg_e")) sprintf(po,"#%4.4X",reg_e);
         if(!strcmp(po,"%reg_m")) sprintf(po,"#%4.4X",reg_m);
         if(!strcmp(po,"%reg_i")) sprintf(po,"#%4.4X",reg_i);
         if(!strcmp(po,"%reg_a")) sprintf(po,"#%4.4X",reg_a);
         if(!strcmp(po,"%reg_b")) sprintf(po,"#%4.4X",reg_b);
         if(!strcmp(po,"%reg_c")) sprintf(po,"#%4.4X",reg_c);
         if(!strcmp(po,"%reg_p")) sprintf(po,"#%4.4X",reg_p);
         if(!strcmp(po,"%reg_l")) sprintf(po,"#%4.4X",reg_l);
         if(!strcmp(po,"%reg_s")) sprintf(po,"#%4.4X",reg_s);
         if(!strcmp(po,"%reg_h")) sprintf(po,"#%4.4X",reg_h);
         if(!strcmp(po,"%name")) strcpy(po,str_name);
         if(!strcmp(po,"%name\"")) sprintf(po,"%s\"",str_name);
         if(!strcmp(po,"%name',0")) sprintf(po,"%s',0",str_name);
         if(!strcmp(po,"%name\",0")) sprintf(po,"%s\",0",str_name);
         if(!strcmp(po,"%name\",'rw1p'")) sprintf(po,"%s\",'rw1p'",str_name);
         if(strchr(po,')')!=NULL)
            sko = 1;
         else
            sko = 0;
         if(po[1]=='w'&&isdigit(po[2]))
         {
            sprintf(po,"%s%4.4X%s",hex1,(unsigned short)w[po[2]-'0'],hex2);
            if(sko) strcat(po,".w)");
         }
         if(po[1]=='b'&&isdigit(po[2]))
         {
            sprintf(po,"%s%2.2X%s",hex1,b[po[2]-'0'],hex2);
            if(sko) strcat(po,".b)");
         }
         if(po[1]=='l'&&isdigit(po[2]))
         {
            for(ll=labels->first;ll!=NULL;ll=ll->next)
            {
               if(!strcmp(ll->str,po)) break;
            }
            if(ll==NULL) return -1;
            sprintf(po,"_l%4.4u",ll->id);
            if(po==str) strcat(po,":");
         }
         if(po[1]=='j'&&isdigit(po[2]))
         {
            sprintf(po,"_j%4.4X",w[po[2]-'0']);
         }
      }
      Write1(str);
      k++;
    }
    l = l->next;
  }
  TextAllDelete(labels);
  return k;
}

int PrintVar(char *s)
{
   int i,i1,i2,k = 0;
   char str[256],st[100],*po,*poo;
   while(1)
   {
     po = strchr(s,'&');
     if(po==NULL) break;
     *po = 0;
     po++;
     strcpy(str,s);
     for(i=0;i<4;i++)
     {
        if(*po==0) return 0;
        st[i] = *po;
        po++;
     }
     st[i] = 0;
     i1 = (short)hex2i(st);
     if(*po==',')
     {
        po++;
        for(i=0;i<4;i++)
        {
           if(*po==0) return 0;
           st[i] = *po;
           po++;
        }
        st[i] = 0;
        i2 = (short)hex2i(st);
        i = 0;
        if(i1>=-256 && i1<0) b_i0[k]=0;
        else
        {
           b_i0[k] = 2;
           b_i1[k] = i1;
           b_i2[k] = i2;
        }
        sprintf(st,"%c",PSEUDOCHAR+k);
        strcat(str,st);
        strcat(str,po);
     }
     else
     {
        if(*po!=' ') return 0;
        b_i0[k] = 1;
        b_i1[k] = i1;
        sprintf(st,"%c",PSEUDOCHAR+k);
        strcat(str,st);
        strcat(str,po);
     }
     strcpy(s,str);
     k++;
   }
   return k;
}

int Command(char *s)
{
  int i,j,kk,k,nn,tt;
  Line *l;
  signed short *sh;
  char str[100],st2[100],*po;
/*  printf("Command [%s]\n",s); */
  if(!strcmp(s,"def2"))
  {
     for(i=0;i<w[3];i++)
     {
        sh = (signed short*)&ro[cur];
        cur += 2;
        w[1] = *sh;
        l = Find1("DEF2");
        if(l==NULL) return 0;
        Write(l->next);
     }
     return 1;
  }
  if(!strcmp(s,"text") || !strcmp(s,"asm"))
  {
     po = (char*)&ro[cur];
     k = strlen(po)+1;
     /* Inline Assembler */
     if(*s=='t') Comment(po);
     else Write1(po);
     cur += k;
     return 1;
  }
  if(!strcmp(s,"say"))
  {
     po = (char*)&ro[cur];
     k = strlen(po)+1;
     if(*po=='!')
     {  /* Special SAY (previously Inline Assembler) */
        po++;
        Write1(po);
     }
     else
     {
        strcpy(str,po);
        sprintf(st2,"SAY '%s'",str);
        Comment(st2);
        nn = PrintVar(str);
        kk = strlen(str);
        for(i=0;i<kk;i++)
        {
           if((unsigned char)str[i]<PSEUDOCHAR)
           {
              if(str[i]=='$') continue;
              l = Find1("SAY0");
              if(l==NULL) return 0;
              b[1] = str[i];
              Write(l->next);
           }
           else
           {
              j = (unsigned char)str[i]-PSEUDOCHAR;
              if(j>=nn) return 0;
              tt = 0;
              if(b_i0[j]==1)
              {
                 if(b_i1[j]>=-256 && b_i1[j]<0)
                 {
                    w[3] = b_i1[j];
                    l = Find1("MEMR");
                    if(l==NULL) return 0;
                    tt = 1;
                 }
                 else
                 {
                    w[3] = b_i1[j];
                    l = Find1("MEMV");
                    if(l==NULL) return 0;
                    tt = 2;
                 }
              }
              if(b_i0[j]==2)
              {
                 if(b_i1[j]==0xFF) return 0;
                 else
                 {
                    w[3] = b_i1[j];
                    if(b_i2[j]==0xFF)
                    {
                       w[4] = b_i2[j];
                       l = Find1("MEMAR");
                       if(l==NULL) return 0;
                       tt = 3;
                    }
                    else
                    {
                       w[4] = b_i2[j];
                       l = Find1("MEMAV");
                       if(l==NULL) return 0;
                       tt = 4;
                    }
                 }
              }
              if(i>0 && str[i-1]=='$')
              {    if(tt==0) return 0;
                   if(tt==1) l = Find1("ADRV");
                   if(tt==2) l = Find1("ADRAR");
                   if(tt==3) l = Find1("ADRAV");
                   if(l==NULL) return 0;
                   Write(l->next);
                   l = Find1("SAY3"); /* string */
              } else {
                   Write(l->next);
                   if(i>0 && str[i-1]=='#')
                        l = Find1("SAY2"); /* hex value */
                   else l = Find1("SAY1"); /* decimal value */
              }
              if(l==NULL) return 0;
              Write(l->next);
           }
        }
     }
     cur += k;
     return 1;
  }
  if(!strcmp(s,"expr"))
  {
     k = ro[cur++];
     for(i=0;i<k;i++)
     {
        j = ro[cur++];
        sprintf(str,"0x%2.2X",j);
        if(j==0xF5)
        {
           sh = (signed short*)&ro[cur];
           w[1] = *sh;
           cur += 2;
           i += 2;
        }
        l = Find("EXPR",str);
        if(l==NULL) return 0;
        Comment(str);
        if(Write(l->next)<0) return 0;
     }
     return 1;
  }
  if(!strcmp(s,"libs"))
  {
     Space();
     k = 1;
     for(l=libs->first;l!=NULL;l=l->next)
     {
        if(!stricmp(l->str,"SYSTEM_"))
        { sprintf(str,"+%s_",str_sys);
          k = 0;
        }
        else if(!stricmp(l->str,"SYSTEM")) sprintf(str,"+%s",str_sys);
        else if(!stricmp(l->str,"PLATFORM")) sprintf(str,"+%s",str_plat);
        else sprintf(str,"+%s%s",str_path,l->str);
        Write1(str);
     }
     if(k)
     {
        sprintf(str,"+%sNULL_",str_path);
        Write1(str);
     }
     return 1;
  }
  if(!strcmp(s,"inclibs"))
  {
     Space();
     k = 1;
     for(l=libs->first;l!=NULL;l=l->next)
     {
        if(!stricmp(l->str,"SYSTEM_"))
        { sprintf(str,"\tinclude '%s_.A'",str_sys);
          k = 0;
        }
        else if(!stricmp(l->str,"SYSTEM")) sprintf(str,"\tinclude '%s.A'",str_sys);
        else if(!stricmp(l->str,"PLATFORM")) sprintf(str,"\tinclude '%s.A'",str_plat);
        else sprintf(str,"\tinclude '%s%s.A'",str_path,l->str);
        Write1(str);
     }
     if(k)
     {
        sprintf(str,"\tinclude '%sNULL_.A'",str_path);
        Write1(str);
     }
     return 1;
  }
  if(!strncmp(s,"addsub ",7))
  {
     po = &s[7];
     for(l=libs->first;l!=NULL;l=l->next)
     {
        if(!strcmp(l->str,po)) break;
     }
     if(l==NULL && strcmp(po,s_ignore)) TextAdd(libs,po);
     return 1;
  }
  if(!strncmp(s,"addnow ",7))
  {
     po = &s[7];
     if(!strcmp(po,"SYSTEM"))
        sprintf(str,"\tinclude \"%s.A\"",str_sys);
     else
        sprintf(str,"\tinclude \"%s%s.A\"",str_path,po);
     Write1(str);
     return 1;
  }
  if(!strncmp(s,"error ",6))
  {
     po = &s[6];
     printf("\x07 ERROR : %s\n",po);
     return 0;
  }
  if(!strncmp(s,"mem",3))
  {
     j = s[3]-'0';
/*     printf("MEM %s %4.4X %4.4X\n",m[j].str,m[j].w3,m[j].w4); */
     w[3] = m[j].w3;
     w[4] = m[j].w4;
     l = Find1(m[j].str);
     if(l==NULL) return 0;
     if(Write(l->next)<0) return 0;
     return 1;
  }
  if(!strncmp(s,"genlab ",7))
  {
     po = &s[7];
     l = TextAdd(labels,po);
     l->id = labcur++;
     return 1;
  }
  return 0;
}

int Set(char *sorg, char *sbase, int stacksize)
{
  strcpy(str_org,sorg);
  strcpy(str_base,sbase);
  ssize = stacksize;
  if(*str_base=='#'||*str_base=='$')
     base = hex2i(&str_base[1]);
  else
     base = atoi(str_base);
  regs = base+2*vars+ssize;
  reg_x = regs;
  reg_y = regs+2;
  reg_d = regs+2*0x02;
  reg_n = regs+2*0x03;
  reg_k = regs+2*0x04;
  reg_r = regs+2*0x05;
  reg_t = regs+2*0x06;
  reg_e = regs+2*0x07;
  reg_m = regs+2*0x08;
  reg_i = regs+2*0x09;
  reg_a = regs+2*0x0A;
  reg_b = regs+2*0x0B;
  reg_c = regs+2*0x0C;
  reg_p = regs+2*0x0D;
  reg_l = regs+2*0x0E;
  reg_s = regs+2*0x0F;
  reg_h = regs+2*0x1E;
  sprintf(str_regs,"#%4.4X",regs);
  printf("ORG  %s\n",str_org);
  printf("BASE %s [%i]\n",str_base,base);
  printf("VARS %i\n",vars);
  printf("REGS %s [%i]\n",str_regs,regs);
  return 1;
}

void Error(char *s1, char *s2, int i)
{
  char str[100],st2[16];
  strcpy(str,"ERROR : ");
  strcat(str,s1);
  if(s2!=NULL)
  {
     strcat(str," (");
     strcat(str,s2);
     strcat(str,")");
  }
  if(i!=-1)
  {
     sprintf(st2," [%i]",i);
     strcat(str,st2);
  }
  printf("\n%s\n\n",str);
  exit(1);
}

void Error2(char *s1, char *s2)
{
  Error(s1,s2,-1);
}

void Error1(char *s1)
{
  Error(s1,NULL,-1);
}

int main(int argc, char **argv)
{
 FILE *f;
 int i,j,k;
 char str[256],*po;
 printf("\nROBBYCC " VERSION " Robby bytecode to arbitrary code cross-compiler\n");
 printf(COPYR "\n" PARTO "\n\n");
 if(argc<2)
 {
    printf("Usage:\n\n");
    printf("  ROBBYCC [options] FILE.RO\n\n");
    printf("  where options could be:\n");
    printf("      -Pprocessor (-Pi8080,-Pz80,-Pm68k etc.)\n");
    printf("      -Ssystem (-Sorion,-Sspeccy,-Sdos,-Spalm etc.)\n");
    printf("      -Oaddress (ORG address in decimal or #hex)\n");
    printf("      -Baddress (BASE address in decimal or #hex)\n");
    printf("      -Ilibrary (ignore library with specified name)\n");
    printf("\n");
    return 0;
 }
 strcpy(Path,argv[0]);
 po = strrchr(Path,'/');
 if(po==NULL) po = strrchr(Path,'\\');
 if(po!=NULL)
 {
    po++;
    *po = 0;
 }
 else
 {
    *Path = 0;
 }
 *s_proc = 0;
 *s_file = 0;
 *s_system = 0;
 *s_path = 0;
 *s_spath = 0;
 *s_rules = 0;
 *s_org = 0;
 *s_base = 0;
 *s_ignore = 0;
 f = fopen("robbycc.ini","rt");
 if(f!=NULL)
 {
    while(1)
    {
       fgets(str,256,f);
       if(feof(f)) break;
       po = strrchr(str,'\n');
       if(po!=NULL) *po=0;
       Option(str);
    }
    fclose(f);
 }
 for(i=1;i<argc;i++) Option(argv[i]);
 if(*s_proc==0) Error1("Undefined processor");
 if(*s_file==0) Error1("Undefined file");
 strcpy(s_path,LIB);
 strcat(s_path,s_proc);
 strcat(s_path,SEPARAT);
 strcpy(s_spath,s_path);
 sprintf(s_platf,"%s%s%u",s_path,PLATF,platform);
 if(*s_system!=0)
    strcat(s_spath,s_system);
 else
    strcat(s_spath,"NULL"); /* since v2.0.2 */
 strcpy(s_rules,s_path);
 strcat(s_rules,RULES);
 LoadRules(s_rules);
 if(!LoadRobj(s_file)) Error2("Bad loading",s_file);
 SetPath(s_path,s_spath,s_platf);
 Set(s_org,s_base,64);
 k = Compile();
 if(k<0) Error("Compile",s_proc,k);
 else printf("Compile Ok!\n\n");
 return 1;
}
