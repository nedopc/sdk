/* DOS2UNIX.C  Shabarshin A.A.  14-FEB-1998 */

/* THIS CODE IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISK */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef linux
#include <unistd.h>
#endif

#define TEMPFILE "_temp_.tmp"

int main(int argc,char **argv)
{
   FILE *fin,*fout;
   int i,j,k;
   long l,size;
   char str[256],ch;

   if(argc!=2)
   {  printf("\n\n DOS2UNIX   DOS.TXT\n\n");
      exit(1);
   }
   strcpy(str,argv[1]);
   fin = fopen(str,"rb");
   fout = fopen(TEMPFILE,"wb");
   fseek(fin,0L,SEEK_END);
   size = ftell(fin);
   fseek(fin,0L,SEEK_SET);
   for(l=0;l<size;l++)
   {  ch = fgetc(fin);
      if(ch=='\r') continue;
      fputc(ch,fout);
   }
   fclose(fout);
   fclose(fin);
   unlink(str);
   rename(TEMPFILE,str);
   unlink(TEMPFILE);
   return 0;
}

