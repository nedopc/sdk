/*  SPR_COMP.C  - Sprites/Tiles compiler

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (C) 2001-2024, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define VER "v2.1"

/*
 v1.0   28-APR-2001
 v1.1   12-MAY-2001
 v1.2   13-JUN-2001
 v1.3   20-JAN-2002
 v1.3.1 22-JAN-2002
 v1.4   28-JAN-2002
 v1.5   09-OCT-2002
 v2.0   20-MAY-2018
 v2.1   15-JUL-2024

 The program makes .BIN and .R files from .SPR file (SPRite library)
*/

#ifdef linux
#define stricmp strcasecmp
#endif

#define TYPE0 "8x8-2/16"

int sys = 0;
int org = 0;
int spr = 0;
int f_finv = 0;
char code[16];
int codelen = 0;
int step = 9;
int palm = 0;

int compile(char *s,int f)
{
  char *poo,*po = s;
  codelen = 0;
  while(*po==' ') po++;
  if(*po!='D') return 0;
  po++;
  if(*po!='B') return 0;
  po++;
  while(1)
  {
    while(*po==' ') po++;
    poo = strchr(po,',');
    if(poo!=NULL) *poo=0;
    if(*po=='#'||*po=='$')
       code[codelen] = strtol(po+1, NULL, 16);
    else
       code[codelen] = atoi(po);
    if(f&&codelen!=8) code[codelen]=~code[codelen];
    codelen++;
    if(poo==NULL) break;
    poo++;
    po = poo;
  }
  return 1;
}

int zx(int i)
{
  int o = 0;
  switch(i&0x07)
  {
    case 0: o = 0; break;
    case 1: o = 1; break;
    case 2: o = 4; break;
    case 3: o = 5; break;
    case 4: o = 2; break;
    case 5: o = 3; break;
    case 6: o = 6; break;
    case 7: o = 7; break;
  }
  return o;
}

int main(int argc, char **argv)
{
 int i,j,k,c1,c2,fir,finv;
 FILE *f1,*f2,*f3,*f4;
 char str[100],str1[100],str2[100],str3[100],str4[100],st[20],*po,*poo;
 printf("\nSPR_COMP " VER " Alexander A. Shabarshin <me@shaos.net>\n\n");
 if(argc<2)
 {
    printf("SPR_COMP [options] name.spr\n\n");
    printf(" options:\n");
    printf("  -O#hhhh : origin in hex form (-O#A000)\n");
    printf("  -Scolor_system :\n");
    printf("    -Sorion  : 4 bits background + 4 bits foreground (default)\n");
    printf("    -Sspeccy : 1 bit bright + 3 bits background + 3 bits foreground\n");
    printf("    -Sspets  : 3 bits foreground (background is always black)\n");
    printf("    -Sspetsmx: 4 bits foreground + 4 bits background\n");
    printf("    -Sradio  : text symbols\n");
    printf("  -BW :   make black-and-white binary file\n");
    printf("  -PALM : make palm black-and-white resources\n");
    printf("\n");
    printf(" Types of SPR files (first comment in SPR file):\n");
    printf("  " TYPE0 " : 8 bytes + 1 byte color attributes\n");
    printf("\n");
    return 0;
 }
 *str1 = 0;
 for(i=1;i<argc;i++)
 {
   if(argv[i][0]=='-')
   {
     if(!strcmp("PALM",&argv[i][1]))
     {
        palm = 1;
        org = 0x1000;
        step = 1;
        sys = 10;
        f_finv = 1;
     }
     if(!stricmp("BW",&argv[i][1]))
     {
        step = 8;
        sys = 10;
        f_finv = 1;
     }
     if(argv[i][1]=='S'||argv[i][1]=='s')
     {
        po = &argv[i][2];
        if(!stricmp(po,"RADIO"))sys=-1;
        if(!stricmp(po,"ORION"))sys=0;
        if(!stricmp(po,"SPECCY"))sys=1;
        if(!stricmp(po,"SPETS")){sys=2;f_finv=1;}
        if(!stricmp(po,"SPETSMX")){sys=0;f_finv=1;}
     }
     if(argv[i][1]=='O'||argv[i][1]=='o')
     {
        po = &argv[i][2];
        if(*po=='#'||*po=='$')
           org = strtol(po+1, NULL, 16);
        else
           org = atoi(po);
     }
   }
   else strcpy(str1,argv[i]);
 }
 printf("ORG %i [%4.4X]\n",org,org);
 strcpy(str2,str1);
 po = strrchr(str2,'.');
 if(po!=NULL) *po=0;
 strcpy(str3,str2);
 strcpy(str4,str2);
 strcat(str2,".BIN");
 strcat(str3,".R");
 strcat(str4,".RES");
 f1 = fopen(str1,"rt");
 if(f1==NULL)
 {
    return 0;
 }
 if(sys>=0&&!palm)
 {
  f2 = fopen(str2,"wb");
  if(f2==NULL)
  {
    fclose(f1);
    return 0;
  }
 }
 f3 = fopen(str3,"wt");
 if(f3==NULL)
 {
    fclose(f1);
    return 0;
 }
 if(palm)
 {
  f4 = fopen(str4,"wt");
  if(f4==NULL)
  {
    fclose(f1);
    fclose(f3);
    return 0;
  }
 }
 fir = 1;
 while(1)
 {
    fgets(str,100,f1);
    if(feof(f1)) break;
    po = strrchr(str,'\n');
    if(po!=NULL) *po=0;
    po = strrchr(str,'\r');
    if(po!=NULL) *po=0;
    if(*str==';' && fir)
    {
       fir = 0;
       po = &str[1];
       printf("TYPE %s\n",po);
       if(!strcmp(po,TYPE0)) spr=0;
    }
    if(*str==0||*str==';') continue;
    k = 0;
    finv = 0;
    po = strchr(str,';');
    if(po!=NULL)
    {
       if(strchr(str,'!')&&f_finv) finv = 1;
       *po = 0;
       po++;
       while(str[strlen(str)-1]==' ')
             str[strlen(str)-1] = 0;
       k = *po;
       if(!k) k=0x20;
    }
    if(isalpha(*str))
    {
       po = strchr(str,' ');
       *po = 0;
       poo = po+1;
       printf("%s\t",str);
       if(sys<0) fprintf(f3,"@%s=%i\n",str,k);
       else fprintf(f3,"@%s=%i\n",str,org);
       *po = ' ';
       po = strrchr(str,'#');
       po++;
       i = strtol(po, NULL, 16);
       switch(sys)
       {
         case 0:
          if(f_finv)
               finv = 1;
          else finv = 0;
          break;

         case 1:
          c1 = (i&0xF0)>>4;
          c2 = i&0x0F;
          if(c1>7||c2>7)
               j = 1;
          else j = 0;
          c1 = zx(c1);
          c2 = zx(c2);
          i = (j<<6)|(c1<<3)|(c2);
          sprintf(po,"%2.2X",i);
          break;

         case 2:
          c1 = (i&0xF0)>>4;
          c2 = i&0x0F;
          if(finv)
             i=c1&7;
          else
             i=c2&7;
          if(i==0) i=1;
          sprintf(po,"%2.2X",i);
          break;
       }
       compile(poo,finv);
    }
    else compile(str,finv);
    if(sys>=0)
    {
       if(palm&&spr==0)
       {
          sprintf(str,"Tbmp%4.4X.bin",org);
          fprintf(f4,"\tres 'Tbmp',%i,\"%s\"\n",org,str);
          f2 = fopen(str,"wb");
          if(f2!=NULL)
          {
             fputc(0,f2);fputc(8,f2);fputc(0,f2);fputc(8,f2);
             fputc(0,f2);fputc(2,f2);fputc(0,f2);fputc(0,f2);
             fputc(1,f2);fputc(1,f2);fputc(0,f2);fputc(0,f2);
             fputc(0,f2);fputc(0,f2);fputc(0,f2);fputc(0,f2);
             for(i=0;i<8;i++)
             {
               fputc(code[i],f2);
               fputc(0,f2);
             }
             fclose(f2);
          }
       }
       else fwrite(code,1,codelen,f2);
       printf("%4.4X : %i bytes wrote (%i)\n",org,codelen,finv);
       if(spr==0) org+=step;
    }
 }
 fclose(f1);
 if(sys>=0&&!palm) fclose(f2);
 fclose(f3);
 if(palm) fclose(f4);
 printf("OK\n\n");
 return 1;
}
