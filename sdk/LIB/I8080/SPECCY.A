\ NEDOPC SDK : SPECCY FOR I8080
\ http://nedoPC.org/nedopc/SDK
\ This code was written by Shaos
\ and moved to PUBLIC DOMAIN
\ File created: 09-MAY-2001
\ Last updated: 24-JUL-2024

\ System SPECCY (i8080)

\ Partially taken from GRAF-ZX.A (Shaos 15-Oct-2000)

@SCREEN EQU #4000
@ATTRIB EQU #5800
@DX     EQU 32
@DY     EQU 23
@CMODEL EQU 4
@TCK    EQU 3000
\ @TCK UNUSED

SYSTEM  DB      "SPECCY",0

FONT8X8 DB 0 \ PLACEHOLDER

\ INIT SYSTEM
INIT:   LXI_H,  #FFFF
        SHLD    _L_FONT
        XRA_A
        STA     #5C8D
        CALL    8859
        CALL    3435
        MVI_A,  2
        CALL    5633
        MVI_A,  1
        STA     #5C6B
        LXI_H,  23883
        SHLD    #5C53
        RET

\ DELAY
\ HL - TICKS

DELAY:  HLT
        DCX_H
        MOV_A,H
        ORA_L
        JNZ     DELAY
        RET

\ SET_T
\ HL - ADDRES OF T

SET_T:  PUSH_H
        CALL    _DE_L
        INX_D
        POP_H
        CALL    _DE_S
        RET

\ GETPIX
\ B - COORD X
\ C - COORD Y
\ -------------------
\ A - COLOR (0/NOT0)
\ E - MASK
\ D - PIXEL OCTET
\ HL - OCTET ADDRESS

GETPIX: MVI_H,  #40
        MOV_A,C
        RRC
        RRC
        RRC
        ANI     #18
        ADD_H
        MOV_H,A
        MOV_A,C
        ANI     #07
        ADD_H
        MOV_H,A
        MOV_A,C
        RLC
        RLC
        ANI     #E0
        MOV_L,A
        MOV_A,B
        RRC
        RRC
        RRC
        ANI     #1F
        ADD_L
        MOV_L,A
        MOV_A,B
        ANI     #07
_GETP1: ORA_A
        MOV_E,A
        MVI_A,  #80
        JZ      _GETP3
_GETP2: RRC
        DCR_E
        JNZ     _GETP2
_GETP3: MOV_E,A
        MOV_D,M
        ANA_D
        RET

\ SETPIX
\ B - COORD X
\ C - COORD Y
\ E - COLOR (0/NOT0)

SETPIX: PUSH_D
        CALL    GETPIX
        POP_B
        MOV_B,A
        MOV_A,C
        ORA_B
        RZ
        MOV_A,B
        ORA_A
        JZ      _PSET1
        MOV_A,C
        ORA_A
        RNZ
        MOV_A,D
        SUB_E
        MOV_M,A
        RET
_PSET1: MOV_A,E
        ADD_D
        MOV_M,A
        RET

\ SETS8
\ B - COORD X (IN PLACES)
\ C - COORD Y (IN PLACES)
\ HL - SPRITE ADDR

SETS8:  PUSH_D
        PUSH_B
        MOV_A,B
        RLC
        RLC
        RLC
        MOV_B,A
        MOV_A,C
        RLC
        RLC
        RLC
        MOV_C,A
        MVI_E,  8
SETS8A: PUSH_D
        PUSH_H
        CALL    GETPIX
        XTHL
        MOV_A,M
        INX_H
        XTHL
        MOV_M,A
        POP_H
        POP_D
        INR_C
        DCR_E
        JNZ     SETS8A
        POP_B
        PUSH_H
        LXI_H,  @ATTRIB
        MOV_A,C
        RRC
        RRC
        RRC
        MOV_C,A
        ANI     3
        MOV_D,A
        MOV_A,C
        ANI     #E0
        ORA_B
        MOV_E,A
        DAD_D
        XTHL
        \ beg @P2_SETATR
        LDA     _ATRS
        ORA_A
        JNZ     SETS8E
        \ end @P2_SETATR
        MOV_A,M
SETS8E: POP_H
        MOV_M,A
        POP_D
        RET

\ PUTCH
\ A - CHARACTER
\ B - X-PLACE
\ C - Y-PLACE
\ D - INK
\ E - PAPER

PUTCH:  PUSH_H
        PUSH_D
        PUSH_B
        PUSH_PSW
        STA     PCH_C
        MOV_A,B
        STA     PCH_X
        MOV_A,C
        STA     PCH_Y
        MOV_A,D
        STA     PCH_S
        MOV_A,E
        STA     PCH_B
        MVI_A,  2
        CALL    #1601
        \ AT ROW,COL
        MVI_A,  #16
        RST_2
        DB      #3E
PCH_Y   DB      0
        RST_2
        DB      #3E
PCH_X   DB      0
        RST_2
        \ INK COLOR
        MVI_A,  #10
        RST_2
        DB      #3E
PCH_S   DB      4
        RST_2
        \ PAPER COLOR
        MVI_A,  #11
        RST_2
        DB      #3E
PCH_B   DB      0
        RST_2
        DB      #3E
PCH_C   DB      '?'
        RST_2
        POP_PSW
        POP_B
        POP_D
        POP_H
        RET

\ RGB
\ A - RGB COLOR (OR GRB)
\ ----------------------
\ A - GRB COLOR (OR RGB)

RGB:    PUSH_B
        MOV_B,A
        ANI     9
        MOV_C,A
        MOV_A,B
        RLC
        ANI     4
        ORA_C
        MOV_C,A
        MOV_A,B
        RRC
        ANI     2
        ORA_C
        POP_B
        RET

\ ATTRIB
\ A - ATTRIB (PAPER|INK)
\ A - ZX-ATTRIB

ATTRIB: PUSH_B
        MOV_B,A
        ANI     #88
        JZ      ATTRI1
        MVI_A,  #40
ATTRI1: MOV_C,A
        MOV_A,B
        ANI     #07
        CALL    RGB
        ORA_C
        MOV_C,A
        MOV_A,B
        ANI     #70
        RRC
        RRC
        RRC
        RRC
        CALL    RGB
        RLC
        RLC
        RLC
        ORA_C
        POP_B
        RET

\ KEY-ZX.A - ALEXANDER SHABARSHIN  07.11.2000

\ �������� ����������
\ ��:
\   A - #00-�� �������, #FF-�������
\ ���:
\   A - #00-������� �� ������, ����� ���

KEY:    PUSH_B
        PUSH_D
        PUSH_H
        PUSH_PSW
KEY_0:  MVI_A,  #FE
        IN      #FE
        ANI     1
        MOV_B,A
        MVI_A,  #7F
        IN      #FE
        ANI     2
        MOV_C,A
        ORA_B
        JNZ     KEY_1
        \ ������ � CS � SS
        MVI_D,  #FE
        JMP     KEY_E
KEY_1:  MOV_A,B
        ORA_A
        JNZ     KEY_2
        LXI_H,  KTAB_CS
        JMP     KEY_4
KEY_2:  MOV_A,C
        ORA_A
        JNZ     KEY_3
        LXI_H,  KTAB_SS
        JMP     KEY_4
KEY_3:  LXI_H,  KTAB
KEY_4:  LXI_B,  #0005
        MVI_E,  #FF
KEY0:   MVI_A,  #FE
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY1:   MVI_A,  #FD
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY2:   MVI_A,  #FB
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY3:   MVI_A,  #F7
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY4:   MVI_A,  #EF
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY5:   MVI_A,  #DF
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY6:   MVI_A,  #BF
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        DAD_B
KEY7:   MVI_A,  #7F
        MOV_D,A
        IN      #FE
        CALL    KEY_F0
        CMP_E
        JNZ     KEY_10
        \ �� ����� ������� ������
        POP_PSW
        PUSH_PSW
        CPI     #FF
        JZ      KEY_0
        MVI_D,  0
        JMP     KEY_E
        \ ����� ������� �������
KEY_10: MOV_C,A
        MVI_B,  0
        DAD_B
        MOV_D,M
        POP_PSW
        PUSH_PSW
        CPI     #FF
        JNZ     KEY_E
KEY_11: MVI_A,  0
        CALL    KEY
        ORA_A
        JNZ     KEY_11
KEY_E:  POP_PSW
        MOV_A,D
        POP_H
        POP_D
        POP_B
        RET

\$ZX ����� ������ ���� � A
\ ��:  A - ���� � ����������
\      D - ����� ����� ����������
\ ���: A - ����� ������� ����

KEY_F0: PUSH_D
        PUSH_B
        MOV_B,D
        MVI_E,  0
        MOV_D,A
KEY_F1: MOV_A,D
        ANI     1
        JZ      KEY_FF
KEY_F2: MOV_A,D
        RRC
        MOV_D,A
        INR_E
        MVI_A,  5
        CMP_E
        JNZ     KEY_F1
        MVI_E,  #FF
KEY_FF: MOV_A,B
        CPI     #FE
        JNZ     KEY_F3
        MOV_A,E
        CPI     0
        JZ      KEY_F2
KEY_F3: CPI     #7F
        JNZ     KEY_F4
        MOV_A,E
        CPI     1
        JZ      KEY_F2
KEY_F4: MOV_A,E
        POP_B
        POP_D
        RET

\ ������� ��������� ����������

KTAB    DB      0,"ZXCV"
        DB      "ASDFG"
        DB      "QWERT"
        DB      "12345"
        DB      "09876"
        DB      "POIUY"
        DB      #0D,"LKJH"
        DB      #20,0,"MNB"
KTAB_CS DB      0,"zxcv"
        DB      "asdfg"
        DB      "qwert"
        DB      #F1,#F2,#F3,#F4,#FB
        DB      #08,#F0,#FA,#F8,#F9
        DB      "poiuy"
        DB      #0E,"lkjh"
        DB      #FF,#FE,"mnb"
KTAB_SS DB      #FE,#3A,#60,#3F,#2F
        DB      #7E,#7C,#5C,#7B,#7D
        DB      #09,#FC,#FD,#3C,#3E
        DB      #21,#40,#23,#24,#25
        DB      #5F,#29,#28,#27,#26
        DB      #22,#3B,#7F,#5D,#5B
        DB      #0F,#3D,#2B,#2D,#5E
        DB      #F7,#00,#2E,#2C,#2A

\ PIXEL1
\ DE - COORD X
\ HL - COORD Y

PIXEL1: MOV_B,E
        MOV_C,L
        CALL    ZXCOL
        MVI_E,  1
        JMP     SETPIX

\ PIXEL0
\ DE - COORD X
\ HL - COORD Y

PIXEL0: MOV_B,E
        MOV_C,L
        CALL    ZXCOL
        MVI_E,  0
        JMP     SETPIX

\ ZXCOL - SET ATTRIBUTE FROM _COLOR
\ B - COORD X
\ C - COORD Y

ZXCOL:  PUSH_B
        LXI_H,  @ATTRIB
        MOV_A,B
        RRC
        RRC
        RRC
        ANI     #1F
        MOV_B,A
        MOV_A,C
        RRC
        RRC
        RRC
        ANI     #1F
        RRC
        RRC
        RRC
        MOV_C,A
        ANI     #03
        MOV_D,A
        MOV_A,C
        ANI     #E0
        ORA_B
        MOV_E,A
        DAD_D
        LDA     _COLOR
        MOV_M,A
        POP_B
        RET
