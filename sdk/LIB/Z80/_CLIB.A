; NEDOPC SDK : _CLIB FOR Z80
; http://nedoPC.org/nedopc/SDK
; File created: 02-MAR-2002
; Last updated: 12-JUL-2024

; hl=de%hl
ccmod:  ld      a,h
        or      l
        ret     z
        push    bc
        push    de
        push    hl
        call    div
        pop     de
        call    ccmult
        pop     de
        call    ccsub
        pop     bc
        ret

; hl=de&&hl
ccland: ld      a,d
        or      e
        jp      z,ccla0
        ld      a,h
        or      l
        jp      z,ccla0
ccla1:  ld      hl,1
        ret
ccla0:  ld      hl,0
        ret

; hl=de||hl
cclor:  ld      a,d
        or      e
        jp      nz,ccla1
        ld      a,h
        or      l
        jp      nz,ccla1
cclo0:  ld      hl,0
        ret
cclo1:  ld      hl,1
        ret

;shift de logicaly right by hl and return in hl
cclsr:  ex      de,hl
cclsr1: dec     e
        ret     m
        scf
        ccf
        ld      a,h
        rra
        ld      h,a
        ld      a,l
        rra
        ld      l,a
        jp      cclsr1

; Code below was borrowed from SMALL C v2.1 by Jim Hedrix
; http://www.cpm.z80.de/small_c/smallc21.zip
; CLIB.ASM - Small-C Library: Copyright 1984 J.E.Hendrix, L.E.Payne
; and translated to Z80 mnemonics by Alexander "Shaos" Shabarshin

;inclusive "or" hl and de into hl
ccor:   ld      a,l
        or      e
        ld      l,a
        ld      a,h
        or      d
        ld      h,a
        ret

;exclusive "or" hl and de into hl
ccxor:  ld      a,l
        xor     e
        ld      l,a
        ld      a,h
        xor     d
        ld      h,a
        ret

;"and" hl and de into hl
ccand:  ld      a,l
        and     e
        ld      l,a
        ld      a,h
        and     d
        ld      h,a
        ret

;in all the following compare routines, hl is set to 1 if the
;  condition is true, otherwise it is set to 0 (zero).
;
;test if hl = de
cceq:   call    cccmp
        ret     z
        dec     hl
        ret

;test if de != hl
ccne:   call    cccmp
        ret     nz
        dec     hl
        ret

;test if de > hl (signed)
ccgt:   ex      de,hl
        call    cccmp
        ret     c
        dec     hl
        ret

;test if de <= hl (signed)
ccle:   call    cccmp
        ret     z
        ret     c
        dec     hl
        ret

;test if de >= hl (signed)
ccge:   call    cccmp
        ret     nc
        dec     hl
        ret

;test if de < hl (signed)
cclt:   call    cccmp
        ret     c
        dec     hl
        ret

; common routine to perform a signed compare of de and hl
; this routine performs de - hl and sets the conditions:
; carry reflects sign of difference (set means de < hl)
; zero/non-zero set according to equality.
cccmp:  ld      a,h
        ;invert sign of hl
        xor     #80
        ld      h,a
        ld      a,d
        ;invert sign of de
        xor     #80
        cp      h
        ;compare msbs
        jp      nz,cccmp1
        ;done if neq
        ld      a,e
        ;compare lsbs
        cp      l
cccmp1: ld      hl,1
        ;preset true cond
        ret

;test if de >= hl (unsigned)
ccuge:  call    ccucmp
        ret     nc
        dec     hl
        ret

;test if de < hl (unsigned)
ccult:  call    ccucmp
        ret     c
        dec     hl
        ret

;test if de > hl (unsigned)
ccugt:  ex      de,hl
        call    ccucmp
        ret     c
        dec     hl
        ret

;test if de <= hl (unsigned)
ccule:  call    ccucmp
        ret     z
        ret     c
        dec     hl
        ret

; common routine to perform unsigned compare
; carry set if de < hl
; zero/nonzero set accordingly
ccucmp: ld      a,d
        cp      h
        jp      nz,ucmp1
        ld      a,e
        cp      l
ucmp1:  ld      hl,1
        ret

;shift de arithmetically right by hl and return in hl
ccasr:  ex      de,hl
ccasr1: dec     e
        ret     m
        ld      a,h
        rla
        ld      a,h
        rra
        ld      h,a
        ld      a,l
        rra
        ld      l,a
        jp      ccasr1

;shift de arithmetically left by hl and return in hl
ccasl:  ex      de,hl
ccasl1: dec     e
        ret     m
        add     hl,hl
        jp      ccasl1

;subtract hl from de and return in hl
; hl=de-hl
ccsub:  ld      a,e
        sub     l
        ld      l,a
        ld      a,d
        sbc     a,h
        ld      h,a
        ret

;form the two's complement of hl
ccneg:  call    cccom
        inc     hl
        ret

;form the one's complement of hl
cccom:  ld      a,h
        cpl
        ld      h,a
        ld      a,l
        cpl
        ld      l,a
        ret

;multiply de by hl and return in hl (signed multiply)
; hl=de*hl
ccmult:
mult:   ld      b,h
        ld      c,l
        ld      hl,0
mult1:  ld      a,c
        rrca
        jp      nc,mult2
        add     hl,de
mult2:  xor     a
        ld      a,b
        rra
        ld      b,a
        ld      a,c
        rra
        ld      c,a
        or      b
        ret     z
        xor     a
        ld      a,e
        rla
        ld      e,a
        ld      a,d
        rla
        ld      d,a
        or      e
        ret     z
        jp      mult1

;divide de by hl and return quotient in hl, remainder in de (signed divide)
; hl=de/hl
ccdiv:
div:    ld      b,h
        ld      c,l
        ld      a,d
        xor     b
        push    af
        ld      a,d
        or      a
        call    m,ccdeneg
        ld      a,b
        or      a
        call    m,ccbcneg
        ld      a,16
        push    af
        ex      de,hl
        ld      de,0
ccdiv1: add     hl,hl
        call    ccrdel
        jp      z,ccdiv2
        call    cccmpbcde
        jp      m,ccdiv2
        ld      a,l
        or      1
        ld      l,a
        ld      a,e
        sub     c
        ld      e,a
        ld      a,d
        sbc     a,b
        ld      d,a
ccdiv2: pop     af
        dec     a
        jp      z,ccdiv3
        push    af
        jp      ccdiv1
ccdiv3: pop     af
        ret     p
        call    ccdeneg
        ex      de,hl
        call    ccdeneg
        ex      de,hl
        ret

;negate the integer in de (internal routine)
; de=-de
ccdeneg: ld     a,d
        cpl
        ld      d,a
        ld      a,e
        cpl
        ld      e,a
        inc     de
        ret

;negate the integer in bc (internal routine)
; bc=-bc
ccbcneg: ld     a,b
        cpl
        ld      b,a
        ld      a,c
        cpl
        ld      c,a
        inc     bc
        ret

;rotate de left one bit (internal routine)
ccrdel: ld      a,e
        rla
        ld      e,a
        ld      a,d
        rla
        ld      d,a
        or      e
        ret

;compare bc to de (internal routine)
cccmpbcde: ld   a,e
        sub     c
        ld      a,d
        sbc     a,b
        ret

;logical negation
cclneg: ld      a,h
        or      l
        jp      nz,cclneg1
        ld      l,1
        ret
cclneg1: ld     hl,0
        ret

