/* BIN2TAP.C - Alexander Shabarshin  24-OCT-2000 */
/* 17-JAN-2023 - added ability to set optional BASIC loader */
/* 14-JUL-2024 - added ability to give decimal or hexadecimal address */

/* THIS CODE IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISK */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma pack(1)

typedef struct _header
{
  unsigned char type; /* 3 - for code */
  char name[10]; /* spaces for end */
  unsigned short length;
  unsigned short start;
  unsigned short word; /* 0x8000 */
} header;

#pragma pack()

/* Convert string with hexadecimal number to integer */
int hex2i(char *s)
{
  char *ptr;
  long l = strtol(s,&ptr,16);
  if(*ptr) return -1;
  return (int)l;
}

int main(int argc,char **argv)
{
  int i,j,k;
  unsigned short start=0xE800;
  unsigned short size;
  unsigned char *ptr;
  header head;
  static char bin[100];
  static char tap[100];
  static char ldr[100];
  char *po;
  FILE *fbin;
  FILE *ftap;
  FILE *fldr;
  if(argc<2)
  {
     printf("\n\nBIN2TAP FILE.BIN [#E800] [LOADER.TAP]\n\n");
     return 0;
  }
  if(argc>2)
  {
     po = argv[2];
     if(*po=='#')
          start = hex2i(&po[1]);
     else start = atoi(po);
  }
  printf("START: 0x%4.4X (%i)\n",start,start);
  if(argc>3) strcpy(ldr,argv[3]);
  else *ldr = 0;
  strcpy(bin,argv[1]);
  strcpy(tap,bin);
  po = strchr(tap,'.');
  if(po!=NULL) *po=0;
  head.type = 3;
  strcpy(head.name,tap);
  for(i=strlen(tap);i<10;i++) head.name[i]=0x20;
  strcat(tap,".tap");
  head.start = start;
  head.word = 0x8000;
  fbin = fopen(bin,"rb");
  if(fbin==NULL)
  {
     printf("ERROR: Can't open file %s\n",bin);
     return -2;
  }
  ftap = fopen(tap,"wb");
  if(ftap==NULL)
  {
     printf("ERROR: Can't open file %s\n",tap);
     return -3;
  }
  fseek(fbin,0L,SEEK_END);
  head.length = ftell(fbin);
  fseek(fbin,0L,SEEK_SET);
  if(*ldr)
  {
     printf("LOADER %s\n",ldr);
     fldr = fopen(ldr,"rb");
     if(fldr)
     {
        fseek(fldr,0L,SEEK_END);
        k = ftell(fldr);
        fseek(fldr,0L,SEEK_SET);
        for(i=0;i<k;i++) fputc(fgetc(fldr),ftap);
        fclose(fldr);
     }
     else printf("ERROR\n");
  }
  size = sizeof(head)+2;
  fputc(size%256,ftap);
  fputc(size/256,ftap);
  fputc(0,ftap);
  ptr = (unsigned char*)&head;
  k = 0;
  for(i=0;i<size-2;i++)
  {
     j = ptr[i];
     fputc(j,ftap);
     k ^= j;
  }
  fputc(k,ftap);
  size = head.length+2;
  fputc(size%256,ftap);
  fputc(size/256,ftap);
  fputc(0xFF,ftap);
  k = 0;
  for(i=0;i<size-2;i++)
  {
     j = fgetc(fbin);
     fputc(j,ftap);
     k ^= j;
  }
  k=(~k)&0xFF;
  fputc(k,ftap);
  fclose(fbin);
  fclose(ftap);
  return 0;
}
