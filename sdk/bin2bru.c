/* BIN2BRU.C - Alexander Shabarshin  17-NOV-2000, 13-JUL-2024 */

/* THIS CODE IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISK */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Convert string with hexadecimal number to integer */
int hex2i(char *s)
{
  char *ptr;
  long l = strtol(s,&ptr,16);
  if(*ptr) return -1;
  return (int)l;
}

int main(int argc, char **argv)
{
 int i,j,k,adr,len,len0;
 char str[100],*po;
 FILE *bin,*bru;
 if(argc<3)
 {
    printf("\n\nBIN2BRU file.bin [offset]\n\n");
    return 0;
 }
 strcpy(str,argv[1]);
 po = strrchr(str,'.');
 if(po!=NULL) *po=0;
 i = strlen(str);
 if(i>7)
 {
    printf("Filename '%s' is too long\n",str);
    return -1;
 }
 bin = fopen(argv[1],"rb");
 if(bin==NULL)
 {
    printf("Can't open file '%s' for reading\n",argv[1]);
    return -2;
 }
 if(argc<2) adr = 0;
 else
 {
   po = argv[2];
   if(*po=='#') adr = hex2i(&po[1]);
   else adr = atoi(po);
 }
 fseek(bin,0L,SEEK_END);
 len = ftell(bin) - adr;
 len0 = len;
 if(len0%16!=0) len0=(len0/16+1)*16;
 fseek(bin,adr,SEEK_SET);
 strcat(str,"$.bru");
 printf("Creating BRU file '%s' %4.4X %4.4X\n",str,adr,len0);
 bru = fopen(str,"wb");
 if(bru==NULL)
 {
    printf("Can't open file '%s' for writing\n",str);
    fclose(bin);
    return -3;
 }
 k = 1;
 for(i=0;i<8;i++)
 {
    if(k)
    {  j = toupper(str[i]);
       if(j=='.')
       {  j = ' ';
          k = 0;
       }
       fputc(j,bru);
    }
    else fputc(' ',bru);
 }
 fwrite(&adr,1,2,bru);
 fwrite(&len0,1,2,bru);
 k = 0xFFFFFF00;
 fwrite(&k,1,4,bru);
 for(i=0;i<len;i++) fputc(fgetc(bin),bru);
 if(len%16!=0) for(i=len%16;i<16;i++) fputc(0,bru);
 fclose(bin);
 fclose(bru);
 return 0;
}
