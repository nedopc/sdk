ORG ????

 code block

BASE ????

 data block

REG = BASE + NVAR + 64

 stack goes from REG to beginning (so it's 64 bytes)
 registers go from REG to end (48 bytes or 16+8 words)

 REG+0  X - coordinate X of the object relatively to robot
 REG+2  Y - coordinate Y of the object relatively to robot 
 REG+4  D - distance
 REG+6  N - object type
 REG+8  K - auxilary register
 REG+10 R - random variable 0..999,
 REG+12 T - clock counter
 REG+14 E - energy level of the robot
 REG+16 M - number of missle of the robot
 REG+18 I - numberic identifier of the robot (starts with 1)
 REG+20 A - generic register A (read/write)
 REG+22 B - generic register B (read/write)
 REG+24 C - generic register C (read/write)
 REG+26 P - current program pointer
 REG+28 L - last result of expression
 REG+30 S - return stack depth (after start it's 0).
 REG+32

SPR ????

 sprites (actually it's "tiles")
