/* READ_VAR.CPP - Alexander Shabarshin  11-MAY-2001 */

/* THIS CODE IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISK */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_text.h"

#define NAMES ">>> NAMES <<<"

int main(int argc,char **argv)
{
  int i,j,k,base;
  unsigned u;
  FILE *f;
  Line *l;
  Text *names;
  static char str[256],*po,nam[100],tip[100];
  if(argc<4)
  {
     printf("\nREAD_VAR FILE.MEM FILE.RLS offset\n\n");
     return 0;
  }
  po = argv[3];
  if(*po=='#') base = hex2i(&po[1]);
  else base = atoi(po);
  names = TextNew();
  k = 0;
  f = fopen(argv[2],"rt");
  if(f==NULL) return -1;
  while(1)
  {
     fgets(str,256,f);
     if(feof(f)){fclose(f);return -2;}
     po = strrchr(str,'\n');
     if(po!=NULL) *po=0;
     if(k&&*str)
     {
        po = strtok(str," \t");
        if(po==NULL){fclose(f);return -3;}
        strcpy(nam,po);
        po = strtok(NULL," \t");
        if(po==NULL){fclose(f);return -4;}
        strcpy(tip,po);
        po = strtok(NULL," \t");
        if(po==NULL){fclose(f);return -5;}
        u = (unsigned)hex2i(po);
        if(*tip=='v' && u<0xFF00)
        {
           l = TextAdd(names,nam);
           l->type = 1;
           l->adr = base+2*u;
        }
        if(*tip=='a')
        {
           po = strchr(tip,']');
           if(po==NULL){fclose(f);return -6;}
           *po = 0;
           po = strchr(tip,'[');
           if(po==NULL){fclose(f);return -7;}
           po++;
           l = TextAdd(names,nam);
           l->type = 2;
           l->adr = base+2*u;
           l->len = atoi(po);
        }

        if(*str=='>') break;
     }
     if(!strcmp(str,NAMES)) k=1;
  }
  fclose(f);

//  names.List();

  f = fopen(argv[1],"rb");
  for(l=names->first;l!=NULL;l=l->next)
  {
     fseek(f,l->adr,SEEK_SET);
     if(l->type==1)
     {
        k = fgetc(f);
        k |= fgetc(f)<<8;
        printf("%s\t%4.4X\t%i\n",l->str,k,k);
     }
     if(l->type==2)
     {
        printf("%s",l->str);
        for(i=0;i<l->len;i++)
        {
           k = fgetc(f);
           k |= fgetc(f)<<8;
           printf(" %4.4X",k);
        }
        printf("\n");
     }
  }
  fclose(f);

  TextDel(names);
  return 0;
}
