/*  pseudo-86rk.cpp - text emulator of Soviet computer Radio-86RK

    Part of nedoPC SDK (software development kit for DIY and RETRO computers)

    Copyright (c) 1996,1997,2024 Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//#define DEBUG
//#define UNICODE
//#define STDOUT

#include "proc8080.h"
#ifndef STDOUT
#ifdef __BORLANDC__
#ifdef UNICODE
#undef UNICODE
#endif
#include <conio.h>
#else
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#define MYCONIO
#endif
#endif
#include "rk86_rom.hpp"
#ifdef UNICODE
#include "pseudo-uni.hpp"
#endif

#define REPEATCOUNT 1100
#define NOKEYCOUNT  500

#define MEMSZ  32768
#define HACKSZ 14336

const char version[] = "v0.4";

class Pseudo86RK : public Processor8080
{
 unsigned char mem[MEMSZ],hack[HACKSZ],keyb;
 int lastx,lasty,lastc,lastc2,adr,foolstop,triggered;
 int keyshift,lastkeycount,nokeycount,filenum,inscount,fstdout,fwaitkey;
 unsigned long time_a,time_b,time_c,time_d,time_e;
public:
 int lastkey;
 Pseudo86RK(); // Constructor
 virtual SH_BYTE get(SH_WORD a); // Read from memory
 virtual void put(SH_WORD a,SH_BYTE b); // Write to memory
 virtual int aux(SH_BYTE com, SH_BIT ok); // Auxilary activity
#ifndef __BORLANDC__
 virtual SH_BYTE inp(SH_BYTE p) { return get((p<<8)|p); }; // Read from port
 virtual void outp(SH_BYTE p,SH_BYTE b) { put((p<<8)|p,b); }; // Write to port
#endif
 void stdout_event(char a);
 void print(char c, int x, int y);
 void trigger(int a) { adr = a; };
 int time2stop(void) { return foolstop; };
 void die(void) { foolstop++; };
 int keyboard(void);
 unsigned long getTimeA(void) { return time_a; };
 unsigned long getTimeB(void) { return time_b; };
 unsigned long getTimeC(void) { return time_c; };
 unsigned long getTimeD(void) { return time_d; };
 unsigned long getTimeE(void) { return time_e; };
 void enableCounters(void) { inscount++; };
 int isCountersEnabled(void) { return inscount?1:0; };
 int kbdTimeout(void) { return lastkey?((lastkeycount>REPEATCOUNT)?1:0):((nokeycount>NOKEYCOUNT)?1:0); };
 void setStdoutOnly(void) { fstdout++; };
 int isStdoutOnly(void) { return fstdout; };
 void waitForKeys(void) { fwaitkey++; };
};

#ifdef MYCONIO

inline int kbhit(int flag=0)
{
  struct termios oldt, newt;
  int ch,oldf;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  if(!flag)
  {
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
  }
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  if(!flag)
  {
    fcntl(STDIN_FILENO, F_SETFL, oldf);
    if(ch != EOF)
    {
      ungetc(ch, stdin);
      return 1;
    }
    return 0;
  }
  return ch;
}

inline int getch(void) { return kbhit(1); }

inline void clrscr(void) { system("clear"); }

inline void gotoxy(int x,int y) { printf("\x1B[%d;%df",y,x); }

#endif

Pseudo86RK::Pseudo86RK()
{
  PC = 0xF800; // Monitor
  adr = lastx = lasty = -1;
  triggered = foolstop = lastc = lastc2 = 0;
  lastkey = lastkeycount = nokeycount = filenum = inscount = 0;
  keyshift = 1;
  keyb = 0;
  time_a = time_b = time_c = time_d = time_e = 0;
  fstdout = fwaitkey = 0;
}

SH_BYTE Pseudo86RK::get(SH_WORD a)
{
 SH_BYTE b = 0xFF;
 if(a < MEMSZ) b = mem[a];
 else
 {
  switch(a>>12)
  {
   case 0x8:
   case 0x9:
     // Keyboard 8255
     if(a==0x8001)
     {
        int k = keyboard();
        b = k&0xFF;
        keyshift = (k&0x100)?1:0;
     }
     else if(a==0x8002)
     {
        b = (keyshift<<5)|0xC0;
     }
//     printf("get %4.4X -> %2.2X\n",a,b);
     break;
   case 0xA:
   case 0xB:
     // External 8255 (romdisk)
     break;
   case 0xC:
   case 0xD:
     // CRT controller 8275
     if(a<0xC010)
     {
     }
     else b = hack[a-0xC000];
     break;
   case 0xE:
     // DMA controller 8257 is not readable
     // Read extended ROM instead
     b = hack[a-0xC000];
     break;
   case 0xF:
     // ROM 2K+2K
     if(a>=0xF800) // regular ROM
       b = rk86_rom_dump[a-0xF800];
     else // 0xF000...0xF7FF (extended ROM)
       b = hack[a-0xC000];
     break;
  }
 }
#ifdef DEBUG
 printf(">>> get(%4.4X) -> %2.2X\n",a,b);
#endif
 return b;
}

void Pseudo86RK::put(SH_WORD a,SH_BYTE b)
{
#ifdef DEBUG
 printf(">>> put(%4.4X,%2.2X)\n",a,b);
#endif
 if(a < MEMSZ)
 {
  mem[a] = b;
  unsigned int scraddr = 0x76D0;
#ifdef __BORLANDC__
  scraddr += 78*3;
#endif
  if(a>=scraddr && a<0x8000 && b<128) // Screen
  {
     a -= scraddr;
     // in case of stdout it should not be printed if 0
     int fnull = 0;
     if(!b)
     {
        b = ' ';
        fnull = 1;
     }
     if((fstdout && !fnull) || !fstdout) print(b,a%78,a/78);
  }
 }
 else
 {
  switch(a>>12)
  {
   case 0x8:
   case 0x9:
     // Keyboard 8255
     if(a==0x8000) keyb=b;
//     printf("put %4.4X %2.2X\n",a,b);
     break;
   case 0xA:
   case 0xB:
     // External 8255 (romdisk)
     break;
   case 0xC:
   case 0xD:
     // CRT controller 8275
     if(a<0xC010)
     {
     }
     else hack[a-0xC000] = b;
     break;
   case 0xE:
   case 0xF:
     // DMA controller 8257
     if((a-0xE000)<0xE010)
     {
     }
     else if(a<0xF800) hack[a-0xC000] = b;
     break;
  }
 }
}

int Pseudo86RK::aux(SH_BYTE com, SH_BIT ok)
{
#ifdef DEBUG
 printf(">>> aux(%2.2X,%c)\n",com,ok?'T':'F');
#endif
 (void)ok;
 FILE *f;
 int i,j;
 char str[16];
 switch(com)
 {
   case 0x7F: // MOV A,A
    if(!inscount) break;
    stdout_event('a');
    time_a = timel;
    break;

   case 0x40: // MOV B,B
    if(!inscount) break;
    stdout_event('b');
    time_b = timel;
    break;

   case 0x49: // MOV C,C
    if(!inscount) break;
    stdout_event('c');
    time_c = timel;
    break;

   case 0x52: // MOV D,D
    if(!inscount) break;
    stdout_event('d');
    time_d = timel;
    break;

   case 0x5B: // MOV E,E
    if(!inscount) break;
    stdout_event('e');
    time_e = timel;
    break;

   case 0x64: // MOV H,H
    if(!inscount) break;
    // Save textual representation of BC bytes starting from HL to
    // text file with name stored from DE (zero-terminated)
    for(i=0;i<15;i++)
    {
       str[i] = get(getDE()+i);
       if(!str[i]) break;
    }
    if(i==15) str[i] = 0;
    f = fopen(str,"wt");
    if(f!=NULL)
    {
       j = getHL();
       for(i=0;i<getBC();i++)
       {
          fprintf(f,"%i",get(j+i));
          if(i<getBC()-1) fputc(',',f);
       }
       fclose(f);
    }
    break;

   case 0x6D: // MOV L,L
    if(!inscount) break;
    // Save lower 32K to file with incremental name 1.BIN, 2.BIN etc.
    sprintf(str,"%i.BIN",++filenum);
    f = fopen(str,"wb");
    if(f!=NULL)
    {
       for(SH_WORD w=0;w<=32767;w++) fputc(get(w),f);
       fclose(f);
    }
    break;

   case 0x76: // HLT
    if(fstdout && inscount)
    {
      printf("\n{halt}\n");
      if(time_a) printf("{from-last-a:%lu}\n",timel-time_a);
      if(time_b) printf("{from-last-b:%lu}\n",timel-time_b);
      if(time_c) printf("{from-last-c:%lu}\n",timel-time_c);
      if(time_d) printf("{from-last-d:%lu}\n",timel-time_d);
      if(time_e) printf("{from-last-e:%lu}\n",timel-time_e);
    }
    foolstop++;
    break;
 }
 return 1;
}

void Pseudo86RK::stdout_event(char e)
{
 if(!fstdout) return;
 if(!inscount) return;
 printf("{%c-event:",e);
 if(time_a) printf("%lu",timel-time_a);
 else if(e=='a') printf("0");
 printf(":");
 if(time_b) printf("%lu",timel-time_b);
 else if(e=='b') printf("0");
 printf(":");
 if(time_c) printf("%lu",timel-time_c);
 else if(e=='c') printf("0");
 printf(":");
 if(time_d) printf("%lu",timel-time_d);
 else if(e=='d') printf("0");
 printf(":");
 if(time_e) printf("%lu",timel-time_e);
 else if(e=='e') printf("0");
 printf("}\n");
}

void Pseudo86RK::print(char c, int x, int y)
{
#ifdef DEBUG
 printf(">>> print(%2.2X,%i,%i)\n",c,x,y);
#endif
 if(fstdout)
 {
   if(y!=lasty || x!=lastx+1) printf("\n");
   if(c>=0x20) printf("%c",c);
   lastx = x;
   lasty = y;
 }
 else
 {
#ifndef STDOUT
   gotoxy(x+1,y+1);
#endif
#ifdef __BORLANDC__
   switch(c)
   {
     case   0: c=32;  break;
     case   1: c=188; break;
     case   2: c=200; break;
     case   3: c=223; break;
     case   4: c=201; break;
     case   5: c=206; break;
     case   6: c=222; break;
     case   7: c=177; break;
     case   9: c=5;   break;
     case  11: c=30;  break;
     case  14: c=16;  break;
     case  15: c=31;  break;
     case  16: c=187; break;
     case  17: c=221; break;
     case  18: c=206; break;
     case  19: c=177; break;
     case  20: c=220; break;
     case  21: c=177; break;
     case  22: c=177; break;
     case  23: c=219; break;
     case  27: c=18;  break;
     case  28: c=22;  break;
     case  29: c=17;  break;
     case  30: c=11;  break;
     case  96: c=158; break;
     case  97: c=128; break;
     case  98: c=129; break;
     case  99: c=150; break;
     case 100: c=132; break;
     case 101: c=133; break;
     case 102: c=148; break;
     case 103: c=131; break;
     case 104: c=149; break;
     case 105: c=136; break;
     case 106: c=137; break;
     case 107: c=138; break;
     case 108: c=139; break;
     case 109: c=140; break;
     case 110: c=141; break;
     case 111: c=142; break;
     case 112: c=143; break;
     case 113: c=159; break;
     case 114: c=144; break;
     case 115: c=145; break;
     case 116: c=146; break;
     case 117: c=147; break;
     case 118: c=134; break;
     case 119: c=130; break;
     case 120: c=156; break;
     case 121: c=155; break;
     case 122: c=135; break;
     case 123: c=152; break;
     case 124: c=157; break;
     case 125: c=153; break;
     case 126: c=151; break;
     case 127: c=177; break;
   }
   cprintf("%c",c);
#else
#ifndef UNICODE
   printf("%c",c);
#else
   printf("%s",conversion[(int)c]);
#endif
#endif
 }
 // check for both stdout and terminal modes
 if(lastc2=='-' && lastc=='-' && c=='>')
 {
#ifdef DEBUG
   printf(">>> trigger %4.4X (%i)\n",adr&0xFFFF,triggered);
#endif
   if(adr >= 0)
   {
     // hack
     hack[0x10] = 0xCD; // CALL
     hack[0x11] = adr&0xFF;
     hack[0x12] = adr>>8;
     hack[0x13] = 0x76; // HLT
     PC = 0xC010; // call hack
   }
   if(fstdout)
   {
     // in case of stdout mode quit if second trigger or no binary was loaded with disabled waiting for keys
     if(++triggered>1 || (adr<0 && !fwaitkey)) foolstop++;
     printf("\n");
   }
 }
 lastc2 = lastc;
 lastc = c;
}

int Pseudo86RK::keyboard(void)
{
 int b = 0x1FF; // 8 columns + 1 shift
 if(lastkey)
 {
   lastkeycount++;
   nokeycount = 0;
/*
   printf("lastkey %i #%2.2X %c (count=%i)\n",
           lastkey,lastkey,(lastkey>32&&lastkey<127)?lastkey:' ',lastkeycount);
*/
   if(!(keyb&2))
   {
      if(lastkey==9) b&=0x1FE;
#ifdef linux
      else if(lastkey==10) b&=0x1FB;
#else
      else if(lastkey==10) b&=0x1FD;
      else if(lastkey==13) b&=0x1FB;
#endif
      else if(lastkey==8||lastkey==127) b&=0x1F7;
/*
      else if(lastkey==) b&=0x1EF; // Left
      else if(lastkey==) b&=0x1DF; // Up
      else if(lastkey==) b&=0x1BF; // Right
      else if(lastkey==) b&=0x17F; // Down
*/
   }
   if(!(keyb&4))
   {
      switch(lastkey)
      {
         case '0': b&=0x1FE; break;
         case '1': b&=0x1FD; break;
         case '2': b&=0x1FB; break;
         case '3': b&=0x1F7; break;
         case '4': b&=0x1EF; break;
         case '5': b&=0x1DF; break;
         case '6': b&=0x1BF; break;
         case '7': b&=0x17F; break;
         case '!': b&=0x0FD; break;
         case '"': b&=0x0FB; break;
         case '#': b&=0x0F7; break;
         case '$': b&=0x0EF; break;
         case '%': b&=0x0DF; break;
         case '&': b&=0x0BF; break;
         case '\'': b&=0x07F; break;
      }
   }
   if(!(keyb&8))
   {
      switch(lastkey)
      {
         case '8': b&=0x1FE; break;
         case '9': b&=0x1FD; break;
         case ':': b&=0x1FB; break;
         case ';': b&=0x1F7; break;
         case ',': b&=0x1EF; break;
         case '-': b&=0x1DF; break;
         case '.': b&=0x1BF; break;
         case '/': b&=0x17F; break;
         case '(': b&=0x0FE; break;
         case ')': b&=0x0FD; break;
         case '*': b&=0x0FB; break;
         case '+': b&=0x0F7; break;
         case '<': b&=0x0EF; break;
         case '=': b&=0x0DF; break;
         case '>': b&=0x0BF; break;
         case '?': b&=0x07F; break;
      }
   }
   if(!(keyb&16))
   {
      switch(lastkey)
      {
         case '@': b&=0x1FE; break;
         case 'A': case 'a': b&=0x1FD; break;
         case 'B': case 'b': b&=0x1FB; break;
         case 'C': case 'c': b&=0x1F7; break;
         case 'D': case 'd': b&=0x1EF; break;
         case 'E': case 'e': b&=0x1DF; break;
         case 'F': case 'f': b&=0x1BF; break;
         case 'G': case 'g': b&=0x17F; break;
#ifdef __BORLANDC__
         case 0x9E: b&=0x0FE; break; // Rus YU
         case 0x80: b&=0x0FD; break; // Rus A
         case 0x81: b&=0x0FB; break; // Rus B
         case 0x96: b&=0x0F7; break; // Rus TS
         case 0x84: b&=0x0EF; break; // Rus D
         case 0x85: b&=0x0DF; break; // Rus E
         case 0x94: b&=0x0BF; break; // Rus F
         case 0x83: b&=0x07F; break; // Rus G
#endif
      }
   }
   if(!(keyb&32))
   {
      switch(lastkey)
      {
         case 'H': case 'h': b&=0x1FE; break;
         case 'I': case 'i': b&=0x1FD; break;
         case 'J': case 'j': b&=0x1FB; break;
         case 'K': case 'k': b&=0x1F7; break;
         case 'L': case 'l': b&=0x1EF; break;
         case 'M': case 'm': b&=0x1DF; break;
         case 'N': case 'n': b&=0x1BF; break;
         case 'O': case 'o': b&=0x17F; break;
#ifdef __BORLANDC__
         case 0x95: b&=0x0FE; break; // Rus KH
         case 0x88: b&=0x0FD; break; // Rus I
         case 0x89: b&=0x0FB; break; // Rus I'
         case 0x8A: b&=0x0F7; break; // Rus K
         case 0x8B: b&=0x0EF; break; // Rus L
         case 0x8C: b&=0x0DF; break; // Rus M
         case 0x8D: b&=0x0BF; break; // Rus N
         case 0x8E: b&=0x07F; break; // Rus O
#endif
      }
   }
   if(!(keyb&64))
   {
      switch(lastkey)
      {
         case 'P': case 'p': b&=0x1FE; break;
         case 'Q': case 'q': b&=0x1FD; break;
         case 'R': case 'r': b&=0x1FB; break;
         case 'S': case 's': b&=0x1F7; break;
         case 'T': case 't': b&=0x1EF; break;
         case 'U': case 'u': b&=0x1DF; break;
         case 'V': case 'v': b&=0x1BF; break;
         case 'W': case 'w': b&=0x17F; break;
#ifdef __BORLANDC__
         case 0x8F: b&=0x0FE; break; // Rus P
         case 0x9F: b&=0x0FD; break; // Rus YA
         case 0x90: b&=0x0FB; break; // Rus R
         case 0x91: b&=0x0F7; break; // Rus S
         case 0x92: b&=0x0EF; break; // Rus T
         case 0x93: b&=0x0DF; break; // Rus U
         case 0x86: b&=0x0BF; break; // Rus ZH
         case 0x82: b&=0x07F; break; // Rus V
#endif
      }
   }
   if(!(keyb&128))
   {
      switch(lastkey)
      {
         case 'X': case 'x': b&=0x1FE; break;
         case 'Y': case 'y': b&=0x1FD; break;
         case 'Z': case 'z': b&=0x1FB; break;
         case '[': b&=0x1F7; break;
         case '\\': b&=0x1EF; break;
         case ']': b&=0x1DF; break;
         case '^': b&=0x1BF; break;
         case ' ': b&=0x17F; break;
#ifdef __BORLANDC__
         case 0x9C: b&=0x0FE; break; // Rus Soft Sign (b)
         case 0x9B: b&=0x0FD; break; // Rus Y (bl)
         case 0x87: b&=0x0FB; break; // Rus Z
         case 0x98: b&=0x0F7; break; // Rus SH
         case 0x9D: b&=0x0EF; break; // Rus EE
         case 0x99: b&=0x0DF; break; // Rus SCH
         case 0x97: b&=0x0BF; break; // Rus CH
#endif
      }
   }
   if(lastkeycount > REPEATCOUNT)
   {
      lastkey = lastkeycount = 0;
   }
 }
 else
 {
   lastkeycount = 0;
   nokeycount++;
 }
#ifdef DEBUG
 if(b!=0x1FF) printf(">>> keyboard %2.2X\n",b);
#endif
 return b;
}

#define KBUFSZ  16
char kbuf[KBUFSZ];

int main(int argc, char **argv)
{
 int i,j,b1,b2;
 int adr = 0;
 int sadr = -1;
 int offset = 0;
 FILE *f;
 unsigned long sz;
 char str[100],*po;
 for(i=0;i<KBUFSZ;i++) kbuf[i] = 0;
 Pseudo86RK rk;
#ifdef STDOUT
 rk.setStdoutOnly();
#endif
 if(argc>1)
 {
   for(i=1;i<argc;i++)
   {
     if(argv[i][0]=='-') // option
     {
       switch(argv[i][1])
       {
          case 'a': // load address (and start address if it was not set with -s)
            adr = strtol(&argv[i][2],NULL,16);
            break;

          case 's': // optional start address (if different from load address)
            sadr = strtol(&argv[i][2],NULL,16);
            break;

          case 'c': // enable counters
            rk.enableCounters();
            break;

          case 'k': // keyboard input simulation
            rk.waitForKeys();
            strncpy(kbuf,&argv[i][2],KBUFSZ-2);
#ifdef linux
            strcat(kbuf,"\xA");
#else
            strcat(kbuf,"\xD");
#endif
            break;

          case 't': // stdout only output
            rk.setStdoutOnly();
            break;
       }
     }
     else // filename
     {
       strcpy(str,argv[i]);
       po = strstr(str,".rk");
       if(po==NULL) po = strstr(str,".RK");
       if(po==NULL) po = strstr(str,".GAM");
       if(po!=NULL) // RK?-file
       {
         f = fopen(str,"rb");
         if(f!=NULL)
         {
            offset = 4;
            b1 = fgetc(f);
            b2 = fgetc(f);
            if(b1==0xF6)
            {
               offset = 5;
               b1 = b2;
               b2 = fgetc(f);
            }
            adr = (b1<<8)|b2;
         }
       }
       else f = fopen(str,"rb");
       if(f==NULL)
       {
         printf("{err-nofile:%s}\n",str);
         rk.die();
       }
       else
       {
         fseek(f,0,SEEK_END);
         sz = ftell(f);
         if(sz>=MEMSZ)
         {
           printf("{err-toobig:%s}\n",str);
           rk.die();
         }
         else
         {
           fseek(f,offset,SEEK_SET);
           for(j=0;j<(int)sz-offset;j++) rk.put(adr+j,fgetc(f));
           if(sadr>=0) rk.trigger(sadr);
           else  rk.trigger(adr);
         }
         fclose(f);
       }
     }
   }
 }

 if(rk.isStdoutOnly()) printf("{pseudo-86rk:%s}\n",version);
#ifndef STDOUT
 else clrscr();
#endif

 while(!rk.time2stop())
 {
#ifdef DEBUG
    unsigned long u =
#endif
    rk.step(25);
#ifdef DEBUG
    printf("N=%lu T=%lu PC=%4.4X SP=%4.4X PSW=%4.4X BC=%4.4X DE=%4.4X HL=%4.4X\n",
            u,rk.getTime(),rk.getPC(),rk.getSP(),rk.getPSW(),rk.getBC(),rk.getDE(),rk.getHL());
#endif
    if(rk.lastkey==27) break;
#ifndef STDOUT
    else if(kbhit()) // some key was pressed
    {
       if(!rk.lastkey)
       {
          rk.lastkey = getch();
          if(rk.lastkey==27) break;
       }
       else // emulator is busy processing last pressed key
       {
          int i;
          for(i=0;i<KBUFSZ;i++)
            if(!kbuf[i]) break;
          if(i==KBUFSZ) i--;
          kbuf[i] = getch();
          if(kbuf[i]==27) break;
       }
    }
#endif
    else if(!rk.lastkey && kbuf[0] && rk.kbdTimeout()) // emulator is not busy, no pressed keys, but buffer has something
    {
       rk.lastkey = kbuf[0];
       if(rk.lastkey==27) break;
       for(i=0;i<=KBUFSZ-2;i++)
       {
          kbuf[i] = kbuf[i+1];
          if(!kbuf[i]) break;
       }
       if(kbuf[i]) kbuf[i] = 0;
    }
 }

 if(rk.isStdoutOnly())
 {
    if(rk.isCountersEnabled()) printf("\n{bye}\n");
    else printf("\n");
 }
#ifndef STDOUT
 else if(!rk.isStdoutOnly() && rk.lastkey!=27) getch();
#endif
#ifndef DEBUG
 rk.save("memory.bin",0,MEMSZ);
#endif
 return 0;
}
